import { SetMetadata } from '@nestjs/common';
import { AppRole } from '../types';

export const Roles = (...roles: AppRole[]) => SetMetadata('roles', roles);
