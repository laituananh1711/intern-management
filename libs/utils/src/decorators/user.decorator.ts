import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export interface ReqUser {
  id: number;
  role: { id: number; name: string };
  organizationId: number;
}

export const User = createParamDecorator(
  (field: string, ctx: ExecutionContext): any => {
    const req = ctx.switchToHttp().getRequest();

    return field != null ? req.user?.[field] : req.user;
  },
);
