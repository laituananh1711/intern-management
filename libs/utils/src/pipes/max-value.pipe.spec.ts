import { MaxValuePipe } from './max-value.pipe';

describe('MaxValuePipe', () => {
  it('should be defined', () => {
    expect(new MaxValuePipe(5)).toBeDefined();
  });
});
