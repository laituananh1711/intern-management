import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';

/**
 * Make sure that a value is string.
 * This is often used to ensure that a query string is truly string, not string[] or Record<string, any>
 * Since using this syntax `query[]=1` could result in a string[].
 */
@Injectable()
export class ParseStringPipe implements PipeTransform {
  transform(
    queryParams: unknown,
    _metadata: ArgumentMetadata,
  ): string | undefined {
    if (typeof queryParams != 'string' && typeof queryParams != 'undefined') {
      throw new BadRequestException(
        `Validation error (string is expected but received ${typeof queryParams})`,
      );
    }
    return queryParams;
  }
}
