import { ParseStringPipe } from './parse-string.pipe';

describe('ParseStringPipe', () => {
  const parseStringPipe = new ParseStringPipe();

  it('should be defined', () => {
    expect(parseStringPipe).toBeDefined();
  });

  describe('transform', () => {
    interface args {
      queryParams: unknown;
      metadata?: any;
    }
    interface TestCase {
      name: string;
      args: args;
      want: string | undefined;
      wantErr: boolean;
    }

    const testCases: TestCase[] = [
      {
        name: 'should transform correctly',
        args: {
          queryParams: 'a',
        },
        want: 'a',
        wantErr: false,
      },
      {
        name: 'should throw error on invalid input',
        args: {
          queryParams: 1,
        },
        want: undefined,
        wantErr: true,
      },
    ];

    let val: any, err: any;
    for (const tt of testCases) {
      it(tt.name, () => {
        try {
          val = parseStringPipe.transform(
            tt.args.queryParams,
            tt.args.metadata,
          );
        } catch (e) {
          err = e;
        }

        if (tt.wantErr) {
          expect(err != null).toEqual(true);
        } else {
          expect(val).toEqual(tt.want);
        }
      });
    }
  });
});
