import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class MaxValuePipe implements PipeTransform {
  private max: number;

  constructor(max: number) {
    this.max = max;
  }

  transform(value: number, metadata: ArgumentMetadata): number {
    if (value > this.max) {
      return this.max;
    }
    return value;
  }
}
