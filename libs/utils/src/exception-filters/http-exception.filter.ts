import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  Logger,
} from '@nestjs/common';
import { Response } from 'express';
import { ErrorResponse } from '../types';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private logger = new Logger(HttpExceptionFilter.name);

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    const errorResp = exception.getResponse();
    const statusCode = exception.getStatus();

    if (errorResp instanceof ErrorResponse) {
      response.status(statusCode).json(errorResp);
      return;
    }

    this.logger.error('http exception with invalid response format catched', {
      errorResp,
    });

    response
      .status(statusCode)
      .json(new ErrorResponse(statusCode, 'ERR_UNKNOWN'));
  }
}
