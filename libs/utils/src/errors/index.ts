export class InvalidInputError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = 'InvalidInputError';
  }
}

export class UnimplementedError extends Error {
  constructor() {
    super('Feature not yet implemented');
    this.name = 'UnimplementedError';
  }
}

export function wrapError<T = any, E extends Error = Error>(
  value: Promise<T>,
): Promise<[T | null, E | null]> {
  return value.then(
    (v) => [v, null],
    (err) => [null, err],
  );
}
