import { Transform } from 'class-transformer';
interface ToNumberOptions {
  default?: number;
  min?: number;
  max?: number;
}

export function toBoolean(value: string): boolean | undefined {
  value = value.toLowerCase();

  if (value === 'true') {
    return true;
  } else if (value === 'false') {
    return false;
  } else {
    return undefined;
  }
}

export function toNumber(
  value: string,
  opts: ToNumberOptions = {},
): number | undefined {
  let newValue: number = Number.parseInt(value || String(opts.default), 10);

  if (Number.isNaN(newValue)) {
    if (opts.default != null) {
      return opts.default;
    }
    return undefined;
  }

  if (opts.min) {
    if (newValue < opts.min) {
      newValue = opts.min;
    }
  }

  if (opts.max != null && newValue > opts.max) {
    newValue = opts.max;
  }

  return newValue;
}

export const TransformNumber = Transform(({ value }) => toNumber(value));
export const TransformBoolean = Transform(({ value }) => toBoolean(value));
