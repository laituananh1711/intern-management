import { avatarUrlFromName } from './avatar-url-from-name';

describe('avatarUrlFromName', () => {
  interface args {
    fullName: string;
  }

  interface TestCase {
    name: string;
    args: args;
    want: string;
  }

  const testCases: TestCase[] = [
    {
      name: 'create url from name',
      args: { fullName: 'example' },
      want: 'https://ui-avatars.com/api?name=example',
    },
    {
      name: 'escape input string',
      args: { fullName: 'example two' },
      want: 'https://ui-avatars.com/api?name=example+two',
    },
  ];

  for (const tt of testCases) {
    it(tt.name, () => {
      const val = avatarUrlFromName(tt.args.fullName);

      expect(val).toEqual(tt.want);
    });
  }
});
