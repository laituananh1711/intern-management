import * as crypto from 'crypto';

export const DEFAULT_AVATAR_PROVIDER = 'https://ui-avatars.com/api';

/**
 *
 * @param fullName
 * @returns
 * @deprecated
 */
export function avatarUrlFromName(fullName: string): string {
  const url = new URL(DEFAULT_AVATAR_PROVIDER);

  url.searchParams.set('name', fullName);

  return url.toString();
}

export function getGravatarURL(email: string) {
  // Trim leading and trailing whitespace from
  // an email address and force all characters
  // to lower case
  const address = String(email).trim().toLowerCase();

  // Create an MD5 hash of the final string
  const hash = crypto.createHash('md5').update(address).digest('hex');

  // Grab the actual image URL
  return `https://www.gravatar.com/avatar/${hash}?d=retro&f=y`;
}
