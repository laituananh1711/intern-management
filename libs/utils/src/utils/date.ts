function isValidDate(d: Date): boolean {
  return !isNaN(d.getTime());
}

export function compareDate(d1: string | Date, d2: string | Date): number {
  let date1: Date;
  let date2: Date;
  if (typeof d1 === 'string') {
    date1 = new Date(d1);
  } else {
    date1 = d1;
  }

  if (typeof d2 === 'string') {
    date2 = new Date(d2);
  } else {
    date2 = d2;
  }

  if (!isValidDate(date1) || !isValidDate(date2)) {
    throw new Error();
  }

  const time1 = date1.getTime();
  const time2 = date2.getTime();

  if (time1 > time2) return 1;
  else if (time1 < time2) return -1;
  else return 0;
}
