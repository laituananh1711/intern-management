import { compareDate } from './date';

describe('compareDate', () => {
  interface args {
    d1: string;
    d2: string;
  }
  interface TestCase {
    name: string;
    args: args;
    want: number | undefined;
    wantErr: boolean;
  }

  const testCases: TestCase[] = [
    {
      name: 'should compare date correctly (1)',
      args: {
        d1: '2000-01-01',
        d2: '2000-01-02',
      },
      want: -1,
      wantErr: false,
    },
    {
      name: 'should compare date correctly (2)',
      args: {
        d1: '2000-01-02',
        d2: '2000-01-01',
      },
      want: 1,
      wantErr: false,
    },
    {
      name: 'should compare date correctly (3)',
      args: {
        d1: '2000-01-01',
        d2: '2000-01-01',
      },
      want: 0,
      wantErr: false,
    },
    {
      name: 'should throw error on invalid input',
      args: {
        d1: '2000-01-01xx',
        d2: '2000-01-02',
      },
      want: undefined,
      wantErr: true,
    },
  ];

  for (const tt of testCases) {
    it(tt.name, () => {
      let val, err;
      try {
        val = compareDate(tt.args.d1, tt.args.d2);
      } catch (e) {
        err = e;
      }

      if (tt.wantErr) {
        expect(err != null).toEqual(tt.wantErr);
      } else {
        expect(val).toEqual(tt.want);
        expect(err != null).toEqual(tt.wantErr);
      }
    });
  }
});
