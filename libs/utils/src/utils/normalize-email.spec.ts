import { normalizeEmail } from './normalize-email';

describe('normalizeEmail', () => {
  interface args {
    eMail: string;
  }

  interface TestCase {
    name: string;
    args: args;
    want: string;
  }

  const testCases: TestCase[] = [
    {
      name: 'normalize gmail (dot)',
      args: {
        eMail: 'lai.t.uan.anh@gmail.com',
      },
      want: 'laituananh@gmail.com',
    },
    {
      name: 'normalize gmail (plus)',
      args: {
        eMail: 'laituananh+facebook@gmail.com',
      },
      want: 'laituananh@gmail.com',
    },
    {
      name: 'normalize googleemail',
      args: {
        eMail: 'lai.t.uan.anh@googlemail.com',
      },
      want: 'laituananh@gmail.com',
    },
  ];

  for (const tt of testCases) {
    it(tt.name, () => {
      const val = normalizeEmail(tt.args.eMail);

      expect(val).toEqual(tt.want);
    });
  }
});
