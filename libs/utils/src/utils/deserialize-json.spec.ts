import { ClassConstructor, classToPlain, Type } from 'class-transformer';
import {
  IsBoolean,
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import 'reflect-metadata';
import { deserializeJSON } from './deserialize-json';

describe('deserializeJSON', () => {
  class Nested {
    @IsBoolean()
    baz!: boolean;
  }
  class Outer {
    @IsString()
    foo!: string;

    @IsNumber()
    @IsOptional()
    bar?: number;

    @IsDefined()
    @ValidateNested()
    @Type(() => Nested)
    nested!: Nested;
  }

  interface args {
    cls: ClassConstructor<Outer>;
    obj: any;
  }

  interface TestCase {
    name: string;
    args: args;
    wantSerialized: any;
    wantErr: boolean;
  }

  const testCases: TestCase[] = [
    {
      name: 'should deserialize when input is valid',
      args: {
        cls: Outer,
        obj: {
          foo: 'a',
          bar: 1,
          nested: {
            baz: true,
          },
        },
      },
      wantSerialized: {
        foo: 'a',
        bar: 1,
        nested: {
          baz: true,
        },
      },
      wantErr: false,
    },
    {
      name: 'should deserialize when optional fields are omitted',
      args: {
        cls: Outer,
        obj: {
          foo: 'a',
          nested: {
            baz: true,
          },
        },
      },
      wantSerialized: {
        foo: 'a',
        bar: undefined,
        nested: {
          baz: true,
        },
      },
      wantErr: false,
    },
    {
      name: 'should deserialize when extra fields are included',
      args: {
        cls: Outer,
        obj: {
          foo: 'a',
          bar: 1,
          nested: {
            baz: true,
            extra: 1,
          },
          extra: 'hello',
        },
      },
      wantSerialized: {
        foo: 'a',
        bar: 1,
        nested: {
          baz: true,
        },
      },
      wantErr: false,
    },
    {
      name: 'should fail when fields type do not match',
      args: {
        cls: Outer,
        obj: {
          foo: 'a',
          bar: 'should_be_number',
          nested: {
            baz: true,
          },
        },
      },
      wantSerialized: undefined,
      wantErr: true,
    },
    {
      name: 'should fail when required fields are omitted',
      args: {
        cls: Outer,
        obj: {
          bar: 1,
          nested: {
            baz: true,
          },
        },
      },
      wantSerialized: undefined,
      wantErr: true,
    },
  ];

  for (const tt of testCases) {
    it(tt.name, () => {
      const { value, err } = deserializeJSON(tt.args.cls, tt.args.obj);

      if (tt.wantErr) {
        expect(err != null).toEqual(tt.wantErr);
      } else {
        expect(value instanceof tt.args.cls).toBeTruthy();
        expect(classToPlain(value)).toEqual(tt.wantSerialized);
      }
    });
  }
});
