import { ValidationError } from '@nestjs/common';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { validate, validateSync } from 'class-validator';

export class DeserializeError extends Error {
  private errors: ValidationError[];

  constructor(errors: ValidationError[]) {
    super();
    this.name = 'DeserializeError';
    this.errors = errors;
    this.message = this.toString();
  }

  toString(): string {
    let errorMessage = '';

    this.errors.forEach((error) => {
      errorMessage += error.toString() + ';';
    });

    return errorMessage;
  }
}

/**
 * Deserialize and validate JSON object.
 * @param cls
 * @param obj
 * @returns
 * @deprecated
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export function deserializeJSON<T extends object>(
  cls: ClassConstructor<T>,
  obj: any,
): { value: T; err: Error | null } {
  let err: Error | null = null;

  const val = plainToClass(cls, obj);
  const errors = validateSync(val, { whitelist: true });

  if (errors.length > 0) {
    err = new DeserializeError(errors);
  }

  return {
    value: val,
    err: err,
  };
}

// eslint-disable-next-line @typescript-eslint/ban-types
export async function deserializeAsync<T extends object>(
  cls: ClassConstructor<T>,
  obj: any,
): Promise<{ value: T; err: DeserializeError | null }> {
  let err: DeserializeError | null = null;

  const val = plainToClass(cls, obj);
  const errors = await validate(val);

  if (errors.length > 0) {
    err = new DeserializeError(errors);
    return {
      value: obj,
      err,
    };
  }

  return {
    value: val,
    err: err,
  };
}
