import { Metadata } from '@grpc/grpc-js';
import { ValidateIf, ValidationOptions } from 'class-validator';
import * as crypto from 'crypto';

export * from './avatar-url-from-name';
export * from './date';
export * from './deserialize-json';
export * from './normalize-email';
export * from './type-cast';

export const camelToSnakeCase = (str: string) =>
  str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);

export const generatePassword = (
  length = 20,
  wishlist = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$',
) =>
  Array.from(crypto.randomFillSync(new Uint32Array(length)))
    .map((x) => wishlist[x % wishlist.length])
    .join('');

export function IsOptionalOrEmpty(validationOptions?: ValidationOptions) {
  return ValidateIf((obj, value) => {
    return value !== null && value !== undefined && value !== '';
  }, validationOptions);
}

// isZeroValue checks if a value is null/0/empty string.
// It comes in handy when checking value deserialized from protobuf message.
export function isZeroValue(v: unknown): boolean {
  return v == null || v === 0 || v === '';
}

// remove undefined fields recursively.
export function cleanse(obj: any): any {
  const newObj = obj;
  Object.keys(newObj).forEach(function (key) {
    // Get this value and its type
    const value = newObj[key];
    const type = typeof value;
    if (type === 'object') {
      // Recurse...
      cleanse(value);
      // ...and remove if now "empty" (NOTE: insert your definition of "empty" here)
      if (!Object.keys(value).length) {
        delete newObj[key];
      }
    } else if (type === 'undefined') {
      // Undefined, remove it
      delete newObj[key];
    }
  });

  return newObj;
}

/** Create grpc metadata object from a regular object.
 *
 */
export function toGrpcMetadata(obj: Record<string, string>): Metadata {
  const metadata = new Metadata();
  for (const key in obj) {
    metadata.set(key, obj[key]);
  }

  return metadata;
}
