interface NormalizeOptions {
  cut: RegExp;
  aliasOf?: string;
}

const PLUS_ONLY = /\+.*$/;
const PLUS_AND_DOT = /\.|\+.*$/g;
const normalizeableProviders: Record<string, NormalizeOptions> = {
  'gmail.com': {
    cut: PLUS_AND_DOT,
  },
  'googlemail.com': {
    cut: PLUS_AND_DOT,
    aliasOf: 'gmail.com',
  },
  'hotmail.com': {
    cut: PLUS_ONLY,
  },
  'live.com': {
    cut: PLUS_AND_DOT,
  },
  'outlook.com': {
    cut: PLUS_ONLY,
  },
};

export function normalizeEmail(rawEmail: string): string {
  const email = rawEmail.toLowerCase();
  const emailParts = email.split('@');

  if (emailParts.length !== 2) {
    return rawEmail;
  }

  let username = emailParts[0];
  let domain = emailParts[1];

  if (normalizeableProviders.hasOwnProperty(domain)) {
    if (normalizeableProviders[domain].hasOwnProperty('cut')) {
      username = username.replace(normalizeableProviders[domain].cut, '');
    }
    domain = normalizeableProviders[domain].aliasOf ?? domain;
  }

  return username + '@' + domain;
}
