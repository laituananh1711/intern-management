import { Metadata, status } from '@grpc/grpc-js';
import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
  UnauthorizedException,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { toGrpcMetadata } from '../utils';
import { Request } from 'express';

const rpcUnauthenticatedError = new RpcException({
  code: status.UNAUTHENTICATED,
  message: 'ERROR_UNAUTHENTICATED',
  metadata: toGrpcMetadata({
    details: 'request is unauthenticated',
  }),
});

const httpUnauthenticatedError = new UnauthorizedException({
  code: status.UNAUTHENTICATED,
  message: 'ERROR_UNAUTHENTICATED',
  details: ['request does not contains necessary api key'],
});

/** Check grpc/http header for secret header and key.
 * If it does not present of is wrong, return.
 */
@Injectable()
export class ApiKeyAuthInterceptor implements NestInterceptor {
  constructor(private secretHeader: string, private secretKey: string) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    let headerValue = '';
    switch (context.getType()) {
      case 'rpc':
        const rpcCtx = context.switchToRpc();
        const gRPCMetadata = rpcCtx.getContext() as Metadata;
        headerValue =
          gRPCMetadata.get(this.secretHeader)?.[0]?.toString() ?? '';

        if (headerValue !== this.secretKey) {
          Logger.error('error no secret header or wrong secret key');
          throw rpcUnauthenticatedError;
        }

        break;
      case 'http':
        const httpCtx = context.switchToHttp();
        const req: Request = httpCtx.getRequest();

        // ignore healthcheck endpoints
        if (isHealthCheckEndpoint(req.path)) {
          return next.handle();
        }

        headerValue = req.header(this.secretHeader) ?? '';

        if (headerValue !== this.secretKey) {
          Logger.error('error no secret header or wrong secret key');
          throw httpUnauthenticatedError;
        }
        break;
    }

    return next.handle();
  }
}

function isHealthCheckEndpoint(path: string): boolean {
  return path.includes('health');
}
