import { ApiKeyAuthInterceptor } from './api-key-auth.interceptor';

describe('ApiKeyAuthInterceptor', () => {
  it('should be defined', () => {
    expect(
      new ApiKeyAuthInterceptor('secret_header', 'secret_key'),
    ).toBeDefined();
  });
});
