export type HttpResponse<Data = any> = {
  data: Data;
  code: number;
  message: string;
};

export enum AppRole {
  STUDENT = 'STUDENT',
  LECTURER = 'LECTURER',
  PARTNER = 'PARTNER',
  SYSTEM_ADMIN = 'SYSTEM_ADMIN',
  ORG_ADMIN = 'ORG_ADMIN',
}

export class ErrorResponse {
  code: number;
  message: string;
  details: string[] = [];
  error: string;

  constructor(
    code: number,
    message: string,
    error?: string,
    ...details: string[]
  ) {
    this.code = code;
    this.message = message ?? error;
    this.error = error ?? message;
    this.details = details ?? [];
  }
}
