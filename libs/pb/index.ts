import { Timestamp } from './google/protobuf/timestamp.pb';

export function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

export function toTimestampNullable(
  date: Date | undefined,
): Timestamp | undefined {
  if (date == null) {
    return undefined;
  }

  return toTimestamp(date);
}

export function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

export function fromTimestampNullable(
  t: Timestamp | undefined,
): Date | undefined {
  if (t == null) return undefined;

  return fromTimestamp(t);
}

export function fromJsonTimestamp(o: any): Timestamp {
  if (o instanceof Date) {
    return toTimestamp(o);
  } else if (typeof o === 'string') {
    return toTimestamp(new Date(o));
  } else {
    return Timestamp.fromJSON(o);
  }
}
