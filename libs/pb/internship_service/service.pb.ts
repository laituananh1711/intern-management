/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { Timestamp } from '../google/protobuf/timestamp.pb';
import { Metadata } from '@grpc/grpc-js';

export const protobufPackage = 'internship_service';

export enum RegistrationStatus {
  WAITING = 0,
  PASSED = 1,
  FAILED = 2,
  SELECTED = 3,
}

export function registrationStatusFromJSON(object: any): RegistrationStatus {
  switch (object) {
    case 0:
    case 'WAITING':
      return RegistrationStatus.WAITING;
    case 1:
    case 'PASSED':
      return RegistrationStatus.PASSED;
    case 2:
    case 'FAILED':
      return RegistrationStatus.FAILED;
    case 3:
    case 'SELECTED':
      return RegistrationStatus.SELECTED;
    default:
      throw new globalThis.Error(
        'Unrecognized enum value ' + object + ' for enum RegistrationStatus',
      );
  }
}

export function registrationStatusToJSON(object: RegistrationStatus): string {
  switch (object) {
    case RegistrationStatus.WAITING:
      return 'WAITING';
    case RegistrationStatus.PASSED:
      return 'PASSED';
    case RegistrationStatus.FAILED:
      return 'FAILED';
    case RegistrationStatus.SELECTED:
      return 'SELECTED';
    default:
      return 'UNKNOWN';
  }
}

export enum InternshipType {
  UNKNOWN = 0,
  ASSOCIATE = 1,
  OTHER = 2,
}

export function internshipTypeFromJSON(object: any): InternshipType {
  switch (object) {
    case 0:
    case 'UNKNOWN':
      return InternshipType.UNKNOWN;
    case 1:
    case 'ASSOCIATE':
      return InternshipType.ASSOCIATE;
    case 2:
    case 'OTHER':
      return InternshipType.OTHER;
    default:
      throw new globalThis.Error(
        'Unrecognized enum value ' + object + ' for enum InternshipType',
      );
  }
}

export function internshipTypeToJSON(object: InternshipType): string {
  switch (object) {
    case InternshipType.UNKNOWN:
      return 'UNKNOWN';
    case InternshipType.ASSOCIATE:
      return 'ASSOCIATE';
    case InternshipType.OTHER:
      return 'OTHER';
    default:
      return 'UNKNOWN';
  }
}

export enum PartnerStatus {
  PENDING = 0,
  ACCEPTED = 1,
  REJECTED = 2,
}

export function partnerStatusFromJSON(object: any): PartnerStatus {
  switch (object) {
    case 0:
    case 'PENDING':
      return PartnerStatus.PENDING;
    case 1:
    case 'ACCEPTED':
      return PartnerStatus.ACCEPTED;
    case 2:
    case 'REJECTED':
      return PartnerStatus.REJECTED;
    default:
      throw new globalThis.Error(
        'Unrecognized enum value ' + object + ' for enum PartnerStatus',
      );
  }
}

export function partnerStatusToJSON(object: PartnerStatus): string {
  switch (object) {
    case PartnerStatus.PENDING:
      return 'PENDING';
    case PartnerStatus.ACCEPTED:
      return 'ACCEPTED';
    case PartnerStatus.REJECTED:
      return 'REJECTED';
    default:
      return 'UNKNOWN';
  }
}

export interface GetTermsRequest {
  page: number;
  perPage: number;
  sort: string[];
  filter: GetTermsRequest_Filter | undefined;
}

export interface GetTermsRequest_Filter {
  organizationId?: number | undefined;
  year?: number | undefined;
}

export interface GetTermsResponse {
  code: number;
  message: string;
  data: GetTermsResponse_Data | undefined;
}

export interface GetTermsResponse_Data {
  meta: PaginationMetadata | undefined;
  terms: Term[];
}

export interface GetTermStudentsRequestFilter {
  supervisorId?: number | undefined;
  partnerSelected?: boolean | undefined;
  termId?: number | undefined;
  scoreGiven?: boolean | undefined;
  reportSubmitted?: boolean | undefined;
  supervisorAssigned?: boolean | undefined;
  selectedPartnerId?: number | undefined;
}

export interface GetTermStudentsRequest {
  page: number;
  perPage: number;
  q: string;
  sort: string[];
  filter: GetTermStudentsRequestFilter | undefined;
}

export interface GetTermStudentsResponse {
  code: number;
  message: string;
  data: GetTermStudentsResponse_Data | undefined;
}

export interface GetTermStudentsResponse_Data {
  meta: PaginationMetadata | undefined;
  students: TermStudent[];
}

export interface GetTermLecturersRequest {
  page: number;
  perPage: number;
  q: string;
  sort: string[];
  filter: GetTermLecturersRequest_Filter | undefined;
}

export interface GetTermLecturersRequest_Filter {
  termId?: number | undefined;
}

export interface GetTermLecturersResponse {
  code: number;
  message: string;
  data: GetTermLecturersResponse_Data | undefined;
}

export interface GetTermLecturersResponse_Data {
  meta: PaginationMetadata | undefined;
  lecturers: TermLecturer[];
}

export interface GetTermPartnersRequestFilter {
  termId?: number | undefined;
  status?: PartnerStatus | undefined;
}

export interface GetTermPartnersRequest {
  page: number;
  perPage: number;
  q: string;
  sort: string[];
  filter: GetTermPartnersRequestFilter | undefined;
}

export interface GetTermPartnersResponse {
  code: number;
  message: string;
  data: GetTermPartnersResponse_Data | undefined;
}

export interface GetTermPartnersResponse_Data {
  meta: PaginationMetadata | undefined;
  partners: TermPartner[];
}

export interface BatchChangeTermPartnerStatusRequest {
  termId: number;
  partnerIds: number[];
  status: PartnerStatus;
}

export interface BatchChangeTermPartnerStatusResponse {
  code: number;
  message: string;
}

export interface AssignSupervisorToStudentsRequest {
  termId: number;
  assignments: AssignSupervisorToStudentsRequest_Assignment[];
}

export interface AssignSupervisorToStudentsRequest_Assignment {
  supervisorId: number;
  studentIds: number[];
}

export interface AssignSupervisorToStudentsResponse {
  code: number;
  message: string;
}

export interface CreatePostRequest {
  partnerId: number;
  partnerContactId: number;
  title: string;
  content: string;
  jobCount: number;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  termId: number;
}

export interface CreatePostResponse {
  code: number;
  message: string;
  data: CreatePostResponse_Data | undefined;
}

export interface CreatePostResponse_Data {
  id: number;
}

export interface UpdatePostRequest {
  id: number;
  partnerContactId: number;
  title: string;
  content: string;
  jobCount: number;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  /** additional filter to prevent unauthorized access */
  filterTermId?: number | undefined;
  filterPartnerId?: number | undefined;
}

export interface UpdatePostResponse {
  code: number;
  message: string;
  data: UpdatePostResponse_Data | undefined;
}

export interface UpdatePostResponse_Data {
  id: number;
}

export interface BatchUpdateRegistrationStatusRequest {
  registrationIds: number[];
  /** should be in: ['PASSED', 'FAILED'] */
  status: RegistrationStatus;
  /** additional filter to prevent unauthorized updates */
  termId?: number | undefined;
  partnerId?: number | undefined;
}

export interface BatchUpdateRegistrationStatusResponse {
  code: number;
  message: string;
}

export interface ApplyToPartnerRequest {
  termId: number;
  studentId: number;
  postId: number;
}

export interface ApplyToPartnerResponse {
  code: number;
  message: string;
  data: ApplyToPartnerResponse_Data | undefined;
}

export interface ApplyToPartnerResponse_Data {
  registrationId: number;
}

export interface SelfRegisterPartnerRequest {
  termId: number;
  studentId: number;
  partnerId: number;
}

export interface SelfRegisterPartnerResponse {
  code: number;
  message: string;
  data: SelfRegisterPartnerResponse_Data | undefined;
}

export interface SelfRegisterPartnerResponse_Data {
  registrationId: number;
}

export interface CheckTermInOrgRequest {
  termId: number;
  organizationId: number;
}

export interface CheckTermInOrgResponse {
  code: number;
  message: string;
  data: CheckTermInOrgResponse_Data | undefined;
}

export interface CheckTermInOrgResponse_Data {
  isIn: boolean;
}

export interface CreateTermRequest {
  year: number;
  term: number;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  startDate: string;
  endDate: string;
  organizationId: number;
}

export interface CreateTermResponse {
  code: number;
  message: string;
  data: CreateTermResponse_Data | undefined;
}

export interface CreateTermResponse_Data {
  id: number;
}

export interface GetStudentApplicationsRequestFilter {
  internshipType?: InternshipType | undefined;
  partnerId?: number | undefined;
  status?: RegistrationStatus | undefined;
  isMailSent?: boolean | undefined;
  termId?: number | undefined;
}

export interface GetStudentApplicationsRequest {
  page: number;
  perPage: number;
  q: string;
  sort: string[];
  filter: GetStudentApplicationsRequestFilter | undefined;
}

export interface GetStudentApplicationsResponse {
  code: number;
  message: string;
  data: GetStudentApplicationsResponse_Data | undefined;
}

export interface GetStudentApplicationsResponse_Data {
  meta: PaginationMetadata | undefined;
  students: StudentApplication[];
}

export interface GetPostsRequest {
  page: number;
  perPage: number;
  q: string;
  sort: string[];
  filter: GetPostsRequest_Filter | undefined;
}

export interface GetPostsRequest_Filter {
  termId?: number | undefined;
  partnerId?: number | undefined;
}

export interface GetPostsResponse {
  code: number;
  message: string;
  data: GetPostsResponse_Data | undefined;
}

export interface GetPostsResponse_Data {
  meta: PaginationMetadata | undefined;
  posts: Post[];
}

export interface GetPostByIdRequest {
  postId: number;
  termId?: number | undefined;
}

export interface GetPostByIdResponse {
  code: number;
  message: string;
  data: GetPostByIdResponse_Data | undefined;
}

export interface GetPostByIdResponse_Data {
  post: Post | undefined;
}

export interface BatchNotifyStudentAboutRegistrationStatusRequest {
  organizationId: number;
  registrationIds: number[];
}

export interface BatchNotifyStudentAboutRegistrationStatusResponse {
  code: number;
  message: string;
}

export interface GiveScoreRequest {
  termId: number;
  studentId: number;
  score: number;
  /** add additional filter to prevent unauthorized access */
  filterLecturerId?: number | undefined;
}

export interface GiveScoreResponse {
  code: number;
  message: string;
}

export interface SelectInternshipPartnerRequest {
  studentId: number;
  termId: number;
  partnerId: number;
}

export interface SelectInternshipPartnerResponse {
  code: number;
  message: string;
}

export interface DownloadStudentReportRequest {
  termId: number;
  studentId: number;
  filterLecturerId?: number | undefined;
}

export interface DownloadStudentReportResponse {
  code: number;
  message: string;
  data: DownloadStudentReportResponse_Data | undefined;
}

export interface DownloadStudentReportResponse_Data {
  downloadUrl: string;
}

export interface GetAppliedPartnersByStudentIdRequest {
  studentId: number;
  termId: number;
}

export interface GetAppliedPartnersByStudentIdResponse {
  code: number;
  message: string;
  data: GetAppliedPartnersByStudentIdResponse_StudentApplication[];
}

export interface GetAppliedPartnersByStudentIdResponse_StudentApplication {
  id: number;
  partnerId: number;
  partnerName: string;
  registrationStatus: RegistrationStatus;
  registeredAt: Timestamp | undefined;
  partnerStatus: PartnerStatus;
  isSelfRegistered: boolean;
}

export interface RemoveLecturerFromTermRequest {
  termId: number;
  lecturerId: number;
}

export interface RemoveLecturerFromTermResponse {
  code: number;
  message: string;
}

export interface SignUpForTermRequest {
  termId: number;
  studentId: number;
}

export interface SignUpForTermResponse {
  code: number;
  message: string;
}

export interface CancelTermSignUpRequest {
  termId: number;
  studentId: number;
}

export interface CancelTermSignUpResponse {
  code: number;
  message: string;
}

export interface BatchAddLecturersToTermRequest {
  termId: number;
  lecturerIds: number[];
  organizationId: number;
}

export interface BatchAddLecturersToTermResponse {
  code: number;
  message: string;
  data: BatchAddLecturersToTermResponse_Data | undefined;
}

export interface BatchAddLecturersToTermResponse_Data {
  successIds: number[];
}

export interface BatchAddPartnersToTermRequest {
  termId: number;
  partnerIds: number[];
}

export interface BatchAddPartnersToTermResponse {
  code: number;
  message: string;
  data: BatchAddPartnersToTermResponse_Data | undefined;
}

export interface BatchAddPartnersToTermResponse_Data {
  successIds: number[];
}

export interface GetPartnerTermListingRequest {
  partnerId: number;
}

export interface GetPartnerTermListingResponse {
  code: number;
  message: string;
  data: TermLittleInfo[];
}

export interface GetLecturerTermListingRequest {
  lecturerId: number;
}

export interface GetLecturerTermListingResponse {
  code: number;
  message: string;
  data: TermLittleInfo[];
}

export interface GetStudentTermListingRequest {
  studentId: number;
}

export interface GetStudentTermListingResponse {
  code: number;
  message: string;
  data: TermLittleInfo[];
}

export interface GetTermListingRequest {
  organizationId: number;
}

export interface GetTermListingResponse {
  code: number;
  message: string;
  data: TermLittleInfo[];
}

export interface GetLatestTermRequest {
  studentId: number;
  organizationId: number;
}

export interface GetLatestTermResponse {
  code: number;
  message: string;
  data: GetLatestTermResponse_Data | undefined;
}

export interface GetLatestTermResponse_Data {
  id: number;
  year: number;
  term: number;
  startDate: string;
  endDate: string;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  isActive: boolean;
  isRegistrationExpired: boolean;
  isRegistered: boolean;
}

export interface GetTermPartnerListingRequest {
  termId: number;
}

export interface GetTermPartnerListingResponse {
  code: number;
  message: string;
  data: TermPartnerLittleInfo[];
}

export interface UpdateTermRequest {
  id: number;
  term: number;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  startDate: string;
  endDate: string;
  year: number;
  /** additional filters to prevent unauthorized access */
  filterOrganizationId?: number | undefined;
}

export interface UpdateTermResponse {
  code: number;
  message: string;
  data: UpdateTermResponse_Data | undefined;
}

export interface UpdateTermResponse_Data {
  id: number;
}

export interface CheckStudentInTermRequest {
  termId: number;
  studentId: number;
}

export interface CheckStudentInTermResponse {
  code: number;
  message: string;
  data: CheckStudentInTermResponse_Data | undefined;
}

export interface CheckStudentInTermResponse_Data {
  isIn: boolean;
}

export interface CheckLecturerInTermRequest {
  termId: number;
  lecturerId: number;
}

export interface CheckLecturerInTermResponse {
  code: number;
  message: string;
  data: CheckLecturerInTermResponse_Data | undefined;
}

export interface CheckLecturerInTermResponse_Data {
  isIn: boolean;
}

export interface CheckPartnerInTermRequest {
  termId: number;
  lecturerId: number;
}

export interface CheckPartnerInTermResponse {
  code: number;
  message: string;
  data: CheckPartnerInTermResponse_Data | undefined;
}

export interface CheckPartnerInTermResponse_Data {
  isIn: boolean;
  status?: PartnerStatus | undefined;
}

export interface GetTermStudentDetailsRequest {
  studentId: number;
  termId: number;
}

export interface GetTermStudentDetailsResponse {
  code: number;
  message: string;
  data: GetTermStudentDetailsResponse_Data | undefined;
}

export interface GetTermStudentDetailsResponse_Supervisor {
  id: number;
  name: string;
  orgEmail: string;
  personalEmail: string;
  phoneNumber: string;
}

export interface GetTermStudentDetailsResponse_AppliedPartner {
  id: number;
  name: string;
  applyStatus: RegistrationStatus;
  partnerStatus: PartnerStatus;
  isSelfRegistered: boolean;
  createdAt: Timestamp | undefined;
  internshipType: InternshipType;
}

export interface GetTermStudentDetailsResponse_Data {
  supervisor: GetTermStudentDetailsResponse_Supervisor | undefined;
  appliedPartners: GetTermStudentDetailsResponse_AppliedPartner[];
  reportFileName: string;
  selectedPartnerIndex?: number | undefined;
}

export interface GetTermLecturerListingRequest {
  termId: number;
}

export interface GetTermLecturerListingResponse {
  code: number;
  message: string;
  data: GetTermLecturerListingResponse_LecturerLittleInfo[];
}

export interface GetTermLecturerListingResponse_LecturerLittleInfo {
  id: number;
  fullName: string;
}

export interface ExportTermStudentsRequest {
  /** the same as GetTermStudents */
  sort: string[];
  filter: GetTermStudentsRequestFilter | undefined;
  q: string;
}

export interface DownloadUrlData {
  downloadUrl: string;
}

export interface ExportTermStudentsResponse {
  code: number;
  message: string;
  data: DownloadUrlData | undefined;
}

export interface ExportTermPartnersRequest {
  sort: string[];
  filter: GetTermPartnersRequestFilter | undefined;
  q: string;
}

export interface ExportTermPartnersResponse {
  code: number;
  message: string;
  data: DownloadUrlData | undefined;
}

export interface ExportStudentApplicationsRequest {
  sort: string[];
  filter: GetStudentApplicationsRequestFilter | undefined;
  q: string;
}

export interface ExportStudentApplicationsResponse {
  code: number;
  message: string;
  data: DownloadUrlData | undefined;
}

export interface GetStudentPostsRequest {
  page: number;
  perPage: number;
  studentId: number;
  termId: number;
}

export interface GetStudentPostsResponse {
  code: number;
  message: string;
  data: GetStudentPostsResponse_Data | undefined;
}

export interface GetStudentPostsResponse_StudentPost {
  post: Post | undefined;
  isRegistered: boolean;
  isSelfRegistered: boolean;
  isRegistrationExpired: boolean;
}

export interface GetStudentPostsResponse_Data {
  posts: GetStudentPostsResponse_StudentPost[];
  meta: PaginationMetadata | undefined;
}

export interface GetStudentPostByIdRequest {
  postId: number;
  studentId: number;
}

export interface GetStudentPostByIdResponse {
  code: number;
  message: string;
  data: GetStudentPostByIdResponse_StudentPost | undefined;
}

export interface GetStudentPostByIdResponse_StudentPost {
  post: Post | undefined;
  isRegistered: boolean;
  isSelfRegistered: boolean;
  isRegistrationExpired: boolean;
}

export interface UnregisterPartnerRequest {
  termId: number;
  partnerId: number;
  studentId: number;
}

export interface UnregisterPartnerResponse {
  code: number;
  message: string;
}

export interface GetMyStudentStatsRequest {
  lecturerId: number;
  organizationId: number;
}

export interface GetMyStudentStatsResponse {
  code: number;
  message: string;
  data: GetMyStudentStatsResponse_Data | undefined;
}

export interface GetMyStudentStatsResponse_ScoreStat {
  scored: number;
  notScored: number;
}

export interface GetMyStudentStatsResponse_ReportStat {
  submitted: number;
  notSubmitted: number;
}

export interface GetMyStudentStatsResponse_Data {
  scoring?: GetMyStudentStatsResponse_ScoreStat | undefined;
  reporting?: GetMyStudentStatsResponse_ReportStat | undefined;
}

export interface PastStat {
  lastTotal?: number | undefined;
  currentTotal?: number | undefined;
}

export interface TermLittleInfo {
  id: number;
  year: number;
  term: number;
}

export interface TermPartnerLittleInfo {
  id: number;
  name: string;
  logoUrl: string;
  status?: PartnerStatus | undefined;
}

export interface StudentApplication {
  id: number;
  fullName: string;
  orgEmail: string;
  phoneNumber: string;
  partnerId: number;
  partnerName: string;
  status: RegistrationStatus;
  registeredAt: Timestamp | undefined;
  internshipType: InternshipType;
  studentIdNumber: string;
  isMailSent: boolean;
  studentId: number;
  termId: number;
}

export interface StudentApplication_SchoolClass {
  id: number;
  name: string;
}

export interface StudentApplication_Supervisor {
  id: number;
  name: string;
}

export interface TermStudent {
  id: number;
  fullName: string;
  phoneNumber: string;
  schoolClass: TermStudent_SchoolClass | undefined;
  userId: number;
  orgEmail: string;
  personalEmail: string;
  selectedAt: Timestamp | undefined;
  selectedPartner: TermStudent_SelectedPartner | undefined;
  organizationId: number;
  studentIdNumber: string;
  supervisor: TermStudent_Supervisor | undefined;
  reportSubmitted: boolean;
  termId: number;
  /**
   * what is the internship type of this student based
   * on the selected partner.
   */
  score?: number | undefined;
}

export interface TermStudent_SchoolClass {
  id: number;
  name: string;
}

export interface TermStudent_Supervisor {
  id: number;
  name: string;
}

export interface TermStudent_SelectedPartner {
  id: number;
  name: string;
}

export interface TermLecturer {
  id: number;
  fullName: string;
  orgEmail: string;
  personalEmail: string;
  userId: number;
  organizationId: number;
  numOfSupervisedStudents: number;
  termId: number;
  createdAt: Timestamp | undefined;
}

export interface TermPartner {
  id: number;
  name: string;
  email: string;
  homepageUrl: string;
  userId?: number | undefined;
  joinedAt: Timestamp | undefined;
  termId: number;
  /** bool is_associate = 10; */
  status: PartnerStatus;
}

export interface TermPartner_AppliedStudents {
  id: number;
  fullName: string;
  orgEmail: string;
  studentIdNumber: string;
  isSelfRegistered: boolean;
  status: string;
}

export interface PaginationMetadata {
  currentPage: number;
  totalPages: number;
  totalItems: number;
  perPage: number;
}

export interface Post {
  id: number;
  termId: number;
  partnerId: number;
  partnerName: string;
  partnerContactId: number;
  partnerContactName: string;
  partnerContactEmail: string;
  partnerContactPhoneNumber: string;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  createdAt: Timestamp | undefined;
  title: string;
  content: string;
  jobCount: number;
}

export interface Term {
  id: number;
  year: number;
  term: number;
  startRegAt: Timestamp | undefined;
  endRegAt: Timestamp | undefined;
  startDate: string;
  endDate: string;
  createdAt: Timestamp | undefined;
  organizationId: number;
  numOfTermStudents: number;
  numOfAcceptedPartners: number;
}

export const INTERNSHIP_SERVICE_PACKAGE_NAME = 'internship_service';

function createBaseGetTermsRequest(): GetTermsRequest {
  return { page: 0, perPage: 0, sort: [], filter: undefined };
}

export const GetTermsRequest = {
  fromJSON(object: any): GetTermsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetTermsRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetTermsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetTermsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetTermsRequest_Filter(): GetTermsRequest_Filter {
  return { organizationId: undefined, year: undefined };
}

export const GetTermsRequest_Filter = {
  fromJSON(object: any): GetTermsRequest_Filter {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : undefined,
      year: isSet(object.year) ? Number(object.year) : undefined,
    };
  },

  toJSON(message: GetTermsRequest_Filter): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.year !== undefined && (obj.year = Math.round(message.year));
    return obj;
  },
};

function createBaseGetTermsResponse(): GetTermsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetTermsResponse = {
  fromJSON(object: any): GetTermsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetTermsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetTermsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetTermsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetTermsResponse_Data(): GetTermsResponse_Data {
  return { meta: undefined, terms: [] };
}

export const GetTermsResponse_Data = {
  fromJSON(object: any): GetTermsResponse_Data {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      terms: Array.isArray(object?.terms)
        ? object.terms.map((e: any) => Term.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetTermsResponse_Data): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.terms) {
      obj.terms = message.terms.map((e) => (e ? Term.toJSON(e) : undefined));
    } else {
      obj.terms = [];
    }
    return obj;
  },
};

function createBaseGetTermStudentsRequestFilter(): GetTermStudentsRequestFilter {
  return {
    supervisorId: undefined,
    partnerSelected: undefined,
    termId: undefined,
    scoreGiven: undefined,
    reportSubmitted: undefined,
    supervisorAssigned: undefined,
    selectedPartnerId: undefined,
  };
}

export const GetTermStudentsRequestFilter = {
  fromJSON(object: any): GetTermStudentsRequestFilter {
    return {
      supervisorId: isSet(object.supervisorId)
        ? Number(object.supervisorId)
        : undefined,
      partnerSelected: isSet(object.partnerSelected)
        ? Boolean(object.partnerSelected)
        : undefined,
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
      scoreGiven: isSet(object.scoreGiven)
        ? Boolean(object.scoreGiven)
        : undefined,
      reportSubmitted: isSet(object.reportSubmitted)
        ? Boolean(object.reportSubmitted)
        : undefined,
      supervisorAssigned: isSet(object.supervisorAssigned)
        ? Boolean(object.supervisorAssigned)
        : undefined,
      selectedPartnerId: isSet(object.selectedPartnerId)
        ? Number(object.selectedPartnerId)
        : undefined,
    };
  },

  toJSON(message: GetTermStudentsRequestFilter): unknown {
    const obj: any = {};
    message.supervisorId !== undefined &&
      (obj.supervisorId = Math.round(message.supervisorId));
    message.partnerSelected !== undefined &&
      (obj.partnerSelected = message.partnerSelected);
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.scoreGiven !== undefined && (obj.scoreGiven = message.scoreGiven);
    message.reportSubmitted !== undefined &&
      (obj.reportSubmitted = message.reportSubmitted);
    message.supervisorAssigned !== undefined &&
      (obj.supervisorAssigned = message.supervisorAssigned);
    message.selectedPartnerId !== undefined &&
      (obj.selectedPartnerId = Math.round(message.selectedPartnerId));
    return obj;
  },
};

function createBaseGetTermStudentsRequest(): GetTermStudentsRequest {
  return { page: 0, perPage: 0, q: '', sort: [], filter: undefined };
}

export const GetTermStudentsRequest = {
  fromJSON(object: any): GetTermStudentsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      q: isSet(object.q) ? String(object.q) : '',
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetTermStudentsRequestFilter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetTermStudentsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    message.q !== undefined && (obj.q = message.q);
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetTermStudentsRequestFilter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetTermStudentsResponse(): GetTermStudentsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetTermStudentsResponse = {
  fromJSON(object: any): GetTermStudentsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetTermStudentsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetTermStudentsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetTermStudentsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetTermStudentsResponse_Data(): GetTermStudentsResponse_Data {
  return { meta: undefined, students: [] };
}

export const GetTermStudentsResponse_Data = {
  fromJSON(object: any): GetTermStudentsResponse_Data {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      students: Array.isArray(object?.students)
        ? object.students.map((e: any) => TermStudent.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetTermStudentsResponse_Data): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.students) {
      obj.students = message.students.map((e) =>
        e ? TermStudent.toJSON(e) : undefined,
      );
    } else {
      obj.students = [];
    }
    return obj;
  },
};

function createBaseGetTermLecturersRequest(): GetTermLecturersRequest {
  return { page: 0, perPage: 0, q: '', sort: [], filter: undefined };
}

export const GetTermLecturersRequest = {
  fromJSON(object: any): GetTermLecturersRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      q: isSet(object.q) ? String(object.q) : '',
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetTermLecturersRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetTermLecturersRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    message.q !== undefined && (obj.q = message.q);
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetTermLecturersRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetTermLecturersRequest_Filter(): GetTermLecturersRequest_Filter {
  return { termId: undefined };
}

export const GetTermLecturersRequest_Filter = {
  fromJSON(object: any): GetTermLecturersRequest_Filter {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
    };
  },

  toJSON(message: GetTermLecturersRequest_Filter): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetTermLecturersResponse(): GetTermLecturersResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetTermLecturersResponse = {
  fromJSON(object: any): GetTermLecturersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetTermLecturersResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetTermLecturersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetTermLecturersResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetTermLecturersResponse_Data(): GetTermLecturersResponse_Data {
  return { meta: undefined, lecturers: [] };
}

export const GetTermLecturersResponse_Data = {
  fromJSON(object: any): GetTermLecturersResponse_Data {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      lecturers: Array.isArray(object?.lecturers)
        ? object.lecturers.map((e: any) => TermLecturer.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetTermLecturersResponse_Data): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.lecturers) {
      obj.lecturers = message.lecturers.map((e) =>
        e ? TermLecturer.toJSON(e) : undefined,
      );
    } else {
      obj.lecturers = [];
    }
    return obj;
  },
};

function createBaseGetTermPartnersRequestFilter(): GetTermPartnersRequestFilter {
  return { termId: undefined, status: undefined };
}

export const GetTermPartnersRequestFilter = {
  fromJSON(object: any): GetTermPartnersRequestFilter {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
      status: isSet(object.status)
        ? partnerStatusFromJSON(object.status)
        : undefined,
    };
  },

  toJSON(message: GetTermPartnersRequestFilter): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.status !== undefined &&
      (obj.status =
        message.status !== undefined
          ? partnerStatusToJSON(message.status)
          : undefined);
    return obj;
  },
};

function createBaseGetTermPartnersRequest(): GetTermPartnersRequest {
  return { page: 0, perPage: 0, q: '', sort: [], filter: undefined };
}

export const GetTermPartnersRequest = {
  fromJSON(object: any): GetTermPartnersRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      q: isSet(object.q) ? String(object.q) : '',
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetTermPartnersRequestFilter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetTermPartnersRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    message.q !== undefined && (obj.q = message.q);
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetTermPartnersRequestFilter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetTermPartnersResponse(): GetTermPartnersResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetTermPartnersResponse = {
  fromJSON(object: any): GetTermPartnersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetTermPartnersResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetTermPartnersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetTermPartnersResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetTermPartnersResponse_Data(): GetTermPartnersResponse_Data {
  return { meta: undefined, partners: [] };
}

export const GetTermPartnersResponse_Data = {
  fromJSON(object: any): GetTermPartnersResponse_Data {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      partners: Array.isArray(object?.partners)
        ? object.partners.map((e: any) => TermPartner.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetTermPartnersResponse_Data): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.partners) {
      obj.partners = message.partners.map((e) =>
        e ? TermPartner.toJSON(e) : undefined,
      );
    } else {
      obj.partners = [];
    }
    return obj;
  },
};

function createBaseBatchChangeTermPartnerStatusRequest(): BatchChangeTermPartnerStatusRequest {
  return { termId: 0, partnerIds: [], status: 0 };
}

export const BatchChangeTermPartnerStatusRequest = {
  fromJSON(object: any): BatchChangeTermPartnerStatusRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      partnerIds: Array.isArray(object?.partnerIds)
        ? object.partnerIds.map((e: any) => Number(e))
        : [],
      status: isSet(object.status) ? partnerStatusFromJSON(object.status) : 0,
    };
  },

  toJSON(message: BatchChangeTermPartnerStatusRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    if (message.partnerIds) {
      obj.partnerIds = message.partnerIds.map((e) => Math.round(e));
    } else {
      obj.partnerIds = [];
    }
    message.status !== undefined &&
      (obj.status = partnerStatusToJSON(message.status));
    return obj;
  },
};

function createBaseBatchChangeTermPartnerStatusResponse(): BatchChangeTermPartnerStatusResponse {
  return { code: 0, message: '' };
}

export const BatchChangeTermPartnerStatusResponse = {
  fromJSON(object: any): BatchChangeTermPartnerStatusResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: BatchChangeTermPartnerStatusResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseAssignSupervisorToStudentsRequest(): AssignSupervisorToStudentsRequest {
  return { termId: 0, assignments: [] };
}

export const AssignSupervisorToStudentsRequest = {
  fromJSON(object: any): AssignSupervisorToStudentsRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      assignments: Array.isArray(object?.assignments)
        ? object.assignments.map((e: any) =>
            AssignSupervisorToStudentsRequest_Assignment.fromJSON(e),
          )
        : [],
    };
  },

  toJSON(message: AssignSupervisorToStudentsRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    if (message.assignments) {
      obj.assignments = message.assignments.map((e) =>
        e ? AssignSupervisorToStudentsRequest_Assignment.toJSON(e) : undefined,
      );
    } else {
      obj.assignments = [];
    }
    return obj;
  },
};

function createBaseAssignSupervisorToStudentsRequest_Assignment(): AssignSupervisorToStudentsRequest_Assignment {
  return { supervisorId: 0, studentIds: [] };
}

export const AssignSupervisorToStudentsRequest_Assignment = {
  fromJSON(object: any): AssignSupervisorToStudentsRequest_Assignment {
    return {
      supervisorId: isSet(object.supervisorId)
        ? Number(object.supervisorId)
        : 0,
      studentIds: Array.isArray(object?.studentIds)
        ? object.studentIds.map((e: any) => Number(e))
        : [],
    };
  },

  toJSON(message: AssignSupervisorToStudentsRequest_Assignment): unknown {
    const obj: any = {};
    message.supervisorId !== undefined &&
      (obj.supervisorId = Math.round(message.supervisorId));
    if (message.studentIds) {
      obj.studentIds = message.studentIds.map((e) => Math.round(e));
    } else {
      obj.studentIds = [];
    }
    return obj;
  },
};

function createBaseAssignSupervisorToStudentsResponse(): AssignSupervisorToStudentsResponse {
  return { code: 0, message: '' };
}

export const AssignSupervisorToStudentsResponse = {
  fromJSON(object: any): AssignSupervisorToStudentsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: AssignSupervisorToStudentsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseCreatePostRequest(): CreatePostRequest {
  return {
    partnerId: 0,
    partnerContactId: 0,
    title: '',
    content: '',
    jobCount: 0,
    startRegAt: undefined,
    endRegAt: undefined,
    termId: 0,
  };
}

export const CreatePostRequest = {
  fromJSON(object: any): CreatePostRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      partnerContactId: isSet(object.partnerContactId)
        ? Number(object.partnerContactId)
        : 0,
      title: isSet(object.title) ? String(object.title) : '',
      content: isSet(object.content) ? String(object.content) : '',
      jobCount: isSet(object.jobCount) ? Number(object.jobCount) : 0,
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: CreatePostRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.partnerContactId !== undefined &&
      (obj.partnerContactId = Math.round(message.partnerContactId));
    message.title !== undefined && (obj.title = message.title);
    message.content !== undefined && (obj.content = message.content);
    message.jobCount !== undefined &&
      (obj.jobCount = Math.round(message.jobCount));
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseCreatePostResponse(): CreatePostResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreatePostResponse = {
  fromJSON(object: any): CreatePostResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreatePostResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreatePostResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreatePostResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreatePostResponse_Data(): CreatePostResponse_Data {
  return { id: 0 };
}

export const CreatePostResponse_Data = {
  fromJSON(object: any): CreatePostResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: CreatePostResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseUpdatePostRequest(): UpdatePostRequest {
  return {
    id: 0,
    partnerContactId: 0,
    title: '',
    content: '',
    jobCount: 0,
    startRegAt: undefined,
    endRegAt: undefined,
    filterTermId: undefined,
    filterPartnerId: undefined,
  };
}

export const UpdatePostRequest = {
  fromJSON(object: any): UpdatePostRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      partnerContactId: isSet(object.partnerContactId)
        ? Number(object.partnerContactId)
        : 0,
      title: isSet(object.title) ? String(object.title) : '',
      content: isSet(object.content) ? String(object.content) : '',
      jobCount: isSet(object.jobCount) ? Number(object.jobCount) : 0,
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      filterTermId: isSet(object.filterTermId)
        ? Number(object.filterTermId)
        : undefined,
      filterPartnerId: isSet(object.filterPartnerId)
        ? Number(object.filterPartnerId)
        : undefined,
    };
  },

  toJSON(message: UpdatePostRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.partnerContactId !== undefined &&
      (obj.partnerContactId = Math.round(message.partnerContactId));
    message.title !== undefined && (obj.title = message.title);
    message.content !== undefined && (obj.content = message.content);
    message.jobCount !== undefined &&
      (obj.jobCount = Math.round(message.jobCount));
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.filterTermId !== undefined &&
      (obj.filterTermId = Math.round(message.filterTermId));
    message.filterPartnerId !== undefined &&
      (obj.filterPartnerId = Math.round(message.filterPartnerId));
    return obj;
  },
};

function createBaseUpdatePostResponse(): UpdatePostResponse {
  return { code: 0, message: '', data: undefined };
}

export const UpdatePostResponse = {
  fromJSON(object: any): UpdatePostResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? UpdatePostResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: UpdatePostResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? UpdatePostResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseUpdatePostResponse_Data(): UpdatePostResponse_Data {
  return { id: 0 };
}

export const UpdatePostResponse_Data = {
  fromJSON(object: any): UpdatePostResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: UpdatePostResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseBatchUpdateRegistrationStatusRequest(): BatchUpdateRegistrationStatusRequest {
  return {
    registrationIds: [],
    status: 0,
    termId: undefined,
    partnerId: undefined,
  };
}

export const BatchUpdateRegistrationStatusRequest = {
  fromJSON(object: any): BatchUpdateRegistrationStatusRequest {
    return {
      registrationIds: Array.isArray(object?.registrationIds)
        ? object.registrationIds.map((e: any) => Number(e))
        : [],
      status: isSet(object.status)
        ? registrationStatusFromJSON(object.status)
        : 0,
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : undefined,
    };
  },

  toJSON(message: BatchUpdateRegistrationStatusRequest): unknown {
    const obj: any = {};
    if (message.registrationIds) {
      obj.registrationIds = message.registrationIds.map((e) => Math.round(e));
    } else {
      obj.registrationIds = [];
    }
    message.status !== undefined &&
      (obj.status = registrationStatusToJSON(message.status));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseBatchUpdateRegistrationStatusResponse(): BatchUpdateRegistrationStatusResponse {
  return { code: 0, message: '' };
}

export const BatchUpdateRegistrationStatusResponse = {
  fromJSON(object: any): BatchUpdateRegistrationStatusResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: BatchUpdateRegistrationStatusResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseApplyToPartnerRequest(): ApplyToPartnerRequest {
  return { termId: 0, studentId: 0, postId: 0 };
}

export const ApplyToPartnerRequest = {
  fromJSON(object: any): ApplyToPartnerRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      postId: isSet(object.postId) ? Number(object.postId) : 0,
    };
  },

  toJSON(message: ApplyToPartnerRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.postId !== undefined && (obj.postId = Math.round(message.postId));
    return obj;
  },
};

function createBaseApplyToPartnerResponse(): ApplyToPartnerResponse {
  return { code: 0, message: '', data: undefined };
}

export const ApplyToPartnerResponse = {
  fromJSON(object: any): ApplyToPartnerResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? ApplyToPartnerResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: ApplyToPartnerResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? ApplyToPartnerResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseApplyToPartnerResponse_Data(): ApplyToPartnerResponse_Data {
  return { registrationId: 0 };
}

export const ApplyToPartnerResponse_Data = {
  fromJSON(object: any): ApplyToPartnerResponse_Data {
    return {
      registrationId: isSet(object.registrationId)
        ? Number(object.registrationId)
        : 0,
    };
  },

  toJSON(message: ApplyToPartnerResponse_Data): unknown {
    const obj: any = {};
    message.registrationId !== undefined &&
      (obj.registrationId = Math.round(message.registrationId));
    return obj;
  },
};

function createBaseSelfRegisterPartnerRequest(): SelfRegisterPartnerRequest {
  return { termId: 0, studentId: 0, partnerId: 0 };
}

export const SelfRegisterPartnerRequest = {
  fromJSON(object: any): SelfRegisterPartnerRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
    };
  },

  toJSON(message: SelfRegisterPartnerRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseSelfRegisterPartnerResponse(): SelfRegisterPartnerResponse {
  return { code: 0, message: '', data: undefined };
}

export const SelfRegisterPartnerResponse = {
  fromJSON(object: any): SelfRegisterPartnerResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? SelfRegisterPartnerResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: SelfRegisterPartnerResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? SelfRegisterPartnerResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseSelfRegisterPartnerResponse_Data(): SelfRegisterPartnerResponse_Data {
  return { registrationId: 0 };
}

export const SelfRegisterPartnerResponse_Data = {
  fromJSON(object: any): SelfRegisterPartnerResponse_Data {
    return {
      registrationId: isSet(object.registrationId)
        ? Number(object.registrationId)
        : 0,
    };
  },

  toJSON(message: SelfRegisterPartnerResponse_Data): unknown {
    const obj: any = {};
    message.registrationId !== undefined &&
      (obj.registrationId = Math.round(message.registrationId));
    return obj;
  },
};

function createBaseCheckTermInOrgRequest(): CheckTermInOrgRequest {
  return { termId: 0, organizationId: 0 };
}

export const CheckTermInOrgRequest = {
  fromJSON(object: any): CheckTermInOrgRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: CheckTermInOrgRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseCheckTermInOrgResponse(): CheckTermInOrgResponse {
  return { code: 0, message: '', data: undefined };
}

export const CheckTermInOrgResponse = {
  fromJSON(object: any): CheckTermInOrgResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CheckTermInOrgResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CheckTermInOrgResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CheckTermInOrgResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCheckTermInOrgResponse_Data(): CheckTermInOrgResponse_Data {
  return { isIn: false };
}

export const CheckTermInOrgResponse_Data = {
  fromJSON(object: any): CheckTermInOrgResponse_Data {
    return {
      isIn: isSet(object.isIn) ? Boolean(object.isIn) : false,
    };
  },

  toJSON(message: CheckTermInOrgResponse_Data): unknown {
    const obj: any = {};
    message.isIn !== undefined && (obj.isIn = message.isIn);
    return obj;
  },
};

function createBaseCreateTermRequest(): CreateTermRequest {
  return {
    year: 0,
    term: 0,
    startRegAt: undefined,
    endRegAt: undefined,
    startDate: '',
    endDate: '',
    organizationId: 0,
  };
}

export const CreateTermRequest = {
  fromJSON(object: any): CreateTermRequest {
    return {
      year: isSet(object.year) ? Number(object.year) : 0,
      term: isSet(object.term) ? Number(object.term) : 0,
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      startDate: isSet(object.startDate) ? String(object.startDate) : '',
      endDate: isSet(object.endDate) ? String(object.endDate) : '',
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: CreateTermRequest): unknown {
    const obj: any = {};
    message.year !== undefined && (obj.year = Math.round(message.year));
    message.term !== undefined && (obj.term = Math.round(message.term));
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.startDate !== undefined && (obj.startDate = message.startDate);
    message.endDate !== undefined && (obj.endDate = message.endDate);
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseCreateTermResponse(): CreateTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreateTermResponse = {
  fromJSON(object: any): CreateTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreateTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreateTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreateTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreateTermResponse_Data(): CreateTermResponse_Data {
  return { id: 0 };
}

export const CreateTermResponse_Data = {
  fromJSON(object: any): CreateTermResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: CreateTermResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseGetStudentApplicationsRequestFilter(): GetStudentApplicationsRequestFilter {
  return {
    internshipType: undefined,
    partnerId: undefined,
    status: undefined,
    isMailSent: undefined,
    termId: undefined,
  };
}

export const GetStudentApplicationsRequestFilter = {
  fromJSON(object: any): GetStudentApplicationsRequestFilter {
    return {
      internshipType: isSet(object.internshipType)
        ? internshipTypeFromJSON(object.internshipType)
        : undefined,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : undefined,
      status: isSet(object.status)
        ? registrationStatusFromJSON(object.status)
        : undefined,
      isMailSent: isSet(object.isMailSent)
        ? Boolean(object.isMailSent)
        : undefined,
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
    };
  },

  toJSON(message: GetStudentApplicationsRequestFilter): unknown {
    const obj: any = {};
    message.internshipType !== undefined &&
      (obj.internshipType =
        message.internshipType !== undefined
          ? internshipTypeToJSON(message.internshipType)
          : undefined);
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.status !== undefined &&
      (obj.status =
        message.status !== undefined
          ? registrationStatusToJSON(message.status)
          : undefined);
    message.isMailSent !== undefined && (obj.isMailSent = message.isMailSent);
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetStudentApplicationsRequest(): GetStudentApplicationsRequest {
  return { page: 0, perPage: 0, q: '', sort: [], filter: undefined };
}

export const GetStudentApplicationsRequest = {
  fromJSON(object: any): GetStudentApplicationsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      q: isSet(object.q) ? String(object.q) : '',
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetStudentApplicationsRequestFilter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetStudentApplicationsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    message.q !== undefined && (obj.q = message.q);
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetStudentApplicationsRequestFilter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentApplicationsResponse(): GetStudentApplicationsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetStudentApplicationsResponse = {
  fromJSON(object: any): GetStudentApplicationsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetStudentApplicationsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetStudentApplicationsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetStudentApplicationsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentApplicationsResponse_Data(): GetStudentApplicationsResponse_Data {
  return { meta: undefined, students: [] };
}

export const GetStudentApplicationsResponse_Data = {
  fromJSON(object: any): GetStudentApplicationsResponse_Data {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      students: Array.isArray(object?.students)
        ? object.students.map((e: any) => StudentApplication.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetStudentApplicationsResponse_Data): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.students) {
      obj.students = message.students.map((e) =>
        e ? StudentApplication.toJSON(e) : undefined,
      );
    } else {
      obj.students = [];
    }
    return obj;
  },
};

function createBaseGetPostsRequest(): GetPostsRequest {
  return { page: 0, perPage: 0, q: '', sort: [], filter: undefined };
}

export const GetPostsRequest = {
  fromJSON(object: any): GetPostsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      q: isSet(object.q) ? String(object.q) : '',
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetPostsRequest_Filter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetPostsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    message.q !== undefined && (obj.q = message.q);
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetPostsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetPostsRequest_Filter(): GetPostsRequest_Filter {
  return { termId: undefined, partnerId: undefined };
}

export const GetPostsRequest_Filter = {
  fromJSON(object: any): GetPostsRequest_Filter {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : undefined,
    };
  },

  toJSON(message: GetPostsRequest_Filter): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseGetPostsResponse(): GetPostsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetPostsResponse = {
  fromJSON(object: any): GetPostsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetPostsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetPostsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetPostsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetPostsResponse_Data(): GetPostsResponse_Data {
  return { meta: undefined, posts: [] };
}

export const GetPostsResponse_Data = {
  fromJSON(object: any): GetPostsResponse_Data {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      posts: Array.isArray(object?.posts)
        ? object.posts.map((e: any) => Post.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetPostsResponse_Data): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.posts) {
      obj.posts = message.posts.map((e) => (e ? Post.toJSON(e) : undefined));
    } else {
      obj.posts = [];
    }
    return obj;
  },
};

function createBaseGetPostByIdRequest(): GetPostByIdRequest {
  return { postId: 0, termId: undefined };
}

export const GetPostByIdRequest = {
  fromJSON(object: any): GetPostByIdRequest {
    return {
      postId: isSet(object.postId) ? Number(object.postId) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : undefined,
    };
  },

  toJSON(message: GetPostByIdRequest): unknown {
    const obj: any = {};
    message.postId !== undefined && (obj.postId = Math.round(message.postId));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetPostByIdResponse(): GetPostByIdResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetPostByIdResponse = {
  fromJSON(object: any): GetPostByIdResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetPostByIdResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetPostByIdResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetPostByIdResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetPostByIdResponse_Data(): GetPostByIdResponse_Data {
  return { post: undefined };
}

export const GetPostByIdResponse_Data = {
  fromJSON(object: any): GetPostByIdResponse_Data {
    return {
      post: isSet(object.post) ? Post.fromJSON(object.post) : undefined,
    };
  },

  toJSON(message: GetPostByIdResponse_Data): unknown {
    const obj: any = {};
    message.post !== undefined &&
      (obj.post = message.post ? Post.toJSON(message.post) : undefined);
    return obj;
  },
};

function createBaseBatchNotifyStudentAboutRegistrationStatusRequest(): BatchNotifyStudentAboutRegistrationStatusRequest {
  return { organizationId: 0, registrationIds: [] };
}

export const BatchNotifyStudentAboutRegistrationStatusRequest = {
  fromJSON(object: any): BatchNotifyStudentAboutRegistrationStatusRequest {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      registrationIds: Array.isArray(object?.registrationIds)
        ? object.registrationIds.map((e: any) => Number(e))
        : [],
    };
  },

  toJSON(message: BatchNotifyStudentAboutRegistrationStatusRequest): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    if (message.registrationIds) {
      obj.registrationIds = message.registrationIds.map((e) => Math.round(e));
    } else {
      obj.registrationIds = [];
    }
    return obj;
  },
};

function createBaseBatchNotifyStudentAboutRegistrationStatusResponse(): BatchNotifyStudentAboutRegistrationStatusResponse {
  return { code: 0, message: '' };
}

export const BatchNotifyStudentAboutRegistrationStatusResponse = {
  fromJSON(object: any): BatchNotifyStudentAboutRegistrationStatusResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: BatchNotifyStudentAboutRegistrationStatusResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseGiveScoreRequest(): GiveScoreRequest {
  return { termId: 0, studentId: 0, score: 0, filterLecturerId: undefined };
}

export const GiveScoreRequest = {
  fromJSON(object: any): GiveScoreRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      score: isSet(object.score) ? Number(object.score) : 0,
      filterLecturerId: isSet(object.filterLecturerId)
        ? Number(object.filterLecturerId)
        : undefined,
    };
  },

  toJSON(message: GiveScoreRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.score !== undefined && (obj.score = message.score);
    message.filterLecturerId !== undefined &&
      (obj.filterLecturerId = Math.round(message.filterLecturerId));
    return obj;
  },
};

function createBaseGiveScoreResponse(): GiveScoreResponse {
  return { code: 0, message: '' };
}

export const GiveScoreResponse = {
  fromJSON(object: any): GiveScoreResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: GiveScoreResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseSelectInternshipPartnerRequest(): SelectInternshipPartnerRequest {
  return { studentId: 0, termId: 0, partnerId: 0 };
}

export const SelectInternshipPartnerRequest = {
  fromJSON(object: any): SelectInternshipPartnerRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
    };
  },

  toJSON(message: SelectInternshipPartnerRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseSelectInternshipPartnerResponse(): SelectInternshipPartnerResponse {
  return { code: 0, message: '' };
}

export const SelectInternshipPartnerResponse = {
  fromJSON(object: any): SelectInternshipPartnerResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: SelectInternshipPartnerResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseDownloadStudentReportRequest(): DownloadStudentReportRequest {
  return { termId: 0, studentId: 0, filterLecturerId: undefined };
}

export const DownloadStudentReportRequest = {
  fromJSON(object: any): DownloadStudentReportRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      filterLecturerId: isSet(object.filterLecturerId)
        ? Number(object.filterLecturerId)
        : undefined,
    };
  },

  toJSON(message: DownloadStudentReportRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.filterLecturerId !== undefined &&
      (obj.filterLecturerId = Math.round(message.filterLecturerId));
    return obj;
  },
};

function createBaseDownloadStudentReportResponse(): DownloadStudentReportResponse {
  return { code: 0, message: '', data: undefined };
}

export const DownloadStudentReportResponse = {
  fromJSON(object: any): DownloadStudentReportResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? DownloadStudentReportResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: DownloadStudentReportResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? DownloadStudentReportResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseDownloadStudentReportResponse_Data(): DownloadStudentReportResponse_Data {
  return { downloadUrl: '' };
}

export const DownloadStudentReportResponse_Data = {
  fromJSON(object: any): DownloadStudentReportResponse_Data {
    return {
      downloadUrl: isSet(object.downloadUrl) ? String(object.downloadUrl) : '',
    };
  },

  toJSON(message: DownloadStudentReportResponse_Data): unknown {
    const obj: any = {};
    message.downloadUrl !== undefined &&
      (obj.downloadUrl = message.downloadUrl);
    return obj;
  },
};

function createBaseGetAppliedPartnersByStudentIdRequest(): GetAppliedPartnersByStudentIdRequest {
  return { studentId: 0, termId: 0 };
}

export const GetAppliedPartnersByStudentIdRequest = {
  fromJSON(object: any): GetAppliedPartnersByStudentIdRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: GetAppliedPartnersByStudentIdRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetAppliedPartnersByStudentIdResponse(): GetAppliedPartnersByStudentIdResponse {
  return { code: 0, message: '', data: [] };
}

export const GetAppliedPartnersByStudentIdResponse = {
  fromJSON(object: any): GetAppliedPartnersByStudentIdResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) =>
            GetAppliedPartnersByStudentIdResponse_StudentApplication.fromJSON(
              e,
            ),
          )
        : [],
    };
  },

  toJSON(message: GetAppliedPartnersByStudentIdResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e
          ? GetAppliedPartnersByStudentIdResponse_StudentApplication.toJSON(e)
          : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetAppliedPartnersByStudentIdResponse_StudentApplication(): GetAppliedPartnersByStudentIdResponse_StudentApplication {
  return {
    id: 0,
    partnerId: 0,
    partnerName: '',
    registrationStatus: 0,
    registeredAt: undefined,
    partnerStatus: 0,
    isSelfRegistered: false,
  };
}

export const GetAppliedPartnersByStudentIdResponse_StudentApplication = {
  fromJSON(
    object: any,
  ): GetAppliedPartnersByStudentIdResponse_StudentApplication {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      partnerName: isSet(object.partnerName) ? String(object.partnerName) : '',
      registrationStatus: isSet(object.registrationStatus)
        ? registrationStatusFromJSON(object.registrationStatus)
        : 0,
      registeredAt: isSet(object.registeredAt)
        ? fromJsonTimestamp(object.registeredAt)
        : undefined,
      partnerStatus: isSet(object.partnerStatus)
        ? partnerStatusFromJSON(object.partnerStatus)
        : 0,
      isSelfRegistered: isSet(object.isSelfRegistered)
        ? Boolean(object.isSelfRegistered)
        : false,
    };
  },

  toJSON(
    message: GetAppliedPartnersByStudentIdResponse_StudentApplication,
  ): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.partnerName !== undefined &&
      (obj.partnerName = message.partnerName);
    message.registrationStatus !== undefined &&
      (obj.registrationStatus = registrationStatusToJSON(
        message.registrationStatus,
      ));
    message.registeredAt !== undefined &&
      (obj.registeredAt = fromTimestamp(message.registeredAt).toISOString());
    message.partnerStatus !== undefined &&
      (obj.partnerStatus = partnerStatusToJSON(message.partnerStatus));
    message.isSelfRegistered !== undefined &&
      (obj.isSelfRegistered = message.isSelfRegistered);
    return obj;
  },
};

function createBaseRemoveLecturerFromTermRequest(): RemoveLecturerFromTermRequest {
  return { termId: 0, lecturerId: 0 };
}

export const RemoveLecturerFromTermRequest = {
  fromJSON(object: any): RemoveLecturerFromTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
    };
  },

  toJSON(message: RemoveLecturerFromTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    return obj;
  },
};

function createBaseRemoveLecturerFromTermResponse(): RemoveLecturerFromTermResponse {
  return { code: 0, message: '' };
}

export const RemoveLecturerFromTermResponse = {
  fromJSON(object: any): RemoveLecturerFromTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: RemoveLecturerFromTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseSignUpForTermRequest(): SignUpForTermRequest {
  return { termId: 0, studentId: 0 };
}

export const SignUpForTermRequest = {
  fromJSON(object: any): SignUpForTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: SignUpForTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseSignUpForTermResponse(): SignUpForTermResponse {
  return { code: 0, message: '' };
}

export const SignUpForTermResponse = {
  fromJSON(object: any): SignUpForTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: SignUpForTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseCancelTermSignUpRequest(): CancelTermSignUpRequest {
  return { termId: 0, studentId: 0 };
}

export const CancelTermSignUpRequest = {
  fromJSON(object: any): CancelTermSignUpRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: CancelTermSignUpRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseCancelTermSignUpResponse(): CancelTermSignUpResponse {
  return { code: 0, message: '' };
}

export const CancelTermSignUpResponse = {
  fromJSON(object: any): CancelTermSignUpResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: CancelTermSignUpResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseBatchAddLecturersToTermRequest(): BatchAddLecturersToTermRequest {
  return { termId: 0, lecturerIds: [], organizationId: 0 };
}

export const BatchAddLecturersToTermRequest = {
  fromJSON(object: any): BatchAddLecturersToTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      lecturerIds: Array.isArray(object?.lecturerIds)
        ? object.lecturerIds.map((e: any) => Number(e))
        : [],
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: BatchAddLecturersToTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    if (message.lecturerIds) {
      obj.lecturerIds = message.lecturerIds.map((e) => Math.round(e));
    } else {
      obj.lecturerIds = [];
    }
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseBatchAddLecturersToTermResponse(): BatchAddLecturersToTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const BatchAddLecturersToTermResponse = {
  fromJSON(object: any): BatchAddLecturersToTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? BatchAddLecturersToTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: BatchAddLecturersToTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? BatchAddLecturersToTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseBatchAddLecturersToTermResponse_Data(): BatchAddLecturersToTermResponse_Data {
  return { successIds: [] };
}

export const BatchAddLecturersToTermResponse_Data = {
  fromJSON(object: any): BatchAddLecturersToTermResponse_Data {
    return {
      successIds: Array.isArray(object?.successIds)
        ? object.successIds.map((e: any) => Number(e))
        : [],
    };
  },

  toJSON(message: BatchAddLecturersToTermResponse_Data): unknown {
    const obj: any = {};
    if (message.successIds) {
      obj.successIds = message.successIds.map((e) => Math.round(e));
    } else {
      obj.successIds = [];
    }
    return obj;
  },
};

function createBaseBatchAddPartnersToTermRequest(): BatchAddPartnersToTermRequest {
  return { termId: 0, partnerIds: [] };
}

export const BatchAddPartnersToTermRequest = {
  fromJSON(object: any): BatchAddPartnersToTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      partnerIds: Array.isArray(object?.partnerIds)
        ? object.partnerIds.map((e: any) => Number(e))
        : [],
    };
  },

  toJSON(message: BatchAddPartnersToTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    if (message.partnerIds) {
      obj.partnerIds = message.partnerIds.map((e) => Math.round(e));
    } else {
      obj.partnerIds = [];
    }
    return obj;
  },
};

function createBaseBatchAddPartnersToTermResponse(): BatchAddPartnersToTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const BatchAddPartnersToTermResponse = {
  fromJSON(object: any): BatchAddPartnersToTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? BatchAddPartnersToTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: BatchAddPartnersToTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? BatchAddPartnersToTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseBatchAddPartnersToTermResponse_Data(): BatchAddPartnersToTermResponse_Data {
  return { successIds: [] };
}

export const BatchAddPartnersToTermResponse_Data = {
  fromJSON(object: any): BatchAddPartnersToTermResponse_Data {
    return {
      successIds: Array.isArray(object?.successIds)
        ? object.successIds.map((e: any) => Number(e))
        : [],
    };
  },

  toJSON(message: BatchAddPartnersToTermResponse_Data): unknown {
    const obj: any = {};
    if (message.successIds) {
      obj.successIds = message.successIds.map((e) => Math.round(e));
    } else {
      obj.successIds = [];
    }
    return obj;
  },
};

function createBaseGetPartnerTermListingRequest(): GetPartnerTermListingRequest {
  return { partnerId: 0 };
}

export const GetPartnerTermListingRequest = {
  fromJSON(object: any): GetPartnerTermListingRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
    };
  },

  toJSON(message: GetPartnerTermListingRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseGetPartnerTermListingResponse(): GetPartnerTermListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetPartnerTermListingResponse = {
  fromJSON(object: any): GetPartnerTermListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => TermLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetPartnerTermListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? TermLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetLecturerTermListingRequest(): GetLecturerTermListingRequest {
  return { lecturerId: 0 };
}

export const GetLecturerTermListingRequest = {
  fromJSON(object: any): GetLecturerTermListingRequest {
    return {
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
    };
  },

  toJSON(message: GetLecturerTermListingRequest): unknown {
    const obj: any = {};
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    return obj;
  },
};

function createBaseGetLecturerTermListingResponse(): GetLecturerTermListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetLecturerTermListingResponse = {
  fromJSON(object: any): GetLecturerTermListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => TermLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetLecturerTermListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? TermLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetStudentTermListingRequest(): GetStudentTermListingRequest {
  return { studentId: 0 };
}

export const GetStudentTermListingRequest = {
  fromJSON(object: any): GetStudentTermListingRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: GetStudentTermListingRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseGetStudentTermListingResponse(): GetStudentTermListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetStudentTermListingResponse = {
  fromJSON(object: any): GetStudentTermListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => TermLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetStudentTermListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? TermLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetTermListingRequest(): GetTermListingRequest {
  return { organizationId: 0 };
}

export const GetTermListingRequest = {
  fromJSON(object: any): GetTermListingRequest {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetTermListingRequest): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetTermListingResponse(): GetTermListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetTermListingResponse = {
  fromJSON(object: any): GetTermListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => TermLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetTermListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? TermLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetLatestTermRequest(): GetLatestTermRequest {
  return { studentId: 0, organizationId: 0 };
}

export const GetLatestTermRequest = {
  fromJSON(object: any): GetLatestTermRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetLatestTermRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetLatestTermResponse(): GetLatestTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetLatestTermResponse = {
  fromJSON(object: any): GetLatestTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetLatestTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetLatestTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetLatestTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetLatestTermResponse_Data(): GetLatestTermResponse_Data {
  return {
    id: 0,
    year: 0,
    term: 0,
    startDate: '',
    endDate: '',
    startRegAt: undefined,
    endRegAt: undefined,
    isActive: false,
    isRegistrationExpired: false,
    isRegistered: false,
  };
}

export const GetLatestTermResponse_Data = {
  fromJSON(object: any): GetLatestTermResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      year: isSet(object.year) ? Number(object.year) : 0,
      term: isSet(object.term) ? Number(object.term) : 0,
      startDate: isSet(object.startDate) ? String(object.startDate) : '',
      endDate: isSet(object.endDate) ? String(object.endDate) : '',
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      isActive: isSet(object.isActive) ? Boolean(object.isActive) : false,
      isRegistrationExpired: isSet(object.isRegistrationExpired)
        ? Boolean(object.isRegistrationExpired)
        : false,
      isRegistered: isSet(object.isRegistered)
        ? Boolean(object.isRegistered)
        : false,
    };
  },

  toJSON(message: GetLatestTermResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.year !== undefined && (obj.year = Math.round(message.year));
    message.term !== undefined && (obj.term = Math.round(message.term));
    message.startDate !== undefined && (obj.startDate = message.startDate);
    message.endDate !== undefined && (obj.endDate = message.endDate);
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.isActive !== undefined && (obj.isActive = message.isActive);
    message.isRegistrationExpired !== undefined &&
      (obj.isRegistrationExpired = message.isRegistrationExpired);
    message.isRegistered !== undefined &&
      (obj.isRegistered = message.isRegistered);
    return obj;
  },
};

function createBaseGetTermPartnerListingRequest(): GetTermPartnerListingRequest {
  return { termId: 0 };
}

export const GetTermPartnerListingRequest = {
  fromJSON(object: any): GetTermPartnerListingRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: GetTermPartnerListingRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetTermPartnerListingResponse(): GetTermPartnerListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetTermPartnerListingResponse = {
  fromJSON(object: any): GetTermPartnerListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => TermPartnerLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetTermPartnerListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? TermPartnerLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseUpdateTermRequest(): UpdateTermRequest {
  return {
    id: 0,
    term: 0,
    startRegAt: undefined,
    endRegAt: undefined,
    startDate: '',
    endDate: '',
    year: 0,
    filterOrganizationId: undefined,
  };
}

export const UpdateTermRequest = {
  fromJSON(object: any): UpdateTermRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      term: isSet(object.term) ? Number(object.term) : 0,
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      startDate: isSet(object.startDate) ? String(object.startDate) : '',
      endDate: isSet(object.endDate) ? String(object.endDate) : '',
      year: isSet(object.year) ? Number(object.year) : 0,
      filterOrganizationId: isSet(object.filterOrganizationId)
        ? Number(object.filterOrganizationId)
        : undefined,
    };
  },

  toJSON(message: UpdateTermRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.term !== undefined && (obj.term = Math.round(message.term));
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.startDate !== undefined && (obj.startDate = message.startDate);
    message.endDate !== undefined && (obj.endDate = message.endDate);
    message.year !== undefined && (obj.year = Math.round(message.year));
    message.filterOrganizationId !== undefined &&
      (obj.filterOrganizationId = Math.round(message.filterOrganizationId));
    return obj;
  },
};

function createBaseUpdateTermResponse(): UpdateTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const UpdateTermResponse = {
  fromJSON(object: any): UpdateTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? UpdateTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: UpdateTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? UpdateTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseUpdateTermResponse_Data(): UpdateTermResponse_Data {
  return { id: 0 };
}

export const UpdateTermResponse_Data = {
  fromJSON(object: any): UpdateTermResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: UpdateTermResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseCheckStudentInTermRequest(): CheckStudentInTermRequest {
  return { termId: 0, studentId: 0 };
}

export const CheckStudentInTermRequest = {
  fromJSON(object: any): CheckStudentInTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: CheckStudentInTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseCheckStudentInTermResponse(): CheckStudentInTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const CheckStudentInTermResponse = {
  fromJSON(object: any): CheckStudentInTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CheckStudentInTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CheckStudentInTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CheckStudentInTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCheckStudentInTermResponse_Data(): CheckStudentInTermResponse_Data {
  return { isIn: false };
}

export const CheckStudentInTermResponse_Data = {
  fromJSON(object: any): CheckStudentInTermResponse_Data {
    return {
      isIn: isSet(object.isIn) ? Boolean(object.isIn) : false,
    };
  },

  toJSON(message: CheckStudentInTermResponse_Data): unknown {
    const obj: any = {};
    message.isIn !== undefined && (obj.isIn = message.isIn);
    return obj;
  },
};

function createBaseCheckLecturerInTermRequest(): CheckLecturerInTermRequest {
  return { termId: 0, lecturerId: 0 };
}

export const CheckLecturerInTermRequest = {
  fromJSON(object: any): CheckLecturerInTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
    };
  },

  toJSON(message: CheckLecturerInTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    return obj;
  },
};

function createBaseCheckLecturerInTermResponse(): CheckLecturerInTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const CheckLecturerInTermResponse = {
  fromJSON(object: any): CheckLecturerInTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CheckLecturerInTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CheckLecturerInTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CheckLecturerInTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCheckLecturerInTermResponse_Data(): CheckLecturerInTermResponse_Data {
  return { isIn: false };
}

export const CheckLecturerInTermResponse_Data = {
  fromJSON(object: any): CheckLecturerInTermResponse_Data {
    return {
      isIn: isSet(object.isIn) ? Boolean(object.isIn) : false,
    };
  },

  toJSON(message: CheckLecturerInTermResponse_Data): unknown {
    const obj: any = {};
    message.isIn !== undefined && (obj.isIn = message.isIn);
    return obj;
  },
};

function createBaseCheckPartnerInTermRequest(): CheckPartnerInTermRequest {
  return { termId: 0, lecturerId: 0 };
}

export const CheckPartnerInTermRequest = {
  fromJSON(object: any): CheckPartnerInTermRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
    };
  },

  toJSON(message: CheckPartnerInTermRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    return obj;
  },
};

function createBaseCheckPartnerInTermResponse(): CheckPartnerInTermResponse {
  return { code: 0, message: '', data: undefined };
}

export const CheckPartnerInTermResponse = {
  fromJSON(object: any): CheckPartnerInTermResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CheckPartnerInTermResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CheckPartnerInTermResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CheckPartnerInTermResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCheckPartnerInTermResponse_Data(): CheckPartnerInTermResponse_Data {
  return { isIn: false, status: undefined };
}

export const CheckPartnerInTermResponse_Data = {
  fromJSON(object: any): CheckPartnerInTermResponse_Data {
    return {
      isIn: isSet(object.isIn) ? Boolean(object.isIn) : false,
      status: isSet(object.status)
        ? partnerStatusFromJSON(object.status)
        : undefined,
    };
  },

  toJSON(message: CheckPartnerInTermResponse_Data): unknown {
    const obj: any = {};
    message.isIn !== undefined && (obj.isIn = message.isIn);
    message.status !== undefined &&
      (obj.status =
        message.status !== undefined
          ? partnerStatusToJSON(message.status)
          : undefined);
    return obj;
  },
};

function createBaseGetTermStudentDetailsRequest(): GetTermStudentDetailsRequest {
  return { studentId: 0, termId: 0 };
}

export const GetTermStudentDetailsRequest = {
  fromJSON(object: any): GetTermStudentDetailsRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: GetTermStudentDetailsRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetTermStudentDetailsResponse(): GetTermStudentDetailsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetTermStudentDetailsResponse = {
  fromJSON(object: any): GetTermStudentDetailsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetTermStudentDetailsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetTermStudentDetailsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetTermStudentDetailsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetTermStudentDetailsResponse_Supervisor(): GetTermStudentDetailsResponse_Supervisor {
  return { id: 0, name: '', orgEmail: '', personalEmail: '', phoneNumber: '' };
}

export const GetTermStudentDetailsResponse_Supervisor = {
  fromJSON(object: any): GetTermStudentDetailsResponse_Supervisor {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
    };
  },

  toJSON(message: GetTermStudentDetailsResponse_Supervisor): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    return obj;
  },
};

function createBaseGetTermStudentDetailsResponse_AppliedPartner(): GetTermStudentDetailsResponse_AppliedPartner {
  return {
    id: 0,
    name: '',
    applyStatus: 0,
    partnerStatus: 0,
    isSelfRegistered: false,
    createdAt: undefined,
    internshipType: 0,
  };
}

export const GetTermStudentDetailsResponse_AppliedPartner = {
  fromJSON(object: any): GetTermStudentDetailsResponse_AppliedPartner {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      applyStatus: isSet(object.applyStatus)
        ? registrationStatusFromJSON(object.applyStatus)
        : 0,
      partnerStatus: isSet(object.partnerStatus)
        ? partnerStatusFromJSON(object.partnerStatus)
        : 0,
      isSelfRegistered: isSet(object.isSelfRegistered)
        ? Boolean(object.isSelfRegistered)
        : false,
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      internshipType: isSet(object.internshipType)
        ? internshipTypeFromJSON(object.internshipType)
        : 0,
    };
  },

  toJSON(message: GetTermStudentDetailsResponse_AppliedPartner): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.applyStatus !== undefined &&
      (obj.applyStatus = registrationStatusToJSON(message.applyStatus));
    message.partnerStatus !== undefined &&
      (obj.partnerStatus = partnerStatusToJSON(message.partnerStatus));
    message.isSelfRegistered !== undefined &&
      (obj.isSelfRegistered = message.isSelfRegistered);
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.internshipType !== undefined &&
      (obj.internshipType = internshipTypeToJSON(message.internshipType));
    return obj;
  },
};

function createBaseGetTermStudentDetailsResponse_Data(): GetTermStudentDetailsResponse_Data {
  return {
    supervisor: undefined,
    appliedPartners: [],
    reportFileName: '',
    selectedPartnerIndex: undefined,
  };
}

export const GetTermStudentDetailsResponse_Data = {
  fromJSON(object: any): GetTermStudentDetailsResponse_Data {
    return {
      supervisor: isSet(object.supervisor)
        ? GetTermStudentDetailsResponse_Supervisor.fromJSON(object.supervisor)
        : undefined,
      appliedPartners: Array.isArray(object?.appliedPartners)
        ? object.appliedPartners.map((e: any) =>
            GetTermStudentDetailsResponse_AppliedPartner.fromJSON(e),
          )
        : [],
      reportFileName: isSet(object.reportFileName)
        ? String(object.reportFileName)
        : '',
      selectedPartnerIndex: isSet(object.selectedPartnerIndex)
        ? Number(object.selectedPartnerIndex)
        : undefined,
    };
  },

  toJSON(message: GetTermStudentDetailsResponse_Data): unknown {
    const obj: any = {};
    message.supervisor !== undefined &&
      (obj.supervisor = message.supervisor
        ? GetTermStudentDetailsResponse_Supervisor.toJSON(message.supervisor)
        : undefined);
    if (message.appliedPartners) {
      obj.appliedPartners = message.appliedPartners.map((e) =>
        e ? GetTermStudentDetailsResponse_AppliedPartner.toJSON(e) : undefined,
      );
    } else {
      obj.appliedPartners = [];
    }
    message.reportFileName !== undefined &&
      (obj.reportFileName = message.reportFileName);
    message.selectedPartnerIndex !== undefined &&
      (obj.selectedPartnerIndex = Math.round(message.selectedPartnerIndex));
    return obj;
  },
};

function createBaseGetTermLecturerListingRequest(): GetTermLecturerListingRequest {
  return { termId: 0 };
}

export const GetTermLecturerListingRequest = {
  fromJSON(object: any): GetTermLecturerListingRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: GetTermLecturerListingRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetTermLecturerListingResponse(): GetTermLecturerListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetTermLecturerListingResponse = {
  fromJSON(object: any): GetTermLecturerListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) =>
            GetTermLecturerListingResponse_LecturerLittleInfo.fromJSON(e),
          )
        : [],
    };
  },

  toJSON(message: GetTermLecturerListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e
          ? GetTermLecturerListingResponse_LecturerLittleInfo.toJSON(e)
          : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetTermLecturerListingResponse_LecturerLittleInfo(): GetTermLecturerListingResponse_LecturerLittleInfo {
  return { id: 0, fullName: '' };
}

export const GetTermLecturerListingResponse_LecturerLittleInfo = {
  fromJSON(object: any): GetTermLecturerListingResponse_LecturerLittleInfo {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
    };
  },

  toJSON(message: GetTermLecturerListingResponse_LecturerLittleInfo): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    return obj;
  },
};

function createBaseExportTermStudentsRequest(): ExportTermStudentsRequest {
  return { sort: [], filter: undefined, q: '' };
}

export const ExportTermStudentsRequest = {
  fromJSON(object: any): ExportTermStudentsRequest {
    return {
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetTermStudentsRequestFilter.fromJSON(object.filter)
        : undefined,
      q: isSet(object.q) ? String(object.q) : '',
    };
  },

  toJSON(message: ExportTermStudentsRequest): unknown {
    const obj: any = {};
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetTermStudentsRequestFilter.toJSON(message.filter)
        : undefined);
    message.q !== undefined && (obj.q = message.q);
    return obj;
  },
};

function createBaseDownloadUrlData(): DownloadUrlData {
  return { downloadUrl: '' };
}

export const DownloadUrlData = {
  fromJSON(object: any): DownloadUrlData {
    return {
      downloadUrl: isSet(object.downloadUrl) ? String(object.downloadUrl) : '',
    };
  },

  toJSON(message: DownloadUrlData): unknown {
    const obj: any = {};
    message.downloadUrl !== undefined &&
      (obj.downloadUrl = message.downloadUrl);
    return obj;
  },
};

function createBaseExportTermStudentsResponse(): ExportTermStudentsResponse {
  return { code: 0, message: '', data: undefined };
}

export const ExportTermStudentsResponse = {
  fromJSON(object: any): ExportTermStudentsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? DownloadUrlData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: ExportTermStudentsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? DownloadUrlData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseExportTermPartnersRequest(): ExportTermPartnersRequest {
  return { sort: [], filter: undefined, q: '' };
}

export const ExportTermPartnersRequest = {
  fromJSON(object: any): ExportTermPartnersRequest {
    return {
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetTermPartnersRequestFilter.fromJSON(object.filter)
        : undefined,
      q: isSet(object.q) ? String(object.q) : '',
    };
  },

  toJSON(message: ExportTermPartnersRequest): unknown {
    const obj: any = {};
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetTermPartnersRequestFilter.toJSON(message.filter)
        : undefined);
    message.q !== undefined && (obj.q = message.q);
    return obj;
  },
};

function createBaseExportTermPartnersResponse(): ExportTermPartnersResponse {
  return { code: 0, message: '', data: undefined };
}

export const ExportTermPartnersResponse = {
  fromJSON(object: any): ExportTermPartnersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? DownloadUrlData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: ExportTermPartnersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? DownloadUrlData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseExportStudentApplicationsRequest(): ExportStudentApplicationsRequest {
  return { sort: [], filter: undefined, q: '' };
}

export const ExportStudentApplicationsRequest = {
  fromJSON(object: any): ExportStudentApplicationsRequest {
    return {
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      filter: isSet(object.filter)
        ? GetStudentApplicationsRequestFilter.fromJSON(object.filter)
        : undefined,
      q: isSet(object.q) ? String(object.q) : '',
    };
  },

  toJSON(message: ExportStudentApplicationsRequest): unknown {
    const obj: any = {};
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetStudentApplicationsRequestFilter.toJSON(message.filter)
        : undefined);
    message.q !== undefined && (obj.q = message.q);
    return obj;
  },
};

function createBaseExportStudentApplicationsResponse(): ExportStudentApplicationsResponse {
  return { code: 0, message: '', data: undefined };
}

export const ExportStudentApplicationsResponse = {
  fromJSON(object: any): ExportStudentApplicationsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? DownloadUrlData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: ExportStudentApplicationsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? DownloadUrlData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentPostsRequest(): GetStudentPostsRequest {
  return { page: 0, perPage: 0, studentId: 0, termId: 0 };
}

export const GetStudentPostsRequest = {
  fromJSON(object: any): GetStudentPostsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: GetStudentPostsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseGetStudentPostsResponse(): GetStudentPostsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetStudentPostsResponse = {
  fromJSON(object: any): GetStudentPostsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetStudentPostsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetStudentPostsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetStudentPostsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentPostsResponse_StudentPost(): GetStudentPostsResponse_StudentPost {
  return {
    post: undefined,
    isRegistered: false,
    isSelfRegistered: false,
    isRegistrationExpired: false,
  };
}

export const GetStudentPostsResponse_StudentPost = {
  fromJSON(object: any): GetStudentPostsResponse_StudentPost {
    return {
      post: isSet(object.post) ? Post.fromJSON(object.post) : undefined,
      isRegistered: isSet(object.isRegistered)
        ? Boolean(object.isRegistered)
        : false,
      isSelfRegistered: isSet(object.isSelfRegistered)
        ? Boolean(object.isSelfRegistered)
        : false,
      isRegistrationExpired: isSet(object.isRegistrationExpired)
        ? Boolean(object.isRegistrationExpired)
        : false,
    };
  },

  toJSON(message: GetStudentPostsResponse_StudentPost): unknown {
    const obj: any = {};
    message.post !== undefined &&
      (obj.post = message.post ? Post.toJSON(message.post) : undefined);
    message.isRegistered !== undefined &&
      (obj.isRegistered = message.isRegistered);
    message.isSelfRegistered !== undefined &&
      (obj.isSelfRegistered = message.isSelfRegistered);
    message.isRegistrationExpired !== undefined &&
      (obj.isRegistrationExpired = message.isRegistrationExpired);
    return obj;
  },
};

function createBaseGetStudentPostsResponse_Data(): GetStudentPostsResponse_Data {
  return { posts: [], meta: undefined };
}

export const GetStudentPostsResponse_Data = {
  fromJSON(object: any): GetStudentPostsResponse_Data {
    return {
      posts: Array.isArray(object?.posts)
        ? object.posts.map((e: any) =>
            GetStudentPostsResponse_StudentPost.fromJSON(e),
          )
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetStudentPostsResponse_Data): unknown {
    const obj: any = {};
    if (message.posts) {
      obj.posts = message.posts.map((e) =>
        e ? GetStudentPostsResponse_StudentPost.toJSON(e) : undefined,
      );
    } else {
      obj.posts = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentPostByIdRequest(): GetStudentPostByIdRequest {
  return { postId: 0, studentId: 0 };
}

export const GetStudentPostByIdRequest = {
  fromJSON(object: any): GetStudentPostByIdRequest {
    return {
      postId: isSet(object.postId) ? Number(object.postId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: GetStudentPostByIdRequest): unknown {
    const obj: any = {};
    message.postId !== undefined && (obj.postId = Math.round(message.postId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseGetStudentPostByIdResponse(): GetStudentPostByIdResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetStudentPostByIdResponse = {
  fromJSON(object: any): GetStudentPostByIdResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetStudentPostByIdResponse_StudentPost.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetStudentPostByIdResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetStudentPostByIdResponse_StudentPost.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentPostByIdResponse_StudentPost(): GetStudentPostByIdResponse_StudentPost {
  return {
    post: undefined,
    isRegistered: false,
    isSelfRegistered: false,
    isRegistrationExpired: false,
  };
}

export const GetStudentPostByIdResponse_StudentPost = {
  fromJSON(object: any): GetStudentPostByIdResponse_StudentPost {
    return {
      post: isSet(object.post) ? Post.fromJSON(object.post) : undefined,
      isRegistered: isSet(object.isRegistered)
        ? Boolean(object.isRegistered)
        : false,
      isSelfRegistered: isSet(object.isSelfRegistered)
        ? Boolean(object.isSelfRegistered)
        : false,
      isRegistrationExpired: isSet(object.isRegistrationExpired)
        ? Boolean(object.isRegistrationExpired)
        : false,
    };
  },

  toJSON(message: GetStudentPostByIdResponse_StudentPost): unknown {
    const obj: any = {};
    message.post !== undefined &&
      (obj.post = message.post ? Post.toJSON(message.post) : undefined);
    message.isRegistered !== undefined &&
      (obj.isRegistered = message.isRegistered);
    message.isSelfRegistered !== undefined &&
      (obj.isSelfRegistered = message.isSelfRegistered);
    message.isRegistrationExpired !== undefined &&
      (obj.isRegistrationExpired = message.isRegistrationExpired);
    return obj;
  },
};

function createBaseUnregisterPartnerRequest(): UnregisterPartnerRequest {
  return { termId: 0, partnerId: 0, studentId: 0 };
}

export const UnregisterPartnerRequest = {
  fromJSON(object: any): UnregisterPartnerRequest {
    return {
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: UnregisterPartnerRequest): unknown {
    const obj: any = {};
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseUnregisterPartnerResponse(): UnregisterPartnerResponse {
  return { code: 0, message: '' };
}

export const UnregisterPartnerResponse = {
  fromJSON(object: any): UnregisterPartnerResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: UnregisterPartnerResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseGetMyStudentStatsRequest(): GetMyStudentStatsRequest {
  return { lecturerId: 0, organizationId: 0 };
}

export const GetMyStudentStatsRequest = {
  fromJSON(object: any): GetMyStudentStatsRequest {
    return {
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetMyStudentStatsRequest): unknown {
    const obj: any = {};
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetMyStudentStatsResponse(): GetMyStudentStatsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetMyStudentStatsResponse = {
  fromJSON(object: any): GetMyStudentStatsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetMyStudentStatsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetMyStudentStatsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetMyStudentStatsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetMyStudentStatsResponse_ScoreStat(): GetMyStudentStatsResponse_ScoreStat {
  return { scored: 0, notScored: 0 };
}

export const GetMyStudentStatsResponse_ScoreStat = {
  fromJSON(object: any): GetMyStudentStatsResponse_ScoreStat {
    return {
      scored: isSet(object.scored) ? Number(object.scored) : 0,
      notScored: isSet(object.notScored) ? Number(object.notScored) : 0,
    };
  },

  toJSON(message: GetMyStudentStatsResponse_ScoreStat): unknown {
    const obj: any = {};
    message.scored !== undefined && (obj.scored = Math.round(message.scored));
    message.notScored !== undefined &&
      (obj.notScored = Math.round(message.notScored));
    return obj;
  },
};

function createBaseGetMyStudentStatsResponse_ReportStat(): GetMyStudentStatsResponse_ReportStat {
  return { submitted: 0, notSubmitted: 0 };
}

export const GetMyStudentStatsResponse_ReportStat = {
  fromJSON(object: any): GetMyStudentStatsResponse_ReportStat {
    return {
      submitted: isSet(object.submitted) ? Number(object.submitted) : 0,
      notSubmitted: isSet(object.notSubmitted)
        ? Number(object.notSubmitted)
        : 0,
    };
  },

  toJSON(message: GetMyStudentStatsResponse_ReportStat): unknown {
    const obj: any = {};
    message.submitted !== undefined &&
      (obj.submitted = Math.round(message.submitted));
    message.notSubmitted !== undefined &&
      (obj.notSubmitted = Math.round(message.notSubmitted));
    return obj;
  },
};

function createBaseGetMyStudentStatsResponse_Data(): GetMyStudentStatsResponse_Data {
  return { scoring: undefined, reporting: undefined };
}

export const GetMyStudentStatsResponse_Data = {
  fromJSON(object: any): GetMyStudentStatsResponse_Data {
    return {
      scoring: isSet(object.scoring)
        ? GetMyStudentStatsResponse_ScoreStat.fromJSON(object.scoring)
        : undefined,
      reporting: isSet(object.reporting)
        ? GetMyStudentStatsResponse_ReportStat.fromJSON(object.reporting)
        : undefined,
    };
  },

  toJSON(message: GetMyStudentStatsResponse_Data): unknown {
    const obj: any = {};
    message.scoring !== undefined &&
      (obj.scoring = message.scoring
        ? GetMyStudentStatsResponse_ScoreStat.toJSON(message.scoring)
        : undefined);
    message.reporting !== undefined &&
      (obj.reporting = message.reporting
        ? GetMyStudentStatsResponse_ReportStat.toJSON(message.reporting)
        : undefined);
    return obj;
  },
};

function createBasePastStat(): PastStat {
  return { lastTotal: undefined, currentTotal: undefined };
}

export const PastStat = {
  fromJSON(object: any): PastStat {
    return {
      lastTotal: isSet(object.lastTotal) ? Number(object.lastTotal) : undefined,
      currentTotal: isSet(object.currentTotal)
        ? Number(object.currentTotal)
        : undefined,
    };
  },

  toJSON(message: PastStat): unknown {
    const obj: any = {};
    message.lastTotal !== undefined &&
      (obj.lastTotal = Math.round(message.lastTotal));
    message.currentTotal !== undefined &&
      (obj.currentTotal = Math.round(message.currentTotal));
    return obj;
  },
};

function createBaseTermLittleInfo(): TermLittleInfo {
  return { id: 0, year: 0, term: 0 };
}

export const TermLittleInfo = {
  fromJSON(object: any): TermLittleInfo {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      year: isSet(object.year) ? Number(object.year) : 0,
      term: isSet(object.term) ? Number(object.term) : 0,
    };
  },

  toJSON(message: TermLittleInfo): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.year !== undefined && (obj.year = Math.round(message.year));
    message.term !== undefined && (obj.term = Math.round(message.term));
    return obj;
  },
};

function createBaseTermPartnerLittleInfo(): TermPartnerLittleInfo {
  return { id: 0, name: '', logoUrl: '', status: undefined };
}

export const TermPartnerLittleInfo = {
  fromJSON(object: any): TermPartnerLittleInfo {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : '',
      status: isSet(object.status)
        ? partnerStatusFromJSON(object.status)
        : undefined,
    };
  },

  toJSON(message: TermPartnerLittleInfo): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.status !== undefined &&
      (obj.status =
        message.status !== undefined
          ? partnerStatusToJSON(message.status)
          : undefined);
    return obj;
  },
};

function createBaseStudentApplication(): StudentApplication {
  return {
    id: 0,
    fullName: '',
    orgEmail: '',
    phoneNumber: '',
    partnerId: 0,
    partnerName: '',
    status: 0,
    registeredAt: undefined,
    internshipType: 0,
    studentIdNumber: '',
    isMailSent: false,
    studentId: 0,
    termId: 0,
  };
}

export const StudentApplication = {
  fromJSON(object: any): StudentApplication {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      partnerName: isSet(object.partnerName) ? String(object.partnerName) : '',
      status: isSet(object.status)
        ? registrationStatusFromJSON(object.status)
        : 0,
      registeredAt: isSet(object.registeredAt)
        ? fromJsonTimestamp(object.registeredAt)
        : undefined,
      internshipType: isSet(object.internshipType)
        ? internshipTypeFromJSON(object.internshipType)
        : 0,
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
      isMailSent: isSet(object.isMailSent) ? Boolean(object.isMailSent) : false,
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
    };
  },

  toJSON(message: StudentApplication): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.partnerName !== undefined &&
      (obj.partnerName = message.partnerName);
    message.status !== undefined &&
      (obj.status = registrationStatusToJSON(message.status));
    message.registeredAt !== undefined &&
      (obj.registeredAt = fromTimestamp(message.registeredAt).toISOString());
    message.internshipType !== undefined &&
      (obj.internshipType = internshipTypeToJSON(message.internshipType));
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    message.isMailSent !== undefined && (obj.isMailSent = message.isMailSent);
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    return obj;
  },
};

function createBaseStudentApplication_SchoolClass(): StudentApplication_SchoolClass {
  return { id: 0, name: '' };
}

export const StudentApplication_SchoolClass = {
  fromJSON(object: any): StudentApplication_SchoolClass {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: StudentApplication_SchoolClass): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseStudentApplication_Supervisor(): StudentApplication_Supervisor {
  return { id: 0, name: '' };
}

export const StudentApplication_Supervisor = {
  fromJSON(object: any): StudentApplication_Supervisor {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: StudentApplication_Supervisor): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseTermStudent(): TermStudent {
  return {
    id: 0,
    fullName: '',
    phoneNumber: '',
    schoolClass: undefined,
    userId: 0,
    orgEmail: '',
    personalEmail: '',
    selectedAt: undefined,
    selectedPartner: undefined,
    organizationId: 0,
    studentIdNumber: '',
    supervisor: undefined,
    reportSubmitted: false,
    termId: 0,
    score: undefined,
  };
}

export const TermStudent = {
  fromJSON(object: any): TermStudent {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      schoolClass: isSet(object.schoolClass)
        ? TermStudent_SchoolClass.fromJSON(object.schoolClass)
        : undefined,
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      selectedAt: isSet(object.selectedAt)
        ? fromJsonTimestamp(object.selectedAt)
        : undefined,
      selectedPartner: isSet(object.selectedPartner)
        ? TermStudent_SelectedPartner.fromJSON(object.selectedPartner)
        : undefined,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
      supervisor: isSet(object.supervisor)
        ? TermStudent_Supervisor.fromJSON(object.supervisor)
        : undefined,
      reportSubmitted: isSet(object.reportSubmitted)
        ? Boolean(object.reportSubmitted)
        : false,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      score: isSet(object.score) ? Number(object.score) : undefined,
    };
  },

  toJSON(message: TermStudent): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.schoolClass !== undefined &&
      (obj.schoolClass = message.schoolClass
        ? TermStudent_SchoolClass.toJSON(message.schoolClass)
        : undefined);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.selectedAt !== undefined &&
      (obj.selectedAt = fromTimestamp(message.selectedAt).toISOString());
    message.selectedPartner !== undefined &&
      (obj.selectedPartner = message.selectedPartner
        ? TermStudent_SelectedPartner.toJSON(message.selectedPartner)
        : undefined);
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    message.supervisor !== undefined &&
      (obj.supervisor = message.supervisor
        ? TermStudent_Supervisor.toJSON(message.supervisor)
        : undefined);
    message.reportSubmitted !== undefined &&
      (obj.reportSubmitted = message.reportSubmitted);
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.score !== undefined && (obj.score = message.score);
    return obj;
  },
};

function createBaseTermStudent_SchoolClass(): TermStudent_SchoolClass {
  return { id: 0, name: '' };
}

export const TermStudent_SchoolClass = {
  fromJSON(object: any): TermStudent_SchoolClass {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: TermStudent_SchoolClass): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseTermStudent_Supervisor(): TermStudent_Supervisor {
  return { id: 0, name: '' };
}

export const TermStudent_Supervisor = {
  fromJSON(object: any): TermStudent_Supervisor {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: TermStudent_Supervisor): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseTermStudent_SelectedPartner(): TermStudent_SelectedPartner {
  return { id: 0, name: '' };
}

export const TermStudent_SelectedPartner = {
  fromJSON(object: any): TermStudent_SelectedPartner {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: TermStudent_SelectedPartner): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseTermLecturer(): TermLecturer {
  return {
    id: 0,
    fullName: '',
    orgEmail: '',
    personalEmail: '',
    userId: 0,
    organizationId: 0,
    numOfSupervisedStudents: 0,
    termId: 0,
    createdAt: undefined,
  };
}

export const TermLecturer = {
  fromJSON(object: any): TermLecturer {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      numOfSupervisedStudents: isSet(object.numOfSupervisedStudents)
        ? Number(object.numOfSupervisedStudents)
        : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
    };
  },

  toJSON(message: TermLecturer): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.numOfSupervisedStudents !== undefined &&
      (obj.numOfSupervisedStudents = Math.round(
        message.numOfSupervisedStudents,
      ));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    return obj;
  },
};

function createBaseTermPartner(): TermPartner {
  return {
    id: 0,
    name: '',
    email: '',
    homepageUrl: '',
    userId: undefined,
    joinedAt: undefined,
    termId: 0,
    status: 0,
  };
}

export const TermPartner = {
  fromJSON(object: any): TermPartner {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      email: isSet(object.email) ? String(object.email) : '',
      homepageUrl: isSet(object.homepageUrl) ? String(object.homepageUrl) : '',
      userId: isSet(object.userId) ? Number(object.userId) : undefined,
      joinedAt: isSet(object.joinedAt)
        ? fromJsonTimestamp(object.joinedAt)
        : undefined,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      status: isSet(object.status) ? partnerStatusFromJSON(object.status) : 0,
    };
  },

  toJSON(message: TermPartner): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.email !== undefined && (obj.email = message.email);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.joinedAt !== undefined &&
      (obj.joinedAt = fromTimestamp(message.joinedAt).toISOString());
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.status !== undefined &&
      (obj.status = partnerStatusToJSON(message.status));
    return obj;
  },
};

function createBaseTermPartner_AppliedStudents(): TermPartner_AppliedStudents {
  return {
    id: 0,
    fullName: '',
    orgEmail: '',
    studentIdNumber: '',
    isSelfRegistered: false,
    status: '',
  };
}

export const TermPartner_AppliedStudents = {
  fromJSON(object: any): TermPartner_AppliedStudents {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
      isSelfRegistered: isSet(object.isSelfRegistered)
        ? Boolean(object.isSelfRegistered)
        : false,
      status: isSet(object.status) ? String(object.status) : '',
    };
  },

  toJSON(message: TermPartner_AppliedStudents): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    message.isSelfRegistered !== undefined &&
      (obj.isSelfRegistered = message.isSelfRegistered);
    message.status !== undefined && (obj.status = message.status);
    return obj;
  },
};

function createBasePaginationMetadata(): PaginationMetadata {
  return { currentPage: 0, totalPages: 0, totalItems: 0, perPage: 0 };
}

export const PaginationMetadata = {
  fromJSON(object: any): PaginationMetadata {
    return {
      currentPage: isSet(object.currentPage) ? Number(object.currentPage) : 0,
      totalPages: isSet(object.totalPages) ? Number(object.totalPages) : 0,
      totalItems: isSet(object.totalItems) ? Number(object.totalItems) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
    };
  },

  toJSON(message: PaginationMetadata): unknown {
    const obj: any = {};
    message.currentPage !== undefined &&
      (obj.currentPage = Math.round(message.currentPage));
    message.totalPages !== undefined &&
      (obj.totalPages = Math.round(message.totalPages));
    message.totalItems !== undefined &&
      (obj.totalItems = Math.round(message.totalItems));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    return obj;
  },
};

function createBasePost(): Post {
  return {
    id: 0,
    termId: 0,
    partnerId: 0,
    partnerName: '',
    partnerContactId: 0,
    partnerContactName: '',
    partnerContactEmail: '',
    partnerContactPhoneNumber: '',
    startRegAt: undefined,
    endRegAt: undefined,
    createdAt: undefined,
    title: '',
    content: '',
    jobCount: 0,
  };
}

export const Post = {
  fromJSON(object: any): Post {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      termId: isSet(object.termId) ? Number(object.termId) : 0,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      partnerName: isSet(object.partnerName) ? String(object.partnerName) : '',
      partnerContactId: isSet(object.partnerContactId)
        ? Number(object.partnerContactId)
        : 0,
      partnerContactName: isSet(object.partnerContactName)
        ? String(object.partnerContactName)
        : '',
      partnerContactEmail: isSet(object.partnerContactEmail)
        ? String(object.partnerContactEmail)
        : '',
      partnerContactPhoneNumber: isSet(object.partnerContactPhoneNumber)
        ? String(object.partnerContactPhoneNumber)
        : '',
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      title: isSet(object.title) ? String(object.title) : '',
      content: isSet(object.content) ? String(object.content) : '',
      jobCount: isSet(object.jobCount) ? Number(object.jobCount) : 0,
    };
  },

  toJSON(message: Post): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.termId !== undefined && (obj.termId = Math.round(message.termId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.partnerName !== undefined &&
      (obj.partnerName = message.partnerName);
    message.partnerContactId !== undefined &&
      (obj.partnerContactId = Math.round(message.partnerContactId));
    message.partnerContactName !== undefined &&
      (obj.partnerContactName = message.partnerContactName);
    message.partnerContactEmail !== undefined &&
      (obj.partnerContactEmail = message.partnerContactEmail);
    message.partnerContactPhoneNumber !== undefined &&
      (obj.partnerContactPhoneNumber = message.partnerContactPhoneNumber);
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.title !== undefined && (obj.title = message.title);
    message.content !== undefined && (obj.content = message.content);
    message.jobCount !== undefined &&
      (obj.jobCount = Math.round(message.jobCount));
    return obj;
  },
};

function createBaseTerm(): Term {
  return {
    id: 0,
    year: 0,
    term: 0,
    startRegAt: undefined,
    endRegAt: undefined,
    startDate: '',
    endDate: '',
    createdAt: undefined,
    organizationId: 0,
    numOfTermStudents: 0,
    numOfAcceptedPartners: 0,
  };
}

export const Term = {
  fromJSON(object: any): Term {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      year: isSet(object.year) ? Number(object.year) : 0,
      term: isSet(object.term) ? Number(object.term) : 0,
      startRegAt: isSet(object.startRegAt)
        ? fromJsonTimestamp(object.startRegAt)
        : undefined,
      endRegAt: isSet(object.endRegAt)
        ? fromJsonTimestamp(object.endRegAt)
        : undefined,
      startDate: isSet(object.startDate) ? String(object.startDate) : '',
      endDate: isSet(object.endDate) ? String(object.endDate) : '',
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      numOfTermStudents: isSet(object.numOfTermStudents)
        ? Number(object.numOfTermStudents)
        : 0,
      numOfAcceptedPartners: isSet(object.numOfAcceptedPartners)
        ? Number(object.numOfAcceptedPartners)
        : 0,
    };
  },

  toJSON(message: Term): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.year !== undefined && (obj.year = Math.round(message.year));
    message.term !== undefined && (obj.term = Math.round(message.term));
    message.startRegAt !== undefined &&
      (obj.startRegAt = fromTimestamp(message.startRegAt).toISOString());
    message.endRegAt !== undefined &&
      (obj.endRegAt = fromTimestamp(message.endRegAt).toISOString());
    message.startDate !== undefined && (obj.startDate = message.startDate);
    message.endDate !== undefined && (obj.endDate = message.endDate);
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.numOfTermStudents !== undefined &&
      (obj.numOfTermStudents = Math.round(message.numOfTermStudents));
    message.numOfAcceptedPartners !== undefined &&
      (obj.numOfAcceptedPartners = Math.round(message.numOfAcceptedPartners));
    return obj;
  },
};

export interface TermServiceClient {
  getTerms(
    request: GetTermsRequest,
    metadata?: Metadata,
  ): Observable<GetTermsResponse>;

  getTermStudents(
    request: GetTermStudentsRequest,
    metadata?: Metadata,
  ): Observable<GetTermStudentsResponse>;

  getTermLecturers(
    request: GetTermLecturersRequest,
    metadata?: Metadata,
  ): Observable<GetTermLecturersResponse>;

  getTermPartners(
    request: GetTermPartnersRequest,
    metadata?: Metadata,
  ): Observable<GetTermPartnersResponse>;

  batchChangeTermPartnerStatus(
    request: BatchChangeTermPartnerStatusRequest,
    metadata?: Metadata,
  ): Observable<BatchChangeTermPartnerStatusResponse>;

  assignSupervisorToStudents(
    request: AssignSupervisorToStudentsRequest,
    metadata?: Metadata,
  ): Observable<AssignSupervisorToStudentsResponse>;

  createPost(
    request: CreatePostRequest,
    metadata?: Metadata,
  ): Observable<CreatePostResponse>;

  updatePost(
    request: UpdatePostRequest,
    metadata?: Metadata,
  ): Observable<UpdatePostResponse>;

  batchUpdateRegistrationStatus(
    request: BatchUpdateRegistrationStatusRequest,
    metadata?: Metadata,
  ): Observable<BatchUpdateRegistrationStatusResponse>;

  applyToPartner(
    request: ApplyToPartnerRequest,
    metadata?: Metadata,
  ): Observable<ApplyToPartnerResponse>;

  selfRegisterPartner(
    request: SelfRegisterPartnerRequest,
    metadata?: Metadata,
  ): Observable<SelfRegisterPartnerResponse>;

  checkTermInOrg(
    request: CheckTermInOrgRequest,
    metadata?: Metadata,
  ): Observable<CheckTermInOrgResponse>;

  createTerm(
    request: CreateTermRequest,
    metadata?: Metadata,
  ): Observable<CreateTermResponse>;

  getStudentApplications(
    request: GetStudentApplicationsRequest,
    metadata?: Metadata,
  ): Observable<GetStudentApplicationsResponse>;

  getPosts(
    request: GetPostsRequest,
    metadata?: Metadata,
  ): Observable<GetPostsResponse>;

  getPostById(
    request: GetPostByIdRequest,
    metadata?: Metadata,
  ): Observable<GetPostByIdResponse>;

  batchNotifyStudentAboutRegistrationStatus(
    request: BatchNotifyStudentAboutRegistrationStatusRequest,
    metadata?: Metadata,
  ): Observable<BatchNotifyStudentAboutRegistrationStatusResponse>;

  giveScore(
    request: GiveScoreRequest,
    metadata?: Metadata,
  ): Observable<GiveScoreResponse>;

  selectInternshipPartner(
    request: SelectInternshipPartnerRequest,
    metadata?: Metadata,
  ): Observable<SelectInternshipPartnerResponse>;

  downloadStudentReport(
    request: DownloadStudentReportRequest,
    metadata?: Metadata,
  ): Observable<DownloadStudentReportResponse>;

  getAppliedPartnersByStudentId(
    request: GetAppliedPartnersByStudentIdRequest,
    metadata?: Metadata,
  ): Observable<GetAppliedPartnersByStudentIdResponse>;

  removeLecturerFromTerm(
    request: RemoveLecturerFromTermRequest,
    metadata?: Metadata,
  ): Observable<RemoveLecturerFromTermResponse>;

  signUpForTerm(
    request: SignUpForTermRequest,
    metadata?: Metadata,
  ): Observable<SignUpForTermResponse>;

  cancelTermSignUp(
    request: CancelTermSignUpRequest,
    metadata?: Metadata,
  ): Observable<CancelTermSignUpResponse>;

  batchAddLecturersToTerm(
    request: BatchAddLecturersToTermRequest,
    metadata?: Metadata,
  ): Observable<BatchAddLecturersToTermResponse>;

  batchAddPartnersToTerm(
    request: BatchAddPartnersToTermRequest,
    metadata?: Metadata,
  ): Observable<BatchAddPartnersToTermResponse>;

  getTermListing(
    request: GetTermListingRequest,
    metadata?: Metadata,
  ): Observable<GetTermListingResponse>;

  getLecturerTermListing(
    request: GetLecturerTermListingRequest,
    metadata?: Metadata,
  ): Observable<GetLecturerTermListingResponse>;

  getStudentTermListing(
    request: GetStudentTermListingRequest,
    metadata?: Metadata,
  ): Observable<GetStudentTermListingResponse>;

  getPartnerTermListing(
    request: GetPartnerTermListingRequest,
    metadata?: Metadata,
  ): Observable<GetPartnerTermListingResponse>;

  getLatestTerm(
    request: GetLatestTermRequest,
    metadata?: Metadata,
  ): Observable<GetLatestTermResponse>;

  getTermPartnerListing(
    request: GetTermPartnerListingRequest,
    metadata?: Metadata,
  ): Observable<GetTermPartnerListingResponse>;

  updateTerm(
    request: UpdateTermRequest,
    metadata?: Metadata,
  ): Observable<UpdateTermResponse>;

  checkStudentInTerm(
    request: CheckStudentInTermRequest,
    metadata?: Metadata,
  ): Observable<CheckStudentInTermResponse>;

  checkLecturerInTerm(
    request: CheckLecturerInTermRequest,
    metadata?: Metadata,
  ): Observable<CheckLecturerInTermResponse>;

  checkPartnerInTerm(
    request: CheckPartnerInTermRequest,
    metadata?: Metadata,
  ): Observable<CheckPartnerInTermResponse>;

  getTermStudentDetails(
    request: GetTermStudentDetailsRequest,
    metadata?: Metadata,
  ): Observable<GetTermStudentDetailsResponse>;

  getTermLecturerListing(
    request: GetTermLecturerListingRequest,
    metadata?: Metadata,
  ): Observable<GetTermLecturerListingResponse>;

  exportTermStudents(
    request: ExportTermStudentsRequest,
    metadata?: Metadata,
  ): Observable<ExportTermStudentsResponse>;

  exportTermPartners(
    request: ExportTermPartnersRequest,
    metadata?: Metadata,
  ): Observable<ExportTermPartnersResponse>;

  exportStudentApplications(
    request: ExportStudentApplicationsRequest,
    metadata?: Metadata,
  ): Observable<ExportStudentApplicationsResponse>;

  getStudentPosts(
    request: GetStudentPostsRequest,
    metadata?: Metadata,
  ): Observable<GetStudentPostsResponse>;

  getStudentPostById(
    request: GetStudentPostByIdRequest,
    metadata?: Metadata,
  ): Observable<GetStudentPostByIdResponse>;

  unregisterPartner(
    request: UnregisterPartnerRequest,
    metadata?: Metadata,
  ): Observable<UnregisterPartnerResponse>;

  getMyStudentStats(
    request: GetMyStudentStatsRequest,
    metadata?: Metadata,
  ): Observable<GetMyStudentStatsResponse>;
}

export interface TermServiceController {
  getTerms(
    request: GetTermsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermsResponse>
    | Observable<GetTermsResponse>
    | GetTermsResponse;

  getTermStudents(
    request: GetTermStudentsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermStudentsResponse>
    | Observable<GetTermStudentsResponse>
    | GetTermStudentsResponse;

  getTermLecturers(
    request: GetTermLecturersRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermLecturersResponse>
    | Observable<GetTermLecturersResponse>
    | GetTermLecturersResponse;

  getTermPartners(
    request: GetTermPartnersRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermPartnersResponse>
    | Observable<GetTermPartnersResponse>
    | GetTermPartnersResponse;

  batchChangeTermPartnerStatus(
    request: BatchChangeTermPartnerStatusRequest,
    metadata?: Metadata,
  ):
    | Promise<BatchChangeTermPartnerStatusResponse>
    | Observable<BatchChangeTermPartnerStatusResponse>
    | BatchChangeTermPartnerStatusResponse;

  assignSupervisorToStudents(
    request: AssignSupervisorToStudentsRequest,
    metadata?: Metadata,
  ):
    | Promise<AssignSupervisorToStudentsResponse>
    | Observable<AssignSupervisorToStudentsResponse>
    | AssignSupervisorToStudentsResponse;

  createPost(
    request: CreatePostRequest,
    metadata?: Metadata,
  ):
    | Promise<CreatePostResponse>
    | Observable<CreatePostResponse>
    | CreatePostResponse;

  updatePost(
    request: UpdatePostRequest,
    metadata?: Metadata,
  ):
    | Promise<UpdatePostResponse>
    | Observable<UpdatePostResponse>
    | UpdatePostResponse;

  batchUpdateRegistrationStatus(
    request: BatchUpdateRegistrationStatusRequest,
    metadata?: Metadata,
  ):
    | Promise<BatchUpdateRegistrationStatusResponse>
    | Observable<BatchUpdateRegistrationStatusResponse>
    | BatchUpdateRegistrationStatusResponse;

  applyToPartner(
    request: ApplyToPartnerRequest,
    metadata?: Metadata,
  ):
    | Promise<ApplyToPartnerResponse>
    | Observable<ApplyToPartnerResponse>
    | ApplyToPartnerResponse;

  selfRegisterPartner(
    request: SelfRegisterPartnerRequest,
    metadata?: Metadata,
  ):
    | Promise<SelfRegisterPartnerResponse>
    | Observable<SelfRegisterPartnerResponse>
    | SelfRegisterPartnerResponse;

  checkTermInOrg(
    request: CheckTermInOrgRequest,
    metadata?: Metadata,
  ):
    | Promise<CheckTermInOrgResponse>
    | Observable<CheckTermInOrgResponse>
    | CheckTermInOrgResponse;

  createTerm(
    request: CreateTermRequest,
    metadata?: Metadata,
  ):
    | Promise<CreateTermResponse>
    | Observable<CreateTermResponse>
    | CreateTermResponse;

  getStudentApplications(
    request: GetStudentApplicationsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentApplicationsResponse>
    | Observable<GetStudentApplicationsResponse>
    | GetStudentApplicationsResponse;

  getPosts(
    request: GetPostsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPostsResponse>
    | Observable<GetPostsResponse>
    | GetPostsResponse;

  getPostById(
    request: GetPostByIdRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPostByIdResponse>
    | Observable<GetPostByIdResponse>
    | GetPostByIdResponse;

  batchNotifyStudentAboutRegistrationStatus(
    request: BatchNotifyStudentAboutRegistrationStatusRequest,
    metadata?: Metadata,
  ):
    | Promise<BatchNotifyStudentAboutRegistrationStatusResponse>
    | Observable<BatchNotifyStudentAboutRegistrationStatusResponse>
    | BatchNotifyStudentAboutRegistrationStatusResponse;

  giveScore(
    request: GiveScoreRequest,
    metadata?: Metadata,
  ):
    | Promise<GiveScoreResponse>
    | Observable<GiveScoreResponse>
    | GiveScoreResponse;

  selectInternshipPartner(
    request: SelectInternshipPartnerRequest,
    metadata?: Metadata,
  ):
    | Promise<SelectInternshipPartnerResponse>
    | Observable<SelectInternshipPartnerResponse>
    | SelectInternshipPartnerResponse;

  downloadStudentReport(
    request: DownloadStudentReportRequest,
    metadata?: Metadata,
  ):
    | Promise<DownloadStudentReportResponse>
    | Observable<DownloadStudentReportResponse>
    | DownloadStudentReportResponse;

  getAppliedPartnersByStudentId(
    request: GetAppliedPartnersByStudentIdRequest,
    metadata?: Metadata,
  ):
    | Promise<GetAppliedPartnersByStudentIdResponse>
    | Observable<GetAppliedPartnersByStudentIdResponse>
    | GetAppliedPartnersByStudentIdResponse;

  removeLecturerFromTerm(
    request: RemoveLecturerFromTermRequest,
    metadata?: Metadata,
  ):
    | Promise<RemoveLecturerFromTermResponse>
    | Observable<RemoveLecturerFromTermResponse>
    | RemoveLecturerFromTermResponse;

  signUpForTerm(
    request: SignUpForTermRequest,
    metadata?: Metadata,
  ):
    | Promise<SignUpForTermResponse>
    | Observable<SignUpForTermResponse>
    | SignUpForTermResponse;

  cancelTermSignUp(
    request: CancelTermSignUpRequest,
    metadata?: Metadata,
  ):
    | Promise<CancelTermSignUpResponse>
    | Observable<CancelTermSignUpResponse>
    | CancelTermSignUpResponse;

  batchAddLecturersToTerm(
    request: BatchAddLecturersToTermRequest,
    metadata?: Metadata,
  ):
    | Promise<BatchAddLecturersToTermResponse>
    | Observable<BatchAddLecturersToTermResponse>
    | BatchAddLecturersToTermResponse;

  batchAddPartnersToTerm(
    request: BatchAddPartnersToTermRequest,
    metadata?: Metadata,
  ):
    | Promise<BatchAddPartnersToTermResponse>
    | Observable<BatchAddPartnersToTermResponse>
    | BatchAddPartnersToTermResponse;

  getTermListing(
    request: GetTermListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermListingResponse>
    | Observable<GetTermListingResponse>
    | GetTermListingResponse;

  getLecturerTermListing(
    request: GetLecturerTermListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetLecturerTermListingResponse>
    | Observable<GetLecturerTermListingResponse>
    | GetLecturerTermListingResponse;

  getStudentTermListing(
    request: GetStudentTermListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentTermListingResponse>
    | Observable<GetStudentTermListingResponse>
    | GetStudentTermListingResponse;

  getPartnerTermListing(
    request: GetPartnerTermListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPartnerTermListingResponse>
    | Observable<GetPartnerTermListingResponse>
    | GetPartnerTermListingResponse;

  getLatestTerm(
    request: GetLatestTermRequest,
    metadata?: Metadata,
  ):
    | Promise<GetLatestTermResponse>
    | Observable<GetLatestTermResponse>
    | GetLatestTermResponse;

  getTermPartnerListing(
    request: GetTermPartnerListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermPartnerListingResponse>
    | Observable<GetTermPartnerListingResponse>
    | GetTermPartnerListingResponse;

  updateTerm(
    request: UpdateTermRequest,
    metadata?: Metadata,
  ):
    | Promise<UpdateTermResponse>
    | Observable<UpdateTermResponse>
    | UpdateTermResponse;

  checkStudentInTerm(
    request: CheckStudentInTermRequest,
    metadata?: Metadata,
  ):
    | Promise<CheckStudentInTermResponse>
    | Observable<CheckStudentInTermResponse>
    | CheckStudentInTermResponse;

  checkLecturerInTerm(
    request: CheckLecturerInTermRequest,
    metadata?: Metadata,
  ):
    | Promise<CheckLecturerInTermResponse>
    | Observable<CheckLecturerInTermResponse>
    | CheckLecturerInTermResponse;

  checkPartnerInTerm(
    request: CheckPartnerInTermRequest,
    metadata?: Metadata,
  ):
    | Promise<CheckPartnerInTermResponse>
    | Observable<CheckPartnerInTermResponse>
    | CheckPartnerInTermResponse;

  getTermStudentDetails(
    request: GetTermStudentDetailsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermStudentDetailsResponse>
    | Observable<GetTermStudentDetailsResponse>
    | GetTermStudentDetailsResponse;

  getTermLecturerListing(
    request: GetTermLecturerListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetTermLecturerListingResponse>
    | Observable<GetTermLecturerListingResponse>
    | GetTermLecturerListingResponse;

  exportTermStudents(
    request: ExportTermStudentsRequest,
    metadata?: Metadata,
  ):
    | Promise<ExportTermStudentsResponse>
    | Observable<ExportTermStudentsResponse>
    | ExportTermStudentsResponse;

  exportTermPartners(
    request: ExportTermPartnersRequest,
    metadata?: Metadata,
  ):
    | Promise<ExportTermPartnersResponse>
    | Observable<ExportTermPartnersResponse>
    | ExportTermPartnersResponse;

  exportStudentApplications(
    request: ExportStudentApplicationsRequest,
    metadata?: Metadata,
  ):
    | Promise<ExportStudentApplicationsResponse>
    | Observable<ExportStudentApplicationsResponse>
    | ExportStudentApplicationsResponse;

  getStudentPosts(
    request: GetStudentPostsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentPostsResponse>
    | Observable<GetStudentPostsResponse>
    | GetStudentPostsResponse;

  getStudentPostById(
    request: GetStudentPostByIdRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentPostByIdResponse>
    | Observable<GetStudentPostByIdResponse>
    | GetStudentPostByIdResponse;

  unregisterPartner(
    request: UnregisterPartnerRequest,
    metadata?: Metadata,
  ):
    | Promise<UnregisterPartnerResponse>
    | Observable<UnregisterPartnerResponse>
    | UnregisterPartnerResponse;

  getMyStudentStats(
    request: GetMyStudentStatsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetMyStudentStatsResponse>
    | Observable<GetMyStudentStatsResponse>
    | GetMyStudentStatsResponse;
}

export function TermServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getTerms',
      'getTermStudents',
      'getTermLecturers',
      'getTermPartners',
      'batchChangeTermPartnerStatus',
      'assignSupervisorToStudents',
      'createPost',
      'updatePost',
      'batchUpdateRegistrationStatus',
      'applyToPartner',
      'selfRegisterPartner',
      'checkTermInOrg',
      'createTerm',
      'getStudentApplications',
      'getPosts',
      'getPostById',
      'batchNotifyStudentAboutRegistrationStatus',
      'giveScore',
      'selectInternshipPartner',
      'downloadStudentReport',
      'getAppliedPartnersByStudentId',
      'removeLecturerFromTerm',
      'signUpForTerm',
      'cancelTermSignUp',
      'batchAddLecturersToTerm',
      'batchAddPartnersToTerm',
      'getTermListing',
      'getLecturerTermListing',
      'getStudentTermListing',
      'getPartnerTermListing',
      'getLatestTerm',
      'getTermPartnerListing',
      'updateTerm',
      'checkStudentInTerm',
      'checkLecturerInTerm',
      'checkPartnerInTerm',
      'getTermStudentDetails',
      'getTermLecturerListing',
      'exportTermStudents',
      'exportTermPartners',
      'exportStudentApplications',
      'getStudentPosts',
      'getStudentPostById',
      'unregisterPartner',
      'getMyStudentStats',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('TermService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('TermService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TERM_SERVICE_NAME = 'TermService';

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Timestamp {
  if (o instanceof Date) {
    return toTimestamp(o);
  } else if (typeof o === 'string') {
    return toTimestamp(new Date(o));
  } else {
    return Timestamp.fromJSON(o);
  }
}

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
