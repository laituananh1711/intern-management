/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { Timestamp } from '../google/protobuf/timestamp.pb';
import { Metadata } from '@grpc/grpc-js';

export const protobufPackage = 'user_service';

export interface GetUsersRequest {
  page: number;
  perPage: number;
  sort: string[];
  q: string;
  filter: GetUsersRequestFilter | undefined;
}

export interface GetUsersRequestFilter {
  role: string;
}

export interface GetUsersResponse {
  code: number;
  message: string;
  data: GetUsersResponseData | undefined;
}

export interface GetUsersResponseData {
  users: User[];
  meta: PaginationMetadata | undefined;
}

export interface CreateLecturerUserRequest {
  fullName: string;
  personalEmail: string;
  phoneNumber: string;
  organizationId: number;
  orgEmail: string;
}

export interface CreateLecturerUserResponse {
  code: number;
  message: string;
  data: CreateLecturerUserResponseData | undefined;
}

export interface CreateLecturerUserResponseData {
  lecturerId: number;
  userId: number;
}

export interface CreatePartnerUserRequest {
  name: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  loginEmail: string;
}

export interface CreatePartnerUserResponse {
  code: number;
  message: string;
  data: CreatePartnerUserResponseData | undefined;
}

export interface CreatePartnerUserResponseData {
  partnerId: number;
  userId: number;
}

export interface CreateStudentUserRequest {
  fullName: string;
  loginEmail: string;
  organizationId: number;
  studentIdNumber: string;
  schoolClassId?: number | undefined;
}

export interface CreateStudentUserResponse {
  code: number;
  message: string;
  data: CreateStudentUserResponseData | undefined;
}

export interface CreateStudentUserResponseData {
  studentId: number;
  userId: number;
}

export interface CreateOrgAdminUserRequest {
  organizationId: number;
  loginEmail: string;
}

export interface CreateOrgAdminUserResponse {
  code: number;
  message: string;
  data: CreateOrgAdminUserResponseData | undefined;
}

export interface CreateOrgAdminUserResponseData {
  userId: number;
}

export interface AssignAccountPartnerRequest {
  partnerId: number;
  loginEmail: string;
}

export interface AssignAccountPartnerResponse {
  code: number;
  message: string;
  data: AssignAccountPartnerResponseData | undefined;
}

export interface AssignAccountPartnerResponseData {
  userId: number;
}

export interface BatchCreateStudentUsersRequest {
  organizationId: number;
  students: BatchCreateStudentUsersRequest_Student[];
}

export interface BatchCreateStudentUsersRequest_Student {
  fullName: string;
  orgEmail: string;
  studentIdNumber: string;
  phoneNumber?: string | undefined;
}

export interface BatchCreateStudentUsersResponse {
  code: number;
  message: string;
  data: BatchCreateStudentUsersResponse_Data | undefined;
}

export interface BatchCreateStudentUsersResponse_FailedRecord {
  index: number;
  message: string;
}

export interface BatchCreateStudentUsersResponse_Data {
  failedRecords: BatchCreateStudentUsersResponse_FailedRecord[];
}

export interface UpdatePartnerInfoRequest {
  partnerId: number;
  name?: string | undefined;
  homepageUrl?: string | undefined;
  logoUrl?: string | undefined;
  description?: string | undefined;
  address?: string | undefined;
  phoneNumber?: string | undefined;
}

export interface UpdatePartnerInfoResponse {
  code: number;
  message: string;
}

export interface GetPartnerInfoRequest {
  partnerId: number;
}

export interface GetPartnerInfoResponse {
  code: number;
  message: string;
  data: GetPartnerInfoResponseData | undefined;
}

export interface GetPartnerInfoResponseData {
  id: number;
  name: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  userId: number;
  contacts: PartnerContact[];
}

export interface GetPartnersRequest {
  page: number;
  perPage: number;
  sort: string[];
  q: string;
  include: string[];
}

export interface GetPartnersResponse {
  code: number;
  message: string;
  data: GetPartnersResponseData | undefined;
}

export interface GetPartnersResponseData {
  partners: Partner[];
  meta: PaginationMetadata | undefined;
}

export interface GetPartnersListingRequest {}

export interface GetPartnersListingResponse {
  code: number;
  message: string;
  data: PartnerSuggestion[];
}

export interface UpsertPartnerRequest {
  name: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  partnerId: number;
  email: string;
}

export interface UpsertPartnerResponse {
  code: number;
  message: string;
  data: UpsertPartnerResponseData | undefined;
}

export interface UpsertPartnerResponseData {
  partnerId: number;
}

export interface GetPartnerContactsRequest {
  partnerId: number;
}

export interface GetPartnerContactsResponse {
  code: number;
  message: string;
  data: PartnerContact[];
}

export interface CreatePartnerContactRequest {
  partnerId: number;
  fullName: string;
  phoneNumber: string;
  email: string;
}

export interface CreatePartnerContactResponse {
  code: number;
  message: string;
  data: CreatePartnerContactResponse_Data | undefined;
}

export interface CreatePartnerContactResponse_Data {
  id: number;
}

export interface UpdatePartnerContactRequest {
  id: number;
  fullName: string;
  phoneNumber: string;
  email: string;
  /** add additional filter to update query. */
  filterPartnerId?: number | undefined;
}

export interface UpdatePartnerContactResponse {
  code: number;
  message: string;
  data: UpdatePartnerContactResponse_Data | undefined;
}

export interface UpdatePartnerContactResponse_Data {
  id: number;
}

export interface DeletePartnerContactRequest {
  id: number;
  filterPartnerId?: number | undefined;
}

export interface DeletePartnerContactResponse {
  code: number;
  message: string;
  data: DeletePartnerContactResponse_Data | undefined;
}

export interface DeletePartnerContactResponse_Data {
  id: number;
}

export interface GetLecturerInfoRequest {
  lecturerId: number;
}

export interface GetLecturerInfoResponse {
  code: number;
  message: string;
  data: GetLecturerInfoResponseData | undefined;
}

export interface GetLecturerInfoResponseData {
  id: number;
  fullName: string;
  orgEmail: string;
  personalEmail: string;
  userId: number;
  phoneNumber: string;
}

export interface UpdateLecturerInfoRequest {
  lecturerId: number;
  fullName?: string | undefined;
  personalEmail?: string | undefined;
  phoneNumber?: string | undefined;
}

export interface UpdateLecturerInfoResponse {
  code: number;
  message: string;
}

export interface GetLecturersRequest {
  page: number;
  perPage: number;
  sort: string[];
  q: string;
  filter: GetLecturersRequestFilter | undefined;
  include: string[];
}

export interface GetLecturersRequestFilter {
  organizationId: number;
}

export interface GetLecturersResponse {
  code: number;
  message: string;
  data: GetLecturersResponseData | undefined;
}

export interface GetLecturersResponseData {
  lecturers: Lecturer[];
  meta: PaginationMetadata | undefined;
}

export interface CreateOrgRequest {
  name: string;
  admin: CreateOrgRequest_Admin | undefined;
}

export interface CreateOrgRequest_Admin {
  email: string;
}

export interface CreateOrgResponse {
  code: number;
  message: string;
  data: CreateOrgResponseData | undefined;
}

export interface CreateOrgResponseData {
  organizationId: number;
  adminId?: number | undefined;
}

export interface GetOrgPartnersRequest {
  page: number;
  perPage: number;
  sort: string[];
  q: string;
  filter: GetOrgPartnersRequestFilter | undefined;
  include: string[];
  organizationId: number;
}

export interface GetOrgPartnersRequestFilter {
  isAssociate?: boolean | undefined;
}

export interface GetOrgPartnersResponse {
  code: number;
  message: string;
  data: GetOrgPartnersResponseData | undefined;
}

export interface GetOrgPartnersResponseData {
  partners: OrgPartner[];
  meta: PaginationMetadata | undefined;
}

export interface GetOrgPartnersListingRequest {
  organizationId: number;
}

export interface GetOrgPartnersListingResponse {
  code: number;
  message: string;
  data: OrgPartnerSuggestion[];
}

export interface GetOrgsRequest {
  page: number;
  perPage: number;
  sort: string[];
  q: string;
}

export interface GetOrgsResponse {
  code: number;
  message: string;
  data: GetOrgsResponseData | undefined;
}

export interface GetOrgsResponseData {
  orgs: Organization[];
  meta: PaginationMetadata | undefined;
}

export interface GetOrgListingRequest {}

export interface GetOrgListingResponse {
  code: number;
  message: string;
  data: OrganizationLittleInfo[];
}

export interface AssignAssociateRequest {
  organizationId: number;
  partnerId: number;
  statusExpirationDate: string;
}

export interface AssignAssociateResponse {
  code: number;
  message: string;
}

export interface GetClassesRequest {
  page: number;
  perPage: number;
}

export interface GetClassesResponse {
  code: number;
  message: string;
  data: GetClassesResponseData | undefined;
}

export interface GetClassesResponseData {
  classes: SchoolClass[];
  meta: PaginationMetadata | undefined;
}

export interface GetClassesListingRequest {}

export interface GetClassesListingResponse {
  code: number;
  message: string;
  data: SchoolClassLittleInfo[];
}

export interface UpsertClassRequest {
  id?: number | undefined;
  name: string;
  programName: string;
}

export interface UpsertClassResponse {
  code: number;
  message: string;
  data: UpsertClassResponseData | undefined;
}

export interface UpsertClassResponseData {
  id: number;
}

export interface GetStudentsRequest {
  page: number;
  perPage: number;
  sort: string[];
  q: string;
  filter: GetStudentsRequest_Filter | undefined;
  include: string[];
}

export interface GetStudentsRequest_Filter {
  schoolClassId?: number | undefined;
  organizationId?: number | undefined;
}

export interface GetStudentsResponse {
  code: number;
  message: string;
  data: GetStudentsResponseData | undefined;
}

export interface GetStudentsResponseData {
  meta: PaginationMetadata | undefined;
  students: Student[];
}

export interface GetStudentInfoRequest {
  studentId: number;
}

export interface GetStudentInfoResponse {
  code: number;
  message: string;
  data: GetStudentInfoResponseData | undefined;
}

export interface GetStudentInfoResponseData {
  id: number;
  fullName: string;
  schoolClassId: number;
  orgEmail: string;
  personalEmail: string;
  userId: number;
  phoneNumber: string;
  studentIdNumber: string;
}

export interface UpdateStudentInfoRequest {
  studentId: number;
  fullName?: string | undefined;
  schoolClassId?: number | undefined;
  personalEmail?: string | undefined;
  phoneNumber?: string | undefined;
}

export interface UpdateStudentInfoResponse {
  code: number;
  message: string;
}

export interface DownloadCvRequest {
  studentId: number;
}

export interface DownloadCvResponse {
  code: number;
  message: string;
  data: DownloadCvResponse_Data | undefined;
}

export interface DownloadCvResponse_Data {
  downloadUrl: string;
}

export interface ChangePasswordRequest {
  userId: number;
  currentPassword: string;
  newPassword: string;
}

export interface ChangePasswordResponse {
  code: number;
  message: string;
}

export interface ResetPasswordRequest {
  email: string;
}

export interface ResetPasswordResponse {
  code: number;
  message: string;
}

export interface CompleteResetPasswordRequest {
  requestId: number;
  resetToken: string;
  newPassword: string;
}

export interface CompleteResetPasswordResponse {
  code: number;
  message: string;
}

export interface GetAccessTokenRequest {
  refreshToken: string;
}

export interface GetAccessTokenResponse {
  code: number;
  message: string;
  data: GetAccessTokenResponse_Data | undefined;
}

export interface GetAccessTokenResponse_Data {
  accessToken: string;
}

export interface LoginWithEmailRequest {
  email: string;
  password: string;
}

export interface LoginWithEmailResponse {
  code: number;
  message: string;
  data: LoginWithEmailResponse_Data | undefined;
}

export interface LoginWithEmailResponse_Data {
  accessToken: string;
  refreshToken: string;
  user: AuthUser | undefined;
}

export interface LoginWithGoogleRequest {
  idToken: string;
}

export interface LoginWithGoogleResponse {
  code: number;
  message: string;
  data: LoginWithGoogleResponse_Data | undefined;
}

export interface LoginWithGoogleResponse_Data {
  accessToken: string;
  refreshToken: string;
  user: AuthUser | undefined;
}

export interface GetUserFromTokenRequest {
  accessToken: string;
}

export interface GetUserFromTokenResponse {
  code: number;
  message: string;
  data: AuthUser | undefined;
}

export interface GetStudentDetailsRequest {
  userId: number;
}

export interface GetStudentDetailsResponse {
  code: number;
  message: string;
  data: Student | undefined;
}

export interface GetLecturerDetailsRequest {
  userId: number;
}

export interface GetLecturerDetailsResponse {
  code: number;
  message: string;
  data: Lecturer | undefined;
}

export interface GetPartnerDetailsRequest {
  userId: number;
}

export interface GetPartnerDetailsResponse {
  code: number;
  message: string;
  data: Partner | undefined;
}

export interface GetOrgAdminDetailsRequest {
  userId: number;
}

export interface GetOrgAdminDetailsResponse {
  code: number;
  message: string;
  data: GetOrgAdminDetailsResponse_Data | undefined;
}

export interface GetOrgAdminDetailsResponse_Data {
  organizationId: number;
}

export interface AuthUser {
  id: number;
  avatarUrl: string;
  orgEmail: string;
  roleId: number;
  role: string;
  isEmailVerified: boolean;
}

export interface User {
  id: number;
  avatarUrl: string;
  orgEmail: string;
  roleId: number;
  role: string;
  lastLoginAt: Timestamp | undefined;
  isEmailVerified: boolean;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
}

export interface Partner {
  id: number;
  name: string;
  email: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  /** 0 for null */
  userId: number;
  contacts: PartnerContact[];
  createdAt: Timestamp | undefined;
  phoneNumber: string;
  address: string;
}

export interface OrgPartner {
  id: number;
  name: string;
  email: string;
  homepageUrl: string;
  logoUrl: string;
  description: string;
  userId: number;
  contacts: PartnerContact[];
  createdAt: Timestamp | undefined;
  isAssociate: boolean;
  /** yyyy-mm-dd */
  associationStatusExpiredDate: string;
  phoneNumber: string;
  address: string;
}

export interface PartnerSuggestion {
  id: number;
  name: string;
}

export interface OrgPartnerSuggestion {
  id: number;
  name: string;
  isAssociate: boolean;
}

export interface PartnerContact {
  id: number;
  fullName: string;
  phoneNumber: string;
  partnerId: number;
  email: string;
}

export interface Lecturer {
  id: number;
  fullName: string;
  phoneNumber: string;
  orgEmail: string;
  personalEmail: string;
  userId: number;
  organizationId: number;
  organization: OrganizationLittleInfo | undefined;
}

export interface PaginationMetadata {
  currentPage: number;
  totalPages: number;
  totalItems: number;
  perPage: number;
}

export interface OrganizationLittleInfo {
  id: number;
  name: string;
}

export interface Organization {
  id: number;
  name: string;
  createdAt: Timestamp | undefined;
  admin: Organization_Admin | undefined;
}

export interface Organization_Admin {
  id: number;
  email: string;
}

export interface SchoolClassLittleInfo {
  id: number;
  name: string;
}

export interface SchoolClass {
  id: number;
  name: string;
  programName: string;
  createdAt: Timestamp | undefined;
}

export interface Student {
  id: number;
  fullName: string;
  phoneNumber: string;
  schoolClass: Student_SchoolClass | undefined;
  userId: number;
  orgEmail: string;
  personalEmail: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  organizationId: number;
  resumeUrl: string;
  studentIdNumber: string;
  organization: OrganizationLittleInfo | undefined;
}

export interface Student_SchoolClass {
  id: number;
  name: string;
}

export const USER_SERVICE_PACKAGE_NAME = 'user_service';

function createBaseGetUsersRequest(): GetUsersRequest {
  return { page: 0, perPage: 0, sort: [], q: '', filter: undefined };
}

export const GetUsersRequest = {
  fromJSON(object: any): GetUsersRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      q: isSet(object.q) ? String(object.q) : '',
      filter: isSet(object.filter)
        ? GetUsersRequestFilter.fromJSON(object.filter)
        : undefined,
    };
  },

  toJSON(message: GetUsersRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.q !== undefined && (obj.q = message.q);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetUsersRequestFilter.toJSON(message.filter)
        : undefined);
    return obj;
  },
};

function createBaseGetUsersRequestFilter(): GetUsersRequestFilter {
  return { role: '' };
}

export const GetUsersRequestFilter = {
  fromJSON(object: any): GetUsersRequestFilter {
    return {
      role: isSet(object.role) ? String(object.role) : '',
    };
  },

  toJSON(message: GetUsersRequestFilter): unknown {
    const obj: any = {};
    message.role !== undefined && (obj.role = message.role);
    return obj;
  },
};

function createBaseGetUsersResponse(): GetUsersResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetUsersResponse = {
  fromJSON(object: any): GetUsersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetUsersResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetUsersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetUsersResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetUsersResponseData(): GetUsersResponseData {
  return { users: [], meta: undefined };
}

export const GetUsersResponseData = {
  fromJSON(object: any): GetUsersResponseData {
    return {
      users: Array.isArray(object?.users)
        ? object.users.map((e: any) => User.fromJSON(e))
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetUsersResponseData): unknown {
    const obj: any = {};
    if (message.users) {
      obj.users = message.users.map((e) => (e ? User.toJSON(e) : undefined));
    } else {
      obj.users = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseCreateLecturerUserRequest(): CreateLecturerUserRequest {
  return {
    fullName: '',
    personalEmail: '',
    phoneNumber: '',
    organizationId: 0,
    orgEmail: '',
  };
}

export const CreateLecturerUserRequest = {
  fromJSON(object: any): CreateLecturerUserRequest {
    return {
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
    };
  },

  toJSON(message: CreateLecturerUserRequest): unknown {
    const obj: any = {};
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    return obj;
  },
};

function createBaseCreateLecturerUserResponse(): CreateLecturerUserResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreateLecturerUserResponse = {
  fromJSON(object: any): CreateLecturerUserResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreateLecturerUserResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreateLecturerUserResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreateLecturerUserResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreateLecturerUserResponseData(): CreateLecturerUserResponseData {
  return { lecturerId: 0, userId: 0 };
}

export const CreateLecturerUserResponseData = {
  fromJSON(object: any): CreateLecturerUserResponseData {
    return {
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: CreateLecturerUserResponseData): unknown {
    const obj: any = {};
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseCreatePartnerUserRequest(): CreatePartnerUserRequest {
  return {
    name: '',
    homepageUrl: '',
    logoUrl: '',
    description: '',
    loginEmail: '',
  };
}

export const CreatePartnerUserRequest = {
  fromJSON(object: any): CreatePartnerUserRequest {
    return {
      name: isSet(object.name) ? String(object.name) : '',
      homepageUrl: isSet(object.homepageUrl) ? String(object.homepageUrl) : '',
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : '',
      description: isSet(object.description) ? String(object.description) : '',
      loginEmail: isSet(object.loginEmail) ? String(object.loginEmail) : '',
    };
  },

  toJSON(message: CreatePartnerUserRequest): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.description !== undefined &&
      (obj.description = message.description);
    message.loginEmail !== undefined && (obj.loginEmail = message.loginEmail);
    return obj;
  },
};

function createBaseCreatePartnerUserResponse(): CreatePartnerUserResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreatePartnerUserResponse = {
  fromJSON(object: any): CreatePartnerUserResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreatePartnerUserResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreatePartnerUserResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreatePartnerUserResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreatePartnerUserResponseData(): CreatePartnerUserResponseData {
  return { partnerId: 0, userId: 0 };
}

export const CreatePartnerUserResponseData = {
  fromJSON(object: any): CreatePartnerUserResponseData {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: CreatePartnerUserResponseData): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseCreateStudentUserRequest(): CreateStudentUserRequest {
  return {
    fullName: '',
    loginEmail: '',
    organizationId: 0,
    studentIdNumber: '',
    schoolClassId: undefined,
  };
}

export const CreateStudentUserRequest = {
  fromJSON(object: any): CreateStudentUserRequest {
    return {
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      loginEmail: isSet(object.loginEmail) ? String(object.loginEmail) : '',
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
      schoolClassId: isSet(object.schoolClassId)
        ? Number(object.schoolClassId)
        : undefined,
    };
  },

  toJSON(message: CreateStudentUserRequest): unknown {
    const obj: any = {};
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.loginEmail !== undefined && (obj.loginEmail = message.loginEmail);
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    message.schoolClassId !== undefined &&
      (obj.schoolClassId = Math.round(message.schoolClassId));
    return obj;
  },
};

function createBaseCreateStudentUserResponse(): CreateStudentUserResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreateStudentUserResponse = {
  fromJSON(object: any): CreateStudentUserResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreateStudentUserResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreateStudentUserResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreateStudentUserResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreateStudentUserResponseData(): CreateStudentUserResponseData {
  return { studentId: 0, userId: 0 };
}

export const CreateStudentUserResponseData = {
  fromJSON(object: any): CreateStudentUserResponseData {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: CreateStudentUserResponseData): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseCreateOrgAdminUserRequest(): CreateOrgAdminUserRequest {
  return { organizationId: 0, loginEmail: '' };
}

export const CreateOrgAdminUserRequest = {
  fromJSON(object: any): CreateOrgAdminUserRequest {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      loginEmail: isSet(object.loginEmail) ? String(object.loginEmail) : '',
    };
  },

  toJSON(message: CreateOrgAdminUserRequest): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.loginEmail !== undefined && (obj.loginEmail = message.loginEmail);
    return obj;
  },
};

function createBaseCreateOrgAdminUserResponse(): CreateOrgAdminUserResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreateOrgAdminUserResponse = {
  fromJSON(object: any): CreateOrgAdminUserResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreateOrgAdminUserResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreateOrgAdminUserResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreateOrgAdminUserResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreateOrgAdminUserResponseData(): CreateOrgAdminUserResponseData {
  return { userId: 0 };
}

export const CreateOrgAdminUserResponseData = {
  fromJSON(object: any): CreateOrgAdminUserResponseData {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: CreateOrgAdminUserResponseData): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseAssignAccountPartnerRequest(): AssignAccountPartnerRequest {
  return { partnerId: 0, loginEmail: '' };
}

export const AssignAccountPartnerRequest = {
  fromJSON(object: any): AssignAccountPartnerRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      loginEmail: isSet(object.loginEmail) ? String(object.loginEmail) : '',
    };
  },

  toJSON(message: AssignAccountPartnerRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.loginEmail !== undefined && (obj.loginEmail = message.loginEmail);
    return obj;
  },
};

function createBaseAssignAccountPartnerResponse(): AssignAccountPartnerResponse {
  return { code: 0, message: '', data: undefined };
}

export const AssignAccountPartnerResponse = {
  fromJSON(object: any): AssignAccountPartnerResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? AssignAccountPartnerResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: AssignAccountPartnerResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? AssignAccountPartnerResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseAssignAccountPartnerResponseData(): AssignAccountPartnerResponseData {
  return { userId: 0 };
}

export const AssignAccountPartnerResponseData = {
  fromJSON(object: any): AssignAccountPartnerResponseData {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: AssignAccountPartnerResponseData): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseBatchCreateStudentUsersRequest(): BatchCreateStudentUsersRequest {
  return { organizationId: 0, students: [] };
}

export const BatchCreateStudentUsersRequest = {
  fromJSON(object: any): BatchCreateStudentUsersRequest {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      students: Array.isArray(object?.students)
        ? object.students.map((e: any) =>
            BatchCreateStudentUsersRequest_Student.fromJSON(e),
          )
        : [],
    };
  },

  toJSON(message: BatchCreateStudentUsersRequest): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    if (message.students) {
      obj.students = message.students.map((e) =>
        e ? BatchCreateStudentUsersRequest_Student.toJSON(e) : undefined,
      );
    } else {
      obj.students = [];
    }
    return obj;
  },
};

function createBaseBatchCreateStudentUsersRequest_Student(): BatchCreateStudentUsersRequest_Student {
  return {
    fullName: '',
    orgEmail: '',
    studentIdNumber: '',
    phoneNumber: undefined,
  };
}

export const BatchCreateStudentUsersRequest_Student = {
  fromJSON(object: any): BatchCreateStudentUsersRequest_Student {
    return {
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
      phoneNumber: isSet(object.phoneNumber)
        ? String(object.phoneNumber)
        : undefined,
    };
  },

  toJSON(message: BatchCreateStudentUsersRequest_Student): unknown {
    const obj: any = {};
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    return obj;
  },
};

function createBaseBatchCreateStudentUsersResponse(): BatchCreateStudentUsersResponse {
  return { code: 0, message: '', data: undefined };
}

export const BatchCreateStudentUsersResponse = {
  fromJSON(object: any): BatchCreateStudentUsersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? BatchCreateStudentUsersResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: BatchCreateStudentUsersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? BatchCreateStudentUsersResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseBatchCreateStudentUsersResponse_FailedRecord(): BatchCreateStudentUsersResponse_FailedRecord {
  return { index: 0, message: '' };
}

export const BatchCreateStudentUsersResponse_FailedRecord = {
  fromJSON(object: any): BatchCreateStudentUsersResponse_FailedRecord {
    return {
      index: isSet(object.index) ? Number(object.index) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: BatchCreateStudentUsersResponse_FailedRecord): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = Math.round(message.index));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseBatchCreateStudentUsersResponse_Data(): BatchCreateStudentUsersResponse_Data {
  return { failedRecords: [] };
}

export const BatchCreateStudentUsersResponse_Data = {
  fromJSON(object: any): BatchCreateStudentUsersResponse_Data {
    return {
      failedRecords: Array.isArray(object?.failedRecords)
        ? object.failedRecords.map((e: any) =>
            BatchCreateStudentUsersResponse_FailedRecord.fromJSON(e),
          )
        : [],
    };
  },

  toJSON(message: BatchCreateStudentUsersResponse_Data): unknown {
    const obj: any = {};
    if (message.failedRecords) {
      obj.failedRecords = message.failedRecords.map((e) =>
        e ? BatchCreateStudentUsersResponse_FailedRecord.toJSON(e) : undefined,
      );
    } else {
      obj.failedRecords = [];
    }
    return obj;
  },
};

function createBaseUpdatePartnerInfoRequest(): UpdatePartnerInfoRequest {
  return {
    partnerId: 0,
    name: undefined,
    homepageUrl: undefined,
    logoUrl: undefined,
    description: undefined,
    address: undefined,
    phoneNumber: undefined,
  };
}

export const UpdatePartnerInfoRequest = {
  fromJSON(object: any): UpdatePartnerInfoRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      name: isSet(object.name) ? String(object.name) : undefined,
      homepageUrl: isSet(object.homepageUrl)
        ? String(object.homepageUrl)
        : undefined,
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : undefined,
      description: isSet(object.description)
        ? String(object.description)
        : undefined,
      address: isSet(object.address) ? String(object.address) : undefined,
      phoneNumber: isSet(object.phoneNumber)
        ? String(object.phoneNumber)
        : undefined,
    };
  },

  toJSON(message: UpdatePartnerInfoRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.name !== undefined && (obj.name = message.name);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.description !== undefined &&
      (obj.description = message.description);
    message.address !== undefined && (obj.address = message.address);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    return obj;
  },
};

function createBaseUpdatePartnerInfoResponse(): UpdatePartnerInfoResponse {
  return { code: 0, message: '' };
}

export const UpdatePartnerInfoResponse = {
  fromJSON(object: any): UpdatePartnerInfoResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: UpdatePartnerInfoResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseGetPartnerInfoRequest(): GetPartnerInfoRequest {
  return { partnerId: 0 };
}

export const GetPartnerInfoRequest = {
  fromJSON(object: any): GetPartnerInfoRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
    };
  },

  toJSON(message: GetPartnerInfoRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseGetPartnerInfoResponse(): GetPartnerInfoResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetPartnerInfoResponse = {
  fromJSON(object: any): GetPartnerInfoResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetPartnerInfoResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetPartnerInfoResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetPartnerInfoResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetPartnerInfoResponseData(): GetPartnerInfoResponseData {
  return {
    id: 0,
    name: '',
    homepageUrl: '',
    logoUrl: '',
    description: '',
    userId: 0,
    contacts: [],
  };
}

export const GetPartnerInfoResponseData = {
  fromJSON(object: any): GetPartnerInfoResponseData {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      homepageUrl: isSet(object.homepageUrl) ? String(object.homepageUrl) : '',
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : '',
      description: isSet(object.description) ? String(object.description) : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      contacts: Array.isArray(object?.contacts)
        ? object.contacts.map((e: any) => PartnerContact.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetPartnerInfoResponseData): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.description !== undefined &&
      (obj.description = message.description);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    if (message.contacts) {
      obj.contacts = message.contacts.map((e) =>
        e ? PartnerContact.toJSON(e) : undefined,
      );
    } else {
      obj.contacts = [];
    }
    return obj;
  },
};

function createBaseGetPartnersRequest(): GetPartnersRequest {
  return { page: 0, perPage: 0, sort: [], q: '', include: [] };
}

export const GetPartnersRequest = {
  fromJSON(object: any): GetPartnersRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      q: isSet(object.q) ? String(object.q) : '',
      include: Array.isArray(object?.include)
        ? object.include.map((e: any) => String(e))
        : [],
    };
  },

  toJSON(message: GetPartnersRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.q !== undefined && (obj.q = message.q);
    if (message.include) {
      obj.include = message.include.map((e) => e);
    } else {
      obj.include = [];
    }
    return obj;
  },
};

function createBaseGetPartnersResponse(): GetPartnersResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetPartnersResponse = {
  fromJSON(object: any): GetPartnersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetPartnersResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetPartnersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetPartnersResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetPartnersResponseData(): GetPartnersResponseData {
  return { partners: [], meta: undefined };
}

export const GetPartnersResponseData = {
  fromJSON(object: any): GetPartnersResponseData {
    return {
      partners: Array.isArray(object?.partners)
        ? object.partners.map((e: any) => Partner.fromJSON(e))
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetPartnersResponseData): unknown {
    const obj: any = {};
    if (message.partners) {
      obj.partners = message.partners.map((e) =>
        e ? Partner.toJSON(e) : undefined,
      );
    } else {
      obj.partners = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseGetPartnersListingRequest(): GetPartnersListingRequest {
  return {};
}

export const GetPartnersListingRequest = {
  fromJSON(_: any): GetPartnersListingRequest {
    return {};
  },

  toJSON(_: GetPartnersListingRequest): unknown {
    const obj: any = {};
    return obj;
  },
};

function createBaseGetPartnersListingResponse(): GetPartnersListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetPartnersListingResponse = {
  fromJSON(object: any): GetPartnersListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => PartnerSuggestion.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetPartnersListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? PartnerSuggestion.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseUpsertPartnerRequest(): UpsertPartnerRequest {
  return {
    name: '',
    homepageUrl: '',
    logoUrl: '',
    description: '',
    partnerId: 0,
    email: '',
  };
}

export const UpsertPartnerRequest = {
  fromJSON(object: any): UpsertPartnerRequest {
    return {
      name: isSet(object.name) ? String(object.name) : '',
      homepageUrl: isSet(object.homepageUrl) ? String(object.homepageUrl) : '',
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : '',
      description: isSet(object.description) ? String(object.description) : '',
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      email: isSet(object.email) ? String(object.email) : '',
    };
  },

  toJSON(message: UpsertPartnerRequest): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.description !== undefined &&
      (obj.description = message.description);
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },
};

function createBaseUpsertPartnerResponse(): UpsertPartnerResponse {
  return { code: 0, message: '', data: undefined };
}

export const UpsertPartnerResponse = {
  fromJSON(object: any): UpsertPartnerResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? UpsertPartnerResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: UpsertPartnerResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? UpsertPartnerResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseUpsertPartnerResponseData(): UpsertPartnerResponseData {
  return { partnerId: 0 };
}

export const UpsertPartnerResponseData = {
  fromJSON(object: any): UpsertPartnerResponseData {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
    };
  },

  toJSON(message: UpsertPartnerResponseData): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseGetPartnerContactsRequest(): GetPartnerContactsRequest {
  return { partnerId: 0 };
}

export const GetPartnerContactsRequest = {
  fromJSON(object: any): GetPartnerContactsRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
    };
  },

  toJSON(message: GetPartnerContactsRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    return obj;
  },
};

function createBaseGetPartnerContactsResponse(): GetPartnerContactsResponse {
  return { code: 0, message: '', data: [] };
}

export const GetPartnerContactsResponse = {
  fromJSON(object: any): GetPartnerContactsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => PartnerContact.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetPartnerContactsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? PartnerContact.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseCreatePartnerContactRequest(): CreatePartnerContactRequest {
  return { partnerId: 0, fullName: '', phoneNumber: '', email: '' };
}

export const CreatePartnerContactRequest = {
  fromJSON(object: any): CreatePartnerContactRequest {
    return {
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      email: isSet(object.email) ? String(object.email) : '',
    };
  },

  toJSON(message: CreatePartnerContactRequest): unknown {
    const obj: any = {};
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },
};

function createBaseCreatePartnerContactResponse(): CreatePartnerContactResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreatePartnerContactResponse = {
  fromJSON(object: any): CreatePartnerContactResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreatePartnerContactResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreatePartnerContactResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreatePartnerContactResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreatePartnerContactResponse_Data(): CreatePartnerContactResponse_Data {
  return { id: 0 };
}

export const CreatePartnerContactResponse_Data = {
  fromJSON(object: any): CreatePartnerContactResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: CreatePartnerContactResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseUpdatePartnerContactRequest(): UpdatePartnerContactRequest {
  return {
    id: 0,
    fullName: '',
    phoneNumber: '',
    email: '',
    filterPartnerId: undefined,
  };
}

export const UpdatePartnerContactRequest = {
  fromJSON(object: any): UpdatePartnerContactRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      email: isSet(object.email) ? String(object.email) : '',
      filterPartnerId: isSet(object.filterPartnerId)
        ? Number(object.filterPartnerId)
        : undefined,
    };
  },

  toJSON(message: UpdatePartnerContactRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.email !== undefined && (obj.email = message.email);
    message.filterPartnerId !== undefined &&
      (obj.filterPartnerId = Math.round(message.filterPartnerId));
    return obj;
  },
};

function createBaseUpdatePartnerContactResponse(): UpdatePartnerContactResponse {
  return { code: 0, message: '', data: undefined };
}

export const UpdatePartnerContactResponse = {
  fromJSON(object: any): UpdatePartnerContactResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? UpdatePartnerContactResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: UpdatePartnerContactResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? UpdatePartnerContactResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseUpdatePartnerContactResponse_Data(): UpdatePartnerContactResponse_Data {
  return { id: 0 };
}

export const UpdatePartnerContactResponse_Data = {
  fromJSON(object: any): UpdatePartnerContactResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: UpdatePartnerContactResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseDeletePartnerContactRequest(): DeletePartnerContactRequest {
  return { id: 0, filterPartnerId: undefined };
}

export const DeletePartnerContactRequest = {
  fromJSON(object: any): DeletePartnerContactRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      filterPartnerId: isSet(object.filterPartnerId)
        ? Number(object.filterPartnerId)
        : undefined,
    };
  },

  toJSON(message: DeletePartnerContactRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.filterPartnerId !== undefined &&
      (obj.filterPartnerId = Math.round(message.filterPartnerId));
    return obj;
  },
};

function createBaseDeletePartnerContactResponse(): DeletePartnerContactResponse {
  return { code: 0, message: '', data: undefined };
}

export const DeletePartnerContactResponse = {
  fromJSON(object: any): DeletePartnerContactResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? DeletePartnerContactResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: DeletePartnerContactResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? DeletePartnerContactResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseDeletePartnerContactResponse_Data(): DeletePartnerContactResponse_Data {
  return { id: 0 };
}

export const DeletePartnerContactResponse_Data = {
  fromJSON(object: any): DeletePartnerContactResponse_Data {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: DeletePartnerContactResponse_Data): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseGetLecturerInfoRequest(): GetLecturerInfoRequest {
  return { lecturerId: 0 };
}

export const GetLecturerInfoRequest = {
  fromJSON(object: any): GetLecturerInfoRequest {
    return {
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
    };
  },

  toJSON(message: GetLecturerInfoRequest): unknown {
    const obj: any = {};
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    return obj;
  },
};

function createBaseGetLecturerInfoResponse(): GetLecturerInfoResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetLecturerInfoResponse = {
  fromJSON(object: any): GetLecturerInfoResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetLecturerInfoResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetLecturerInfoResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetLecturerInfoResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetLecturerInfoResponseData(): GetLecturerInfoResponseData {
  return {
    id: 0,
    fullName: '',
    orgEmail: '',
    personalEmail: '',
    userId: 0,
    phoneNumber: '',
  };
}

export const GetLecturerInfoResponseData = {
  fromJSON(object: any): GetLecturerInfoResponseData {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
    };
  },

  toJSON(message: GetLecturerInfoResponseData): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    return obj;
  },
};

function createBaseUpdateLecturerInfoRequest(): UpdateLecturerInfoRequest {
  return {
    lecturerId: 0,
    fullName: undefined,
    personalEmail: undefined,
    phoneNumber: undefined,
  };
}

export const UpdateLecturerInfoRequest = {
  fromJSON(object: any): UpdateLecturerInfoRequest {
    return {
      lecturerId: isSet(object.lecturerId) ? Number(object.lecturerId) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : undefined,
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : undefined,
      phoneNumber: isSet(object.phoneNumber)
        ? String(object.phoneNumber)
        : undefined,
    };
  },

  toJSON(message: UpdateLecturerInfoRequest): unknown {
    const obj: any = {};
    message.lecturerId !== undefined &&
      (obj.lecturerId = Math.round(message.lecturerId));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    return obj;
  },
};

function createBaseUpdateLecturerInfoResponse(): UpdateLecturerInfoResponse {
  return { code: 0, message: '' };
}

export const UpdateLecturerInfoResponse = {
  fromJSON(object: any): UpdateLecturerInfoResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: UpdateLecturerInfoResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseGetLecturersRequest(): GetLecturersRequest {
  return {
    page: 0,
    perPage: 0,
    sort: [],
    q: '',
    filter: undefined,
    include: [],
  };
}

export const GetLecturersRequest = {
  fromJSON(object: any): GetLecturersRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      q: isSet(object.q) ? String(object.q) : '',
      filter: isSet(object.filter)
        ? GetLecturersRequestFilter.fromJSON(object.filter)
        : undefined,
      include: Array.isArray(object?.include)
        ? object.include.map((e: any) => String(e))
        : [],
    };
  },

  toJSON(message: GetLecturersRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.q !== undefined && (obj.q = message.q);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetLecturersRequestFilter.toJSON(message.filter)
        : undefined);
    if (message.include) {
      obj.include = message.include.map((e) => e);
    } else {
      obj.include = [];
    }
    return obj;
  },
};

function createBaseGetLecturersRequestFilter(): GetLecturersRequestFilter {
  return { organizationId: 0 };
}

export const GetLecturersRequestFilter = {
  fromJSON(object: any): GetLecturersRequestFilter {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetLecturersRequestFilter): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetLecturersResponse(): GetLecturersResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetLecturersResponse = {
  fromJSON(object: any): GetLecturersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetLecturersResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetLecturersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetLecturersResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetLecturersResponseData(): GetLecturersResponseData {
  return { lecturers: [], meta: undefined };
}

export const GetLecturersResponseData = {
  fromJSON(object: any): GetLecturersResponseData {
    return {
      lecturers: Array.isArray(object?.lecturers)
        ? object.lecturers.map((e: any) => Lecturer.fromJSON(e))
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetLecturersResponseData): unknown {
    const obj: any = {};
    if (message.lecturers) {
      obj.lecturers = message.lecturers.map((e) =>
        e ? Lecturer.toJSON(e) : undefined,
      );
    } else {
      obj.lecturers = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseCreateOrgRequest(): CreateOrgRequest {
  return { name: '', admin: undefined };
}

export const CreateOrgRequest = {
  fromJSON(object: any): CreateOrgRequest {
    return {
      name: isSet(object.name) ? String(object.name) : '',
      admin: isSet(object.admin)
        ? CreateOrgRequest_Admin.fromJSON(object.admin)
        : undefined,
    };
  },

  toJSON(message: CreateOrgRequest): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.admin !== undefined &&
      (obj.admin = message.admin
        ? CreateOrgRequest_Admin.toJSON(message.admin)
        : undefined);
    return obj;
  },
};

function createBaseCreateOrgRequest_Admin(): CreateOrgRequest_Admin {
  return { email: '' };
}

export const CreateOrgRequest_Admin = {
  fromJSON(object: any): CreateOrgRequest_Admin {
    return {
      email: isSet(object.email) ? String(object.email) : '',
    };
  },

  toJSON(message: CreateOrgRequest_Admin): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },
};

function createBaseCreateOrgResponse(): CreateOrgResponse {
  return { code: 0, message: '', data: undefined };
}

export const CreateOrgResponse = {
  fromJSON(object: any): CreateOrgResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? CreateOrgResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: CreateOrgResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? CreateOrgResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseCreateOrgResponseData(): CreateOrgResponseData {
  return { organizationId: 0, adminId: undefined };
}

export const CreateOrgResponseData = {
  fromJSON(object: any): CreateOrgResponseData {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      adminId: isSet(object.adminId) ? Number(object.adminId) : undefined,
    };
  },

  toJSON(message: CreateOrgResponseData): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.adminId !== undefined &&
      (obj.adminId = Math.round(message.adminId));
    return obj;
  },
};

function createBaseGetOrgPartnersRequest(): GetOrgPartnersRequest {
  return {
    page: 0,
    perPage: 0,
    sort: [],
    q: '',
    filter: undefined,
    include: [],
    organizationId: 0,
  };
}

export const GetOrgPartnersRequest = {
  fromJSON(object: any): GetOrgPartnersRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      q: isSet(object.q) ? String(object.q) : '',
      filter: isSet(object.filter)
        ? GetOrgPartnersRequestFilter.fromJSON(object.filter)
        : undefined,
      include: Array.isArray(object?.include)
        ? object.include.map((e: any) => String(e))
        : [],
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetOrgPartnersRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.q !== undefined && (obj.q = message.q);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetOrgPartnersRequestFilter.toJSON(message.filter)
        : undefined);
    if (message.include) {
      obj.include = message.include.map((e) => e);
    } else {
      obj.include = [];
    }
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetOrgPartnersRequestFilter(): GetOrgPartnersRequestFilter {
  return { isAssociate: undefined };
}

export const GetOrgPartnersRequestFilter = {
  fromJSON(object: any): GetOrgPartnersRequestFilter {
    return {
      isAssociate: isSet(object.isAssociate)
        ? Boolean(object.isAssociate)
        : undefined,
    };
  },

  toJSON(message: GetOrgPartnersRequestFilter): unknown {
    const obj: any = {};
    message.isAssociate !== undefined &&
      (obj.isAssociate = message.isAssociate);
    return obj;
  },
};

function createBaseGetOrgPartnersResponse(): GetOrgPartnersResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetOrgPartnersResponse = {
  fromJSON(object: any): GetOrgPartnersResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetOrgPartnersResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetOrgPartnersResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetOrgPartnersResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetOrgPartnersResponseData(): GetOrgPartnersResponseData {
  return { partners: [], meta: undefined };
}

export const GetOrgPartnersResponseData = {
  fromJSON(object: any): GetOrgPartnersResponseData {
    return {
      partners: Array.isArray(object?.partners)
        ? object.partners.map((e: any) => OrgPartner.fromJSON(e))
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetOrgPartnersResponseData): unknown {
    const obj: any = {};
    if (message.partners) {
      obj.partners = message.partners.map((e) =>
        e ? OrgPartner.toJSON(e) : undefined,
      );
    } else {
      obj.partners = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseGetOrgPartnersListingRequest(): GetOrgPartnersListingRequest {
  return { organizationId: 0 };
}

export const GetOrgPartnersListingRequest = {
  fromJSON(object: any): GetOrgPartnersListingRequest {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetOrgPartnersListingRequest): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetOrgPartnersListingResponse(): GetOrgPartnersListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetOrgPartnersListingResponse = {
  fromJSON(object: any): GetOrgPartnersListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => OrgPartnerSuggestion.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetOrgPartnersListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? OrgPartnerSuggestion.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseGetOrgsRequest(): GetOrgsRequest {
  return { page: 0, perPage: 0, sort: [], q: '' };
}

export const GetOrgsRequest = {
  fromJSON(object: any): GetOrgsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      q: isSet(object.q) ? String(object.q) : '',
    };
  },

  toJSON(message: GetOrgsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.q !== undefined && (obj.q = message.q);
    return obj;
  },
};

function createBaseGetOrgsResponse(): GetOrgsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetOrgsResponse = {
  fromJSON(object: any): GetOrgsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetOrgsResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetOrgsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetOrgsResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetOrgsResponseData(): GetOrgsResponseData {
  return { orgs: [], meta: undefined };
}

export const GetOrgsResponseData = {
  fromJSON(object: any): GetOrgsResponseData {
    return {
      orgs: Array.isArray(object?.orgs)
        ? object.orgs.map((e: any) => Organization.fromJSON(e))
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetOrgsResponseData): unknown {
    const obj: any = {};
    if (message.orgs) {
      obj.orgs = message.orgs.map((e) =>
        e ? Organization.toJSON(e) : undefined,
      );
    } else {
      obj.orgs = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseGetOrgListingRequest(): GetOrgListingRequest {
  return {};
}

export const GetOrgListingRequest = {
  fromJSON(_: any): GetOrgListingRequest {
    return {};
  },

  toJSON(_: GetOrgListingRequest): unknown {
    const obj: any = {};
    return obj;
  },
};

function createBaseGetOrgListingResponse(): GetOrgListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetOrgListingResponse = {
  fromJSON(object: any): GetOrgListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => OrganizationLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetOrgListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? OrganizationLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseAssignAssociateRequest(): AssignAssociateRequest {
  return { organizationId: 0, partnerId: 0, statusExpirationDate: '' };
}

export const AssignAssociateRequest = {
  fromJSON(object: any): AssignAssociateRequest {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      statusExpirationDate: isSet(object.statusExpirationDate)
        ? String(object.statusExpirationDate)
        : '',
    };
  },

  toJSON(message: AssignAssociateRequest): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.statusExpirationDate !== undefined &&
      (obj.statusExpirationDate = message.statusExpirationDate);
    return obj;
  },
};

function createBaseAssignAssociateResponse(): AssignAssociateResponse {
  return { code: 0, message: '' };
}

export const AssignAssociateResponse = {
  fromJSON(object: any): AssignAssociateResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: AssignAssociateResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseGetClassesRequest(): GetClassesRequest {
  return { page: 0, perPage: 0 };
}

export const GetClassesRequest = {
  fromJSON(object: any): GetClassesRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
    };
  },

  toJSON(message: GetClassesRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    return obj;
  },
};

function createBaseGetClassesResponse(): GetClassesResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetClassesResponse = {
  fromJSON(object: any): GetClassesResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetClassesResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetClassesResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetClassesResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetClassesResponseData(): GetClassesResponseData {
  return { classes: [], meta: undefined };
}

export const GetClassesResponseData = {
  fromJSON(object: any): GetClassesResponseData {
    return {
      classes: Array.isArray(object?.classes)
        ? object.classes.map((e: any) => SchoolClass.fromJSON(e))
        : [],
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
    };
  },

  toJSON(message: GetClassesResponseData): unknown {
    const obj: any = {};
    if (message.classes) {
      obj.classes = message.classes.map((e) =>
        e ? SchoolClass.toJSON(e) : undefined,
      );
    } else {
      obj.classes = [];
    }
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    return obj;
  },
};

function createBaseGetClassesListingRequest(): GetClassesListingRequest {
  return {};
}

export const GetClassesListingRequest = {
  fromJSON(_: any): GetClassesListingRequest {
    return {};
  },

  toJSON(_: GetClassesListingRequest): unknown {
    const obj: any = {};
    return obj;
  },
};

function createBaseGetClassesListingResponse(): GetClassesListingResponse {
  return { code: 0, message: '', data: [] };
}

export const GetClassesListingResponse = {
  fromJSON(object: any): GetClassesListingResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: Array.isArray(object?.data)
        ? object.data.map((e: any) => SchoolClassLittleInfo.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetClassesListingResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    if (message.data) {
      obj.data = message.data.map((e) =>
        e ? SchoolClassLittleInfo.toJSON(e) : undefined,
      );
    } else {
      obj.data = [];
    }
    return obj;
  },
};

function createBaseUpsertClassRequest(): UpsertClassRequest {
  return { id: undefined, name: '', programName: '' };
}

export const UpsertClassRequest = {
  fromJSON(object: any): UpsertClassRequest {
    return {
      id: isSet(object.id) ? Number(object.id) : undefined,
      name: isSet(object.name) ? String(object.name) : '',
      programName: isSet(object.programName) ? String(object.programName) : '',
    };
  },

  toJSON(message: UpsertClassRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.programName !== undefined &&
      (obj.programName = message.programName);
    return obj;
  },
};

function createBaseUpsertClassResponse(): UpsertClassResponse {
  return { code: 0, message: '', data: undefined };
}

export const UpsertClassResponse = {
  fromJSON(object: any): UpsertClassResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? UpsertClassResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: UpsertClassResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? UpsertClassResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseUpsertClassResponseData(): UpsertClassResponseData {
  return { id: 0 };
}

export const UpsertClassResponseData = {
  fromJSON(object: any): UpsertClassResponseData {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: UpsertClassResponseData): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },
};

function createBaseGetStudentsRequest(): GetStudentsRequest {
  return {
    page: 0,
    perPage: 0,
    sort: [],
    q: '',
    filter: undefined,
    include: [],
  };
}

export const GetStudentsRequest = {
  fromJSON(object: any): GetStudentsRequest {
    return {
      page: isSet(object.page) ? Number(object.page) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
      sort: Array.isArray(object?.sort)
        ? object.sort.map((e: any) => String(e))
        : [],
      q: isSet(object.q) ? String(object.q) : '',
      filter: isSet(object.filter)
        ? GetStudentsRequest_Filter.fromJSON(object.filter)
        : undefined,
      include: Array.isArray(object?.include)
        ? object.include.map((e: any) => String(e))
        : [],
    };
  },

  toJSON(message: GetStudentsRequest): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    if (message.sort) {
      obj.sort = message.sort.map((e) => e);
    } else {
      obj.sort = [];
    }
    message.q !== undefined && (obj.q = message.q);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? GetStudentsRequest_Filter.toJSON(message.filter)
        : undefined);
    if (message.include) {
      obj.include = message.include.map((e) => e);
    } else {
      obj.include = [];
    }
    return obj;
  },
};

function createBaseGetStudentsRequest_Filter(): GetStudentsRequest_Filter {
  return { schoolClassId: undefined, organizationId: undefined };
}

export const GetStudentsRequest_Filter = {
  fromJSON(object: any): GetStudentsRequest_Filter {
    return {
      schoolClassId: isSet(object.schoolClassId)
        ? Number(object.schoolClassId)
        : undefined,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : undefined,
    };
  },

  toJSON(message: GetStudentsRequest_Filter): unknown {
    const obj: any = {};
    message.schoolClassId !== undefined &&
      (obj.schoolClassId = Math.round(message.schoolClassId));
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseGetStudentsResponse(): GetStudentsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetStudentsResponse = {
  fromJSON(object: any): GetStudentsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetStudentsResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetStudentsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetStudentsResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentsResponseData(): GetStudentsResponseData {
  return { meta: undefined, students: [] };
}

export const GetStudentsResponseData = {
  fromJSON(object: any): GetStudentsResponseData {
    return {
      meta: isSet(object.meta)
        ? PaginationMetadata.fromJSON(object.meta)
        : undefined,
      students: Array.isArray(object?.students)
        ? object.students.map((e: any) => Student.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GetStudentsResponseData): unknown {
    const obj: any = {};
    message.meta !== undefined &&
      (obj.meta = message.meta
        ? PaginationMetadata.toJSON(message.meta)
        : undefined);
    if (message.students) {
      obj.students = message.students.map((e) =>
        e ? Student.toJSON(e) : undefined,
      );
    } else {
      obj.students = [];
    }
    return obj;
  },
};

function createBaseGetStudentInfoRequest(): GetStudentInfoRequest {
  return { studentId: 0 };
}

export const GetStudentInfoRequest = {
  fromJSON(object: any): GetStudentInfoRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: GetStudentInfoRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseGetStudentInfoResponse(): GetStudentInfoResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetStudentInfoResponse = {
  fromJSON(object: any): GetStudentInfoResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetStudentInfoResponseData.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetStudentInfoResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetStudentInfoResponseData.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetStudentInfoResponseData(): GetStudentInfoResponseData {
  return {
    id: 0,
    fullName: '',
    schoolClassId: 0,
    orgEmail: '',
    personalEmail: '',
    userId: 0,
    phoneNumber: '',
    studentIdNumber: '',
  };
}

export const GetStudentInfoResponseData = {
  fromJSON(object: any): GetStudentInfoResponseData {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      schoolClassId: isSet(object.schoolClassId)
        ? Number(object.schoolClassId)
        : 0,
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
    };
  },

  toJSON(message: GetStudentInfoResponseData): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.schoolClassId !== undefined &&
      (obj.schoolClassId = Math.round(message.schoolClassId));
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    return obj;
  },
};

function createBaseUpdateStudentInfoRequest(): UpdateStudentInfoRequest {
  return {
    studentId: 0,
    fullName: undefined,
    schoolClassId: undefined,
    personalEmail: undefined,
    phoneNumber: undefined,
  };
}

export const UpdateStudentInfoRequest = {
  fromJSON(object: any): UpdateStudentInfoRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : undefined,
      schoolClassId: isSet(object.schoolClassId)
        ? Number(object.schoolClassId)
        : undefined,
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : undefined,
      phoneNumber: isSet(object.phoneNumber)
        ? String(object.phoneNumber)
        : undefined,
    };
  },

  toJSON(message: UpdateStudentInfoRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.schoolClassId !== undefined &&
      (obj.schoolClassId = Math.round(message.schoolClassId));
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    return obj;
  },
};

function createBaseUpdateStudentInfoResponse(): UpdateStudentInfoResponse {
  return { code: 0, message: '' };
}

export const UpdateStudentInfoResponse = {
  fromJSON(object: any): UpdateStudentInfoResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: UpdateStudentInfoResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseDownloadCvRequest(): DownloadCvRequest {
  return { studentId: 0 };
}

export const DownloadCvRequest = {
  fromJSON(object: any): DownloadCvRequest {
    return {
      studentId: isSet(object.studentId) ? Number(object.studentId) : 0,
    };
  },

  toJSON(message: DownloadCvRequest): unknown {
    const obj: any = {};
    message.studentId !== undefined &&
      (obj.studentId = Math.round(message.studentId));
    return obj;
  },
};

function createBaseDownloadCvResponse(): DownloadCvResponse {
  return { code: 0, message: '', data: undefined };
}

export const DownloadCvResponse = {
  fromJSON(object: any): DownloadCvResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? DownloadCvResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: DownloadCvResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? DownloadCvResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseDownloadCvResponse_Data(): DownloadCvResponse_Data {
  return { downloadUrl: '' };
}

export const DownloadCvResponse_Data = {
  fromJSON(object: any): DownloadCvResponse_Data {
    return {
      downloadUrl: isSet(object.downloadUrl) ? String(object.downloadUrl) : '',
    };
  },

  toJSON(message: DownloadCvResponse_Data): unknown {
    const obj: any = {};
    message.downloadUrl !== undefined &&
      (obj.downloadUrl = message.downloadUrl);
    return obj;
  },
};

function createBaseChangePasswordRequest(): ChangePasswordRequest {
  return { userId: 0, currentPassword: '', newPassword: '' };
}

export const ChangePasswordRequest = {
  fromJSON(object: any): ChangePasswordRequest {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      currentPassword: isSet(object.currentPassword)
        ? String(object.currentPassword)
        : '',
      newPassword: isSet(object.newPassword) ? String(object.newPassword) : '',
    };
  },

  toJSON(message: ChangePasswordRequest): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.currentPassword !== undefined &&
      (obj.currentPassword = message.currentPassword);
    message.newPassword !== undefined &&
      (obj.newPassword = message.newPassword);
    return obj;
  },
};

function createBaseChangePasswordResponse(): ChangePasswordResponse {
  return { code: 0, message: '' };
}

export const ChangePasswordResponse = {
  fromJSON(object: any): ChangePasswordResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: ChangePasswordResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseResetPasswordRequest(): ResetPasswordRequest {
  return { email: '' };
}

export const ResetPasswordRequest = {
  fromJSON(object: any): ResetPasswordRequest {
    return {
      email: isSet(object.email) ? String(object.email) : '',
    };
  },

  toJSON(message: ResetPasswordRequest): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },
};

function createBaseResetPasswordResponse(): ResetPasswordResponse {
  return { code: 0, message: '' };
}

export const ResetPasswordResponse = {
  fromJSON(object: any): ResetPasswordResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: ResetPasswordResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseCompleteResetPasswordRequest(): CompleteResetPasswordRequest {
  return { requestId: 0, resetToken: '', newPassword: '' };
}

export const CompleteResetPasswordRequest = {
  fromJSON(object: any): CompleteResetPasswordRequest {
    return {
      requestId: isSet(object.requestId) ? Number(object.requestId) : 0,
      resetToken: isSet(object.resetToken) ? String(object.resetToken) : '',
      newPassword: isSet(object.newPassword) ? String(object.newPassword) : '',
    };
  },

  toJSON(message: CompleteResetPasswordRequest): unknown {
    const obj: any = {};
    message.requestId !== undefined &&
      (obj.requestId = Math.round(message.requestId));
    message.resetToken !== undefined && (obj.resetToken = message.resetToken);
    message.newPassword !== undefined &&
      (obj.newPassword = message.newPassword);
    return obj;
  },
};

function createBaseCompleteResetPasswordResponse(): CompleteResetPasswordResponse {
  return { code: 0, message: '' };
}

export const CompleteResetPasswordResponse = {
  fromJSON(object: any): CompleteResetPasswordResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
    };
  },

  toJSON(message: CompleteResetPasswordResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },
};

function createBaseGetAccessTokenRequest(): GetAccessTokenRequest {
  return { refreshToken: '' };
}

export const GetAccessTokenRequest = {
  fromJSON(object: any): GetAccessTokenRequest {
    return {
      refreshToken: isSet(object.refreshToken)
        ? String(object.refreshToken)
        : '',
    };
  },

  toJSON(message: GetAccessTokenRequest): unknown {
    const obj: any = {};
    message.refreshToken !== undefined &&
      (obj.refreshToken = message.refreshToken);
    return obj;
  },
};

function createBaseGetAccessTokenResponse(): GetAccessTokenResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetAccessTokenResponse = {
  fromJSON(object: any): GetAccessTokenResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetAccessTokenResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetAccessTokenResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetAccessTokenResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetAccessTokenResponse_Data(): GetAccessTokenResponse_Data {
  return { accessToken: '' };
}

export const GetAccessTokenResponse_Data = {
  fromJSON(object: any): GetAccessTokenResponse_Data {
    return {
      accessToken: isSet(object.accessToken) ? String(object.accessToken) : '',
    };
  },

  toJSON(message: GetAccessTokenResponse_Data): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    return obj;
  },
};

function createBaseLoginWithEmailRequest(): LoginWithEmailRequest {
  return { email: '', password: '' };
}

export const LoginWithEmailRequest = {
  fromJSON(object: any): LoginWithEmailRequest {
    return {
      email: isSet(object.email) ? String(object.email) : '',
      password: isSet(object.password) ? String(object.password) : '',
    };
  },

  toJSON(message: LoginWithEmailRequest): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    message.password !== undefined && (obj.password = message.password);
    return obj;
  },
};

function createBaseLoginWithEmailResponse(): LoginWithEmailResponse {
  return { code: 0, message: '', data: undefined };
}

export const LoginWithEmailResponse = {
  fromJSON(object: any): LoginWithEmailResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? LoginWithEmailResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: LoginWithEmailResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? LoginWithEmailResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseLoginWithEmailResponse_Data(): LoginWithEmailResponse_Data {
  return { accessToken: '', refreshToken: '', user: undefined };
}

export const LoginWithEmailResponse_Data = {
  fromJSON(object: any): LoginWithEmailResponse_Data {
    return {
      accessToken: isSet(object.accessToken) ? String(object.accessToken) : '',
      refreshToken: isSet(object.refreshToken)
        ? String(object.refreshToken)
        : '',
      user: isSet(object.user) ? AuthUser.fromJSON(object.user) : undefined,
    };
  },

  toJSON(message: LoginWithEmailResponse_Data): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    message.refreshToken !== undefined &&
      (obj.refreshToken = message.refreshToken);
    message.user !== undefined &&
      (obj.user = message.user ? AuthUser.toJSON(message.user) : undefined);
    return obj;
  },
};

function createBaseLoginWithGoogleRequest(): LoginWithGoogleRequest {
  return { idToken: '' };
}

export const LoginWithGoogleRequest = {
  fromJSON(object: any): LoginWithGoogleRequest {
    return {
      idToken: isSet(object.idToken) ? String(object.idToken) : '',
    };
  },

  toJSON(message: LoginWithGoogleRequest): unknown {
    const obj: any = {};
    message.idToken !== undefined && (obj.idToken = message.idToken);
    return obj;
  },
};

function createBaseLoginWithGoogleResponse(): LoginWithGoogleResponse {
  return { code: 0, message: '', data: undefined };
}

export const LoginWithGoogleResponse = {
  fromJSON(object: any): LoginWithGoogleResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? LoginWithGoogleResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: LoginWithGoogleResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? LoginWithGoogleResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseLoginWithGoogleResponse_Data(): LoginWithGoogleResponse_Data {
  return { accessToken: '', refreshToken: '', user: undefined };
}

export const LoginWithGoogleResponse_Data = {
  fromJSON(object: any): LoginWithGoogleResponse_Data {
    return {
      accessToken: isSet(object.accessToken) ? String(object.accessToken) : '',
      refreshToken: isSet(object.refreshToken)
        ? String(object.refreshToken)
        : '',
      user: isSet(object.user) ? AuthUser.fromJSON(object.user) : undefined,
    };
  },

  toJSON(message: LoginWithGoogleResponse_Data): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    message.refreshToken !== undefined &&
      (obj.refreshToken = message.refreshToken);
    message.user !== undefined &&
      (obj.user = message.user ? AuthUser.toJSON(message.user) : undefined);
    return obj;
  },
};

function createBaseGetUserFromTokenRequest(): GetUserFromTokenRequest {
  return { accessToken: '' };
}

export const GetUserFromTokenRequest = {
  fromJSON(object: any): GetUserFromTokenRequest {
    return {
      accessToken: isSet(object.accessToken) ? String(object.accessToken) : '',
    };
  },

  toJSON(message: GetUserFromTokenRequest): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    return obj;
  },
};

function createBaseGetUserFromTokenResponse(): GetUserFromTokenResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetUserFromTokenResponse = {
  fromJSON(object: any): GetUserFromTokenResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data) ? AuthUser.fromJSON(object.data) : undefined,
    };
  },

  toJSON(message: GetUserFromTokenResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data ? AuthUser.toJSON(message.data) : undefined);
    return obj;
  },
};

function createBaseGetStudentDetailsRequest(): GetStudentDetailsRequest {
  return { userId: 0 };
}

export const GetStudentDetailsRequest = {
  fromJSON(object: any): GetStudentDetailsRequest {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: GetStudentDetailsRequest): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseGetStudentDetailsResponse(): GetStudentDetailsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetStudentDetailsResponse = {
  fromJSON(object: any): GetStudentDetailsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data) ? Student.fromJSON(object.data) : undefined,
    };
  },

  toJSON(message: GetStudentDetailsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data ? Student.toJSON(message.data) : undefined);
    return obj;
  },
};

function createBaseGetLecturerDetailsRequest(): GetLecturerDetailsRequest {
  return { userId: 0 };
}

export const GetLecturerDetailsRequest = {
  fromJSON(object: any): GetLecturerDetailsRequest {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: GetLecturerDetailsRequest): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseGetLecturerDetailsResponse(): GetLecturerDetailsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetLecturerDetailsResponse = {
  fromJSON(object: any): GetLecturerDetailsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data) ? Lecturer.fromJSON(object.data) : undefined,
    };
  },

  toJSON(message: GetLecturerDetailsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data ? Lecturer.toJSON(message.data) : undefined);
    return obj;
  },
};

function createBaseGetPartnerDetailsRequest(): GetPartnerDetailsRequest {
  return { userId: 0 };
}

export const GetPartnerDetailsRequest = {
  fromJSON(object: any): GetPartnerDetailsRequest {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: GetPartnerDetailsRequest): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseGetPartnerDetailsResponse(): GetPartnerDetailsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetPartnerDetailsResponse = {
  fromJSON(object: any): GetPartnerDetailsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data) ? Partner.fromJSON(object.data) : undefined,
    };
  },

  toJSON(message: GetPartnerDetailsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data ? Partner.toJSON(message.data) : undefined);
    return obj;
  },
};

function createBaseGetOrgAdminDetailsRequest(): GetOrgAdminDetailsRequest {
  return { userId: 0 };
}

export const GetOrgAdminDetailsRequest = {
  fromJSON(object: any): GetOrgAdminDetailsRequest {
    return {
      userId: isSet(object.userId) ? Number(object.userId) : 0,
    };
  },

  toJSON(message: GetOrgAdminDetailsRequest): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    return obj;
  },
};

function createBaseGetOrgAdminDetailsResponse(): GetOrgAdminDetailsResponse {
  return { code: 0, message: '', data: undefined };
}

export const GetOrgAdminDetailsResponse = {
  fromJSON(object: any): GetOrgAdminDetailsResponse {
    return {
      code: isSet(object.code) ? Number(object.code) : 0,
      message: isSet(object.message) ? String(object.message) : '',
      data: isSet(object.data)
        ? GetOrgAdminDetailsResponse_Data.fromJSON(object.data)
        : undefined,
    };
  },

  toJSON(message: GetOrgAdminDetailsResponse): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = Math.round(message.code));
    message.message !== undefined && (obj.message = message.message);
    message.data !== undefined &&
      (obj.data = message.data
        ? GetOrgAdminDetailsResponse_Data.toJSON(message.data)
        : undefined);
    return obj;
  },
};

function createBaseGetOrgAdminDetailsResponse_Data(): GetOrgAdminDetailsResponse_Data {
  return { organizationId: 0 };
}

export const GetOrgAdminDetailsResponse_Data = {
  fromJSON(object: any): GetOrgAdminDetailsResponse_Data {
    return {
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
    };
  },

  toJSON(message: GetOrgAdminDetailsResponse_Data): unknown {
    const obj: any = {};
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    return obj;
  },
};

function createBaseAuthUser(): AuthUser {
  return {
    id: 0,
    avatarUrl: '',
    orgEmail: '',
    roleId: 0,
    role: '',
    isEmailVerified: false,
  };
}

export const AuthUser = {
  fromJSON(object: any): AuthUser {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      avatarUrl: isSet(object.avatarUrl) ? String(object.avatarUrl) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      roleId: isSet(object.roleId) ? Number(object.roleId) : 0,
      role: isSet(object.role) ? String(object.role) : '',
      isEmailVerified: isSet(object.isEmailVerified)
        ? Boolean(object.isEmailVerified)
        : false,
    };
  },

  toJSON(message: AuthUser): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.avatarUrl !== undefined && (obj.avatarUrl = message.avatarUrl);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.roleId !== undefined && (obj.roleId = Math.round(message.roleId));
    message.role !== undefined && (obj.role = message.role);
    message.isEmailVerified !== undefined &&
      (obj.isEmailVerified = message.isEmailVerified);
    return obj;
  },
};

function createBaseUser(): User {
  return {
    id: 0,
    avatarUrl: '',
    orgEmail: '',
    roleId: 0,
    role: '',
    lastLoginAt: undefined,
    isEmailVerified: false,
    createdAt: undefined,
    updatedAt: undefined,
  };
}

export const User = {
  fromJSON(object: any): User {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      avatarUrl: isSet(object.avatarUrl) ? String(object.avatarUrl) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      roleId: isSet(object.roleId) ? Number(object.roleId) : 0,
      role: isSet(object.role) ? String(object.role) : '',
      lastLoginAt: isSet(object.lastLoginAt)
        ? fromJsonTimestamp(object.lastLoginAt)
        : undefined,
      isEmailVerified: isSet(object.isEmailVerified)
        ? Boolean(object.isEmailVerified)
        : false,
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      updatedAt: isSet(object.updatedAt)
        ? fromJsonTimestamp(object.updatedAt)
        : undefined,
    };
  },

  toJSON(message: User): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.avatarUrl !== undefined && (obj.avatarUrl = message.avatarUrl);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.roleId !== undefined && (obj.roleId = Math.round(message.roleId));
    message.role !== undefined && (obj.role = message.role);
    message.lastLoginAt !== undefined &&
      (obj.lastLoginAt = fromTimestamp(message.lastLoginAt).toISOString());
    message.isEmailVerified !== undefined &&
      (obj.isEmailVerified = message.isEmailVerified);
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.updatedAt !== undefined &&
      (obj.updatedAt = fromTimestamp(message.updatedAt).toISOString());
    return obj;
  },
};

function createBasePartner(): Partner {
  return {
    id: 0,
    name: '',
    email: '',
    homepageUrl: '',
    logoUrl: '',
    description: '',
    userId: 0,
    contacts: [],
    createdAt: undefined,
    phoneNumber: '',
    address: '',
  };
}

export const Partner = {
  fromJSON(object: any): Partner {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      email: isSet(object.email) ? String(object.email) : '',
      homepageUrl: isSet(object.homepageUrl) ? String(object.homepageUrl) : '',
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : '',
      description: isSet(object.description) ? String(object.description) : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      contacts: Array.isArray(object?.contacts)
        ? object.contacts.map((e: any) => PartnerContact.fromJSON(e))
        : [],
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      address: isSet(object.address) ? String(object.address) : '',
    };
  },

  toJSON(message: Partner): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.email !== undefined && (obj.email = message.email);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.description !== undefined &&
      (obj.description = message.description);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    if (message.contacts) {
      obj.contacts = message.contacts.map((e) =>
        e ? PartnerContact.toJSON(e) : undefined,
      );
    } else {
      obj.contacts = [];
    }
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.address !== undefined && (obj.address = message.address);
    return obj;
  },
};

function createBaseOrgPartner(): OrgPartner {
  return {
    id: 0,
    name: '',
    email: '',
    homepageUrl: '',
    logoUrl: '',
    description: '',
    userId: 0,
    contacts: [],
    createdAt: undefined,
    isAssociate: false,
    associationStatusExpiredDate: '',
    phoneNumber: '',
    address: '',
  };
}

export const OrgPartner = {
  fromJSON(object: any): OrgPartner {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      email: isSet(object.email) ? String(object.email) : '',
      homepageUrl: isSet(object.homepageUrl) ? String(object.homepageUrl) : '',
      logoUrl: isSet(object.logoUrl) ? String(object.logoUrl) : '',
      description: isSet(object.description) ? String(object.description) : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      contacts: Array.isArray(object?.contacts)
        ? object.contacts.map((e: any) => PartnerContact.fromJSON(e))
        : [],
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      isAssociate: isSet(object.isAssociate)
        ? Boolean(object.isAssociate)
        : false,
      associationStatusExpiredDate: isSet(object.associationStatusExpiredDate)
        ? String(object.associationStatusExpiredDate)
        : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      address: isSet(object.address) ? String(object.address) : '',
    };
  },

  toJSON(message: OrgPartner): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.email !== undefined && (obj.email = message.email);
    message.homepageUrl !== undefined &&
      (obj.homepageUrl = message.homepageUrl);
    message.logoUrl !== undefined && (obj.logoUrl = message.logoUrl);
    message.description !== undefined &&
      (obj.description = message.description);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    if (message.contacts) {
      obj.contacts = message.contacts.map((e) =>
        e ? PartnerContact.toJSON(e) : undefined,
      );
    } else {
      obj.contacts = [];
    }
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.isAssociate !== undefined &&
      (obj.isAssociate = message.isAssociate);
    message.associationStatusExpiredDate !== undefined &&
      (obj.associationStatusExpiredDate = message.associationStatusExpiredDate);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.address !== undefined && (obj.address = message.address);
    return obj;
  },
};

function createBasePartnerSuggestion(): PartnerSuggestion {
  return { id: 0, name: '' };
}

export const PartnerSuggestion = {
  fromJSON(object: any): PartnerSuggestion {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: PartnerSuggestion): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseOrgPartnerSuggestion(): OrgPartnerSuggestion {
  return { id: 0, name: '', isAssociate: false };
}

export const OrgPartnerSuggestion = {
  fromJSON(object: any): OrgPartnerSuggestion {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      isAssociate: isSet(object.isAssociate)
        ? Boolean(object.isAssociate)
        : false,
    };
  },

  toJSON(message: OrgPartnerSuggestion): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.isAssociate !== undefined &&
      (obj.isAssociate = message.isAssociate);
    return obj;
  },
};

function createBasePartnerContact(): PartnerContact {
  return { id: 0, fullName: '', phoneNumber: '', partnerId: 0, email: '' };
}

export const PartnerContact = {
  fromJSON(object: any): PartnerContact {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      partnerId: isSet(object.partnerId) ? Number(object.partnerId) : 0,
      email: isSet(object.email) ? String(object.email) : '',
    };
  },

  toJSON(message: PartnerContact): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.partnerId !== undefined &&
      (obj.partnerId = Math.round(message.partnerId));
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },
};

function createBaseLecturer(): Lecturer {
  return {
    id: 0,
    fullName: '',
    phoneNumber: '',
    orgEmail: '',
    personalEmail: '',
    userId: 0,
    organizationId: 0,
    organization: undefined,
  };
}

export const Lecturer = {
  fromJSON(object: any): Lecturer {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      organization: isSet(object.organization)
        ? OrganizationLittleInfo.fromJSON(object.organization)
        : undefined,
    };
  },

  toJSON(message: Lecturer): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.organization !== undefined &&
      (obj.organization = message.organization
        ? OrganizationLittleInfo.toJSON(message.organization)
        : undefined);
    return obj;
  },
};

function createBasePaginationMetadata(): PaginationMetadata {
  return { currentPage: 0, totalPages: 0, totalItems: 0, perPage: 0 };
}

export const PaginationMetadata = {
  fromJSON(object: any): PaginationMetadata {
    return {
      currentPage: isSet(object.currentPage) ? Number(object.currentPage) : 0,
      totalPages: isSet(object.totalPages) ? Number(object.totalPages) : 0,
      totalItems: isSet(object.totalItems) ? Number(object.totalItems) : 0,
      perPage: isSet(object.perPage) ? Number(object.perPage) : 0,
    };
  },

  toJSON(message: PaginationMetadata): unknown {
    const obj: any = {};
    message.currentPage !== undefined &&
      (obj.currentPage = Math.round(message.currentPage));
    message.totalPages !== undefined &&
      (obj.totalPages = Math.round(message.totalPages));
    message.totalItems !== undefined &&
      (obj.totalItems = Math.round(message.totalItems));
    message.perPage !== undefined &&
      (obj.perPage = Math.round(message.perPage));
    return obj;
  },
};

function createBaseOrganizationLittleInfo(): OrganizationLittleInfo {
  return { id: 0, name: '' };
}

export const OrganizationLittleInfo = {
  fromJSON(object: any): OrganizationLittleInfo {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: OrganizationLittleInfo): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseOrganization(): Organization {
  return { id: 0, name: '', createdAt: undefined, admin: undefined };
}

export const Organization = {
  fromJSON(object: any): Organization {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      admin: isSet(object.admin)
        ? Organization_Admin.fromJSON(object.admin)
        : undefined,
    };
  },

  toJSON(message: Organization): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.admin !== undefined &&
      (obj.admin = message.admin
        ? Organization_Admin.toJSON(message.admin)
        : undefined);
    return obj;
  },
};

function createBaseOrganization_Admin(): Organization_Admin {
  return { id: 0, email: '' };
}

export const Organization_Admin = {
  fromJSON(object: any): Organization_Admin {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      email: isSet(object.email) ? String(object.email) : '',
    };
  },

  toJSON(message: Organization_Admin): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },
};

function createBaseSchoolClassLittleInfo(): SchoolClassLittleInfo {
  return { id: 0, name: '' };
}

export const SchoolClassLittleInfo = {
  fromJSON(object: any): SchoolClassLittleInfo {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: SchoolClassLittleInfo): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

function createBaseSchoolClass(): SchoolClass {
  return { id: 0, name: '', programName: '', createdAt: undefined };
}

export const SchoolClass = {
  fromJSON(object: any): SchoolClass {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
      programName: isSet(object.programName) ? String(object.programName) : '',
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
    };
  },

  toJSON(message: SchoolClass): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    message.programName !== undefined &&
      (obj.programName = message.programName);
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    return obj;
  },
};

function createBaseStudent(): Student {
  return {
    id: 0,
    fullName: '',
    phoneNumber: '',
    schoolClass: undefined,
    userId: 0,
    orgEmail: '',
    personalEmail: '',
    createdAt: undefined,
    updatedAt: undefined,
    organizationId: 0,
    resumeUrl: '',
    studentIdNumber: '',
    organization: undefined,
  };
}

export const Student = {
  fromJSON(object: any): Student {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      fullName: isSet(object.fullName) ? String(object.fullName) : '',
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : '',
      schoolClass: isSet(object.schoolClass)
        ? Student_SchoolClass.fromJSON(object.schoolClass)
        : undefined,
      userId: isSet(object.userId) ? Number(object.userId) : 0,
      orgEmail: isSet(object.orgEmail) ? String(object.orgEmail) : '',
      personalEmail: isSet(object.personalEmail)
        ? String(object.personalEmail)
        : '',
      createdAt: isSet(object.createdAt)
        ? fromJsonTimestamp(object.createdAt)
        : undefined,
      updatedAt: isSet(object.updatedAt)
        ? fromJsonTimestamp(object.updatedAt)
        : undefined,
      organizationId: isSet(object.organizationId)
        ? Number(object.organizationId)
        : 0,
      resumeUrl: isSet(object.resumeUrl) ? String(object.resumeUrl) : '',
      studentIdNumber: isSet(object.studentIdNumber)
        ? String(object.studentIdNumber)
        : '',
      organization: isSet(object.organization)
        ? OrganizationLittleInfo.fromJSON(object.organization)
        : undefined,
    };
  },

  toJSON(message: Student): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.fullName !== undefined && (obj.fullName = message.fullName);
    message.phoneNumber !== undefined &&
      (obj.phoneNumber = message.phoneNumber);
    message.schoolClass !== undefined &&
      (obj.schoolClass = message.schoolClass
        ? Student_SchoolClass.toJSON(message.schoolClass)
        : undefined);
    message.userId !== undefined && (obj.userId = Math.round(message.userId));
    message.orgEmail !== undefined && (obj.orgEmail = message.orgEmail);
    message.personalEmail !== undefined &&
      (obj.personalEmail = message.personalEmail);
    message.createdAt !== undefined &&
      (obj.createdAt = fromTimestamp(message.createdAt).toISOString());
    message.updatedAt !== undefined &&
      (obj.updatedAt = fromTimestamp(message.updatedAt).toISOString());
    message.organizationId !== undefined &&
      (obj.organizationId = Math.round(message.organizationId));
    message.resumeUrl !== undefined && (obj.resumeUrl = message.resumeUrl);
    message.studentIdNumber !== undefined &&
      (obj.studentIdNumber = message.studentIdNumber);
    message.organization !== undefined &&
      (obj.organization = message.organization
        ? OrganizationLittleInfo.toJSON(message.organization)
        : undefined);
    return obj;
  },
};

function createBaseStudent_SchoolClass(): Student_SchoolClass {
  return { id: 0, name: '' };
}

export const Student_SchoolClass = {
  fromJSON(object: any): Student_SchoolClass {
    return {
      id: isSet(object.id) ? Number(object.id) : 0,
      name: isSet(object.name) ? String(object.name) : '',
    };
  },

  toJSON(message: Student_SchoolClass): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },
};

export interface UserServiceClient {
  getUsers(
    request: GetUsersRequest,
    metadata?: Metadata,
  ): Observable<GetUsersResponse>;

  createLecturerUser(
    request: CreateLecturerUserRequest,
    metadata?: Metadata,
  ): Observable<CreateLecturerUserResponse>;

  createPartnerUser(
    request: CreatePartnerUserRequest,
    metadata?: Metadata,
  ): Observable<CreatePartnerUserResponse>;

  createStudentUser(
    request: CreateStudentUserRequest,
    metadata?: Metadata,
  ): Observable<CreateStudentUserResponse>;

  createOrgAdminUser(
    request: CreateOrgAdminUserRequest,
    metadata?: Metadata,
  ): Observable<CreateOrgAdminUserResponse>;

  assignAccountPartner(
    request: AssignAccountPartnerRequest,
    metadata?: Metadata,
  ): Observable<AssignAccountPartnerResponse>;

  batchCreateStudentUsers(
    request: BatchCreateStudentUsersRequest,
    metadata?: Metadata,
  ): Observable<BatchCreateStudentUsersResponse>;
}

export interface UserServiceController {
  getUsers(
    request: GetUsersRequest,
    metadata?: Metadata,
  ):
    | Promise<GetUsersResponse>
    | Observable<GetUsersResponse>
    | GetUsersResponse;

  createLecturerUser(
    request: CreateLecturerUserRequest,
    metadata?: Metadata,
  ):
    | Promise<CreateLecturerUserResponse>
    | Observable<CreateLecturerUserResponse>
    | CreateLecturerUserResponse;

  createPartnerUser(
    request: CreatePartnerUserRequest,
    metadata?: Metadata,
  ):
    | Promise<CreatePartnerUserResponse>
    | Observable<CreatePartnerUserResponse>
    | CreatePartnerUserResponse;

  createStudentUser(
    request: CreateStudentUserRequest,
    metadata?: Metadata,
  ):
    | Promise<CreateStudentUserResponse>
    | Observable<CreateStudentUserResponse>
    | CreateStudentUserResponse;

  createOrgAdminUser(
    request: CreateOrgAdminUserRequest,
    metadata?: Metadata,
  ):
    | Promise<CreateOrgAdminUserResponse>
    | Observable<CreateOrgAdminUserResponse>
    | CreateOrgAdminUserResponse;

  assignAccountPartner(
    request: AssignAccountPartnerRequest,
    metadata?: Metadata,
  ):
    | Promise<AssignAccountPartnerResponse>
    | Observable<AssignAccountPartnerResponse>
    | AssignAccountPartnerResponse;

  batchCreateStudentUsers(
    request: BatchCreateStudentUsersRequest,
    metadata?: Metadata,
  ):
    | Promise<BatchCreateStudentUsersResponse>
    | Observable<BatchCreateStudentUsersResponse>
    | BatchCreateStudentUsersResponse;
}

export function UserServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getUsers',
      'createLecturerUser',
      'createPartnerUser',
      'createStudentUser',
      'createOrgAdminUser',
      'assignAccountPartner',
      'batchCreateStudentUsers',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('UserService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('UserService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const USER_SERVICE_NAME = 'UserService';

export interface PartnerServiceClient {
  getPartners(
    request: GetPartnersRequest,
    metadata?: Metadata,
  ): Observable<GetPartnersResponse>;

  getPartnersListing(
    request: GetPartnersListingRequest,
    metadata?: Metadata,
  ): Observable<GetPartnersListingResponse>;

  upsertPartner(
    request: UpsertPartnerRequest,
    metadata?: Metadata,
  ): Observable<UpsertPartnerResponse>;

  getPartnerInfo(
    request: GetPartnerInfoRequest,
    metadata?: Metadata,
  ): Observable<GetPartnerInfoResponse>;

  updatePartnerInfo(
    request: UpdatePartnerInfoRequest,
    metadata?: Metadata,
  ): Observable<UpdatePartnerInfoResponse>;

  getPartnerContacts(
    request: GetPartnerContactsRequest,
    metadata?: Metadata,
  ): Observable<GetPartnerContactsResponse>;

  createPartnerContact(
    request: CreatePartnerContactRequest,
    metadata?: Metadata,
  ): Observable<CreatePartnerContactResponse>;

  updatePartnerContact(
    request: UpdatePartnerContactRequest,
    metadata?: Metadata,
  ): Observable<UpdatePartnerContactResponse>;

  deletePartnerContact(
    request: DeletePartnerContactRequest,
    metadata?: Metadata,
  ): Observable<DeletePartnerContactResponse>;
}

export interface PartnerServiceController {
  getPartners(
    request: GetPartnersRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPartnersResponse>
    | Observable<GetPartnersResponse>
    | GetPartnersResponse;

  getPartnersListing(
    request: GetPartnersListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPartnersListingResponse>
    | Observable<GetPartnersListingResponse>
    | GetPartnersListingResponse;

  upsertPartner(
    request: UpsertPartnerRequest,
    metadata?: Metadata,
  ):
    | Promise<UpsertPartnerResponse>
    | Observable<UpsertPartnerResponse>
    | UpsertPartnerResponse;

  getPartnerInfo(
    request: GetPartnerInfoRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPartnerInfoResponse>
    | Observable<GetPartnerInfoResponse>
    | GetPartnerInfoResponse;

  updatePartnerInfo(
    request: UpdatePartnerInfoRequest,
    metadata?: Metadata,
  ):
    | Promise<UpdatePartnerInfoResponse>
    | Observable<UpdatePartnerInfoResponse>
    | UpdatePartnerInfoResponse;

  getPartnerContacts(
    request: GetPartnerContactsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPartnerContactsResponse>
    | Observable<GetPartnerContactsResponse>
    | GetPartnerContactsResponse;

  createPartnerContact(
    request: CreatePartnerContactRequest,
    metadata?: Metadata,
  ):
    | Promise<CreatePartnerContactResponse>
    | Observable<CreatePartnerContactResponse>
    | CreatePartnerContactResponse;

  updatePartnerContact(
    request: UpdatePartnerContactRequest,
    metadata?: Metadata,
  ):
    | Promise<UpdatePartnerContactResponse>
    | Observable<UpdatePartnerContactResponse>
    | UpdatePartnerContactResponse;

  deletePartnerContact(
    request: DeletePartnerContactRequest,
    metadata?: Metadata,
  ):
    | Promise<DeletePartnerContactResponse>
    | Observable<DeletePartnerContactResponse>
    | DeletePartnerContactResponse;
}

export function PartnerServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPartners',
      'getPartnersListing',
      'upsertPartner',
      'getPartnerInfo',
      'updatePartnerInfo',
      'getPartnerContacts',
      'createPartnerContact',
      'updatePartnerContact',
      'deletePartnerContact',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('PartnerService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('PartnerService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PARTNER_SERVICE_NAME = 'PartnerService';

export interface LecturerServiceClient {
  getLecturers(
    request: GetLecturersRequest,
    metadata?: Metadata,
  ): Observable<GetLecturersResponse>;

  getLecturerInfo(
    request: GetLecturerInfoRequest,
    metadata?: Metadata,
  ): Observable<GetLecturerInfoResponse>;

  updateLecturerInfo(
    request: UpdateLecturerInfoRequest,
    metadata?: Metadata,
  ): Observable<UpdateLecturerInfoResponse>;
}

export interface LecturerServiceController {
  getLecturers(
    request: GetLecturersRequest,
    metadata?: Metadata,
  ):
    | Promise<GetLecturersResponse>
    | Observable<GetLecturersResponse>
    | GetLecturersResponse;

  getLecturerInfo(
    request: GetLecturerInfoRequest,
    metadata?: Metadata,
  ):
    | Promise<GetLecturerInfoResponse>
    | Observable<GetLecturerInfoResponse>
    | GetLecturerInfoResponse;

  updateLecturerInfo(
    request: UpdateLecturerInfoRequest,
    metadata?: Metadata,
  ):
    | Promise<UpdateLecturerInfoResponse>
    | Observable<UpdateLecturerInfoResponse>
    | UpdateLecturerInfoResponse;
}

export function LecturerServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getLecturers',
      'getLecturerInfo',
      'updateLecturerInfo',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('LecturerService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('LecturerService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const LECTURER_SERVICE_NAME = 'LecturerService';

export interface OrgServiceClient {
  createOrg(
    request: CreateOrgRequest,
    metadata?: Metadata,
  ): Observable<CreateOrgResponse>;

  getOrgs(
    request: GetOrgsRequest,
    metadata?: Metadata,
  ): Observable<GetOrgsResponse>;

  getClasses(
    request: GetClassesRequest,
    metadata?: Metadata,
  ): Observable<GetClassesResponse>;

  getClassesListing(
    request: GetClassesListingRequest,
    metadata?: Metadata,
  ): Observable<GetClassesListingResponse>;

  upsertClass(
    request: UpsertClassRequest,
    metadata?: Metadata,
  ): Observable<UpsertClassResponse>;

  getOrgPartners(
    request: GetOrgPartnersRequest,
    metadata?: Metadata,
  ): Observable<GetOrgPartnersResponse>;

  getOrgPartnersListing(
    request: GetOrgPartnersListingRequest,
    metadata?: Metadata,
  ): Observable<GetOrgPartnersListingResponse>;

  getOrgListing(
    request: GetOrgListingRequest,
    metadata?: Metadata,
  ): Observable<GetOrgListingResponse>;

  assignAssociate(
    request: AssignAssociateRequest,
    metadata?: Metadata,
  ): Observable<AssignAssociateResponse>;
}

export interface OrgServiceController {
  createOrg(
    request: CreateOrgRequest,
    metadata?: Metadata,
  ):
    | Promise<CreateOrgResponse>
    | Observable<CreateOrgResponse>
    | CreateOrgResponse;

  getOrgs(
    request: GetOrgsRequest,
    metadata?: Metadata,
  ): Promise<GetOrgsResponse> | Observable<GetOrgsResponse> | GetOrgsResponse;

  getClasses(
    request: GetClassesRequest,
    metadata?: Metadata,
  ):
    | Promise<GetClassesResponse>
    | Observable<GetClassesResponse>
    | GetClassesResponse;

  getClassesListing(
    request: GetClassesListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetClassesListingResponse>
    | Observable<GetClassesListingResponse>
    | GetClassesListingResponse;

  upsertClass(
    request: UpsertClassRequest,
    metadata?: Metadata,
  ):
    | Promise<UpsertClassResponse>
    | Observable<UpsertClassResponse>
    | UpsertClassResponse;

  getOrgPartners(
    request: GetOrgPartnersRequest,
    metadata?: Metadata,
  ):
    | Promise<GetOrgPartnersResponse>
    | Observable<GetOrgPartnersResponse>
    | GetOrgPartnersResponse;

  getOrgPartnersListing(
    request: GetOrgPartnersListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetOrgPartnersListingResponse>
    | Observable<GetOrgPartnersListingResponse>
    | GetOrgPartnersListingResponse;

  getOrgListing(
    request: GetOrgListingRequest,
    metadata?: Metadata,
  ):
    | Promise<GetOrgListingResponse>
    | Observable<GetOrgListingResponse>
    | GetOrgListingResponse;

  assignAssociate(
    request: AssignAssociateRequest,
    metadata?: Metadata,
  ):
    | Promise<AssignAssociateResponse>
    | Observable<AssignAssociateResponse>
    | AssignAssociateResponse;
}

export function OrgServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createOrg',
      'getOrgs',
      'getClasses',
      'getClassesListing',
      'upsertClass',
      'getOrgPartners',
      'getOrgPartnersListing',
      'getOrgListing',
      'assignAssociate',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('OrgService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('OrgService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORG_SERVICE_NAME = 'OrgService';

export interface StudentServiceClient {
  getStudents(
    request: GetStudentsRequest,
    metadata?: Metadata,
  ): Observable<GetStudentsResponse>;

  getStudentInfo(
    request: GetStudentInfoRequest,
    metadata?: Metadata,
  ): Observable<GetStudentInfoResponse>;

  updateStudentInfo(
    request: UpdateStudentInfoRequest,
    metadata?: Metadata,
  ): Observable<UpdateStudentInfoResponse>;

  downloadCv(
    request: DownloadCvRequest,
    metadata?: Metadata,
  ): Observable<DownloadCvResponse>;
}

export interface StudentServiceController {
  getStudents(
    request: GetStudentsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentsResponse>
    | Observable<GetStudentsResponse>
    | GetStudentsResponse;

  getStudentInfo(
    request: GetStudentInfoRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentInfoResponse>
    | Observable<GetStudentInfoResponse>
    | GetStudentInfoResponse;

  updateStudentInfo(
    request: UpdateStudentInfoRequest,
    metadata?: Metadata,
  ):
    | Promise<UpdateStudentInfoResponse>
    | Observable<UpdateStudentInfoResponse>
    | UpdateStudentInfoResponse;

  downloadCv(
    request: DownloadCvRequest,
    metadata?: Metadata,
  ):
    | Promise<DownloadCvResponse>
    | Observable<DownloadCvResponse>
    | DownloadCvResponse;
}

export function StudentServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getStudents',
      'getStudentInfo',
      'updateStudentInfo',
      'downloadCv',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('StudentService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('StudentService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const STUDENT_SERVICE_NAME = 'StudentService';

export interface AuthServiceClient {
  changePassword(
    request: ChangePasswordRequest,
    metadata?: Metadata,
  ): Observable<ChangePasswordResponse>;

  resetPassword(
    request: ResetPasswordRequest,
    metadata?: Metadata,
  ): Observable<ResetPasswordResponse>;

  completeResetPassword(
    request: CompleteResetPasswordRequest,
    metadata?: Metadata,
  ): Observable<CompleteResetPasswordResponse>;

  getAccessToken(
    request: GetAccessTokenRequest,
    metadata?: Metadata,
  ): Observable<GetAccessTokenResponse>;

  loginWithEmail(
    request: LoginWithEmailRequest,
    metadata?: Metadata,
  ): Observable<LoginWithEmailResponse>;

  loginWithGoogle(
    request: LoginWithGoogleRequest,
    metadata?: Metadata,
  ): Observable<LoginWithGoogleResponse>;

  getUserFromToken(
    request: GetUserFromTokenRequest,
    metadata?: Metadata,
  ): Observable<GetUserFromTokenResponse>;

  getStudentDetails(
    request: GetStudentDetailsRequest,
    metadata?: Metadata,
  ): Observable<GetStudentDetailsResponse>;

  getLecturerDetails(
    request: GetLecturerDetailsRequest,
    metadata?: Metadata,
  ): Observable<GetLecturerDetailsResponse>;

  getPartnerDetails(
    request: GetPartnerDetailsRequest,
    metadata?: Metadata,
  ): Observable<GetPartnerDetailsResponse>;

  getOrgAdminDetails(
    request: GetOrgAdminDetailsRequest,
    metadata?: Metadata,
  ): Observable<GetOrgAdminDetailsResponse>;
}

export interface AuthServiceController {
  changePassword(
    request: ChangePasswordRequest,
    metadata?: Metadata,
  ):
    | Promise<ChangePasswordResponse>
    | Observable<ChangePasswordResponse>
    | ChangePasswordResponse;

  resetPassword(
    request: ResetPasswordRequest,
    metadata?: Metadata,
  ):
    | Promise<ResetPasswordResponse>
    | Observable<ResetPasswordResponse>
    | ResetPasswordResponse;

  completeResetPassword(
    request: CompleteResetPasswordRequest,
    metadata?: Metadata,
  ):
    | Promise<CompleteResetPasswordResponse>
    | Observable<CompleteResetPasswordResponse>
    | CompleteResetPasswordResponse;

  getAccessToken(
    request: GetAccessTokenRequest,
    metadata?: Metadata,
  ):
    | Promise<GetAccessTokenResponse>
    | Observable<GetAccessTokenResponse>
    | GetAccessTokenResponse;

  loginWithEmail(
    request: LoginWithEmailRequest,
    metadata?: Metadata,
  ):
    | Promise<LoginWithEmailResponse>
    | Observable<LoginWithEmailResponse>
    | LoginWithEmailResponse;

  loginWithGoogle(
    request: LoginWithGoogleRequest,
    metadata?: Metadata,
  ):
    | Promise<LoginWithGoogleResponse>
    | Observable<LoginWithGoogleResponse>
    | LoginWithGoogleResponse;

  getUserFromToken(
    request: GetUserFromTokenRequest,
    metadata?: Metadata,
  ):
    | Promise<GetUserFromTokenResponse>
    | Observable<GetUserFromTokenResponse>
    | GetUserFromTokenResponse;

  getStudentDetails(
    request: GetStudentDetailsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetStudentDetailsResponse>
    | Observable<GetStudentDetailsResponse>
    | GetStudentDetailsResponse;

  getLecturerDetails(
    request: GetLecturerDetailsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetLecturerDetailsResponse>
    | Observable<GetLecturerDetailsResponse>
    | GetLecturerDetailsResponse;

  getPartnerDetails(
    request: GetPartnerDetailsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetPartnerDetailsResponse>
    | Observable<GetPartnerDetailsResponse>
    | GetPartnerDetailsResponse;

  getOrgAdminDetails(
    request: GetOrgAdminDetailsRequest,
    metadata?: Metadata,
  ):
    | Promise<GetOrgAdminDetailsResponse>
    | Observable<GetOrgAdminDetailsResponse>
    | GetOrgAdminDetailsResponse;
}

export function AuthServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'changePassword',
      'resetPassword',
      'completeResetPassword',
      'getAccessToken',
      'loginWithEmail',
      'loginWithGoogle',
      'getUserFromToken',
      'getStudentDetails',
      'getLecturerDetails',
      'getPartnerDetails',
      'getOrgAdminDetails',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('AuthService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('AuthService', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const AUTH_SERVICE_NAME = 'AuthService';

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Timestamp {
  if (o instanceof Date) {
    return toTimestamp(o);
  } else if (typeof o === 'string') {
    return toTimestamp(new Date(o));
  } else {
    return Timestamp.fromJSON(o);
  }
}

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
