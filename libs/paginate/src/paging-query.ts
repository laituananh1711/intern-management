import { Type } from 'class-transformer';
import { IsBase64, IsNumber, IsOptional, Max } from 'class-validator';
import { base64str, DEFAULT_PAGE_NUM, DEFAULT_PER_PAGE } from './types';

export class OffsetPagingQuery {
  @IsNumber()
  @Max(100)
  @IsOptional()
  @Type(() => Number)
  perPage = DEFAULT_PER_PAGE;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  page = DEFAULT_PAGE_NUM;
}

// See https://slack.engineering/evolving-api-pagination-at-slack
export class CursorPagingQuery {
  @IsBase64()
  @IsOptional()
  cursor?: base64str;

  @IsNumber()
  @Max(100)
  @IsOptional()
  @Type(() => Number)
  limit = 50;
}
