import { SelectQueryBuilder } from 'typeorm';

export interface PaginatedResultsMeta {
  perPage: number;
  totalItems: number;
  totalPages: number;
  currentPage: number;
}

export interface PaginatedResults<T> {
  items: T[];
  meta: PaginatedResultsMeta;
}

export interface PaginateOptions {
  perPage: number;
  page: number;
}

/**
 * Paginate the Mongo query.
 * @param query mongo query to be paginated.
 * @param options paging options
 * @returns a list of paginated items and metadata.
 */
export async function paginate<T = any>(
  query: SelectQueryBuilder<T>,
  options: PaginateOptions,
): Promise<PaginatedResults<T>> {
  // eslint-disable-next-line prefer-const
  let { perPage, page } = options;
  page = Math.max(1, page);

  const offset = (page - 1) * perPage;
  const getQuery = query.limit(perPage).offset(offset);

  const [items, count] = await getQuery.getManyAndCount();

  return {
    items,
    meta: {
      perPage,
      currentPage: page,
      totalItems: count,
      totalPages: Math.ceil(count / perPage),
    },
  };
}
