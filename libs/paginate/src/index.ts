import { DEFAULT_PAGE_NUM, DEFAULT_PER_PAGE } from './types';

export * from './paginate';
export * from './paging-query';
export * from './parse-sort-query';
export * from './types';

/**
 * Return DEFAULT_PAGE if the given value is <= 0.
 * @param page
 * @returns
 */
export function defaultPage(page: number): number {
  if (page <= 0) {
    return DEFAULT_PAGE_NUM;
  }
  return page;
}

/**
 * Return DEFAULT_PER_PAGE if the given value is <= 0.
 * @param perPage
 * @returns
 */
export function defaultPerPage(perPage: number): number {
  if (perPage <= 0) {
    return DEFAULT_PER_PAGE;
  }
  return perPage;
}
