export const PAGE_QUERY = 'page';
export const PER_PAGE_QUERY = 'perPage';
export type base64str = string;
export const DEFAULT_PAGE_NUM = 1;
export const DEFAULT_PER_PAGE = 10;
export const MAX_PER_PAGE = 100;
