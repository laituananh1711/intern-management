# UETWork - Internship Management System

[![pipeline status](https://gitlab.com/laituananh1711/intern-management/badges/master/pipeline.svg)](https://gitlab.com/laituananh1711/intern-management/-/commits/master) [![coverage report](https://gitlab.com/laituananh1711/intern-management/badges/master/coverage.svg)](https://gitlab.com/laituananh1711/intern-management/-/commits/master)

## Description

This is the backend server for the UETWork Internship Management System. It aims to help students find an internship at a suitable company and tracks their progress through their internship period.

## Setup

### Requirements

1. Docker 20.10
2. Make (can be installed by `sudo apt-get install build-essential`)
3. Yarn 1.x (<https://classic.yarnpkg.com/lang/en/docs/install/>)
4. HTTPie 3.x (<https://httpie.io/docs/cli/installation>)

### Steps

1/ Install npm dependencies

```bash
# Install dependencies
yarn install

# Install @nestjs/cli
npm i -g @nestjs/cli
```

2/ Pulling and running Docker dependencies

```shell
make start-infra
```

**NOTE**: After this step, you may need to wait for a bit for the services to start up before running the commands below.

3/ Create `.env` files from `.env.example`

Find all `.env.example` files in this directory and copy the content to a `.env` file.

The `.env` files will be used when running a service or performing DB migration, so it's crucial that this step is done.

4/ Run migration for User Service and Internship Service

```shell
# Migrate User Service DB
make user-service-migrate-up
```

```shell
# Migrate Internship Service DB
make internship-service-migrate-up
```

5/ Apply Debezium Configuration

We need to config Debezium to replicate changes in User Service DB to Internship Service DB.

```sh
# Send Debezium config to its endpoint
http POST :8083/connectors < user-service.dbz.json
```

## Running the app

### Starting the app infrastructure

```sh
make start-infra
```

### Stopping the app infrastructure

```sh
make stop-infra
```

For more predefined scripts, see [Makefile](Makefile).

### Starting the development server

Start a service using `@nestjs/cli`. Where `service-name` is the name of a folder inside [`apps/`](./apps). To start the gateway,
leave out the `<service-name>` part.

```bash
yarn start:debug <service-name>
# or...
nest start <service-name> --watch

# Example: yarn start:debug user-service
```

For more information about `@nestjs/cli`, see <https://docs.nestjs.com/cli/monorepo#monorepo-mode>


## Test

```bash
# unit tests
yarn run test

# ci test (with coverage)
yarn run test:ci

# test coverage
yarn run test:cov
```

## CI/CD functionalities

This project have been set up with CI/CD. It will auto run the unit tests, build a new docker image for both service,
upload it to Gitlab Registry and deploy it to our server.

For more information, please see [.gitlab-ci.yml](./.gitlab-ci.yml) file.

## FAQ

### How to test send mail functionality?

1. Create an account on <https://ethereal.email/>.
2. Update the environment variables in `.env` files for the service you want to test.

### How to build Docker Image?

Pre-written scripts for building docker images of each services are specified in [Makefile](Makefile).

Example script-building user service docker image:

```sh
make build-user-service
```

### How to create new migration file?

To create a new migration file, you could either...
1. Update the models in `database/entities` folder, then generate a file based on those change.
2. Create an empty migration file and fill it in yourself.

Finally, you need to run `make <service_name>-migrate-up` to apply the changes.

```shell
# Generate migration from changes to database/entities folder. 
# Example: make internship-service-migrate-gen name=change_school_class_and_remove_major
make <service-name>-migrate-gen name=<migration_name>

# Create empty file
# Example: make internship-service-migrate-new name=change_school_class_and_remove_major
make <service-name>-migrate-new name=<migration_name>
```

### How to generate/update DB documentations?

```shell
# Example: tbls doc mysql://uetwork:password@localhost:3327/user_service dbdoc/userService --force 
```

For more commands, see [Makefile](./Makefile).
