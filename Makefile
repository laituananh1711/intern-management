USER_SERVICE_ORM_CONFIG_PATH=apps/user-service/src/ormconfig.ts
INTERNSHIP_SERVICE_ORM_CONFIG_PATH=apps/internship-service/src/ormconfig.ts

USER_SERVICE_DOCKERFILE=apps/user-service/Dockerfile
INTERNSHIP_SERVICE_DOCKERFILE=apps/internship-service/Dockerfile

# Start all containers specified in docker-compose file
start-infra:
	docker-compose --env-file .env.docker up -d

# Stop all running containers but does not remove them
stop-infra:
	docker-compose --env-file .env.docker stop

# List all docker container processes
status-infra:
	docker-compose --env-file .env.docker ps

# Stop and remove all running containers, should be used when docker-compose is updated
down-infra:
	docker-compose --env-file .env.docker down

# Generate a new migration file from changes to userService's entities
# Example: make user-service-migrate-gen name=init_tables
user-service-migrate-gen:
	echo "usage: make user-service-migrate-gen name={name}"
	npm run migrate:gen -- ${name} --config ${USER_SERVICE_ORM_CONFIG_PATH}
	npm run format:migration 1>/dev/null

user-service-migrate-up:
	npm run typeorm migration:run -- -f ${USER_SERVICE_ORM_CONFIG_PATH}

user-service-migrate-down:
	npm run typeorm migration:revert -- -f ${USER_SERVICE_ORM_CONFIG_PATH}

user-service-migrate-gup: user-service-migrate-gen user-service-migrate-up

# Generate a new migration file from changes to userService's entities
# Example: make user-service-migrate-gen name=init_tables
internship-service-migrate-gen:
	echo "usage: make internship-service-migrate-gen name={name}"
	npm run migrate:gen -- ${name} --config ${INTERNSHIP_SERVICE_ORM_CONFIG_PATH}
	npm run format:migration 1>/dev/null

internship-service-migrate-new:
	npm run migrate:new -- ${name} --config ${INTERNSHIP_SERVICE_ORM_CONFIG_PATH}

internship-service-migrate-up:
	npm run typeorm migration:run -- -f ${INTERNSHIP_SERVICE_ORM_CONFIG_PATH}

internship-service-migrate-down:
	npm run typeorm migration:revert -- -f ${INTERNSHIP_SERVICE_ORM_CONFIG_PATH}

internship-service-migrate-gup: internship-service-migrate-gen internship-service-migrate-up

build-user-service:
	docker build -f ${USER_SERVICE_DOCKERFILE} .

build-internship-service:
	docker build -f ${INTERNSHIP_SERVICE_DOCKERFILE} .protoc3.19 --plugin=node_modules/ts-proto/protoc-gen-ts_proto -I proto -I /usr/local/include ./proto/user_service/service.proto --ts_proto_out=./libs/pb --ts_proto_opt=nestJs=true,addGrpcMetadata=true,useDate=true

gen-proto:
	protoc3.19 --plugin=node_modules/ts-proto/protoc-gen-ts_proto -I proto -I /usr/local/include ./proto/${SERVICE}/service.proto --ts_proto_out=./libs/pb --ts_proto_opt=nestJs=true,addGrpcMetadata=true,env=node,fileSuffix=.pb,outputJsonMethods=true,unrecognizedEnum=false
