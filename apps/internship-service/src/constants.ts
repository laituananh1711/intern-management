import { config } from 'dotenv';

// TODO: standardize the way we set configs
config({
  path: 'apps/internship-service/.env',
});

export const KAFKA_PROVIDER = 'KAFKA';

export const KAFKA_STUDENTS_TOPIC =
  process.env.KAFKA_STUDENTS_TOPIC || 'dbserver1.user_service.students';
export const KAFKA_LECTURERS_TOPIC =
  process.env.KAFKA_LECTURERS_TOPIC || 'dbserver1.user_service.lecturers';
export const KAFKA_PARTNERS_TOPIC =
  process.env.KAFKA_PARTNERS_TOPIC || 'dbserver1.user_service.partners';
export const KAFKA_ORGS_TOPIC =
  process.env.KAFKA_ORGS_TOPIC || 'dbserver1.user_service.organizations';
export const KAFKA_SCHOOL_CLASSES_TOPIC =
  process.env.KAFKA_SCHOOL_CLASSES_TOPIC ||
  'dbserver1.user_service.school_classes';
export const KAFKA_PARTNER_ORG_TOPIC =
  process.env.KAFKA_PARTNER_ORG_TOPIC ||
  'dbserver1.user_service.partner_organizations';
export const KAFKA_PARTNER_CONTACT_TOPIC =
  process.env.KAFKA_PARTNER_CONTACT_TOPIC ||
  'dbserver1.user_service.partner_contacts';

export const TERM_PARTNER_SEARCH_LIMIT = 20;

export const events = {
  TERM_SIGN_UP: 'term.signed_up',
  PARTNER_STATUS_CHANGED: 'term.partner.status_changed',
  PARTNER_REG_STATUS_CHANGED: 'term.registration.status_changed',
  SUPERVISOR_ASSIGNED: 'term.supervisor.assigned',
  SCORE_GIVEN: 'term.score.given',
  BATCH_PARTNER_REG_STATUS_CHANGED: 'term.registration.batch_status_changed',
};

export const FILE_SEPARATOR = '/';
export const REPORT_FOLDER_NAME = 'reports';
export const REPORT_FILE_KEY = 'file';
export const REPORT_MAX_SIZE_IN_BYTES = 10 * 1024 * 1024;
export const PDF_MIME_TYPE = 'application/pdf';
export const MINIO_TEMP_FOLDER = 'temp';
export const EXPORT_EXPIRY_SECONDS = 30 * 60; // 30 minutes
