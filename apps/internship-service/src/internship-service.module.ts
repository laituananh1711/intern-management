import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  KafkaAvroRequestSerializer,
  KafkaAvroResponseDeserializer,
  KafkaModule,
} from '@uetwork/nestjs-kafka';
import { NestMinioModule, NestMinioOptions } from 'nestjs-minio';
import { HealthController } from '../../user-service/src/health.controller';
import { KAFKA_PROVIDER } from './constants';
import { ConsumerModule } from './modules/consumer/consumer.module';
import { MailModule } from './modules/mail/mail.module';
import { TermModule } from './modules/term/term.module';
import typeOrmOptions from './ormconfig';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: 'apps/internship-service/.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(typeOrmOptions),
    NestMinioModule.registerAsync({
      inject: [ConfigService],
      imports: [ConfigModule],
      useFactory: (configService: ConfigService): NestMinioOptions => {
        const portStr = configService.get('MINIO_PORT', '9000');
        const port = parseInt(portStr);
        const useSSL = configService.get('MINIO_SECURE', 'false');

        return {
          endPoint: configService.get('MINIO_ENDPOINT', 'localhost'),
          accessKey: configService.get('MINIO_ACCESS_KEY', 'root'),
          secretKey: configService.get('MINIO_SECRET_KEY', 'password'),
          port,
          useSSL: useSSL === 'true' || useSSL === true,
        };
      },
    }),
    KafkaModule.registerAsync([KAFKA_PROVIDER], {
      useFactory: async (configService: ConfigService) => {
        const kafkaBrokers = configService
          .get<string>('KAFKA_BROKERS', 'localhost:9092')
          .split(',');
        const schemaRegistryUrl = configService.get<string>(
          'SCHEMA_REGISTRY_URL',
          'http://localhost:8081',
        );

        return [
          {
            name: KAFKA_PROVIDER,
            options: {
              // TODO: update nestjs-kafka to allows per-topic configurations
              // For db replication topic, we don't have a problem with data of the same key
              // since we use `repo.save()`.
              consumeFromBeginning: true,
              client: {
                brokers: kafkaBrokers,
                clientId: configService.get<string>(
                  'KAFKA_CONSUMER_CLIENT_ID',
                  'internship-service',
                ),
              },
              consumer: {
                groupId: configService.get<string>(
                  'KAFKA_CONSUMER_GROUP_ID',
                  'internship',
                ),
              },
              deserializer: new KafkaAvroResponseDeserializer({
                host: schemaRegistryUrl,
              }),
              serializer: new KafkaAvroRequestSerializer({
                config: {
                  host: schemaRegistryUrl,
                },
                schemas: [],
                options: {},
              }),
            },
          },
        ];
      },
      inject: [ConfigService],
    }),
    ConsumerModule,
    TermModule,
    MailModule,
    EventEmitterModule.forRoot(),
  ],
  controllers: [HealthController],
})
export class InternshipServiceModule {}
