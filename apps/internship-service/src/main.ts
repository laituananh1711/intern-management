import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { INTERNSHIP_SERVICE_PACKAGE_NAME } from '@app/pb/internship_service/service.pb';
import { ApiKeyAuthInterceptor } from '@app/utils';
import { InternshipServiceModule } from './internship-service.module';

async function bootstrap() {
  const app = await NestFactory.create(InternshipServiceModule);
  const configService = app.get(ConfigService);

  const grpcPort = configService.get<number>('GRPC_PORT', 4101);

  const secretHeader = configService.get('SECRET_HEADER', 'x-uetwork-api');
  const secretKey = configService.get('SECRET_KEY', 'secret');

  app.useGlobalInterceptors(new ApiKeyAuthInterceptor(secretHeader, secretKey));

  app.connectMicroservice<MicroserviceOptions>(
    {
      transport: Transport.GRPC,
      options: {
        package: INTERNSHIP_SERVICE_PACKAGE_NAME,
        protoPath: 'proto/internship_service/service.proto',
        url: `0.0.0.0:${grpcPort}`,
        loader: {
          defaults: true,
        },
      },
    },
    {
      inheritAppConfig: true,
    },
  );

  const port = configService.get<number>('HTTP_PORT', 4001);

  await app.startAllMicroservices();
  await app.listen(port);
}
bootstrap();
