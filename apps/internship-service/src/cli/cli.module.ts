import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConsoleModule } from 'nestjs-console';
import { InternshipTerm } from '../database/entities/internship-term.entity';
import { SeederService } from './seeder.service';
import typeOrmOptions from '../ormconfig';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConsoleModule,
    ConfigModule.forRoot({
      envFilePath: 'apps/internship-service/.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      ...typeOrmOptions,
      logging: ['error'],
    }),
    TypeOrmModule.forFeature([InternshipTerm]),
  ],
  providers: [SeederService],
})
export class CliModule {}
