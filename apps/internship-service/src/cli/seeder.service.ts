import { Command, Console } from 'nestjs-console';
import { InternshipTerm } from '../database/entities/internship-term.entity';
import { faker } from '@faker-js/faker';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import { Organization } from '../database/entities/organization.entity';
import { TermStudent } from '../database/entities/term-student.entity';
import { Student } from '../database/entities/student.entity';
import { TermLecturer } from '../database/entities/term-lecturer.entity';
import { Lecturer } from '../database/entities/lecturer.entity';
import {
  PartnerInternshipStatus,
  TermPartner,
} from '../database/entities/term-partner.entity';
import { Partner } from '../database/entities/partner.entity';

@Console({
  command: 'seed',
})
export class SeederService {
  private manager: EntityManager;

  constructor(
    @InjectRepository(InternshipTerm)
    private termRepo: Repository<InternshipTerm>,
  ) {
    this.manager = termRepo.manager;
  }

  @Command({
    command: 'all',
  })
  async seedAll() {
    await this.seedTerms();
    await Promise.all([
      this.seedTermStudents(),
      this.seedTermLecturers(),
      this.seedTermPartners(),
    ]);
  }

  @Command({
    command: 'term',
    options: [
      {
        flags: '-n, --number [number of item]',
        required: false,
      },
    ],
  })
  async seedTerms(options?: SeedOptions) {
    const seedAmount = options?.number ?? defaultSeedAmount.term;

    const orgs = await this.manager.find(Organization, {
      select: ['id'],
    });

    const orgIds = orgs.map((o) => o.id!);
    const terms = this.generateTerms(seedAmount, orgIds);

    await this.termRepo.insert(terms);
  }

  @Command({
    command: 'term-student',
  })
  async seedTermStudents() {
    const orgs = await this.manager.find(Organization, {
      select: ['id'],
    });

    const orgIds = orgs.map((o) => o.id!);

    const termStudents: TermStudent[] = [];
    for (const orgId of orgIds) {
      const termsPromise = this.manager.find(InternshipTerm, {
        select: ['id'],
        where: {
          organizationId: orgId,
        },
        take: 10,
      });
      const studentsPromise = this.manager.find(Student, {
        select: ['id'],
        where: {
          organizationId: orgId,
        },
        take: 30,
      });

      const [terms, students] = await Promise.all([
        termsPromise,
        studentsPromise,
      ]);
      if (terms.length === 0 || students.length === 0) {
        continue;
      }

      const termIds = terms.map((t) => t.id!);
      const studentIds = students.map((s) => s.id!);

      termStudents.push(...this.generateTermStudents(termIds, studentIds));
    }

    await this.manager.save(TermStudent, termStudents);
  }

  @Command({
    command: 'term-lecturer',
  })
  async seedTermLecturers() {
    const orgs = await this.manager.find(Organization, {
      select: ['id'],
    });

    const orgIds = orgs.map((o) => o.id!);

    const termLecturers: TermLecturer[] = [];
    for (const orgId of orgIds) {
      const termsPromise = this.manager.find(InternshipTerm, {
        select: ['id'],
        where: {
          organizationId: orgId,
        },
        take: 10,
      });
      const lecturersPromise = this.manager.find(Lecturer, {
        select: ['id'],
        where: {
          organizationId: orgId,
        },
        take: 30,
      });

      const [terms, lecturers] = await Promise.all([
        termsPromise,
        lecturersPromise,
      ]);
      if (terms.length === 0 || lecturers.length === 0) {
        continue;
      }

      const termIds = terms.map((t) => t.id!);
      const lecturerIds = lecturers.map((s) => s.id!);

      termLecturers.push(...this.generateTermLecturers(termIds, lecturerIds));
    }

    await this.manager.save(TermLecturer, termLecturers);
  }

  @Command({
    command: 'term-partner',
  })
  async seedTermPartners() {
    const termsPromise = this.manager.find(InternshipTerm, {
      select: ['id'],
    });
    const partnersPromise = this.manager
      .createQueryBuilder(Partner, 'p')
      .select(['p.id'])
      .orderBy('RAND()')
      .limit(20)
      .getMany();

    const [terms, partners] = await Promise.all([
      termsPromise,
      partnersPromise,
    ]);
    const termIds = terms.map((t) => t.id!);
    const partnerIds = partners.map((p) => p.id!);

    await this.manager.save(
      TermPartner,
      this.generateTermPartners(termIds, partnerIds),
    );
  }

  private generateTermLecturers(
    termIds: number[],
    lecturerIds: number[],
  ): TermLecturer[] {
    const items: TermLecturer[] = [];

    for (const termId of termIds) {
      for (const lecturerId of lecturerIds) {
        const item: TermLecturer = {
          termId,
          lecturerId,
        };

        items.push(item);
      }
    }

    return items;
  }

  private generateTerms(count: number, orgIds: number[]): InternshipTerm[] {
    const items: InternshipTerm[] = [];

    for (let i = 0; i < count; i++) {
      const year = faker.datatype.number({ min: 2015, max: 2022 });
      const item: InternshipTerm = new InternshipTerm({
        term: faker.datatype.number({ min: 1, max: 3 }),
        year,
        startDate: `${year}-02-10`,
        endDate: `${year}-04-20`,
        startRegAt: new Date(`${year}-02-10`),
        endRegAt: new Date(`${year}-02-28`),
        organizationId: faker.random.arrayElement(orgIds),
      });
      items.push(item);
    }

    return items;
  }

  private generateTermStudents(
    termIds: number[],
    studentIds: number[],
  ): TermStudent[] {
    const items: TermStudent[] = [];

    for (let i = 0; i < termIds.length; i++) {
      for (let j = 0; j < studentIds.length; j++) {
        const item: TermStudent = {
          termId: termIds[i],
          studentId: studentIds[j],
        };

        items.push(item);
      }
    }

    return items;
  }

  private generateTermPartners(
    termIds: number[],
    partnerIds: number[],
  ): TermPartner[] {
    const items: TermPartner[] = [];

    for (let i = 0; i < termIds.length; i++) {
      for (let j = 0; j < partnerIds.length; j++) {
        const item: TermPartner = {
          termId: termIds[i],
          partnerId: partnerIds[j],
          status: faker.random.arrayElement(
            Object.values(PartnerInternshipStatus),
          ),
        };

        items.push(item);
      }
    }

    return items;
  }
}

interface SeedOptions {
  number?: number;
}

const defaultSeedAmount = {
  term: 10,
};
