import { MigrationInterface, QueryRunner } from 'typeorm';

export class lecturerAddPhoneNumber1649317760408 implements MigrationInterface {
  name = 'lecturerAddPhoneNumber1649317760408';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` ADD \`phone_number\` varchar(255) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` DROP COLUMN \`phone_number\``,
    );
  }
}
