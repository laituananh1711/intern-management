import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerContactsRemovePartnerFk1649212264493
  implements MigrationInterface
{
  name = 'partnerContactsRemovePartnerFk1649212264493';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`partner_contacts\` DROP FOREIGN KEY \`FK_608759e68dbcd493b2e3e9af0d1\``,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`partner_contacts\` ADD CONSTRAINT \`FK_608759e68dbcd493b2e3e9af0d1\` FOREIGN KEY (\`partner_id\`) REFERENCES \`partners\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
