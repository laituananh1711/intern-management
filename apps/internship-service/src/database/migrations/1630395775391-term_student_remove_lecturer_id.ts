import { MigrationInterface, QueryRunner } from 'typeorm';

export class termStudentRemoveLecturerId1630395775391
  implements MigrationInterface
{
  name = 'termStudentRemoveLecturerId1630395775391';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `term_students` DROP COLUMN `lecturer_id`',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `term_students` ADD `lecturer_id` int UNSIGNED NULL',
    );
  }
}
