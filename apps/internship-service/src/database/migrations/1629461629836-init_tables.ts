import { MigrationInterface, QueryRunner } from 'typeorm';

export class initTables1629461629836 implements MigrationInterface {
  name = 'initTables1629461629836';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `majors` (`id` int UNSIGNED NOT NULL, `name` varchar(255) NOT NULL COMMENT 'ex. Máy tính và Robot', PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      "CREATE TABLE `students` (`id` int UNSIGNED NOT NULL, `student_id_number` char(8) NOT NULL COMMENT 'A 8-digit id number provided by the school (i.e. 18020001)', `full_name` varchar(255) NOT NULL, `resume_url` varchar(1000) NULL, `phone_number` varchar(255) NULL, `major_id` int UNSIGNED NULL, `organization_id` int UNSIGNED NOT NULL, FULLTEXT INDEX `idx_students_full_name` (`full_name`), INDEX `idx_students_major_id` (`major_id`), INDEX `idx_students_organization_id` (`organization_id`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'CREATE TABLE `organizations` (`id` int UNSIGNED NOT NULL, `name` varchar(255) NOT NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      "CREATE TABLE `internship_terms` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `year` year NOT NULL, `term` tinyint UNSIGNED NOT NULL, `start_reg_at` timestamp NOT NULL COMMENT 'students can not join this term before this timestamp.', `end_reg_at` timestamp NOT NULL COMMENT 'students can not join this term after this timestamp.', `start_date` date NOT NULL, `end_date` date NOT NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `organization_id` int UNSIGNED NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'CREATE TABLE `lecturers` (`id` int UNSIGNED NOT NULL, `full_name` varchar(255) NOT NULL, `organization_id` int UNSIGNED NOT NULL, INDEX `idx_lecturers_org_id` (`organization_id`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      "CREATE TABLE `partners` (`id` int UNSIGNED NOT NULL, `name` varchar(255) NOT NULL COMMENT 'Company name', `homepage_url` varchar(1000) NOT NULL, `logo_url` varchar(1000) NOT NULL, `description` varchar(4000) NOT NULL DEFAULT '', FULLTEXT INDEX `idx_partners_name` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      "CREATE TABLE `partner_internship_terms` (`term_id` int UNSIGNED NOT NULL, `partner_id` int UNSIGNED NOT NULL, `status` enum ('accepted', 'pending', 'rejected') NOT NULL DEFAULT 'pending', `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (`term_id`, `partner_id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'CREATE TABLE `partner_organizations` (`id` int UNSIGNED NOT NULL, `partner_id` int UNSIGNED NOT NULL, `organization_id` int UNSIGNED NOT NULL, `expired_date` date NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), INDEX `idx_partner_orgs_partner_id` (`partner_id`), INDEX `idx_partner_orgs_org_id` (`organization_id`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'ALTER TABLE `internship_terms` ADD CONSTRAINT `FK_e73d151100d3df91bb05d73b789` FOREIGN KEY (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_internship_terms` ADD CONSTRAINT `FK_ee727d3fc78bdfc5de40ced410f` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_internship_terms` ADD CONSTRAINT `FK_ef1196ec60c41044427df5fcdd2` FOREIGN KEY (`term_id`) REFERENCES `internship_terms`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `partner_internship_terms` DROP FOREIGN KEY `FK_ef1196ec60c41044427df5fcdd2`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_internship_terms` DROP FOREIGN KEY `FK_ee727d3fc78bdfc5de40ced410f`',
    );
    await queryRunner.query(
      'ALTER TABLE `internship_terms` DROP FOREIGN KEY `FK_e73d151100d3df91bb05d73b789`',
    );
    await queryRunner.query(
      'DROP INDEX `idx_partner_orgs_org_id` ON `partner_organizations`',
    );
    await queryRunner.query(
      'DROP INDEX `idx_partner_orgs_partner_id` ON `partner_organizations`',
    );
    await queryRunner.query('DROP TABLE `partner_organizations`');
    await queryRunner.query('DROP TABLE `partner_internship_terms`');
    await queryRunner.query('DROP INDEX `idx_partners_name` ON `partners`');
    await queryRunner.query('DROP TABLE `partners`');
    await queryRunner.query('DROP INDEX `idx_lecturers_org_id` ON `lecturers`');
    await queryRunner.query('DROP TABLE `lecturers`');
    await queryRunner.query('DROP TABLE `internship_terms`');
    await queryRunner.query('DROP TABLE `organizations`');
    await queryRunner.query(
      'DROP INDEX `idx_students_organization_id` ON `students`',
    );
    await queryRunner.query('DROP INDEX `idx_students_major_id` ON `students`');
    await queryRunner.query(
      'DROP INDEX `idx_students_full_name` ON `students`',
    );
    await queryRunner.query('DROP TABLE `students`');
    await queryRunner.query('DROP TABLE `majors`');
  }
}
