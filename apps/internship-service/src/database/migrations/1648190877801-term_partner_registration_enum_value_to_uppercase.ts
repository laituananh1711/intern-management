import { MigrationInterface, QueryRunner } from 'typeorm';

export class termPartnerRegistrationEnumValueToUppercase1648190877801
  implements MigrationInterface
{
  name = 'termPartnerRegistrationEnumValueToUppercase1648190877801';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`status\` \`status\` enum ('WAITING', 'PASSED', 'FAILED', 'SELECTED') NOT NULL DEFAULT 'WAITING'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`internship_type\` \`internship_type\` enum ('ASSOCIATE', 'OTHER') NOT NULL COMMENT 'TRUE if this partner was an associate at the time of creation.'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`internship_type\` \`internship_type\` enum ('associate', 'other') NOT NULL COMMENT 'TRUE if this partner was an associate at the time of creation.'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`status\` \`status\` enum ('waiting', 'passed', 'failed', 'selected') NOT NULL DEFAULT 'waiting'`,
    );
  }
}
