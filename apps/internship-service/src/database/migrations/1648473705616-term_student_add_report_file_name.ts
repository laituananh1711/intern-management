import { MigrationInterface, QueryRunner } from 'typeorm';

export class termStudentAddReportFileName1648473705616
  implements MigrationInterface
{
  name = 'termStudentAddReportFileName1648473705616';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_students\` ADD \`report_file_name\` varchar(1000) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_students\` DROP COLUMN \`report_file_name\``,
    );
  }
}
