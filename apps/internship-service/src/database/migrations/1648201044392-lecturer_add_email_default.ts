import { MigrationInterface, QueryRunner } from 'typeorm';

export class lecturerAddEmailDefault1648201044392
  implements MigrationInterface
{
  name = 'lecturerAddEmailDefault1648201044392';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` CHANGE \`org_email\` \`org_email\` varchar(255) NOT NULL DEFAULT ''`,
    );
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` CHANGE \`personal_email\` \`personal_email\` varchar(255) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` CHANGE \`personal_email\` \`personal_email\` varchar(255) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` CHANGE \`org_email\` \`org_email\` varchar(255) NOT NULL`,
    );
  }
}
