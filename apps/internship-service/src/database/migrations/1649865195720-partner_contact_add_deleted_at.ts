import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerContactAddDeletedAt1649865195720
  implements MigrationInterface
{
  name = 'partnerContactAddDeletedAt1649865195720';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`partner_contacts\` ADD \`deleted_at\` datetime(6) NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`partner_contacts\` DROP COLUMN \`deleted_at\``,
    );
  }
}
