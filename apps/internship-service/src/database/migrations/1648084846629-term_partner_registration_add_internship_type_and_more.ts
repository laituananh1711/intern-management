import { MigrationInterface, QueryRunner } from 'typeorm';

export class termPartnerRegistrationAddInternshipTypeAndMore1648084846629
  implements MigrationInterface
{
  name = 'termPartnerRegistrationAddInternshipTypeAndMore1648084846629';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX \`idx_students_major_id\` ON \`students\``,
    );
    await queryRunner.query(
      `DROP INDEX \`idx_students_organization_id\` ON \`students\``,
    );
    await queryRunner.query(
      `CREATE TABLE \`school_classes\` (\`id\` int UNSIGNED NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`major_id\` int UNSIGNED NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`major_id\``,
    );
    await queryRunner.query(`ALTER TABLE \`students\` DROP COLUMN \`user_id\``);
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`school_class_id\` int UNSIGNED NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`org_email\` varchar(255) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`personal_email\` varchar(255) NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP PRIMARY KEY`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD \`id\` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD \`is_mail_sent\` tinyint NOT NULL COMMENT 'Has a mail been sent to the student about this registration?' DEFAULT 0`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD \`internship_type\` enum ('associate', 'other') NOT NULL COMMENT 'TRUE if this partner was an associate at the time of creation.'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_students\` DROP FOREIGN KEY \`FK_c940966fc0ebc2d10487ad9106c\``,
    );
    await queryRunner.query(`ALTER TABLE \`students\` DROP PRIMARY KEY`);
    await queryRunner.query(`ALTER TABLE \`students\` DROP COLUMN \`id\``);
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`id\` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP FOREIGN KEY \`FK_260e8ac73f867e15ddb02ff5b7d\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP FOREIGN KEY \`FK_aee165781b8113c257daea022de\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`is_self_registered\` \`is_self_registered\` tinyint NOT NULL COMMENT 'Did this student register this company by himself?' DEFAULT 0`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX \`IDX_d2e79a9a6ce81ed2d3226c8314\` ON \`term_partner_registrations\` (\`term_id\`, \`partner_id\`, \`student_id\`)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_students\` ADD CONSTRAINT \`FK_c940966fc0ebc2d10487ad9106c\` FOREIGN KEY (\`student_id\`) REFERENCES \`students\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD CONSTRAINT \`FK_aee165781b8113c257daea022de\` FOREIGN KEY (\`partner_id\`, \`term_id\`) REFERENCES \`term_partners\`(\`partner_id\`,\`term_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD CONSTRAINT \`FK_260e8ac73f867e15ddb02ff5b7d\` FOREIGN KEY (\`student_id\`, \`term_id\`) REFERENCES \`term_students\`(\`student_id\`,\`term_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP FOREIGN KEY \`FK_260e8ac73f867e15ddb02ff5b7d\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP FOREIGN KEY \`FK_aee165781b8113c257daea022de\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_students\` DROP FOREIGN KEY \`FK_c940966fc0ebc2d10487ad9106c\``,
    );
    await queryRunner.query(
      `DROP INDEX \`IDX_d2e79a9a6ce81ed2d3226c8314\` ON \`term_partner_registrations\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`is_self_registered\` \`is_self_registered\` tinyint NOT NULL COMMENT 'Is this student already going to this company?' DEFAULT '0'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD CONSTRAINT \`FK_aee165781b8113c257daea022de\` FOREIGN KEY (\`partner_id\`, \`term_id\`) REFERENCES \`term_partners\`(\`partner_id\`,\`term_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD CONSTRAINT \`FK_260e8ac73f867e15ddb02ff5b7d\` FOREIGN KEY (\`student_id\`, \`term_id\`) REFERENCES \`term_students\`(\`student_id\`,\`term_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(`ALTER TABLE \`students\` DROP COLUMN \`id\``);
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`id\` int UNSIGNED NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD PRIMARY KEY (\`id\`)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_students\` ADD CONSTRAINT \`FK_c940966fc0ebc2d10487ad9106c\` FOREIGN KEY (\`student_id\`) REFERENCES \`students\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP COLUMN \`internship_type\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP COLUMN \`is_mail_sent\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP COLUMN \`id\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD PRIMARY KEY (\`term_id\`, \`partner_id\`, \`student_id\`)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`updated_at\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`created_at\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`personal_email\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`org_email\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`school_class_id\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`user_id\` int UNSIGNED NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`major_id\` int UNSIGNED NULL`,
    );
    await queryRunner.query(`DROP TABLE \`school_classes\``);
    await queryRunner.query(
      `CREATE INDEX \`idx_students_organization_id\` ON \`students\` (\`organization_id\`)`,
    );
    await queryRunner.query(
      `CREATE INDEX \`idx_students_major_id\` ON \`students\` (\`major_id\`)`,
    );
  }
}
