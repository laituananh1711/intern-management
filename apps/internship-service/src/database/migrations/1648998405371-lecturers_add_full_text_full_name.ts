import { MigrationInterface, QueryRunner } from 'typeorm';

export class lecturersAddFullTextFullName1648998405371
  implements MigrationInterface
{
  name = 'lecturersAddFullTextFullName1648998405371';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE FULLTEXT INDEX \`idx_lecturers_full_name\` ON \`lecturers\` (\`full_name\`)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX \`idx_lecturers_full_name\` ON \`lecturers\``,
    );
  }
}
