import { MigrationInterface, QueryRunner } from 'typeorm';

export class addTermPartnerRegistration1629823320553
  implements MigrationInterface
{
  name = 'addTermPartnerRegistration1629823320553';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `term_partner_registrations` (`term_id` int UNSIGNED NOT NULL, `partner_id` int UNSIGNED NOT NULL, `student_id` int UNSIGNED NOT NULL, `status` enum ('waiting', 'passed', 'failed', 'selected') NOT NULL DEFAULT 'waiting', `is_self_registered` tinyint NOT NULL COMMENT 'Is this student already going to this company?' DEFAULT 0, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (`term_id`, `partner_id`, `student_id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'ALTER TABLE `term_partner_registrations` ADD CONSTRAINT `FK_aee165781b8113c257daea022de` FOREIGN KEY (`partner_id`, `term_id`) REFERENCES `term_partners`(`partner_id`,`term_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partner_registrations` ADD CONSTRAINT `FK_260e8ac73f867e15ddb02ff5b7d` FOREIGN KEY (`student_id`, `term_id`) REFERENCES `term_students`(`student_id`,`term_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `term_partner_registrations` DROP FOREIGN KEY `FK_260e8ac73f867e15ddb02ff5b7d`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partner_registrations` DROP FOREIGN KEY `FK_aee165781b8113c257daea022de`',
    );
    await queryRunner.query('DROP TABLE `term_partner_registrations`');
  }
}
