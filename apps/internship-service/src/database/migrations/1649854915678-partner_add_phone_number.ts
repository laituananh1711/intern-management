import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerAddPhoneNumber1649854915678 implements MigrationInterface {
  name = 'partnerAddPhoneNumber1649854915678';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`partners\` ADD \`phone_number\` varchar(255) NOT NULL DEFAULT ''`,
    );
    await queryRunner.query(
      `ALTER TABLE \`partners\` ADD \`address\` varchar(255) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`partners\` DROP COLUMN \`address\``);
    await queryRunner.query(
      `ALTER TABLE \`partners\` DROP COLUMN \`phone_number\``,
    );
  }
}
