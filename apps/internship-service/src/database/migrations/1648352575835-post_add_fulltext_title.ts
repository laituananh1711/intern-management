import { MigrationInterface, QueryRunner } from 'typeorm';

export class postAddFulltextTitle1648352575835 implements MigrationInterface {
  name = 'postAddFulltextTitle1648352575835';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE FULLTEXT INDEX \`idx_recruitment_posts_title\` ON \`recruitment_posts\` (\`title\`)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX \`idx_recruitment_posts_title\` ON \`recruitment_posts\``,
    );
  }
}
