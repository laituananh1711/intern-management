import { MigrationInterface, QueryRunner } from 'typeorm';

export class addPostAndContact1629990896089 implements MigrationInterface {
  name = 'addPostAndContact1629990896089';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'CREATE TABLE `partner_contacts` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `full_name` varchar(255) NOT NULL, `phone_number` varchar(20) NOT NULL, `email` varchar(255) NOT NULL, `partner_id` int UNSIGNED NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `recruitment_posts` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `term_id` int UNSIGNED NOT NULL, `partner_id` int UNSIGNED NOT NULL, `partner_contact_id` int UNSIGNED NOT NULL, `start_reg_at` timestamp NOT NULL, `end_reg_at` timestamp NOT NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` ADD CONSTRAINT `FK_608759e68dbcd493b2e3e9af0d1` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `recruitment_posts` ADD CONSTRAINT `FK_ae7dcc43b518aca1da6acf24159` FOREIGN KEY (`partner_id`, `term_id`) REFERENCES `term_partners`(`partner_id`,`term_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `recruitment_posts` ADD CONSTRAINT `FK_e79de7998edd940c8db933f2bf0` FOREIGN KEY (`partner_id`, `partner_contact_id`) REFERENCES `partner_contacts`(`partner_id`,`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `recruitment_posts` DROP FOREIGN KEY `FK_e79de7998edd940c8db933f2bf0`',
    );
    await queryRunner.query(
      'ALTER TABLE `recruitment_posts` DROP FOREIGN KEY `FK_ae7dcc43b518aca1da6acf24159`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` DROP FOREIGN KEY `FK_608759e68dbcd493b2e3e9af0d1`',
    );
    await queryRunner.query('DROP TABLE `recruitment_posts`');
    await queryRunner.query('DROP TABLE `partner_contacts`');
  }
}
