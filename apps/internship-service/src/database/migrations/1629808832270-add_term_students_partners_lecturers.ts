import { MigrationInterface, QueryRunner } from 'typeorm';

export class addTermStudentsPartnersLecturers1629808832270
  implements MigrationInterface
{
  name = 'addTermStudentsPartnersLecturers1629808832270';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `term_partners` DROP FOREIGN KEY `FK_ee727d3fc78bdfc5de40ced410f`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partners` DROP FOREIGN KEY `FK_ef1196ec60c41044427df5fcdd2`',
    );
    await queryRunner.query(
      'CREATE TABLE `term_lecturers` (`term_id` int UNSIGNED NOT NULL, `lecturer_id` int UNSIGNED NOT NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`term_id`, `lecturer_id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `term_students` (`term_id` int UNSIGNED NOT NULL, `student_id` int UNSIGNED NOT NULL, `supervisor_id` int UNSIGNED NULL, `score` decimal(4,2) UNSIGNED NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `lecturer_id` int UNSIGNED NULL, PRIMARY KEY (`term_id`, `student_id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'ALTER TABLE `term_lecturers` ADD CONSTRAINT `FK_140b3d296680341293122f57fc7` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturers`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_lecturers` ADD CONSTRAINT `FK_6cbab93ee6ef3c3743b181ba293` FOREIGN KEY (`term_id`) REFERENCES `internship_terms`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partners` ADD CONSTRAINT `FK_fb9d163ad1d136c4f0d8ff693b1` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partners` ADD CONSTRAINT `FK_7d4fd5990351813bbc25ea7d6a8` FOREIGN KEY (`term_id`) REFERENCES `internship_terms`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_students` ADD CONSTRAINT `FK_c940966fc0ebc2d10487ad9106c` FOREIGN KEY (`student_id`) REFERENCES `students`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_students` ADD CONSTRAINT `FK_90ae2c9e175a92c17c167a9d016` FOREIGN KEY (`term_id`) REFERENCES `internship_terms`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_students` ADD CONSTRAINT `FK_da93599d5e3fad14db4ef4c73dd` FOREIGN KEY (`term_id`, `supervisor_id`) REFERENCES `term_lecturers`(`term_id`,`lecturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `term_students` DROP FOREIGN KEY `FK_da93599d5e3fad14db4ef4c73dd`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_students` DROP FOREIGN KEY `FK_90ae2c9e175a92c17c167a9d016`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_students` DROP FOREIGN KEY `FK_c940966fc0ebc2d10487ad9106c`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partners` DROP FOREIGN KEY `FK_7d4fd5990351813bbc25ea7d6a8`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partners` DROP FOREIGN KEY `FK_fb9d163ad1d136c4f0d8ff693b1`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_lecturers` DROP FOREIGN KEY `FK_6cbab93ee6ef3c3743b181ba293`',
    );
    await queryRunner.query(
      'ALTER TABLE `term_lecturers` DROP FOREIGN KEY `FK_140b3d296680341293122f57fc7`',
    );
    await queryRunner.query('DROP TABLE `term_students`');
    await queryRunner.query('DROP TABLE `term_lecturers`');
    await queryRunner.query(
      'ALTER TABLE `term_partners` ADD CONSTRAINT `FK_ef1196ec60c41044427df5fcdd2` FOREIGN KEY (`term_id`) REFERENCES `internship_terms`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `term_partners` ADD CONSTRAINT `FK_ee727d3fc78bdfc5de40ced410f` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }
}
