import { MigrationInterface, QueryRunner } from 'typeorm';

export class studentsUserIdNullable1649055427196 implements MigrationInterface {
  name = 'studentsUserIdNullable1649055427196';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` CHANGE \`user_id\` \`user_id\` int UNSIGNED NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` CHANGE \`user_id\` \`user_id\` int UNSIGNED NOT NULL`,
    );
  }
}
