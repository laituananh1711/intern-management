import { MigrationInterface, QueryRunner } from 'typeorm';

export class termPartnerMakeStatusUpperCase1648204423316
  implements MigrationInterface
{
  name = 'termPartnerMakeStatusUpperCase1648204423316';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partners\` CHANGE \`status\` \`status\` enum ('ACCEPTED', 'PENDING', 'REJECTED') NOT NULL DEFAULT 'PENDING'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partners\` CHANGE \`status\` \`status\` enum ('accepted', 'pending', 'rejected') NOT NULL DEFAULT 'pending'`,
    );
  }
}
