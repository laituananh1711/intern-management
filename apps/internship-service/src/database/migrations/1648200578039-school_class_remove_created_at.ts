import { MigrationInterface, QueryRunner } from 'typeorm';

export class schoolClassRemoveCreatedAt1648200578039
  implements MigrationInterface
{
  name = 'schoolClassRemoveCreatedAt1648200578039';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP COLUMN \`created_at\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP COLUMN \`updated_at\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`partners\` ADD \`email\` varchar(255) NOT NULL DEFAULT ''`,
    );
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` ADD \`org_email\` varchar(255) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` ADD \`personal_email\` varchar(255) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`status\` \`status\` enum ('WAITING', 'PASSED', 'FAILED', 'SELECTED') NOT NULL DEFAULT 'WAITING'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`internship_type\` \`internship_type\` enum ('ASSOCIATE', 'OTHER') NOT NULL COMMENT 'TRUE if this partner was an associate at the time of creation.'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`internship_type\` \`internship_type\` enum ('associate', 'other') NOT NULL COMMENT 'TRUE if this partner was an associate at the time of creation.'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`status\` \`status\` enum ('waiting', 'passed', 'failed', 'selected') NOT NULL DEFAULT 'waiting'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` DROP COLUMN \`personal_email\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` DROP COLUMN \`org_email\``,
    );
    await queryRunner.query(`ALTER TABLE \`partners\` DROP COLUMN \`email\``);
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`,
    );
  }
}
