import { MigrationInterface, QueryRunner } from 'typeorm';

export class termStudentAddSelectedPartner1648348714443
  implements MigrationInterface
{
  name = 'termStudentAddSelectedPartner1648348714443';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_students\` ADD \`selected_partner_id\` int UNSIGNED NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_students\` ADD CONSTRAINT \`FK_d466e1bb72f1ceb5b01429bca24\` FOREIGN KEY (\`term_id\`, \`selected_partner_id\`) REFERENCES \`term_partners\`(\`term_id\`,\`partner_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_students\` DROP FOREIGN KEY \`FK_d466e1bb72f1ceb5b01429bca24\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_students\` DROP COLUMN \`selected_partner_id\``,
    );
  }
}
