import { MigrationInterface, QueryRunner } from 'typeorm';

export class studentsAddUserId1648201299319 implements MigrationInterface {
  name = 'studentsAddUserId1648201299319';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`user_id\` int UNSIGNED NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`students\` DROP COLUMN \`user_id\``);
  }
}
