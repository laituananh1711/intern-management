import { MigrationInterface, QueryRunner } from 'typeorm';

export class postAddJobCount1648365108734 implements MigrationInterface {
  name = 'postAddJobCount1648365108734';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`recruitment_posts\` ADD \`job_count\` int UNSIGNED NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`recruitment_posts\` DROP COLUMN \`job_count\``,
    );
  }
}
