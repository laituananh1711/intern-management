import { MigrationInterface, QueryRunner } from 'typeorm';

export class termPartnerRegistrationCascadeDeleteStudents1648717125271
  implements MigrationInterface
{
  name = 'termPartnerRegistrationCascadeDeleteStudents1648717125271';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP FOREIGN KEY \`FK_260e8ac73f867e15ddb02ff5b7d\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`internship_type\` \`internship_type\` enum ('ASSOCIATE', 'OTHER') NOT NULL COMMENT 'ASSOCIATE if this partner is an associate at the time of this registration'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD CONSTRAINT \`FK_260e8ac73f867e15ddb02ff5b7d\` FOREIGN KEY (\`student_id\`, \`term_id\`) REFERENCES \`term_students\`(\`student_id\`,\`term_id\`) ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` DROP FOREIGN KEY \`FK_260e8ac73f867e15ddb02ff5b7d\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` CHANGE \`internship_type\` \`internship_type\` enum ('ASSOCIATE', 'OTHER') NOT NULL COMMENT 'TRUE if this partner was an associate at the time of creation.'`,
    );
    await queryRunner.query(
      `ALTER TABLE \`term_partner_registrations\` ADD CONSTRAINT \`FK_260e8ac73f867e15ddb02ff5b7d\` FOREIGN KEY (\`student_id\`, \`term_id\`) REFERENCES \`term_students\`(\`student_id\`,\`term_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
