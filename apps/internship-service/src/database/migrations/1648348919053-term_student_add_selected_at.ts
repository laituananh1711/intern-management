import { MigrationInterface, QueryRunner } from 'typeorm';

export class termStudentAddSelectedAt1648348919053
  implements MigrationInterface
{
  name = 'termStudentAddSelectedAt1648348919053';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_students\` ADD \`selected_at\` timestamp NULL COMMENT 'When did this student select his partner?'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`term_students\` DROP COLUMN \`selected_at\``,
    );
  }
}
