import { MigrationInterface, QueryRunner } from 'typeorm';

export class changePartnerTermsName1629728905442 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(
      'ALTER TABLE partner_internship_terms RENAME TO term_partners',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(
      'ALTER TABLE term_partners RENAME TO partner_internship_terms',
    );
  }
}
