import { MigrationInterface, QueryRunner } from 'typeorm';

export class changeSchoolClassAndRemoveMajor1651235143624
  implements MigrationInterface
{
  name = 'changeSchoolClassAndRemoveMajor1651235143624';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` CHANGE \`major_id\` \`program_name\` int UNSIGNED NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP COLUMN \`program_name\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD \`program_name\` varchar(255) NOT NULL DEFAULT ''`,
    );
    await queryRunner.query('DROP TABLE `majors`');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `majors` (`id` int UNSIGNED NOT NULL, `name` varchar(255) NOT NULL COMMENT 'ex. Máy tính và Robot', PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP COLUMN \`program_name\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD \`program_name\` int UNSIGNED NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` CHANGE \`program_name\` \`major_id\` int UNSIGNED NOT NULL`,
    );
  }
}
