import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerStudentLecturerAddUserId1630143064114
  implements MigrationInterface
{
  name = 'partnerStudentLecturerAddUserId1630143064114';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `students` ADD `user_id` int UNSIGNED NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD `user_id` int UNSIGNED NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` ADD `user_id` int UNSIGNED NULL',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE `partners` DROP COLUMN `user_id`');
    await queryRunner.query('ALTER TABLE `lecturers` DROP COLUMN `user_id`');
    await queryRunner.query('ALTER TABLE `students` DROP COLUMN `user_id`');
  }
}
