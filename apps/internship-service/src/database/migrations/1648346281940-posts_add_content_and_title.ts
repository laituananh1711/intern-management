import { MigrationInterface, QueryRunner } from 'typeorm';

export class postsAddContentAndTitle1648346281940
  implements MigrationInterface
{
  name = 'postsAddContentAndTitle1648346281940';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`recruitment_posts\` ADD \`title\` varchar(500) NOT NULL DEFAULT ''`,
    );
    await queryRunner.query(
      `ALTER TABLE \`recruitment_posts\` ADD \`content\` varchar(4000) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`recruitment_posts\` DROP COLUMN \`content\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`recruitment_posts\` DROP COLUMN \`title\``,
    );
  }
}
