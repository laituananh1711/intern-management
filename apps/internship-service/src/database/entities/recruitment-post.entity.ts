import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PartnerContact } from './partner-contact.entity';
import { Partner } from './partner.entity';
import { TermPartner } from './term-partner.entity';

@Entity('recruitment_posts')
export class RecruitmentPost {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Index('idx_recruitment_posts_title', {
    fulltext: true,
  })
  @Column({ type: 'varchar', length: '500', default: '' })
  title?: string;

  @Column({ type: 'varchar', length: '4000', default: '' })
  content?: string;

  @Column({ type: 'int', unsigned: true })
  jobCount?: number;

  @Column({ type: 'int', unsigned: true })
  termId?: number;

  @Column({ type: 'int', unsigned: true })
  partnerId?: number;

  @Column({ type: 'int', unsigned: true })
  partnerContactId?: number;

  @Column({ type: 'timestamp' })
  startRegAt?: Date;

  @Column({ type: 'timestamp' })
  endRegAt?: Date;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @ManyToOne(() => TermPartner)
  @JoinColumn({ name: 'term_id', referencedColumnName: 'termId' })
  @JoinColumn({ name: 'partner_id', referencedColumnName: 'partnerId' })
  termPartner?: TermPartner;

  @ManyToOne(() => PartnerContact)
  @JoinColumn({ name: 'partner_contact_id', referencedColumnName: 'id' })
  @JoinColumn({ name: 'partner_id', referencedColumnName: 'partnerId' })
  partnerContact?: PartnerContact;

  @ManyToOne(() => Partner, { createForeignKeyConstraints: false })
  partner?: Partner;
}
