import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Organization } from './organization.entity';
import { SchoolClass } from './school-class.entity';

@Entity('students')
export class Student {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'char',
    length: '8',
    comment: 'A 8-digit id number provided by the school (i.e. 18020001)',
  })
  studentIdNumber?: string;

  @Index('idx_students_full_name', {
    fulltext: true,
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  fullName?: string;

  @Column({
    type: 'varchar',
    nullable: true,
    length: '1000',
  })
  resumeUrl?: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  phoneNumber?: string;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: true,
  })
  schoolClassId?: number;

  @ManyToOne(() => SchoolClass, { createForeignKeyConstraints: false })
  schoolClass?: SchoolClass;

  @Column({
    type: 'varchar',
  })
  orgEmail?: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  personalEmail?: string;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: true,
  })
  userId?: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @Column({ type: 'int', unsigned: true })
  organizationId?: number;

  @ManyToOne(() => Organization, (org) => org.students, {
    createForeignKeyConstraints: false,
  })
  organization?: Organization;

  constructor(student: Partial<Student>) {
    Object.assign(this, student);
  }
}
