import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { Organization } from './organization.entity';
import { TermLecturer } from './term-lecturer.entity';

@Entity('lecturers')
export class Lecturer {
  @PrimaryColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({ type: 'int', unsigned: true, nullable: true })
  userId?: number;

  @Index('idx_lecturers_full_name', { fulltext: true })
  @Column({
    type: 'varchar',
    length: '255',
  })
  fullName?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  orgEmail?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  personalEmail?: string;

  @Column({
    type: 'varchar',
    nullable: false,
    default: '',
  })
  phoneNumber?: string;

  @Index('idx_lecturers_org_id')
  @Column({ type: 'int', unsigned: true })
  organizationId?: number;

  @ManyToOne(() => Organization, { createForeignKeyConstraints: false })
  organization?: Organization;

  @OneToMany(() => TermLecturer, (tl) => tl.lecturer)
  termLecturers?: TermLecturer[];

  constructor(lecturer: Partial<Lecturer>) {
    Object.assign(this, lecturer);
  }
}
