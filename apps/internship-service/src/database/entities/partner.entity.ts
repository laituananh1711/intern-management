import { Column, Entity, Index, OneToMany, PrimaryColumn } from 'typeorm';
import { TermPartner } from './term-partner.entity';

@Entity('partners')
export class Partner {
  @PrimaryColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({ type: 'int', unsigned: true, nullable: true })
  userId?: number;

  @Index('idx_partners_name', {
    fulltext: true,
  })
  @Column({
    type: 'varchar',
    length: '255',
    comment: 'Company name',
  })
  name?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  email?: string;

  @Column({
    type: 'varchar',
    length: '1000',
  })
  homepageUrl?: string;

  @Column({
    type: 'varchar',
    length: '1000',
  })
  logoUrl?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  phoneNumber?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  address?: string;

  @Column({
    type: 'varchar',
    length: '4000',
    default: '',
  })
  description?: string;

  @OneToMany(() => TermPartner, (tp) => tp.partner)
  termPartners?: TermPartner[];

  constructor(partner: Partial<Partner>) {
    Object.assign(this, partner);
  }
}
