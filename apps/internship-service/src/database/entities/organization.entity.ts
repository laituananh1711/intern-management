import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { Student } from './student.entity';

@Entity('organizations')
export class Organization {
  @PrimaryColumn({ type: 'int', unsigned: true })
  id?: number;

  @Column({
    type: 'varchar',
    length: '255',
  })
  name?: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @OneToMany(() => Student, (student) => student.organization)
  students?: Student[];

  constructor(org: Partial<Organization>) {
    Object.assign(this, org);
  }
}
