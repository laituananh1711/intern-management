import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { InternshipTerm } from './internship-term.entity';
import { Partner } from './partner.entity';
import { Student } from './student.entity';
import { TermLecturer } from './term-lecturer.entity';
import { TermPartnerRegistration } from './term-partner-registration.entity';
import { TermPartner } from './term-partner.entity';

export class ColumnNumericTransformer {
  to(data: number): number {
    return data;
  }
  from(data?: string): number | undefined {
    if (data == null) return data;
    return parseFloat(data);
  }
}

@Entity('term_students')
export class TermStudent {
  @PrimaryColumn({
    type: 'int',
    unsigned: true,
    nullable: false,
    name: 'term_id',
  })
  termId?: number;

  @PrimaryColumn({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  studentId?: number;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: true,
    name: 'supervisor_id',
  })
  supervisorId?: number;

  @Column({
    type: 'decimal',
    precision: 4,
    scale: 2,
    unsigned: true,
    nullable: true,
    default: null,
    transformer: new ColumnNumericTransformer(),
  })
  score?: number;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: true,
  })
  selectedPartnerId?: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @ManyToOne(() => Student)
  @JoinColumn({ name: 'student_id' })
  student?: Student;

  @ManyToOne(() => InternshipTerm)
  term?: InternshipTerm;

  @ManyToOne(() => TermLecturer)
  @JoinColumn([
    { name: 'term_id', referencedColumnName: 'termId' },
    { name: 'supervisor_id', referencedColumnName: 'lecturerId' },
  ])
  termLecturer?: TermLecturer;

  @ManyToOne(() => TermPartner)
  @JoinColumn({
    name: 'selected_partner_id',
    referencedColumnName: 'partnerId',
  })
  @JoinColumn({ name: 'term_id', referencedColumnName: 'termId' })
  selectedTermPartner?: TermPartner;

  @Column({
    type: 'timestamp',
    nullable: true,
    comment: 'When did this student select his partner?',
  })
  selectedAt?: Date;

  @Column({
    type: 'varchar',
    length: '1000',
    default: '',
  })
  reportFileName?: string;

  @OneToMany(() => TermPartnerRegistration, (tpr) => tpr.termStudent)
  termPartnerRegistrations?: TermPartnerRegistration[];

  // // the relationship is as followed:
  // // term_students -> term_lecturers -> lecturers
  // //
  // // We don't need a foreign key here because term_students have
  // // a foreign key constraint with term_lecturers, which in turn have foreign key
  // // contstraint with `lecturers`.
  // @ManyToOne(() => Lecturer, { createForeignKeyConstraints: false })
  // @JoinColumn({ name: 'supervisor_id' })
  // supervisor?: Lecturer;

  @ManyToOne(() => Partner, { createForeignKeyConstraints: false })
  @JoinColumn({ name: 'selected_partner_id' })
  selectedPartner?: Partner;

  constructor(termStudent: Partial<TermStudent>) {
    Object.assign(this, termStudent);
  }
}
