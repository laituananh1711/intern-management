import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { InternshipTerm } from './internship-term.entity';
import { Lecturer } from './lecturer.entity';
import { TermStudent } from './term-student.entity';

/**
 * Lecturer - Internship Term Mapping
 */
@Entity('term_lecturers')
export class TermLecturer {
  @PrimaryColumn({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  termId?: number;

  @PrimaryColumn({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  lecturerId?: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @ManyToOne(() => Lecturer)
  lecturer?: Lecturer;

  @OneToMany(() => TermStudent, (s) => s.termLecturer)
  supervisedStudents?: TermStudent[];

  @ManyToOne(() => InternshipTerm)
  term?: InternshipTerm;
}
