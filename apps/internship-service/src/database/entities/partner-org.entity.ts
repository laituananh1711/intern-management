import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';
import { Organization } from './organization.entity';
import { Partner } from './partner.entity';

@Entity('partner_organizations')
export class PartnerOrganization {
  @PrimaryColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Index('idx_partner_orgs_partner_id')
  @Column({
    type: 'int',
    unsigned: true,
  })
  partnerId?: number;

  @Index('idx_partner_orgs_org_id')
  @Column({
    type: 'int',
    unsigned: true,
  })
  organizationId?: number;

  @Column({ type: 'date', nullable: true })
  expiredDate?: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @ManyToOne(() => Partner, { createForeignKeyConstraints: false })
  partner?: Partner;

  @ManyToOne(() => Organization, { createForeignKeyConstraints: false })
  organization?: Organization;
}
