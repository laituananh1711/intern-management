import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Organization } from './organization.entity';
import { TermLecturer } from './term-lecturer.entity';
import { TermPartner } from './term-partner.entity';
import { TermStudent } from './term-student.entity';

@Entity('internship_terms')
export class InternshipTerm {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({ type: 'year' })
  year?: number;

  @Column({ type: 'tinyint', unsigned: true })
  term?: number;

  @Column({
    type: 'timestamp',
    comment: 'students can not join this term before this timestamp.',
  })
  startRegAt?: Date;

  @Column({
    type: 'timestamp',
    comment: 'students can not join this term after this timestamp.',
  })
  endRegAt?: Date;

  @Column({
    type: 'date',
  })
  startDate?: string;

  @Column({
    type: 'date',
  })
  endDate?: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @Column({
    type: 'int',
    unsigned: true,
  })
  organizationId?: number;

  @ManyToOne(() => Organization)
  organization?: Organization;

  @OneToMany(() => TermLecturer, (tl) => tl.term)
  termLecturers?: TermLecturer[];

  @OneToMany(() => TermPartner, (tp) => tp.term)
  termPartners?: TermPartner[];

  @OneToMany(() => TermStudent, (ts) => ts.term)
  termStudents?: TermStudent[];

  numOfTermStudents?: number;
  numOfAcceptedPartners?: number;

  constructor(term: Partial<InternshipTerm>) {
    Object.assign(this, term);
  }
}
