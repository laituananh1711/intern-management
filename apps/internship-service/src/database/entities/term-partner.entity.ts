import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { InternshipTerm } from './internship-term.entity';
import { Partner } from './partner.entity';

export enum PartnerInternshipStatus {
  ACCEPTED = 'ACCEPTED',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
}

@Entity('term_partners')
export class TermPartner {
  @PrimaryColumn({ type: 'int', unsigned: true, nullable: false })
  termId?: number;

  @PrimaryColumn({ type: 'int', unsigned: true, nullable: false })
  partnerId?: number;

  @Column({
    type: 'enum',
    enum: PartnerInternshipStatus,
    default: PartnerInternshipStatus.PENDING,
  })
  status?: PartnerInternshipStatus;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @ManyToOne(() => Partner)
  @JoinColumn({ name: 'partner_id' })
  partner?: Partner;

  // NOTE: we need to populate this field ourselves because
  // TypeORM is stupid.
  numOfAppliedStudents?: number;

  @ManyToOne(() => InternshipTerm)
  @JoinColumn({ name: 'term_id' })
  term?: InternshipTerm;
}
