import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('school_classes')
export class SchoolClass {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  name?: string;

  @Column({
    type: 'varchar',
    nullable: false,
    default: '',
  })
  programName?: string;

  constructor(obj: Partial<SchoolClass>) {
    Object.assign(this, obj);
  }
}
