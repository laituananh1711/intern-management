import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import {
  InternshipType as PbIType,
  RegistrationStatus as PbRegistrationStatus,
  Term,
} from '../../../../../libs/pb/internship_service/service.pb';
import { InternshipTerm } from './internship-term.entity';
import { TermPartner } from './term-partner.entity';
import { TermStudent } from './term-student.entity';

export enum RegistrationStatus {
  WAITING = 'WAITING',
  PASSED = 'PASSED',
  FAILED = 'FAILED',
  SELECTED = 'SELECTED',
}

export const RegistrationStatusMethods = {
  fromPb(pb: PbRegistrationStatus): RegistrationStatus {
    switch (pb) {
      case PbRegistrationStatus.SELECTED:
        return RegistrationStatus.SELECTED;
      case PbRegistrationStatus.WAITING:
        return RegistrationStatus.WAITING;
      case PbRegistrationStatus.PASSED:
        return RegistrationStatus.PASSED;
      case PbRegistrationStatus.FAILED:
        return RegistrationStatus.FAILED;
      default:
        throw new Error('registration status value not exist: ' + pb);
    }
  },
};

export enum InternshipType {
  ASSOCIATE = 'ASSOCIATE',
  OTHER = 'OTHER',
}

export const InternshipTypeMethods = {
  fromPb(pb: PbIType): InternshipType {
    switch (pb) {
      case PbIType.ASSOCIATE:
        return InternshipType.ASSOCIATE;
      case PbIType.OTHER:
        return InternshipType.OTHER;
      default:
        throw new Error('internship type value not exist' + pb);
    }
  },
};

@Entity('term_partner_registrations')
@Unique(['termId', 'partnerId', 'studentId'])
export class TermPartnerRegistration {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  termId?: number;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  partnerId?: number;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  studentId?: number;

  @Column({
    type: 'boolean',
    default: false,
    comment: 'Did this student register this company by himself?',
  })
  isSelfRegistered?: boolean;

  @Column({
    type: 'enum',
    enum: RegistrationStatus,
    default: RegistrationStatus.WAITING,
  })
  status?: RegistrationStatus;

  @Column({
    type: 'bool',
    default: false,
    comment: 'Has a mail been sent to the student about this registration?',
  })
  // will be set to FALSE when status changed.
  isMailSent?: boolean;

  @Column({
    type: 'enum',
    enum: InternshipType,
    comment:
      'ASSOCIATE if this partner is an associate at the time of this registration',
  })
  internshipType?: InternshipType;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @ManyToOne(() => TermPartner)
  @JoinColumn({ name: 'term_id', referencedColumnName: 'termId' })
  @JoinColumn({ name: 'partner_id', referencedColumnName: 'partnerId' })
  termPartner?: TermPartner;

  @ManyToOne(() => TermStudent, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'term_id', referencedColumnName: 'termId' })
  @JoinColumn({ name: 'student_id', referencedColumnName: 'studentId' })
  termStudent?: TermStudent;

  constructor(obj: Partial<TermPartnerRegistration>) {
    Object.assign(this, obj);
  }
}
