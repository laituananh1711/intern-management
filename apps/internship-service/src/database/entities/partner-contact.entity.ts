import {
  Column,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Partner } from './partner.entity';

@Entity('partner_contacts')
export class PartnerContact {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'varchar',
    length: '255',
  })
  fullName?: string;

  @Column({ type: 'varchar', length: '20' })
  phoneNumber?: string;

  @Column({ type: 'varchar', length: '255' })
  email?: string;

  @Column({
    type: 'int',
    unsigned: true,
  })
  partnerId?: number;

  @ManyToOne(() => Partner, { createForeignKeyConstraints: false })
  partner?: Partner;

  @DeleteDateColumn()
  deletedAt?: Date;

  constructor(contact: Partial<PartnerContact>) {
    Object.assign(this, contact);
  }
}
