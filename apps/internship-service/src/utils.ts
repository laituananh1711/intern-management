import * as crypto from 'crypto';

import { FILE_SEPARATOR, MINIO_TEMP_FOLDER } from './constants';
const getRandomString = (length = 4) => {
  return crypto.randomBytes(length).toString('hex');
};

const getCurrentTimestamp = () => {
  const curDate = new Date().toISOString();
  return curDate.replace(/[^0-9]/g, '');
};

export const getMinioTempObjectName = (
  fileName: string,
  extname: string,
): string => {
  if (fileName === '') {
    fileName = 'file';
  }
  fileName =
    [fileName, getCurrentTimestamp(), getRandomString()].join('_') +
    '.' +
    extname;
  return [MINIO_TEMP_FOLDER, fileName].join(FILE_SEPARATOR);
};
