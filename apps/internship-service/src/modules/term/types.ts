import {
  InternshipType,
  RegistrationStatus,
} from '../../database/entities/term-partner-registration.entity';
import { PartnerInternshipStatus } from '../../database/entities/term-partner.entity';

export type FindStudentApplicationsFilterOption = {
  internshipType?: InternshipType;
  partnerId?: number;
  status?: RegistrationStatus;
  isMailSent?: boolean;
  termId?: number;
};

export type FindStudentApplicationsSortOption = {
  fullName?: 'ASC' | 'DESC';
  studentIdNumber?: 'ASC' | 'DESC';
};

export type FindStudentApplicationsOptions = {
  page: number;
  perPage: number;
  q: string;
  sort: FindStudentApplicationsSortOption;
  filter?: FindStudentApplicationsFilterOption;
};

export class AssignSupervisorEvent {
  supervisorId: number;
  studentIds: number[];

  constructor(supervisorId: number, studentIds: number[]) {
    this.supervisorId = supervisorId;
    this.studentIds = studentIds;
  }
}

export class PartnerStatusChangedEvent {
  termId: number;
  partnerId: number;
  oldStatus: PartnerInternshipStatus;
  newStatus: PartnerInternshipStatus;

  constructor(
    termId: number,
    partnerId: number,
    oldStatus: PartnerInternshipStatus,
    newStatus: PartnerInternshipStatus,
  ) {
    this.termId = termId;
    this.partnerId = partnerId;
    this.oldStatus = oldStatus;
    this.newStatus = newStatus;
  }
}

export class PartnerRegistrationChangedEvent {
  termId: number;
  registrationId: number;
  oldStatus: RegistrationStatus;
  newStatus: RegistrationStatus;

  constructor(
    termId: number,
    registrationId: number,
    oldStatus: RegistrationStatus,
    newStatus: RegistrationStatus,
  ) {
    this.termId = termId;
    this.registrationId = registrationId;
    this.oldStatus = oldStatus;
    this.newStatus = newStatus;
  }
}

export class BatchPartnerRegistrationChangedEvent {
  registrationIds: number[];
  oldStatus: RegistrationStatus;
  newStatus: RegistrationStatus;

  constructor(
    registrationIds: number[],
    oldStatus: RegistrationStatus,
    newStatus: RegistrationStatus,
  ) {
    this.registrationIds = registrationIds;
    this.oldStatus = oldStatus;
    this.newStatus = newStatus;
  }
}

export class NewPartnerRequestAcceptedEvent {
  requesterStudentId: number;
  resolvedPartnerId: number;

  constructor(studentId: number, resolvedPartnerId: number) {
    this.requesterStudentId = studentId;
    this.resolvedPartnerId = resolvedPartnerId;
  }
}

export class TermSignUpEvent {
  studentId: number;
  termId: number;

  constructor(studentId: number, termId: number) {
    this.studentId = studentId;
    this.termId = termId;
  }
}

export class SupervisorAssignedEvent {
  termId: number;
  supervisorId: number;
  studentIds: number[];

  constructor(termId: number, supervisorId: number, studentIds: number[]) {
    this.termId = termId;
    this.supervisorId = supervisorId;
    this.studentIds = studentIds;
  }
}

export class ScoreGivenEvent {
  termId: number;
  studentId: number;
  score: number;

  constructor(termId: number, studentId: number, score: number) {
    this.termId = termId;
    this.studentId = studentId;
    this.score = score;
  }
}
