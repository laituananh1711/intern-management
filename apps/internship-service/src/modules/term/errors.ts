import { status } from '@grpc/grpc-js';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { ErrorResponse } from '@app/utils';

export const termNotInOrgError = new UnauthorizedException(
  'message.error.term_not_in_org',
);

export const errPostNotInTerm = (e: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_POST_NOT_IN_TERM',
    error: e,
  });

export const errPartnerNotInTerm = (e: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_PARTNER_NOT_IN_TERM',
    error: e,
  });

export const errStudentNotInTerm = (e: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_STUDENT_NOT_IN_TERM',
    error: e,
  });

export const errPartnerIsRejected = new RpcException({
  code: status.INVALID_ARGUMENT,
  message: 'ERR_PARTNER_IS_REJECTED',
});

export const errPartnerOrTermNotExist = (e: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_PARTNER_OR_TERM_NOT_EXISTS',
    error: e,
  });

export const errAlreadyRegistered = (e: Error) =>
  new RpcException({
    code: status.ALREADY_EXISTS,
    message: 'ERR_ALREADY_REGISTERED',
    error: e,
  });

export const errContactNotBelongToPartner = (e: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_CONTACT_NOT_BELONG_TO_PARTNER',
    error: e,
  });

export const errLecturerNotInTerm = (e: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_LECTURER_NOT_IN_TERM',
    error: e,
  });

export const errFailedToUpdateSupervisor = new Error(
  'error while updating supervisor assignments',
);

export const errTermNotExist = (e: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_TERM_NOT_EXISTS',
    error: e,
  });

export const errTermNotExistOrStudentNotInTerm = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_TERM_NOT_EXISTS_OR_STUDENT_NOT_IN_TERM',
    error: e,
  });

export const errFailToUpdateScore = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_FAIL_TO_UPDATE_SCORE',
    error: e,
  });

export const errFailToSelectPartner = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_FAIL_TO_SELECT_PARTNER',
    error: e,
  });

export const errRegistrationPeriodExpired = new RpcException({
  code: status.INVALID_ARGUMENT,
  message: 'ERR_REGISTRATION_PERIOD_EXPIRED',
});

export const errPostRegPeriodExpired = new RpcException({
  code: status.INVALID_ARGUMENT,
  message: 'ERR_POST_REGISTRATION_PERIOD_EXPIRED',
});

export const errPartnerAlreadySelected = new RpcException({
  code: status.ALREADY_EXISTS,
  message: 'ERR_PARTNER_ALREADY_SELECTED',
});

export const errFailedToChangePartnerStatus = (e?: Error) => {
  return new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_FAILED_TO_CHANGE_PARTNER_STATUS',
    error: e,
  });
};

export const errFailedToUpdateRegistrationStatus = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_FAILED_TO_UPDATE_REG_STATUS',
    error: e,
  });

export const errFailedToBatchAddTermLecturers = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_FAILED_TO_BATCH_ADD_TERM_LECTURERS',
    error: e,
  });

export const errFailedToBatchAddTermPartners = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_FAILED_TO_BATCH_ADD_TERM_PARTNERS',
    error: e,
  });

export const errInvalidArg = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_INVALID_ARG',
    error: e,
  });

export const errPostNotExistedOrNoPermission = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_POST_NOT_EXISTED_OR_NO_PERMISSION',
    error: e,
  });

export const errTermNotExistedOrIsExpired = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_TERM_NOT_EXISTED_OR_EXPIRED',
    error: e,
  });

export const errStudentNotFoundOrNotInTerm = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_STUDENT_NOT_FOUND_OR_NOT_IN_TERM',
    error: e,
  });

export const errMaxFileSizeExceeded = new BadRequestException(
  new ErrorResponse(status.INVALID_ARGUMENT, 'ERR_MAX_FILE_SIZE_EXCEEDED'),
);

export const errInvalidFileType = (
  requiredMimeType: string,
  actualMimeType: string,
) =>
  new BadRequestException(
    new ErrorResponse(
      status.INVALID_ARGUMENT,
      'ERR_INVALID_FILE_TYPE',
      undefined,
      `error invalid type received: ${actualMimeType}, need ${requiredMimeType}`,
    ),
  );

export const errFileNotFound = () =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_FILE_NOT_FOUND',
  });

export const errFailedToUploadFile = (e?: Error) =>
  new InternalServerErrorException(
    new ErrorResponse(
      status.INTERNAL,
      'ERR_FAILED_TO_UPLOAD_FILE',
      undefined,
      e?.message ?? '',
    ),
  );

export const errPostNotFound = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_POST_NOT_FOUND',
    error: e,
  });
