import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Organization } from '../../database/entities/organization.entity';
import { PartnerContact } from '../../database/entities/partner-contact.entity';
import { Partner } from '../../database/entities/partner.entity';
import { RecruitmentPost } from '../../database/entities/recruitment-post.entity';
import { SchoolClass } from '../../database/entities/school-class.entity';
import { Student } from '../../database/entities/student.entity';
import { TermLecturer } from '../../database/entities/term-lecturer.entity';
import { TermPartnerRegistration } from '../../database/entities/term-partner-registration.entity';
import { TermPartner } from '../../database/entities/term-partner.entity';
import { TermStudent } from '../../database/entities/term-student.entity';
import { TermController } from './term.controller';
import { InternshipTermRepository } from './term.repository';
import { TermService } from './term.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      InternshipTermRepository,
      TermPartner,
      TermStudent,
      TermLecturer,
      RecruitmentPost,
      PartnerContact,
      TermPartnerRegistration,
      Partner,
      Organization,
      Student,
      SchoolClass,
      Lecturer,
    ]),
  ],
  controllers: [TermController],
  providers: [TermService],
  exports: [TypeOrmModule],
})
export class TermModule {}
