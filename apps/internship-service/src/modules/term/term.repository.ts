import { ResultSetHeader } from 'mysql2';
import {
  EntityRepository,
  FindOptionsWhere,
  In,
  Not,
  Repository,
  UpdateResult,
} from 'typeorm';
import { InternshipTerm } from '../../database/entities/internship-term.entity';
import { Partner } from '../../database/entities/partner.entity';
import {
  RegistrationStatus,
  TermPartnerRegistration,
} from '../../database/entities/term-partner-registration.entity';
import {
  PartnerInternshipStatus,
  TermPartner,
} from '../../database/entities/term-partner.entity';
import { TermStudent } from '../../database/entities/term-student.entity';
import { AssignStudentsDto } from './dto/assign-students.dto';
import { errFailedToUpdateSupervisor, errLecturerNotInTerm } from './errors';

@EntityRepository(InternshipTerm)
export class InternshipTermRepository extends Repository<InternshipTerm> {
  /**
   * Create new internship term and add associated partners to this term.
   * @param term new internship term
   * @returns
   */
  createTerm(term: InternshipTerm): Promise<InternshipTerm> {
    return this.manager
      .transaction(async (manager) => {
        term = await manager.save<InternshipTerm>(term);

        // add associated partners to this term
        await manager.query(
          'INSERT INTO term_partners (term_id, partner_id, status)' +
            " SELECT ?, partner_id, 'accepted' FROM partner_organizations" +
            ' WHERE organization_id = ? AND (expired_date IS NULL OR expired_date > NOW());',
          [term.id, term.organizationId],
        );

        // add all lecturers in org to internship term
        // unavailable lecturer may be removed later
        await manager.query(
          'INSERT INTO term_lecturers (term_id, lecturer_id) SELECT ?, id FROM lecturers WHERE organization_id = ?',
          [term.id, term.organizationId],
        );
      })
      .then(() => term);
  }

  getPendingPartners(termId: number): Promise<Partner[]> {
    return this.manager
      .createQueryBuilder(Partner, 'p')
      .select('p.id, p.name, p.homepage_url, p.logo_url')
      .innerJoin(TermPartner, 'pit', 'pit.partner_id = p.id')
      .where('pit.term_id = :termId', {
        termId,
      })
      .andWhere('pit.status = :status', {
        status: PartnerInternshipStatus.PENDING,
      })
      .getMany();
  }

  /**
   * Update partner status from PENDING to ACCEPTED or REJECTED.
   * Accepted or rejected partners cannot be changed.
   * @param partnerId id of partner
   * @param termId id of internship term
   * @param status partner status to be updated
   * @returns
   */
  changePartnerTermStatus(
    partnerId: number,
    termId: number,
    status: PartnerInternshipStatus,
  ): Promise<UpdateResult> {
    return this.manager.update(
      TermPartner,
      {
        termId,
        partnerId,
        status: PartnerInternshipStatus.PENDING,
      },
      {
        status,
      },
    );
  }

  /**
   * Check if an internship term belong to an org.
   * @param termId id of internship term
   * @param orgId id of organization
   * @returns true if this term belongs to this org, false otherwise.
   */
  isTermBelongToOrgs(termId: number, orgIds: number[]): Promise<boolean> {
    return this.manager
      .createQueryBuilder(InternshipTerm, 'pit')
      .where('id = :termId AND organization_id IN (:orgIds)', {
        termId,
        orgIds,
      })
      .getOne()
      .then((val) => {
        return val != null;
      });
  }

  createPartnerRegistration({
    termId,
    studentId,
    partnerId,
    status,
    isSelfRegistered,
  }: PartnerRegistration): Promise<TermPartnerRegistration> {
    return this.manager.save(
      new TermPartnerRegistration({
        termId,
        studentId,
        partnerId,
        status,
        isSelfRegistered,
      }),
    );
  }

  /**
   * Check if students can register a partner in an internship term
   * @param termId id of internship term
   * @param partnerId id of partner
   * @deprecated
   * @returns true if students can join this partner
   */
  isPartnerRegistrable(termId: number, partnerId: number): Promise<boolean> {
    return this.manager
      .findOne(TermPartner, {
        where: {
          termId,
          partnerId,
          status: Not<PartnerInternshipStatus.REJECTED>(
            PartnerInternshipStatus.REJECTED,
          ),
        },
      })
      .then((val) => val != null);
  }

  getTermPartnerById(
    termId: number,
    partnerId: number,
  ): Promise<TermPartner | null> {
    return this.manager.findOne(TermPartner, {
      where: {
        termId,
        partnerId,
      },
    });
  }

  insertParterToTermAsPending(
    termId: number,
    partnerId: number,
  ): Promise<void> {
    return this.manager.query(
      'INSERT INTO term_partners (term_id, partner_id, status) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE term_id=term_id',
      [termId, partnerId, PartnerInternshipStatus.PENDING],
    );
  }

  getResolvedPartner(
    termId: number,
    studentId: number,
  ): Promise<TermPartnerRegistration | null> {
    return this.manager.findOne(TermPartnerRegistration, {
      where: {
        termId,
        studentId,
        status: RegistrationStatus.SELECTED,
      },
    });
  }

  /**
   * Update term_partner_registration status from PASSED to SELECTED if and only if
   * term_partner is ACCEPTED.
   * @param termId
   * @param partnerId
   * @param studentId
   * @returns
   */
  resolvePartner(
    termId: number,
    partnerId: number,
    studentId: number,
  ): Promise<ResultSetHeader> {
    // TODO: limit the period during which a student can update their selected company
    return this.manager.query(
      'update term_partner_registrations tpr inner join term_partners \
      tp on tpr.partner_id = tp.partner_id and tpr.term_id = tp.term_id\
      set tpr.status = ? where tpr.term_id = ? and tpr.partner_id = ?\
      and tpr.student_id = ? and tpr.status = ? and tp.status = ?;',
      [
        RegistrationStatus.SELECTED,
        termId,
        partnerId,
        studentId,
        RegistrationStatus.PASSED,
        PartnerInternshipStatus.ACCEPTED,
      ],
    );
  }

  updateScore(
    termId: number,
    studentId: number,
    score: number,
    supervisorId?: number,
  ) {
    const whereClause: FindOptionsWhere<TermStudent> = { termId, studentId };
    if (supervisorId != null) {
      whereClause.supervisorId = supervisorId;
    }

    return this.manager.update(TermStudent, whereClause, { score });
  }

  /**
   * Assign students to an advisor.
   * Data consistency is preserved since `term_students` has a foreign key to `term_lecturers`.
   * @param termId id of internship term
   * @param dtos list of lecturers and their students
   */
  assignLecturersToStudents(
    termId: number,
    dtos: AssignStudentsDto[],
  ): Promise<{ supervisorId: number; studentIds: number[] }[]> {
    return this.manager.transaction(async (manager) => {
      const updateQueries = [];

      for (const dto of dtos) {
        if (dto.studentIds.length === 0) {
          continue;
        }

        const termStudents = await manager.find(TermStudent, {
          select: ['studentId'],
          where: {
            termId,
            studentId: In(dto.studentIds),
          },
        });

        if (termStudents.length === 0) {
          throw errFailedToUpdateSupervisor;
        }

        const studentIds = termStudents.map((ts) => ts.studentId!);

        // TODO: prevent assignment if students already have a supervisor?
        updateQueries.push(
          manager
            .query(
              'UPDATE term_students SET supervisor_id = ?\
          WHERE term_id = ? AND student_id IN (?)',
              [dto.supervisorId, termId, studentIds],
            )
            .then((res) => {
              // TODO: may need to add additional queries
              // to figure out which students got changed.
              if (res.affectedRows === 0) {
                throw errFailedToUpdateSupervisor;
              }

              return {
                supervisorId: dto.supervisorId,
                studentIds,
              };
            })
            .catch((e) => {
              if (e === errFailedToUpdateSupervisor) {
                throw e;
              }
              throw errLecturerNotInTerm(e);
            }),
        );
      }

      const updateResults = await Promise.all(updateQueries);
      return updateResults;
    });
  }
}

interface PartnerRegistration {
  termId: number;
  studentId: number;
  partnerId: number;
  status: RegistrationStatus;
  isSelfRegistered: boolean;
}
