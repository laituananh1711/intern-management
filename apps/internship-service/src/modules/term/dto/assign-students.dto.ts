import { IsInt } from 'class-validator';

export class AssignStudentsDto {
  @IsInt()
  supervisorId!: number;

  @IsInt({ each: true })
  studentIds!: number[];
}
