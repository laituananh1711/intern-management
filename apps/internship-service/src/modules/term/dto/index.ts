import { OffsetPagingQuery } from '@app/paginate';
import {
  InternshipType,
  RegistrationStatus,
} from '../../../database/entities/term-partner-registration.entity';
import { PartnerInternshipStatus } from '../../../database/entities/term-partner.entity';

export * from './assign-students.dto';
export * from './change-partner-status.dto';
export * from './create-term.dto';

export class GetTermStudentsSortQuery {
  id?: 'ASC' | 'DESC';
  fullName?: 'ASC' | 'DESC';
  studentIdNumber?: 'ASC' | 'DESC';
}

export class GetTermStudentsFilterQuery {
  partnerSelected?: boolean;
  termId?: number;
  supervisorId?: number;
  scoreGiven?: boolean;
  reportSubmitted?: boolean;
  supervisorAssigned?: boolean;
  selectedPartnerId?: number;
}

export class ExportTermStudentsQuery {
  q = '';
  sort: GetTermStudentsSortQuery = {};
  filter?: GetTermStudentsFilterQuery = {};
}

export class GetTermStudentsQuery extends OffsetPagingQuery {
  q = '';
  sort: GetTermStudentsSortQuery = {};
  filter?: GetTermStudentsFilterQuery = {};
}

export class GetTermLecturersFilterQuery {
  termId?: number;
}

export class GetTermLecturersSortQuery {
  name?: 'ASC' | 'DESC';
}

export class GetTermLecturersQuery extends OffsetPagingQuery {
  q = '';
  sort?: GetTermLecturersSortQuery = {};
  filter?: GetTermLecturersFilterQuery = {};
}

export class GetTermPartnersSortQuery {
  name?: 'ASC' | 'DESC';
  joinedAt?: 'ASC' | 'DESC';
}

export class GetTermPartnersFilterQuery {
  status?: PartnerInternshipStatus;
  termId?: number;
}

export class ExportTermPartnersQuery {
  q = '';
  sort?: GetTermPartnersSortQuery;
  filter?: GetTermPartnersFilterQuery;
}

export class GetTermPartnersQuery extends OffsetPagingQuery {
  q = '';
  sort?: GetTermPartnersSortQuery;
  filter?: GetTermPartnersFilterQuery;
}

export class GetPostSortQuery {
  title?: 'ASC' | 'DESC';
  createdAt?: 'ASC' | 'DESC';
}

export class GetPostFilterQuery {
  termId?: number;
  partnerId?: number;
}

export class GetPostsQuery extends OffsetPagingQuery {
  q = '';
  sort: GetPostSortQuery = {};
  filter?: GetPostFilterQuery;
}

export class GetStudentPostQuery extends OffsetPagingQuery {
  studentId!: number;
  termId!: number;
}

export class CreatePostDto {
  partnerId!: number;
  partnerContactId!: number;
  title!: string;
  content!: string;
  jobCount!: number;
  startRegAt!: Date;
  endRegAt!: Date;
  termId!: number;
}

export class UpdatePostDto {
  partnerContactId!: number;
  title!: string;
  content!: string;
  jobCount!: number;
  startRegAt!: Date;
  endRegAt!: Date;
}

export class GetTermsSortQuery {
  createdAt?: 'ASC' | 'DESC';
}

export class GetTermsFilterQuery {
  organizationId?: number;
  year?: number;
}

export class GetTermsQuery extends OffsetPagingQuery {
  sort: GetTermsSortQuery = {};
  filter?: GetTermsFilterQuery;
}

export class GetStudentApplicationsFilterQuery {
  internshipType?: InternshipType;
  partnerId?: number;
  status?: RegistrationStatus;
  isMailSent?: boolean;
  termId?: number;
}

export class GetStudentApplicationsSortQuery {
  fullName?: 'ASC' | 'DESC';
  studentIdNumber?: 'ASC' | 'DESC';
}

export class ExportStudentApplicationsQuery {
  q = '';
  sort: GetStudentApplicationsSortQuery = {};
  filter?: GetStudentApplicationsFilterQuery;
}

export class GetStudentApplicationsQuery extends OffsetPagingQuery {
  q = '';
  sort: GetStudentApplicationsSortQuery = {};
  filter?: GetStudentApplicationsFilterQuery;
}
