import {
  IsDate,
  IsDateString,
  IsIn,
  IsNumber,
  IsOptional,
} from 'class-validator';

export class CreateTermReqDto {
  @IsNumber()
  @IsOptional()
  year: number = new Date().getFullYear(); // default to current year

  @IsIn([1, 2, 3])
  term!: number;

  @IsDate()
  startRegAt!: Date;

  @IsDate()
  endRegAt!: Date;

  @IsDateString()
  startDate!: string;

  @IsDateString()
  endDate!: string;
}
