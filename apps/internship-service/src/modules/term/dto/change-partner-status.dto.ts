import { IsEnum } from 'class-validator';
import { PartnerInternshipStatus } from '../../../database/entities/term-partner.entity';

export class ChangePartnerStatusDto {
  @IsEnum(PartnerInternshipStatus)
  status!: PartnerInternshipStatus;
}
