import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Parser } from 'json2csv';
import { Client } from 'minio';
import { MINIO_CONNECTION } from 'nestjs-minio';
import {
  Brackets,
  EntityNotFoundError,
  FindOptionsWhere,
  In,
  LessThan,
  MoreThan,
  OrderByCondition,
  QueryFailedError,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import { paginate, PaginatedResults } from '../../../../../libs/paginate/src';
import { isZeroValue } from '../../../../../libs/utils/src';
import {
  events,
  EXPORT_EXPIRY_SECONDS,
  FILE_SEPARATOR,
  REPORT_FOLDER_NAME,
} from '../../constants';
import { InternshipTerm } from '../../database/entities/internship-term.entity';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { PartnerContact } from '../../database/entities/partner-contact.entity';
import { Partner } from '../../database/entities/partner.entity';
import { RecruitmentPost } from '../../database/entities/recruitment-post.entity';
import { TermLecturer } from '../../database/entities/term-lecturer.entity';
import {
  RegistrationStatus,
  TermPartnerRegistration,
} from '../../database/entities/term-partner-registration.entity';
import {
  PartnerInternshipStatus,
  TermPartner,
} from '../../database/entities/term-partner.entity';
import { TermStudent } from '../../database/entities/term-student.entity';
import { getMinioTempObjectName } from '../../utils';
import {
  CreatePostDto,
  ExportStudentApplicationsQuery,
  ExportTermPartnersQuery,
  ExportTermStudentsQuery,
  GetPostsQuery,
  GetStudentApplicationsQuery,
  GetStudentPostQuery,
  GetTermLecturersQuery,
  GetTermPartnersQuery,
  GetTermsQuery,
  GetTermStudentsQuery,
  UpdatePostDto,
} from './dto';
import { AssignStudentsDto } from './dto/assign-students.dto';
import { CreateTermReqDto } from './dto/create-term.dto';
import {
  errAlreadyRegistered,
  errContactNotBelongToPartner,
  errFailedToBatchAddTermLecturers,
  errFailedToBatchAddTermPartners,
  errFailedToChangePartnerStatus,
  errFailedToUpdateRegistrationStatus,
  errFailedToUpdateSupervisor,
  errFailToSelectPartner,
  errFailToUpdateScore,
  errFileNotFound,
  errPartnerAlreadySelected,
  errPartnerIsRejected,
  errPartnerNotInTerm,
  errPartnerOrTermNotExist,
  errPostNotExistedOrNoPermission,
  errPostNotFound,
  errPostNotInTerm,
  errPostRegPeriodExpired,
  errRegistrationPeriodExpired,
  errStudentNotFoundOrNotInTerm,
  errStudentNotInTerm,
  errTermNotExist,
  errTermNotExistedOrIsExpired,
  errTermNotExistOrStudentNotInTerm,
} from './errors';
import { InternshipTermRepository } from './term.repository';
import {
  BatchPartnerRegistrationChangedEvent,
  PartnerStatusChangedEvent,
  ScoreGivenEvent,
  SupervisorAssignedEvent,
  TermSignUpEvent,
} from './types';

const MysqlDuplicateEntryCode = 'ER_DUP_ENTRY';

@Injectable()
export class TermService {
  private internshipBucketName: string;

  // TODO: should only import 1 repository and get its manager.
  constructor(
    private termRepo: InternshipTermRepository,
    @InjectRepository(RecruitmentPost)
    private postRepo: Repository<RecruitmentPost>,
    @InjectRepository(TermPartnerRegistration)
    private studentApplicationRepo: Repository<TermPartnerRegistration>,
    @InjectRepository(TermStudent)
    private termStudentRepo: Repository<TermStudent>,
    @InjectRepository(PartnerContact)
    private partnerContactRepo: Repository<PartnerContact>,
    @InjectRepository(TermLecturer)
    private termLecturerRepo: Repository<TermLecturer>,
    @InjectRepository(TermPartner)
    private termPartnerRepo: Repository<TermPartner>,
    private eventEmitter: EventEmitter2,
    @Inject(MINIO_CONNECTION) private readonly minioClient: Client,
    private readonly configService: ConfigService,
  ) {
    this.internshipBucketName = configService.get(
      'INTERNSHIP_BUCKET_NAME',
      'internship-service',
    );
  }

  getStudentApplications({
    q,
    page,
    perPage,
    sort,
    filter,
  }: GetStudentApplicationsQuery): Promise<
    PaginatedResults<TermPartnerRegistration>
  > {
    const queryBuilder = this.buildGetStudentApplicationsQuery({
      q,
      sort,
      filter,
    });

    return paginate(queryBuilder as any, { page, perPage });
  }

  /**
   * Create a new internship terms. Associated partners and organization's
   * lecturers will automatically be added to this term.
   * Both of them can be removed later
   * @param dto
   * @returns new term
   */
  createTerm(dto: CreateTermReqDto, orgId: number): Promise<InternshipTerm> {
    return this.termRepo.createTerm(
      new InternshipTerm({
        year: dto.year,
        term: dto.term,
        startRegAt: new Date(dto.startRegAt),
        endRegAt: new Date(dto.endRegAt),
        startDate: dto.startDate,
        endDate: dto.endDate,
        organizationId: orgId,
      }),
    );
  }

  async updateTerm(
    dto: Partial<CreateTermReqDto>,
    termId: number,
    orgId?: number,
  ): Promise<void> {
    const whereClause: FindOptionsWhere<InternshipTerm> = {
      id: termId,
    };

    if (orgId != null) {
      whereClause.organizationId = orgId;
    }

    await this.termRepo.update(whereClause, dto);
  }

  getTermStudents({
    filter,
    page,
    perPage,
    q,
    sort,
  }: GetTermStudentsQuery): Promise<PaginatedResults<TermStudent>> {
    const queryBuilder = this.buildGetTermStudentsQuery({ q, sort, filter });

    return paginate(queryBuilder as any, { page, perPage });
  }

  getTermLecturers({
    page,
    perPage,
    q,
    filter,
    sort,
  }: GetTermLecturersQuery): Promise<PaginatedResults<TermLecturer>> {
    let qb = this.termLecturerRepo
      .createQueryBuilder('tl')
      .leftJoinAndSelect('tl.lecturer', 'l')
      .leftJoinAndSelect('tl.supervisedStudents', 'svs');

    if (filter != null) {
      const { termId } = filter;
      if (termId != null) {
        qb = qb.andWhere('tl.termId = :termId', { termId });
      }
    }

    if (!isZeroValue(q)) {
      qb = qb.andWhere(
        new Brackets((qb) =>
          qb
            .where(`MATCH (l.fullName) AGAINST (:q in natural language mode)`, {
              q,
            })
            .orWhere(`l.orgEmail like CONCAT('%', :orgEmail, '%')`, {
              orgEmail: q,
            }),
        ),
      );
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.name) orderBy['l.fullName'] = sort.name;

      qb = qb.orderBy(orderBy);
    }

    return paginate(qb as any, { page, perPage });
  }

  async getTermPartners({
    q,
    page,
    perPage,
    filter,
    sort,
  }: GetTermPartnersQuery): Promise<PaginatedResults<TermPartner>> {
    let qb = this.termPartnerRepo
      .createQueryBuilder('tp')
      .leftJoinAndSelect('tp.partner', 'p');

    if (!isZeroValue(q)) {
      qb = qb.andWhere(`MATCH(p.name) AGAINST (:q in natural language mode)`, {
        q,
      });
    }

    if (filter != null) {
      const { status, termId } = filter;
      if (termId != null) {
        qb = qb.andWhere('tp.termId = :termId', { termId });
      }
      if (status != null) {
        qb = qb.andWhere('tp.status = :status', { status });
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.joinedAt) orderBy['tp.createdAt'] = sort.joinedAt;
      if (sort.name) orderBy['p.name'] = sort.name;

      qb = qb.orderBy(orderBy);
    }

    return paginate(qb as any, { page, perPage });
  }

  isTermInOrgs(termId: number, orgIds: number[]): Promise<boolean> {
    return this.termRepo.isTermBelongToOrgs(termId, orgIds);
  }

  /** Add a student to an internship term.
   */
  async addTermStudent(
    termId: number,
    studentId: number,
  ): Promise<TermStudent> {
    let termStudent: TermStudent = new TermStudent({ termId, studentId });

    await this.termStudentRepo.manager.transaction(async (manager) => {
      const termDetails = await manager
        .findOneOrFail(InternshipTerm, {
          select: ['startRegAt', 'endRegAt'],
          where: {
            id: termId,
          },
        })
        .catch((e) => {
          throw errTermNotExist(e);
        });

      const { startRegAt, endRegAt } = termDetails;
      const currentTime = Date.now();

      if (
        startRegAt!.getTime() > currentTime ||
        endRegAt!.getTime() < currentTime
      ) {
        throw errRegistrationPeriodExpired;
      }

      termStudent = await manager.save<TermStudent>(termStudent);
    });

    this.eventEmitter.emit(
      events.TERM_SIGN_UP,
      new TermSignUpEvent(studentId, termId),
    );

    return termStudent;
  }

  async removeTermStudent(termId: number, studentId: number): Promise<void> {
    await this.termStudentRepo.delete({ termId, studentId });
  }

  async removeTermLecturer(termId: number, lecturerId: number): Promise<void> {
    // TODO: inform students that their lecturers has been removed?
    await this.termStudentRepo.manager.transaction(async (manager) => {
      await manager.update(
        TermStudent,
        { supervisorId: lecturerId, termId },
        {
          supervisorId: undefined,
        },
      );

      await manager.delete(TermLecturer, { termId, lecturerId });
    });
  }

  /**
   * Change partner status from pending to ACCEPTED or REJECTED.
   * @param termId id of term
   * @param partnerId id of partner
   * @param status ACCEPTED or REJECTED
   * @throws {BadRequestException} when user tried to update non-existent term or update ACCEPTED/REJECTED partner.
   * @throws {UnauthorizedException} when user tried to update term not belonging to their orgs
   */
  async acceptOrRejectPartner(
    termId: number,
    partnerId: number,
    status: PartnerInternshipStatus,
  ): Promise<void> {
    const updateResult = await this.termRepo.changePartnerTermStatus(
      partnerId,
      termId,
      status,
    );

    if (updateResult.affected === 0) {
      throw new BadRequestException(
        'message.error.partner_status_update_failed',
      );
    }
  }

  getTermsByOrgId(orgId: number) {
    return this.termRepo.find({
      where: {
        organizationId: orgId,
      },
    });
  }

  async getTerms({
    page,
    perPage,
    sort,
    filter,
  }: GetTermsQuery): Promise<PaginatedResults<InternshipTerm>> {
    let qb = this.termRepo.createQueryBuilder('t');

    if (filter != null) {
      const { organizationId, year } = filter;
      if (organizationId != null) {
        qb = qb.andWhere('t.organizationId = :orgId', {
          orgId: organizationId,
        });
      }
      if (year != null) {
        qb = qb.andWhere('t.year = :year', { year });
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.createdAt) orderBy['t.createdAt'] = sort.createdAt;

      qb = qb.orderBy(orderBy);
    }

    return await paginate(qb as any, { page, perPage }).then(async (res) => {
      const { items: terms } = res;

      const termIds: number[] = [];
      terms.map((t) => {
        termIds.push(t.id!);
      });

      // NOTE: addSelectAndMap isn't available yet, so this
      // is a sufficient workaround.
      const [studentCount, partnerCount] = await Promise.all([
        this.getTermStudentCount(termIds),
        this.getAcceptedPartnersCount(termIds),
      ]);

      for (const term of terms) {
        term.numOfTermStudents = studentCount[term.id]?.studentCount ?? 0;
        term.numOfAcceptedPartners = partnerCount[term.id]?.partnerCount ?? 0;
      }

      return res;
    });
  }

  private async getTermStudentCount(
    termIds: number[],
  ): Promise<Record<number, { termId: number; studentCount: number }>> {
    if (termIds.length === 0) {
      return {};
    }

    const counts = await this.termRepo.query(
      'SELECT term_id as termId, COUNT(student_id) as studentCount FROM term_students WHERE term_id IN (?) GROUP BY term_id',
      [termIds],
    );

    const countMapping: Record<
      number,
      { termId: number; studentCount: number }
    > = {};

    for (const cnt of counts) {
      countMapping[cnt.termId] = {
        termId: cnt.termId,
        studentCount: parseInt(cnt.studentCount),
      };
    }

    return countMapping;
  }

  private async getAcceptedPartnersCount(
    termIds: number[],
  ): Promise<Record<number, { termId: number; partnerCount: number }>> {
    if (termIds.length === 0) {
      return {};
    }

    const counts = await this.termRepo.query(
      'SELECT term_id as termId, COUNT(partner_id) as partnerCount' +
        ` FROM term_partners WHERE term_id IN (?) AND status = 'ACCEPTED' GROUP BY term_id`,
      [termIds],
    );

    const countMapping: Record<
      number,
      { termId: number; partnerCount: number }
    > = {};

    for (const cnt of counts) {
      countMapping[cnt.termId] = {
        termId: cnt.termId,
        partnerCount: parseInt(cnt.partnerCount),
      };
    }

    return countMapping;
  }

  async applyForInternship(
    termId: number,
    studentId: number,
    postId: number,
  ): Promise<TermPartnerRegistration> {
    const post = await this.postRepo
      .findOneOrFail({
        select: ['partnerId', 'endRegAt'],
        where: { id: postId, termId },
      })
      .catch((e) => {
        throw errPostNotInTerm(e);
      });

    if (post.endRegAt!.getTime() < Date.now()) {
      throw errPostRegPeriodExpired;
    }

    const partnerId = post.partnerId!;

    const termPartner = await this.termRepo.getTermPartnerById(
      termId,
      partnerId,
    );

    if (
      termPartner != null &&
      termPartner.status! === PartnerInternshipStatus.REJECTED
    ) {
      throw errPartnerIsRejected;
    }

    if (termPartner == null) {
      await this.termRepo
        .insertParterToTermAsPending(termId, partnerId)
        .catch((e) => {
          throw errPartnerOrTermNotExist(e);
        });
    }

    return await this.termRepo
      .createPartnerRegistration({
        termId,
        studentId,
        partnerId: post.partnerId!,
        isSelfRegistered: false,
        status: RegistrationStatus.WAITING,
      })
      .catch((e: QueryFailedError) => {
        if (e.driverError.code === MysqlDuplicateEntryCode) {
          throw errAlreadyRegistered(e);
        }
        throw errStudentNotInTerm(e);
      });
  }

  async selfRegisterInternship(
    termId: number,
    studentId: number,
    partnerId: number,
  ): Promise<TermPartnerRegistration> {
    // TODO: emit self internship event + send a mail to the student.
    const termPartner = await this.termRepo.getTermPartnerById(
      termId,
      partnerId,
    );

    if (
      termPartner != null &&
      termPartner.status! === PartnerInternshipStatus.REJECTED
    ) {
      throw errPartnerIsRejected;
    }

    if (termPartner == null) {
      await this.termRepo
        .insertParterToTermAsPending(termId, partnerId)
        .catch((e) => {
          throw errPartnerOrTermNotExist(e);
        });
    }

    return await this.termRepo
      .createPartnerRegistration({
        termId,
        studentId,
        partnerId,
        isSelfRegistered: true,
        status: RegistrationStatus.PASSED,
      })
      .catch((e) => {
        if (e.driverError.code === MysqlDuplicateEntryCode) {
          throw errAlreadyRegistered(e);
        }
        throw errStudentNotInTerm(e);
      });
  }

  /**
   * Students finalize which company to go to.
   * The selected company must have the status ACCEPTED in this term,
   * and the student must already be PASSED for this company.
   * @param termId id of term
   * @param studentId id of student
   * @param partnerId id of partner
   * @returns void, throw errors if cannot update
   */
  async selectFinalPartner(
    termId: number,
    studentId: number,
    partnerId: number,
  ): Promise<void> {
    const termStudent = await this.termStudentRepo
      .findOneOrFail({
        where: {
          termId,
          studentId,
        },
      })
      .catch((e) => {
        throw errTermNotExistOrStudentNotInTerm(e);
      });

    if (termStudent.selectedPartnerId != null) {
      throw errPartnerAlreadySelected;
    }

    await this.termStudentRepo.manager.transaction(async (manager) => {
      // hopefully, by the time someone else read this, TypeORM has pull their head out
      // of their asses and fix this issue.
      // https://github.com/typeorm/typeorm/issues/8228
      const regs = await manager.query(
        'SELECT tpr.status as status, tp.status as partnerStatus' +
          ' FROM term_partner_registrations tpr INNER JOIN term_partners tp' +
          ' ON tpr.term_id = tp.term_id AND tpr.partner_id = tp.partner_id' +
          ' WHERE tpr.term_id = ? AND tpr.student_id = ? AND tpr.partner_id = ? LIMIT 1',
        [termId, studentId, partnerId],
      );

      if (regs.length === 0) {
        throw errTermNotExistOrStudentNotInTerm();
      }

      const reg: {
        status: RegistrationStatus;
        partnerStatus: PartnerInternshipStatus;
      } = regs[0];

      if (
        reg.status !== RegistrationStatus.PASSED ||
        reg.partnerStatus !== PartnerInternshipStatus.ACCEPTED
      ) {
        throw errFailToSelectPartner();
      }

      const updateTermStudentPromise = manager.update(
        TermStudent,
        {
          termId,
          studentId,
        },
        {
          selectedPartnerId: partnerId,
          selectedAt: new Date(),
        },
      );

      const updateRegistrationPromise = manager.update(
        TermPartnerRegistration,
        { termId, studentId, partnerId },
        {
          status: RegistrationStatus.SELECTED,
        },
      );

      await Promise.all([updateTermStudentPromise, updateRegistrationPromise]);
    });

    return;
  }

  async updateStudentScore(
    termId: number,
    studentId: number,
    score: number,
    supervisorId?: number,
  ) {
    // TODO: should I prevent score updates if the user already have a score?
    const updatedResult = await this.termRepo
      .updateScore(termId, studentId, score, supervisorId)
      .catch((e) => {
        throw errFailToUpdateScore(e);
      });

    if (updatedResult.affected === 0) {
      throw errFailToUpdateScore();
    }

    this.eventEmitter.emit(
      events.SCORE_GIVEN,
      new ScoreGivenEvent(termId, studentId, score),
    );
  }

  assignLecturersToStudents(
    termId: number,
    dtos: AssignStudentsDto[],
  ): Promise<void> {
    return this.termRepo
      .assignLecturersToStudents(termId, dtos)
      .catch((e) => {
        if (e === errFailedToUpdateSupervisor) {
          throw errTermNotExistOrStudentNotInTerm(e);
        }
        throw e;
      })
      .then((results) => {
        for (const result of results) {
          const { supervisorId, studentIds } = result;
          this.eventEmitter.emit(
            events.SUPERVISOR_ASSIGNED,
            new SupervisorAssignedEvent(termId, supervisorId, studentIds),
          );
        }
      });
  }

  getTermPosts({
    page,
    perPage,
    q,
    sort,
    filter,
  }: GetPostsQuery): Promise<PaginatedResults<RecruitmentPost>> {
    // we are getting multiple posts at once, so we will not return the post content.
    let qb = this.postRepo
      .createQueryBuilder('p')
      .select([
        'p.id',
        'p.jobCount',
        'p.partnerContactId',
        'p.partnerId',
        'p.title',
        'p.termId',
        'p.startRegAt',
        'p.endRegAt',
        'p.createdAt',
      ])
      .innerJoinAndSelect('p.partner', 'pa')
      .innerJoinAndSelect('p.partnerContact', 'pc');

    if (filter != null) {
      const { termId, partnerId } = filter;
      if (termId != null) {
        qb = qb.andWhere('p.termId = :termId', { termId });
      }
      if (partnerId != null) {
        qb = qb.andWhere('p.partnerId = :partnerId', { partnerId });
      }
    }

    if (!isZeroValue(q)) {
      qb = qb.andWhere('MATCH(p.title) AGAINST (:q in natural language mode)', {
        q,
      });
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.createdAt != null) orderBy['p.createdAt'] = sort.createdAt;
      if (sort.title != null) orderBy['p.title'] = sort.title;

      qb = qb.orderBy(orderBy);
    }

    return paginate(qb as any, { page, perPage });
  }

  getTermPostById(postId: number, termId?: number): Promise<RecruitmentPost> {
    const whereClause: FindOptionsWhere<RecruitmentPost> = {
      id: postId,
    };

    if (termId != null) {
      whereClause.termId = termId;
    }

    return this.postRepo
      .findOneOrFail({
        relations: ['partner', 'partnerContact'],
        where: whereClause,
        withDeleted: true,
      })
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errPostNotFound(e);
        }
        throw e;
      });
  }

  getStudentPostById(
    postId: number,
    studentId: number,
  ): Promise<{
    post: RecruitmentPost;
    isSelfRegistered: boolean;
    isRegistered: boolean;
    isExpired: boolean;
  }> {
    return this.getTermPostById(postId).then(async (post) => {
      const manager = this.termRepo.manager;

      const qb = manager
        .createQueryBuilder(TermPartnerRegistration, 'tpr')
        .select([
          'tpr.termId',
          'tpr.partnerId',
          'tpr.status',
          'tpr.isSelfRegistered',
        ])
        .orWhere(
          'tpr.termId = :termId AND tpr.partnerId = :partnerId AND tpr.studentId = :studentId',
          { termId: post.termId, partnerId: post.partnerId, studentId },
        );

      const reg = await qb.getOne();

      return {
        post,
        isRegistered: reg != null,
        isExpired: post.endRegAt!.getTime() < new Date().getTime(),
        isSelfRegistered: reg != null && reg.isSelfRegistered!,
      };
    });
  }

  getStudentPosts({
    perPage,
    page,
    studentId,
    termId,
  }: GetStudentPostQuery): Promise<
    PaginatedResults<{
      post: RecruitmentPost;
      isSelfRegistered: boolean;
      isRegistered: boolean;
      isExpired: boolean;
    }>
  > {
    return this.getTermPosts({
      page,
      perPage,
      q: '',
      sort: {
        createdAt: 'DESC',
      },
      filter: {
        termId,
      },
    }).then(async (res) => {
      const manager = this.termRepo.manager;

      let qb = manager
        .createQueryBuilder(TermPartnerRegistration, 'tpr')
        .select([
          'tpr.termId',
          'tpr.partnerId',
          'tpr.status',
          'tpr.isSelfRegistered',
          'tpr.studentId',
        ]);

      for (let i = 0; i < res.items.length; i++) {
        const { termId, partnerId } = res.items[i];

        // NOTE: termId, partnerId and studentId came from our output, so it's safe to interpolate it directly.
        //
        // Why didn't we use TypeORM parameters?
        // https://github.com/typeorm/typeorm/issues/8327
        qb = qb.orWhere(
          `(tpr.termId = ${termId} AND tpr.partnerId = ${partnerId} AND tpr.studentId = ${studentId})`,
        );
      }

      const registrations = await qb.getMany();

      return {
        items: res.items.map((p) => {
          const reg = registrations.find(
            (r) => r.termId === p.termId && r.partnerId === p.partnerId,
          );

          return {
            post: p,
            isRegistered: reg != null,
            isExpired: p.endRegAt!.getTime() < new Date().getTime(),
            isSelfRegistered: reg != null && reg.isSelfRegistered!,
          };
        }),
        meta: res.meta,
      };
    });
  }

  async createPost(dto: CreatePostDto): Promise<RecruitmentPost> {
    return await this.postRepo.manager.transaction(async (manager) => {
      // TODO: don't allow the same company to make 2 posts?
      await manager
        .findOneOrFail(PartnerContact, {
          select: ['id'],
          where: {
            id: dto.partnerContactId,
            partnerId: dto.partnerId,
          },
        })
        .catch((e) => {
          throw errContactNotBelongToPartner(e);
        });

      // check if the term is still active.
      await manager
        .findOneOrFail(InternshipTerm, {
          select: ['id'],
          where: {
            startDate: LessThan(new Date().toISOString().substring(0, 10)),
            endDate: MoreThan(new Date().toISOString().substring(0, 10)),
            id: dto.termId,
          },
        })
        .catch((e) => {
          if (e instanceof EntityNotFoundError) {
            throw errTermNotExistedOrIsExpired(e);
          }
          throw e;
        });

      // TODO: should I prevent post creation
      // if the partner is NOT ACCEPTED?
      return await this.postRepo.save<RecruitmentPost>(dto).catch((e) => {
        throw errPartnerNotInTerm(e);
      });
    });
  }

  async updatePost(
    id: number,
    dto: UpdatePostDto,
    termId?: number,
    partnerId?: number,
  ): Promise<number> {
    await this.postRepo.manager.transaction(async (manager) => {
      const whereClause: FindOptionsWhere<RecruitmentPost> = {
        id,
      };

      if (termId != null) {
        whereClause.termId = termId;
      }

      if (partnerId != null) {
        whereClause.partnerId = partnerId;
      }

      if (partnerId == null) {
        partnerId = (
          await manager.findOneOrFail(RecruitmentPost, {
            select: ['partnerId'],
            where: {
              id,
            },
          })
        ).partnerId!;
      }

      await manager
        .findOneOrFail(PartnerContact, {
          select: ['id'],
          where: {
            id: dto.partnerContactId,
            partnerId: partnerId,
          },
        })
        .catch((e) => {
          throw errContactNotBelongToPartner(e);
        });

      const res = await manager.update(RecruitmentPost, whereClause, dto);
      if (res.affected === 0) {
        throw errPostNotExistedOrNoPermission();
      }
    });

    return id;
  }

  getAppliedPartnersByStudentId(
    termId: number,
    studentId: number,
  ): Promise<TermPartnerRegistration[]> {
    return this.studentApplicationRepo
      .createQueryBuilder('sa')
      .select([
        'sa.id',
        'sa.isSelfRegistered',
        'sa.status',
        'sa.partnerId',
        'p.name',
        'sa.createdAt',
        'tp.status',
      ])
      .leftJoinAndSelect('sa.termPartner', 'tp')
      .leftJoinAndSelect('tp.partner', 'p')
      .where('sa.termId = :termId', { termId })
      .andWhere('sa.studentId = :studentId', { studentId })
      .getMany();
  }

  /** Update partners with PENDING status to the desired status.
   */
  async batchChangeTermPartnerStatus(
    termId: number,
    partnerIds: number[],
    partnerStatus: PartnerInternshipStatus,
  ): Promise<void> {
    if (
      partnerStatus !== PartnerInternshipStatus.ACCEPTED &&
      partnerStatus !== PartnerInternshipStatus.REJECTED
    ) {
      throw errFailedToChangePartnerStatus(
        new Error('invalid status enum passed: ' + partnerStatus),
      );
    }
    await this.termPartnerRepo.manager.transaction(async (manager) => {
      const termPartners = await manager.find(TermPartner, {
        select: ['partnerId'],
        where: {
          termId,
          partnerId: In(partnerIds),
          status: PartnerInternshipStatus.PENDING,
        },
      });

      if (termPartners.length === 0) {
        throw errFailedToChangePartnerStatus();
      }

      partnerIds = termPartners.map((tp) => tp.partnerId!);

      await manager.update(
        TermPartner,
        {
          termId,
          partnerId: In(partnerIds),
          status: PartnerInternshipStatus.PENDING,
        },
        {
          status: partnerStatus,
        },
      );
    });

    for (const partnerId of partnerIds) {
      this.eventEmitter.emit(
        events.PARTNER_STATUS_CHANGED,
        new PartnerStatusChangedEvent(
          termId,
          partnerId,
          PartnerInternshipStatus.PENDING,
          partnerStatus,
        ),
      );
    }
  }

  /** Update registration status.
   */
  async batchUpdateRegistrationStatus(
    registrationIds: number[],
    status: RegistrationStatus,
    termId?: number,
    partnerId?: number,
  ): Promise<void> {
    if (
      status !== RegistrationStatus.FAILED &&
      status !== RegistrationStatus.PASSED
    ) {
      throw errFailedToUpdateRegistrationStatus(
        new Error('error invalid status enum passed: ' + status),
      );
    }

    await this.termStudentRepo.manager.transaction(async (manager) => {
      const whereClause: FindOptionsWhere<TermPartnerRegistration> = {
        id: In(registrationIds),
        status: RegistrationStatus.WAITING,
      };

      if (partnerId != null) {
        whereClause.partnerId = partnerId;
      }
      if (termId != null) {
        whereClause.termId = termId;
      }

      const termRegistrations = await manager.find(TermPartnerRegistration, {
        select: ['id'],
        where: whereClause,
      });

      if (termRegistrations.length === 0) {
        throw errFailedToUpdateRegistrationStatus();
      }

      registrationIds = termRegistrations.map((tp) => tp.id!);

      await manager.update(
        TermPartnerRegistration,
        { ...whereClause, id: In(registrationIds) },
        {
          status,
          isMailSent: false,
        },
      );
    });

    this.eventEmitter.emit(
      events.BATCH_PARTNER_REG_STATUS_CHANGED,
      new BatchPartnerRegistrationChangedEvent(
        registrationIds,
        RegistrationStatus.WAITING,
        status,
      ),
    );
  }

  /* Add lecturers to an internship term.
   *
   * This function assumes that `termId` belongs to `organizationId`.
   */
  async batchAddLecturersToTerm(
    termId: number,
    lecturerIds: number[],
    organizationId: number,
  ): Promise<number[]> {
    if (lecturerIds.length === 0) {
      return [];
    }

    await this.termLecturerRepo.manager.transaction(async (manager) => {
      // get lecturers who are not already in this terms.
      const lecturers: { id: number }[] = await manager.query(
        'SELECT l.id FROM lecturers l LEFT JOIN term_lecturers tl' +
          ' ON l.id = tl.lecturer_id AND tl.term_id = ?' +
          ' WHERE l.id IN (?) AND tl.term_id IS NULL AND l.organization_id = ?',
        [termId, lecturerIds, organizationId],
      );

      lecturerIds = lecturers.map((l) => l.id);

      if (lecturerIds.length === 0) {
        throw errFailedToBatchAddTermLecturers(
          new Error('error filtered lecturer ids is empty'),
        );
      }

      const insertObjects: TermLecturer[] = lecturerIds.map((id) => ({
        termId,
        lecturerId: id,
      }));

      manager.insert(TermLecturer, insertObjects);
    });

    return lecturerIds;
  }

  async batchAddPartnersToTerm(
    termId: number,
    partnerIds: number[],
  ): Promise<number[]> {
    if (partnerIds.length === 0) {
      return [];
    }

    await this.termPartnerRepo.manager.transaction(async (manager) => {
      // get partners who are not already in this terms.
      const partners: { id: number }[] = await manager.query(
        'SELECT p.id FROM partners p LEFT JOIN term_partners tp ON p.id = tp.partner_id AND tp.term_id = ? WHERE p.id IN (?) AND tp.term_id IS NULL',
        [termId, partnerIds],
      );

      partnerIds = partners.map((p) => p.id);

      if (partnerIds.length === 0) {
        throw errFailedToBatchAddTermPartners(
          new Error('error filtered partner ids is empty'),
        );
      }

      const insertObjects: TermPartner[] = partnerIds.map((id) => ({
        termId,
        partnerId: id,
        status: PartnerInternshipStatus.ACCEPTED,
      }));

      manager.insert(TermPartner, insertObjects);
    });

    return partnerIds;
  }

  getTermListing(orgId: number): Promise<InternshipTerm[]> {
    return this.termRepo
      .createQueryBuilder('t')
      .select(['t.id', 't.year', 't.term'])
      .where('t.organizationId = :orgId', { orgId })
      .orderBy({
        't.year': 'DESC',
        't.term': 'DESC',
      })
      .getMany();
  }

  async getLecturerTermListing(lecturerId: number): Promise<InternshipTerm[]> {
    const lecturerTermQb = this.termLecturerRepo
      .createQueryBuilder('tl')
      .select('tl.termId')
      .where('tl.lecturerId = :id', { id: lecturerId });

    return this.termRepo
      .createQueryBuilder('t')
      .select(['t.id', 't.year', 't.term'])
      .where('t.id IN (' + lecturerTermQb.getQuery() + ')')
      .orderBy({
        't.year': 'DESC',
        't.term': 'DESC',
      })
      .setParameters(lecturerTermQb.getParameters())
      .getMany();
  }

  /** Get a list of terms that a partner is in. The status of this partner
   * in which term must be ACCEPTED.
   */
  async getPartnersTermListing(partnerId: number): Promise<InternshipTerm[]> {
    const partnerTermQb = this.termPartnerRepo
      .createQueryBuilder('tp')
      .select('tp.termId')
      .where(`tp.partnerId = :id AND tp.status = 'ACCEPTED' `, {
        id: partnerId,
      });

    return this.termRepo
      .createQueryBuilder('t')
      .select(['t.id', 't.year', 't.term'])
      .where('t.id IN (' + partnerTermQb.getQuery() + ')')
      .orderBy({
        't.year': 'DESC',
        't.term': 'DESC',
      })
      .setParameters(partnerTermQb.getParameters())
      .getMany();
  }

  async getStudentTermListing(studentId: number): Promise<InternshipTerm[]> {
    const studentTermQb = this.termStudentRepo
      .createQueryBuilder('ts')
      .select('ts.termId')
      .where('ts.studentId = :id', { id: studentId });

    return this.termRepo
      .createQueryBuilder('t')
      .select(['t.id', 't.year', 't.term'])
      .where('t.id IN (' + studentTermQb.getQuery() + ')')
      .orderBy({
        't.year': 'DESC',
        't.term': 'DESC',
      })
      .setParameters(studentTermQb.getParameters())
      .getMany();
  }

  getOrgLatestTerm(orgId: number): Promise<InternshipTerm> {
    return this.termRepo
      .createQueryBuilder('t')
      .select([
        't.id',
        't.year',
        't.term',
        't.startRegAt',
        't.endRegAt',
        't.startDate',
        't.endDate',
      ])
      .where('t.organizationId = :orgId', { orgId })
      .orderBy({
        't.year': 'DESC',
        't.term': 'DESC',
      })
      .getOneOrFail()
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errTermNotExist(e);
        }
        throw e;
      });
  }

  checkStudentInTerm(studentId: number, termId: number): Promise<boolean> {
    return this.termStudentRepo
      .findOne({
        select: ['termId'],
        where: {
          studentId,
          termId,
        },
      })
      .then((res) => res != null);
  }

  checkLecturerInTerm(lecturerId: number, termId: number): Promise<boolean> {
    return this.termLecturerRepo
      .findOne({
        select: ['termId'],
        where: {
          lecturerId,
          termId,
        },
      })
      .then((res) => res != null);
  }

  checkPartnerInTerm(
    partnerId: number,
    termId: number,
  ): Promise<{ isIn: boolean; status?: PartnerInternshipStatus }> {
    return this.termPartnerRepo
      .findOne({
        select: ['status'],
        where: {
          partnerId,
          termId,
        },
      })
      .then((res) => {
        if (res == null) return { isIn: false };

        return {
          isIn: true,
          status: res.status,
        };
      });
  }

  getTermPartnerListing(termId: number): Promise<Partner[]> {
    const manager = this.termPartnerRepo.manager;
    return manager
      .createQueryBuilder(Partner, 'p')
      .select(['p.id', 'p.name', 'p.logoUrl', 'tp.status'])
      .leftJoin('p.termPartners', 'tp', 'tp.termId = :termId', { termId })
      .orderBy({
        'p.name': 'ASC',
      })
      .getMany();
  }

  getTermStudentDetails(
    termId: number,
    studentId: number,
  ): Promise<TermStudent> {
    // NOTE: one quirk about TypeORM:
    // If you want to left join something, you MUST select at least
    // one field from that relation.

    return this.termStudentRepo
      .createQueryBuilder('ts')
      .select([
        'ts.studentId',
        'ts.reportFileName',
        'tpr.studentId',
        'tpr.termId',
        'l.id',
        'l.fullName',
        'l.orgEmail',
        'l.personalEmail',
        'l.phoneNumber',
        'p.id',
        'p.name',
        'tpr.isSelfRegistered',
        'tpr.createdAt',
        'tpr.status',
        'tp.status',
        'tpr.internshipType',
        'tl.lecturerId',
      ])
      .leftJoin('ts.termLecturer', 'tl')
      .leftJoin('tl.lecturer', 'l')
      .leftJoin(
        'ts.termPartnerRegistrations',
        'tpr',
        'tpr.termId = :tid AND tpr.studentId = :sid',
        {
          tid: termId,
          sid: studentId,
        },
      )
      .leftJoin('tpr.termPartner', 'tp')
      .leftJoin('tp.partner', 'p')
      .where('ts.termId = :termId', { termId })
      .andWhere('ts.studentId = :studentId', { studentId })
      .getOneOrFail()
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errStudentNotFoundOrNotInTerm(e);
        }
        throw e;
      });
  }

  async unregisterPartner(
    termId: number,
    partnerId: number,
    studentId: number,
  ): Promise<void> {
    const manager = this.termRepo.manager;

    await manager.delete(TermPartnerRegistration, {
      termId,
      studentId,
      partnerId,
    });
  }

  async uploadReport(
    termId: number,
    studentId: number,
    reportFile: Express.Multer.File,
  ): Promise<void> {
    const objectName = getReportFileObjectName(
      termId,
      studentId,
      reportFile.originalname,
    );

    await this.termStudentRepo.manager.transaction(async (manager) => {
      Logger.log(`uploading ${objectName} to minio`, 'uploadReport');
      await this.minioClient
        .putObject(this.internshipBucketName, objectName, reportFile.buffer)
        .catch((e) => {
          Logger.error(e.message, 'uploadReport');
          throw e;
        });

      Logger.log(`finish uploading ${objectName} to minio`, 'uploadReport');

      await manager.update(
        TermStudent,
        {
          termId,
          studentId,
        },
        {
          reportFileName: objectName,
        },
      );
    });
  }

  async downloadReport(
    termId: number,
    studentId: number,
    filterLecturerId?: number,
  ): Promise<string> {
    const whereClause: FindOptionsWhere<TermStudent> = {
      termId,
      studentId,
    };

    if (filterLecturerId != null) {
      whereClause.supervisorId = filterLecturerId;
    }

    const { reportFileName } = await this.termStudentRepo
      .findOneOrFail({
        select: ['reportFileName'],
        where: whereClause,
      })
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errFileNotFound();
        }
        throw e;
      });

    if (isZeroValue(reportFileName)) {
      throw errFileNotFound();
    }

    return await this.minioClient
      .presignedGetObject(this.internshipBucketName, reportFileName!)
      .catch((e) => {
        throw e;
      });
  }

  /** Export students in a term to a file and return the download URL.
   */
  async exportTermStudents(opts: ExportTermStudentsQuery): Promise<string> {
    const students = await this.buildGetTermStudentsQuery(opts).getMany();

    const csvParser = new Parser({
      fields: [
        {
          label: 'Full name',
          value: 'student.fullName',
        },
        {
          label: 'Student ID Number',
          value: 'student.studentIdNumber',
        },
        {
          label: 'VNU Email',
          value: 'student.orgEmail',
        },
        {
          label: 'Class name',
          value: 'student.schoolClass.name',
        },
        {
          label: 'Supervisor',
          value: 'termLecturer.lecturer.fullName',
        },
        {
          label: 'Selected partner',
          value: 'selectedPartner.name',
        },
        {
          label: 'Score',
          value: 'score',
        },
      ],
    });

    // TODO: optimize using Stream API if we have perf problem.
    const csv = csvParser.parse(students);

    return await this.uploadAndGetLink(csv, 'export_students', 'csv');
  }

  async exportStudentApplications(
    opts: ExportStudentApplicationsQuery,
  ): Promise<string> {
    const applications = await this.buildGetStudentApplicationsQuery(
      opts,
    ).getMany();

    const csvParser = new Parser({
      fields: [
        {
          label: 'Full name',
          value: 'termStudent.student.fullName',
        },
        {
          label: 'Student ID Number',
          value: 'termStudent.student.studentIdNumber',
        },
        {
          label: 'VNU Email',
          value: 'termStudent.student.orgEmail',
        },
        {
          label: 'Personal email',
          value: 'termStudent.student.personalEmail',
        },
        {
          label: 'Phone number',
          value: 'termStudent.student.phoneNumber',
        },
        {
          label: 'Registered at',
          value: 'createdAt',
        },
      ],
    });

    const csvData = csvParser.parse(applications);

    return await this.uploadAndGetLink(csvData, 'export_applications', 'csv');
  }

  async exportTermPartners(opts: ExportTermPartnersQuery): Promise<string> {
    const partners = await this.buildGetTermPartnersQuery(opts).getMany();

    const csvParser = new Parser({
      fields: [
        {
          label: 'Name',
          value: 'partner.name',
        },
        {
          label: 'Homepage URL',
          value: 'partner.homepageUrl',
        },
        {
          label: 'Email',
          value: 'partner.email',
        },
        {
          label: 'Phone number',
          value: 'partner.phoneNumber',
        },
        {
          label: 'Address',
          value: 'partner.address',
        },
        {
          label: 'Description',
          value: 'partner.description',
        },
        {
          label: 'Term ID',
          value: 'termId',
        },
        {
          label: 'Term status',
          value: 'status',
        },
      ],
    });

    const csvData = csvParser.parse(partners);

    return await this.uploadAndGetLink(csvData, 'export_partners', 'csv');
  }

  async getMyStudentStats(lecturerId: number, orgId: number) {
    const term = await this.termRepo
      .createQueryBuilder('t')
      .select(['t.id'])
      .where('t.organizationId = :orgId', { orgId })
      .orderBy({
        't.year': 'DESC',
        't.term': 'DESC',
      })
      .getOne();

    if (term == null) {
      // return null if no term was founded in org.
      return {
        scoring: undefined,
        reporting: undefined,
      };
    }

    const termId = term.id!;

    const getScoringStat: Promise<{ scored: string; notScored: string }[]> =
      this.termStudentRepo.manager.query(
        'SELECT COUNT(CASE WHEN score is not null THEN 1 END) as scored, COUNT(CASE WHEN score is NULL THEN 1 END) as notScored FROM term_students WHERE term_id = ? AND supervisor_id = ?',
        [termId, lecturerId],
      );

    const getReportStat: Promise<
      { submitted: string; notSubmitted: string }[]
    > = this.termStudentRepo.manager.query(
      "SELECT COUNT(CASE WHEN report_file_name != '' THEN 1 END) as submitted, COUNT(CASE WHEN report_file_name = '' THEN 1 END) as notSubmitted FROM term_students WHERE term_id = ? AND supervisor_id = ?",
      [termId, lecturerId],
    );

    const [scoreStats, reportStats] = await Promise.all([
      getScoringStat,
      getReportStat,
    ]);

    const scoreStat = scoreStats[0];
    const reportStat = reportStats[0];

    return {
      scoring: {
        scored: parseInt(scoreStat.scored),
        notScored: parseInt(scoreStat.notScored),
      },
      reporting: {
        submitted: parseInt(reportStat.submitted),
        notSubmitted: parseInt(reportStat.notSubmitted),
      },
    };
  }

  getTermLecturerListing(termId: number): Promise<Lecturer[]> {
    const manager = this.termLecturerRepo.manager;
    return manager
      .createQueryBuilder(Lecturer, 'l')
      .select(['l.id', 'l.fullName'])
      .innerJoin('l.termLecturers', 'tl', 'tl.termId = :termId', { termId })
      .orderBy({ 'l.fullName': 'ASC' })
      .getMany();
  }

  private buildGetTermStudentsQuery({
    sort,
    q,
    filter,
  }: ExportTermStudentsQuery): SelectQueryBuilder<TermStudent> {
    let queryBuilder = this.termStudentRepo
      .createQueryBuilder('ts')
      .leftJoinAndSelect('ts.student', 's')
      .leftJoinAndSelect('s.schoolClass', 'sc')
      // NOTE: Why don't you create another field to allow joining ts directly with lecturer?
      // Because TypeORM doesn't support multiple references on the same column like a f*cking moron.
      // Since TermStudent already reference `lecturer_id` field in `termLecturer` relation, we can't use
      // it in `supervisor` relation anymore.
      //
      // https://github.com/typeorm/typeorm/issues/8228
      .leftJoinAndSelect('ts.termLecturer', 'tl')
      .leftJoinAndSelect('tl.lecturer', 'l')
      .leftJoinAndSelect('ts.selectedPartner', 'sp');

    if (filter != null) {
      const {
        termId,
        partnerSelected,
        supervisorId,
        scoreGiven,
        reportSubmitted,
        supervisorAssigned,
        selectedPartnerId,
      } = filter;
      if (termId != null) {
        queryBuilder = queryBuilder.andWhere('ts.termId = :termId', { termId });
      }
      if (partnerSelected != null) {
        if (partnerSelected) {
          queryBuilder = queryBuilder.andWhere(
            `ts.selectedPartnerId IS NOT NULL`,
          );
        } else {
          queryBuilder = queryBuilder.andWhere(`ts.selectedPartnerId IS NULL`);
        }
      }
      if (selectedPartnerId != null) {
        queryBuilder = queryBuilder.andWhere(
          'ts.selectedPartnerId = :selectedPartnerId',
          { selectedPartnerId },
        );
      }
      if (supervisorId != null) {
        queryBuilder = queryBuilder.andWhere(
          'ts.supervisorId = :supervisorId',
          { supervisorId },
        );
      }
      if (scoreGiven != null) {
        if (scoreGiven) {
          queryBuilder = queryBuilder.andWhere('ts.score IS NOT NULL');
        } else {
          queryBuilder = queryBuilder.andWhere('ts.score IS NULL');
        }
      }
      if (supervisorAssigned != null) {
        if (supervisorAssigned) {
          queryBuilder = queryBuilder.andWhere('ts.supervisorId IS NOT NULL');
        } else {
          queryBuilder = queryBuilder.andWhere('ts.supervisorId IS NULL');
        }
      }
      if (reportSubmitted != null) {
        if (reportSubmitted) {
          queryBuilder = queryBuilder.andWhere(
            `(ts.reportFileName IS NOT NULL AND ts.reportFileName != '')`,
          );
        } else {
          queryBuilder = queryBuilder.andWhere(
            `(ts.reportFileName IS NULL OR ts.reportFileName = '')`,
          );
        }
      }
    }

    if (!isZeroValue(q)) {
      queryBuilder = queryBuilder.andWhere(
        new Brackets((qb) =>
          qb
            .orWhere(
              'MATCH(s.fullName) AGAINST (:q in natural language mode)',
              { q },
            )
            .orWhere(`s.orgEmail like CONCAT('%', :email, '%')`, {
              email: q,
            }),
        ),
      );
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.id != null) orderBy['s.id'] = sort.id;
      if (sort.fullName != null) orderBy['s.fullName'] = sort.fullName;
      if (sort.studentIdNumber != null)
        orderBy['s.studentIdNumber'] = sort.studentIdNumber;
      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return queryBuilder;
  }

  private buildGetStudentApplicationsQuery({
    q,
    sort,
    filter,
  }: ExportStudentApplicationsQuery): SelectQueryBuilder<TermPartnerRegistration> {
    let queryBuilder = this.studentApplicationRepo
      .createQueryBuilder('sa')
      .innerJoinAndSelect('sa.termStudent', 'ts')
      .innerJoinAndSelect('ts.student', 's')
      .innerJoinAndSelect('sa.termPartner', 'tp')
      .innerJoinAndSelect('tp.partner', 'p');

    if (!isZeroValue(q)) {
      queryBuilder = queryBuilder.where(
        new Brackets((qb) =>
          qb
            .orWhere(
              'MATCH(s.fullName) AGAINST (:q in natural language mode)',
              { q },
            )
            .orWhere(`s.orgEmail like CONCAT('%', :email, '%')`, {
              email: q,
            }),
        ),
      );
    }

    if (filter != null) {
      if (!isZeroValue(filter.status)) {
        queryBuilder = queryBuilder.andWhere('sa.status = :status', {
          status: filter.status,
        });
      }
      if (!isZeroValue(filter.partnerId)) {
        queryBuilder = queryBuilder.andWhere('sa.partnerId = :partnerId', {
          partnerId: filter.partnerId,
        });
      }
      if (!isZeroValue(filter.isMailSent)) {
        queryBuilder = queryBuilder.andWhere('sa.isMailSent = :isMailSent', {
          isMailSent: filter.isMailSent,
        });
      }
      if (!isZeroValue(filter.internshipType)) {
        queryBuilder = queryBuilder.andWhere('sa.internshipType = :itype', {
          itype: filter.internshipType,
        });
      }
      if (!isZeroValue(filter.termId)) {
        queryBuilder = queryBuilder.andWhere('sa.termId = :termId', {
          termId: filter.termId,
        });
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.fullName != null) orderBy['s.fullName'] = sort.fullName;
      if (sort.studentIdNumber != null)
        orderBy['s.studentIdNumber'] = sort.studentIdNumber;
      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return queryBuilder;
  }

  private buildGetTermPartnersQuery({
    q,
    sort,
    filter,
  }: ExportTermPartnersQuery): SelectQueryBuilder<TermPartner> {
    let qb = this.termPartnerRepo
      .createQueryBuilder('tp')
      .leftJoinAndSelect('tp.partner', 'p');

    if (!isZeroValue(q)) {
      qb = qb.andWhere(`MATCH(p.name) AGAINST (:q in natural language mode)`, {
        q,
      });
    }

    if (filter != null) {
      const { status, termId } = filter;
      if (termId != null) {
        qb = qb.andWhere('tp.termId = :termId', { termId });
      }
      if (status != null) {
        qb = qb.andWhere('tp.status = :status', { status });
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.joinedAt) orderBy['tp.createdAt'] = sort.joinedAt;
      if (sort.name) orderBy['p.name'] = sort.name;

      qb = qb.orderBy(orderBy);
    }

    return qb;
  }

  private async uploadAndGetLink(
    data: string | Buffer,
    fileName: string,
    extname: string,
  ): Promise<string> {
    const objectName = getMinioTempObjectName(fileName, extname);
    await this.minioClient.putObject(
      this.internshipBucketName,
      objectName,
      data,
      EXPORT_EXPIRY_SECONDS,
    );

    return await this.minioClient.presignedGetObject(
      this.internshipBucketName,
      objectName,
    );
  }
}

const getReportFileObjectName = (
  termId: number,
  studentId: number,
  fileName: string,
): string => {
  return [
    REPORT_FOLDER_NAME,
    termId.toString(),
    studentId.toString(),
    fileName,
  ].join(FILE_SEPARATOR);
};
