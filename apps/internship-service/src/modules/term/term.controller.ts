import { defaultPage, defaultPerPage, parseSortQuery } from '@app/paginate';
import {
  fromTimestamp,
  fromTimestampNullable,
  toTimestampNullable,
} from '@app/pb';
import {
  ApplyToPartnerRequest,
  ApplyToPartnerResponse,
  AssignSupervisorToStudentsRequest,
  AssignSupervisorToStudentsResponse,
  BatchAddLecturersToTermRequest,
  BatchAddLecturersToTermResponse,
  BatchAddPartnersToTermRequest,
  BatchAddPartnersToTermResponse,
  BatchChangeTermPartnerStatusRequest,
  BatchChangeTermPartnerStatusResponse,
  BatchNotifyStudentAboutRegistrationStatusRequest,
  BatchNotifyStudentAboutRegistrationStatusResponse,
  BatchUpdateRegistrationStatusRequest,
  BatchUpdateRegistrationStatusResponse,
  CancelTermSignUpRequest,
  CancelTermSignUpResponse,
  CheckLecturerInTermRequest,
  CheckLecturerInTermResponse,
  CheckPartnerInTermRequest,
  CheckPartnerInTermResponse,
  CheckStudentInTermRequest,
  CheckStudentInTermResponse,
  CheckTermInOrgRequest,
  CheckTermInOrgResponse,
  CreatePostRequest,
  CreatePostResponse,
  CreateTermRequest,
  CreateTermResponse,
  DownloadStudentReportRequest,
  DownloadStudentReportResponse,
  ExportStudentApplicationsRequest,
  ExportStudentApplicationsResponse,
  ExportTermPartnersRequest,
  ExportTermPartnersResponse,
  ExportTermStudentsRequest,
  ExportTermStudentsResponse,
  GetAppliedPartnersByStudentIdRequest,
  GetAppliedPartnersByStudentIdResponse,
  GetLatestTermRequest,
  GetLatestTermResponse,
  GetLecturerTermListingRequest,
  GetLecturerTermListingResponse,
  GetMyStudentStatsRequest,
  GetMyStudentStatsResponse,
  GetPartnerTermListingRequest,
  GetPartnerTermListingResponse,
  GetPostByIdRequest,
  GetPostByIdResponse,
  GetPostsRequest,
  GetPostsResponse,
  GetStudentApplicationsRequest,
  GetStudentApplicationsResponse,
  GetStudentPostByIdRequest,
  GetStudentPostByIdResponse,
  GetStudentPostsRequest,
  GetStudentPostsResponse,
  GetStudentTermListingRequest,
  GetStudentTermListingResponse,
  GetTermLecturerListingRequest,
  GetTermLecturerListingResponse,
  GetTermLecturersRequest,
  GetTermLecturersResponse,
  GetTermListingRequest,
  GetTermListingResponse,
  GetTermPartnerListingRequest,
  GetTermPartnerListingResponse,
  GetTermPartnersRequest,
  GetTermPartnersResponse,
  GetTermsRequest,
  GetTermsResponse,
  GetTermStudentDetailsRequest,
  GetTermStudentDetailsResponse,
  GetTermStudentsRequest,
  GetTermStudentsResponse,
  GiveScoreRequest,
  GiveScoreResponse,
  internshipTypeFromJSON,
  partnerStatusFromJSON,
  partnerStatusToJSON,
  registrationStatusFromJSON,
  registrationStatusToJSON,
  RemoveLecturerFromTermRequest,
  RemoveLecturerFromTermResponse,
  SelectInternshipPartnerRequest,
  SelectInternshipPartnerResponse,
  SelfRegisterPartnerRequest,
  SelfRegisterPartnerResponse,
  SignUpForTermRequest,
  SignUpForTermResponse,
  TermServiceController,
  TermServiceControllerMethods,
  UnregisterPartnerRequest,
  UnregisterPartnerResponse,
  UpdatePostRequest,
  UpdatePostResponse,
  UpdateTermRequest,
  UpdateTermResponse,
} from '@app/pb/internship_service/service.pb';
import {
  compareDate,
  deserializeAsync,
  isZeroValue,
  toGrpcMetadata,
} from '@app/utils';
import { status } from '@grpc/grpc-js';
import {
  Controller,
  Param,
  ParseIntPipe,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { FileInterceptor } from '@nestjs/platform-express';
import { EntityNotFoundError } from 'typeorm';
import {
  PDF_MIME_TYPE,
  REPORT_FILE_KEY,
  REPORT_MAX_SIZE_IN_BYTES,
} from '../../constants';
import {
  InternshipType,
  InternshipTypeMethods,
  RegistrationStatus,
  RegistrationStatusMethods,
} from '../../database/entities/term-partner-registration.entity';
import { PartnerInternshipStatus } from '../../database/entities/term-partner.entity';
import {
  CreatePostDto,
  CreateTermReqDto,
  GetPostsQuery,
  GetTermLecturersQuery,
  GetTermPartnersQuery,
  GetTermsQuery,
  GetTermStudentsQuery,
  UpdatePostDto,
} from './dto';
import {
  errFailedToUploadFile,
  errInvalidArg,
  errInvalidFileType,
  errPostNotExistedOrNoPermission,
} from './errors';
import { TermService } from './term.service';

@Controller('terms')
@TermServiceControllerMethods()
export class TermController implements TermServiceController {
  constructor(private readonly termService: TermService) {}

  async getMyStudentStats({
    lecturerId,
    organizationId,
  }: GetMyStudentStatsRequest): Promise<GetMyStudentStatsResponse> {
    const stats = await this.termService.getMyStudentStats(
      lecturerId,
      organizationId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: stats,
    };
  }

  async getTerms({
    page,
    perPage,
    filter,
    sort,
  }: GetTermsRequest): Promise<GetTermsResponse> {
    const allowedCols = ['createdAt'];

    const dtoRaw: GetTermsQuery = {
      page: defaultPage(page),
      perPage: defaultPerPage(perPage),
      filter,
      sort: parseSortQuery(sort, { allowedCols }),
    };

    const { value: dto, err } = await deserializeAsync(GetTermsQuery, dtoRaw);
    if (err != null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
      });
    }

    const { items, meta } = await this.termService.getTerms(dto).catch((e) => {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_ARG',
        error: e,
      });
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        terms: items.map((t) => ({
          id: t.id!,
          createdAt: toTimestampNullable(t.createdAt),
          startRegAt: toTimestampNullable(t.startRegAt),
          endRegAt: toTimestampNullable(t.endRegAt),
          organizationId: t.organizationId!,
          startDate: t.startDate!,
          endDate: t.endDate!,
          term: t.term!,
          year: t.year!,
          numOfAcceptedPartners: t.numOfAcceptedPartners ?? 0,
          numOfTermStudents: t.numOfTermStudents ?? 0,
        })),
      },
    };
  }

  async getTermStudents(
    request: GetTermStudentsRequest,
  ): Promise<GetTermStudentsResponse> {
    const allowedCols = ['id', 'fullName', 'studentIdNumber'];

    const dtoRaw = {
      ...request,
      page: defaultPage(request.page),
      perPage: defaultPerPage(request.perPage),
      sort: parseSortQuery(request.sort ?? [], { allowedCols }),
    };

    const { value: dto, err } = await deserializeAsync(
      GetTermStudentsQuery,
      dtoRaw,
    );
    if (err != null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
      });
    }

    const { items: termStudents, meta } =
      await this.termService.getTermStudents(dto);

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        students: termStudents.map((ts) => ({
          fullName: ts.student?.fullName ?? '',
          id: ts.studentId ?? 0,
          orgEmail: ts.student?.orgEmail ?? '',
          organizationId: ts.student?.organizationId ?? 0,
          personalEmail: ts.student?.personalEmail ?? '',
          phoneNumber: ts.student?.phoneNumber ?? '',
          schoolClass:
            ts.student?.schoolClass == null
              ? undefined
              : {
                  id: ts.student.schoolClassId ?? 0,
                  name: ts.student.schoolClass.name ?? '',
                },
          selectedAt: toTimestampNullable(ts.selectedAt),
          selectedPartner:
            ts.selectedPartner == null
              ? undefined
              : {
                  id: ts.selectedPartnerId ?? 0,
                  name: ts.selectedPartner.name ?? '',
                },
          studentIdNumber: ts.student?.studentIdNumber ?? '',
          supervisor:
            ts.termLecturer?.lecturer == null
              ? undefined
              : {
                  id: ts.supervisorId ?? 0,
                  name: ts.termLecturer.lecturer.fullName ?? '',
                },
          userId: ts.student?.userId ?? 0,
          reportSubmitted: !isZeroValue(ts.reportFileName),
          termId: ts.termId!,
          score: ts.score,
        })),
      },
    };
  }

  async getTermLecturers(
    request: GetTermLecturersRequest,
  ): Promise<GetTermLecturersResponse> {
    const allowedSortCols = ['name'];

    const dtoRaw: GetTermLecturersQuery = {
      ...request,
      page: defaultPage(request.page),
      perPage: defaultPerPage(request.perPage),
      sort: parseSortQuery(request.sort, { allowedCols: allowedSortCols }),
    };

    const { err, value: dto } = await deserializeAsync(
      GetTermLecturersQuery,
      dtoRaw,
    );
    if (err != null) {
      throw errInvalidArg;
    }

    const { items, meta } = await this.termService.getTermLecturers(dto);

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        lecturers: items.map((tl) => ({
          createdAt: toTimestampNullable(tl.createdAt),
          fullName: tl.lecturer?.fullName ?? '',
          id: tl.lecturerId ?? 0,
          numOfSupervisedStudents: tl.supervisedStudents?.length ?? 0,
          orgEmail: tl.lecturer?.orgEmail ?? '',
          organizationId: tl.lecturer?.organizationId ?? 0,
          personalEmail: tl.lecturer?.personalEmail ?? '',
          termId: tl.termId ?? 0,
          userId: tl.lecturer?.userId ?? 0,
          supervisedStudents: [],
        })),
      },
    };
  }

  async getTermPartners(
    request: GetTermPartnersRequest,
  ): Promise<GetTermPartnersResponse> {
    const allowedSortCols = ['name', 'joinedAt'];
    const dtoRaw: GetTermPartnersQuery = {
      ...request,
      page: defaultPage(request.page),
      perPage: defaultPerPage(request.perPage),
      sort: parseSortQuery(request.sort, { allowedCols: allowedSortCols }),
      filter: {
        termId: request.filter?.termId,
        status:
          request.filter?.status == null
            ? undefined
            : (partnerStatusToJSON(
                request.filter.status,
              ) as PartnerInternshipStatus),
      },
    };

    const { value: dto, err } = await deserializeAsync(
      GetTermLecturersQuery,
      dtoRaw,
    );
    if (err != null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_ARG',
        error: err,
      });
    }

    const { items, meta } = await this.termService.getTermPartners(dto);

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        partners: items.map((tp) => ({
          id: tp.partnerId ?? 0,
          email: tp.partner?.email ?? '',
          homepageUrl: tp.partner?.homepageUrl ?? '',
          joinedAt: toTimestampNullable(tp.createdAt),
          name: tp.partner?.name ?? '',
          status: partnerStatusFromJSON(tp.status),
          termId: tp.termId ?? 0,
          userId: tp.partner?.userId,
        })),
      },
    };
  }

  async assignSupervisorToStudents(
    request: AssignSupervisorToStudentsRequest,
  ): Promise<AssignSupervisorToStudentsResponse> {
    await this.termService.assignLecturersToStudents(
      request.termId,
      request.assignments,
    );

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async applyToPartner({
    termId,
    studentId,
    postId,
  }: ApplyToPartnerRequest): Promise<ApplyToPartnerResponse> {
    const { id } = await this.termService.applyForInternship(
      termId,
      studentId,
      postId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        registrationId: id!,
      },
    };
  }

  async selfRegisterPartner({
    termId,
    studentId,
    partnerId,
  }: SelfRegisterPartnerRequest): Promise<SelfRegisterPartnerResponse> {
    const { id } = await this.termService.selfRegisterInternship(
      termId,
      studentId,
      partnerId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        registrationId: id!,
      },
    };
  }

  async checkTermInOrg({
    organizationId,
    termId,
  }: CheckTermInOrgRequest): Promise<CheckTermInOrgResponse> {
    const isTermInOrg = await this.termService
      .isTermInOrgs(termId, [organizationId])
      .catch((e) => {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'ERR_TERM_NOT_IN_ORG',
          error: e,
        });
      });

    return {
      code: status.OK,
      message: 'success',
      data: {
        isIn: isTermInOrg,
      },
    };
  }

  async createTerm({
    organizationId,
    ...req
  }: CreateTermRequest): Promise<CreateTermResponse> {
    if (req.startRegAt == null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_NO_REGISTRATION_DATE',
      });
    }
    if (req.endRegAt == null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_NO_REGISTRATION_DATE',
      });
    }

    const dtoRaw: CreateTermReqDto = {
      ...req,
      startRegAt: fromTimestamp(req.startRegAt),
      endRegAt: fromTimestamp(req.endRegAt),
    };
    const { value: dto, err } = await deserializeAsync(
      CreateTermReqDto,
      dtoRaw,
    );
    if (err != null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_DTO',
        metadata: toGrpcMetadata({
          details: 'error invalid dto object',
        }),
      });
    }

    const { id } = await this.termService.createTerm(dto, organizationId);

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: id!,
      },
    };
  }

  async getStudentApplications({
    page,
    q,
    sort,
    filter,
    perPage,
  }: GetStudentApplicationsRequest): Promise<GetStudentApplicationsResponse> {
    const allowedSortCols = ['fullName', 'studentIdNumber'];

    let filterInternshipType: InternshipType | undefined;
    let filterStatus: RegistrationStatus | undefined;
    try {
      if (filter?.status != null) {
        filterStatus = RegistrationStatusMethods.fromPb(filter.status);
      }
      if (filter?.internshipType != null) {
        filterInternshipType = InternshipTypeMethods.fromPb(
          filter.internshipType,
        );
      }
    } catch (e) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_ENUM',
      });
    }

    const { items, meta } = await this.termService.getStudentApplications({
      page: defaultPage(page),
      perPage: defaultPerPage(perPage),
      filter: {
        partnerId: filter?.partnerId,
        isMailSent: filter?.isMailSent,
        status: filterStatus,
        internshipType: filterInternshipType,
        termId: filter?.termId,
      },
      sort: parseSortQuery(sort ?? [], { allowedCols: allowedSortCols }),
      q,
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        students: items.map((s) => ({
          id: s.id!,
          studentId: s.studentId!,
          isMailSent: s.isMailSent!,
          partnerId: s.partnerId!,
          studentIdNumber: s.termStudent?.student?.studentIdNumber ?? '',
          fullName: s.termStudent?.student?.fullName ?? '',
          orgEmail: s.termStudent?.student?.orgEmail ?? '',
          partnerName: s.termPartner?.partner?.name ?? '',
          phoneNumber: s.termStudent?.student?.phoneNumber ?? '',
          registeredAt: toTimestampNullable(s.createdAt),
          status: registrationStatusFromJSON(s.status!),
          internshipType: internshipTypeFromJSON(s.internshipType!),
          termId: s.termId!,
        })),
      },
    };
  }

  async createPost(request: CreatePostRequest): Promise<CreatePostResponse> {
    if (request.startRegAt == null || request.endRegAt == null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_REGISTRATION_PERIOD_MISSING',
      });
    }
    const dto: CreatePostDto = {
      ...request,
      startRegAt: fromTimestamp(request.startRegAt),
      endRegAt: fromTimestamp(request.endRegAt),
    };

    const { id } = await this.termService.createPost(dto);
    return {
      code: status.OK,
      message: 'success',
      data: {
        id: id!,
      },
    };
  }

  async updatePost({
    id,
    filterTermId,
    filterPartnerId,
    ...request
  }: UpdatePostRequest): Promise<UpdatePostResponse> {
    if (request.startRegAt == null || request.endRegAt == null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_REGISTRATION_PERIOD_MISSING',
      });
    }
    const dto: UpdatePostDto = {
      ...request,
      startRegAt: fromTimestamp(request.startRegAt),
      endRegAt: fromTimestamp(request.endRegAt),
    };

    const postId = await this.termService.updatePost(
      id,
      dto,
      filterTermId,
      filterPartnerId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: postId,
      },
    };
  }

  async getPosts(request: GetPostsRequest): Promise<GetPostsResponse> {
    const allowedSortCols = ['title', 'createdAt'];
    const dto: GetPostsQuery = {
      ...request,
      sort: parseSortQuery(request.sort, { allowedCols: allowedSortCols }),
    };

    const { items, meta } = await this.termService.getTermPosts(dto);

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        posts: items.map((p) => ({
          content: '',
          createdAt: toTimestampNullable(p.createdAt),
          startRegAt: toTimestampNullable(p.startRegAt),
          endRegAt: toTimestampNullable(p.endRegAt),
          id: p.id ?? 0,
          jobCount: p.jobCount ?? 0,
          partnerContactEmail: p.partnerContact?.email ?? '',
          partnerContactId: p.partnerContactId ?? 0,
          partnerContactName: p.partnerContact?.fullName ?? '',
          partnerContactPhoneNumber: p.partnerContact?.phoneNumber ?? '',
          partnerId: p.partnerId ?? 0,
          partnerName: p.partner?.name ?? '',
          termId: p.termId ?? 0,
          title: p.title ?? '',
        })),
      },
    };
  }

  async getPostById({
    postId,
    termId,
  }: GetPostByIdRequest): Promise<GetPostByIdResponse> {
    const p = await this.termService
      .getTermPostById(postId, termId)
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errPostNotExistedOrNoPermission(e);
        }
        throw e;
      });

    return {
      code: status.OK,
      message: 'success',
      data: {
        post: {
          content: p.content ?? '',
          createdAt: toTimestampNullable(p.createdAt),
          startRegAt: toTimestampNullable(p.startRegAt),
          endRegAt: toTimestampNullable(p.endRegAt),
          id: p.id ?? 0,
          jobCount: p.jobCount ?? 0,
          partnerContactEmail: p.partnerContact?.email ?? '',
          partnerContactId: p.partnerContactId ?? 0,
          partnerContactName: p.partnerContact?.fullName ?? '',
          partnerContactPhoneNumber: p.partnerContact?.phoneNumber ?? '',
          partnerId: p.partnerId ?? 0,
          partnerName: p.partner?.name ?? '',
          termId: p.termId ?? 0,
          title: p.title ?? '',
        },
      },
    };
  }

  async getStudentPosts(
    req: GetStudentPostsRequest,
  ): Promise<GetStudentPostsResponse> {
    const { items, meta } = await this.termService.getStudentPosts(req);

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        posts: items.map(
          ({ post: p, isRegistered, isSelfRegistered, isExpired }) => ({
            post: {
              content: '',
              createdAt: toTimestampNullable(p.createdAt),
              startRegAt: toTimestampNullable(p.startRegAt),
              endRegAt: toTimestampNullable(p.endRegAt),
              id: p.id ?? 0,
              jobCount: p.jobCount ?? 0,
              partnerContactEmail: p.partnerContact?.email ?? '',
              partnerContactId: p.partnerContactId ?? 0,
              partnerContactName: p.partnerContact?.fullName ?? '',
              partnerContactPhoneNumber: p.partnerContact?.phoneNumber ?? '',
              partnerId: p.partnerId ?? 0,
              partnerName: p.partner?.name ?? '',
              termId: p.termId ?? 0,
              title: p.title ?? '',
            },
            isRegistered: isRegistered,
            isSelfRegistered: isSelfRegistered,
            isRegistrationExpired: isExpired,
          }),
        ),
      },
    };
  }

  async getStudentPostById({
    studentId,
    postId,
  }: GetStudentPostByIdRequest): Promise<GetStudentPostByIdResponse> {
    const { post: p, ...res } = await this.termService.getStudentPostById(
      postId,
      studentId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        ...res,
        isRegistrationExpired: res.isExpired,
        post: {
          content: p.content ?? '',
          createdAt: toTimestampNullable(p.createdAt),
          startRegAt: toTimestampNullable(p.startRegAt),
          endRegAt: toTimestampNullable(p.endRegAt),
          id: p.id ?? 0,
          jobCount: p.jobCount ?? 0,
          partnerContactEmail: p.partnerContact?.email ?? '',
          partnerContactId: p.partnerContactId ?? 0,
          partnerContactName: p.partnerContact?.fullName ?? '',
          partnerContactPhoneNumber: p.partnerContact?.phoneNumber ?? '',
          partnerId: p.partnerId ?? 0,
          partnerName: p.partner?.name ?? '',
          termId: p.termId ?? 0,
          title: p.title ?? '',
        },
      },
    };
  }

  async batchUpdateRegistrationStatus({
    termId,
    partnerId,
    status: regStatus,
    registrationIds,
  }: BatchUpdateRegistrationStatusRequest): Promise<BatchUpdateRegistrationStatusResponse> {
    await this.termService.batchUpdateRegistrationStatus(
      registrationIds,
      registrationStatusToJSON(regStatus) as RegistrationStatus,
      termId,
      partnerId,
    );

    return {
      code: status.OK,
      message: 'success',
    };
  }

  batchNotifyStudentAboutRegistrationStatus(
    request: BatchNotifyStudentAboutRegistrationStatusRequest,
  ): Promise<BatchNotifyStudentAboutRegistrationStatusResponse> {
    throw new Error('Method not implemented.');
  }

  async giveScore({
    termId,
    studentId,
    score,
    filterLecturerId,
  }: GiveScoreRequest): Promise<GiveScoreResponse> {
    await this.termService.updateStudentScore(
      termId,
      studentId,
      score,
      filterLecturerId,
    );

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async selectInternshipPartner({
    termId,
    studentId,
    partnerId,
  }: SelectInternshipPartnerRequest): Promise<SelectInternshipPartnerResponse> {
    await this.termService.selectFinalPartner(termId, studentId, partnerId);

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async downloadStudentReport({
    termId,
    studentId,
    filterLecturerId,
  }: DownloadStudentReportRequest): Promise<DownloadStudentReportResponse> {
    const downloadUrl = await this.termService.downloadReport(
      termId,
      studentId,
      filterLecturerId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        downloadUrl,
      },
    };
  }

  async getAppliedPartnersByStudentId({
    studentId,
    termId,
  }: GetAppliedPartnersByStudentIdRequest): Promise<GetAppliedPartnersByStudentIdResponse> {
    const registrations = await this.termService.getAppliedPartnersByStudentId(
      termId,
      studentId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: registrations.map((r) => ({
        partnerId: r.partnerId!,
        id: r.id!,
        partnerName: r.termPartner!.partner!.name!,
        registeredAt: toTimestampNullable(r.createdAt),
        partnerStatus: partnerStatusFromJSON(r.termPartner!.status!),
        registrationStatus: registrationStatusFromJSON(r.status!),
        isSelfRegistered: r.isSelfRegistered!,
      })),
    };
  }

  async removeLecturerFromTerm({
    termId,
    lecturerId,
  }: RemoveLecturerFromTermRequest): Promise<RemoveLecturerFromTermResponse> {
    await this.termService.removeTermLecturer(termId, lecturerId);

    return {
      code: status.OK,
      message: 'success',
    };
  }

  /** Sign a student up for an internship term.
   *  The term must be in registration period in order to sign up.
   */
  async signUpForTerm({
    termId,
    studentId,
  }: SignUpForTermRequest): Promise<SignUpForTermResponse> {
    await this.termService.addTermStudent(termId, studentId);

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async cancelTermSignUp({
    termId,
    studentId,
  }: CancelTermSignUpRequest): Promise<CancelTermSignUpResponse> {
    await this.termService.removeTermStudent(termId, studentId);

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async batchChangeTermPartnerStatus({
    status: partnerStatus,
    termId,
    partnerIds,
  }: BatchChangeTermPartnerStatusRequest): Promise<BatchChangeTermPartnerStatusResponse> {
    await this.termService.batchChangeTermPartnerStatus(
      termId,
      partnerIds,
      partnerStatusToJSON(partnerStatus) as PartnerInternshipStatus,
    );

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async batchAddLecturersToTerm({
    termId,
    lecturerIds,
    organizationId,
  }: BatchAddLecturersToTermRequest): Promise<BatchAddLecturersToTermResponse> {
    const successIds = await this.termService.batchAddLecturersToTerm(
      termId,
      lecturerIds,
      organizationId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        successIds,
      },
    };
  }

  async batchAddPartnersToTerm({
    termId,
    partnerIds,
  }: BatchAddPartnersToTermRequest): Promise<BatchAddPartnersToTermResponse> {
    const successIds = await this.termService.batchAddPartnersToTerm(
      termId,
      partnerIds,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        successIds,
      },
    };
  }

  async getTermListing({
    organizationId,
  }: GetTermListingRequest): Promise<GetTermListingResponse> {
    const terms = await this.termService.getTermListing(organizationId);

    return {
      code: status.OK,
      message: 'success',
      data: terms.map((t) => ({
        id: t.id!,
        term: t.term!,
        year: t.year!,
      })),
    };
  }

  async getLecturerTermListing({
    lecturerId,
  }: GetLecturerTermListingRequest): Promise<GetLecturerTermListingResponse> {
    const terms = await this.termService.getLecturerTermListing(lecturerId);

    return {
      code: status.OK,
      message: 'success',
      data: terms.map((t) => ({
        id: t.id!,
        term: t.term!,
        year: t.year!,
      })),
    };
  }

  async getStudentTermListing({
    studentId,
  }: GetStudentTermListingRequest): Promise<GetStudentTermListingResponse> {
    const terms = await this.termService.getStudentTermListing(studentId);

    return {
      code: status.OK,
      message: 'success',
      data: terms.map((t) => ({
        id: t.id!,
        term: t.term!,
        year: t.year!,
      })),
    };
  }

  async getPartnerTermListing({
    partnerId,
  }: GetPartnerTermListingRequest): Promise<GetPartnerTermListingResponse> {
    const terms = await this.termService.getPartnersTermListing(partnerId);

    return {
      code: status.OK,
      message: 'success',
      data: terms.map((t) => ({
        id: t.id!,
        term: t.term!,
        year: t.year!,
      })),
    };
  }

  async getLatestTerm({
    studentId,
    organizationId,
  }: GetLatestTermRequest): Promise<GetLatestTermResponse> {
    const term = await this.termService.getOrgLatestTerm(organizationId);

    const studentInTerm = await this.termService.checkStudentInTerm(
      studentId,
      term.id!,
    );

    const currentTime = new Date();

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: term.id!,
        term: term.term!,
        year: term.year!,
        startDate: term.startDate!,
        endDate: term.endDate!,
        startRegAt: toTimestampNullable(term.startRegAt),
        endRegAt: toTimestampNullable(term.endRegAt),
        isRegistrationExpired: compareDate(currentTime, term.endRegAt!) > 0,
        isActive:
          compareDate(currentTime, new Date(term.endDate!)) < 0 &&
          compareDate(currentTime, new Date(term.startDate!)) > 0,
        isRegistered: studentInTerm,
      },
    };
  }

  async getTermPartnerListing({
    termId,
  }: GetTermPartnerListingRequest): Promise<GetTermPartnerListingResponse> {
    const partners = await this.termService.getTermPartnerListing(termId);

    return {
      code: status.OK,
      message: 'success',
      data: partners.map((p) => ({
        id: p.id!,
        name: p.name!,
        logoUrl: p.logoUrl ?? '',
        status:
          p.termPartners == null || p.termPartners.length === 0
            ? undefined
            : partnerStatusFromJSON(p.termPartners[0].status),
      })),
    };
  }

  async updateTerm({
    id: termId,
    filterOrganizationId,
    ...dto
  }: UpdateTermRequest): Promise<UpdateTermResponse> {
    await this.termService.updateTerm(
      {
        ...dto,
        startRegAt: fromTimestampNullable(dto.startRegAt),
        endRegAt: fromTimestampNullable(dto.endRegAt),
      },
      termId,
      filterOrganizationId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: termId,
      },
    };
  }

  async checkStudentInTerm({
    studentId,
    termId,
  }: CheckStudentInTermRequest): Promise<CheckStudentInTermResponse> {
    const isIn = await this.termService.checkStudentInTerm(studentId, termId);

    return {
      code: status.OK,
      message: 'success',
      data: {
        isIn,
      },
    };
  }

  checkLecturerInTerm(
    request: CheckLecturerInTermRequest,
  ): Promise<CheckLecturerInTermResponse> {
    throw new Error('Method not implemented.');
  }

  checkPartnerInTerm(
    request: CheckPartnerInTermRequest,
  ): Promise<CheckPartnerInTermResponse> {
    throw new Error('Method not implemented.');
  }

  async getTermStudentDetails({
    termId,
    studentId,
  }: GetTermStudentDetailsRequest): Promise<GetTermStudentDetailsResponse> {
    const termStudent = await this.termService.getTermStudentDetails(
      termId,
      studentId,
    );

    const supervisor = termStudent.termLecturer?.lecturer;
    const registrations = termStudent.termPartnerRegistrations ?? [];
    const selectedPartnerIndex = registrations.findIndex(
      (v) => v.status === RegistrationStatus.SELECTED,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        supervisor:
          supervisor == null
            ? undefined
            : {
                name: supervisor.fullName ?? '',
                id: supervisor.id ?? 0,
                orgEmail: supervisor.orgEmail ?? '',
                phoneNumber: supervisor.phoneNumber ?? '',
                personalEmail: supervisor.personalEmail ?? '',
              },
        appliedPartners: registrations.map((tpr) => {
          const partner = tpr.termPartner?.partner;
          return {
            id: partner?.id ?? 0,
            name: partner?.name ?? '',
            createdAt: toTimestampNullable(tpr.createdAt),
            applyStatus: registrationStatusFromJSON(tpr.status),
            partnerStatus: partnerStatusFromJSON(tpr.termPartner?.status),
            isSelfRegistered: tpr?.isSelfRegistered ?? false,
            internshipType: internshipTypeFromJSON(tpr.internshipType),
          };
        }),
        selectedPartnerIndex:
          selectedPartnerIndex === -1 ? undefined : selectedPartnerIndex,
        reportFileName: termStudent.reportFileName ?? '',
      },
    };
  }

  @Post(':termId/students/:studentId/report')
  @UseInterceptors(
    FileInterceptor(REPORT_FILE_KEY, {
      limits: {
        fileSize: REPORT_MAX_SIZE_IN_BYTES,
      },
    }),
  )
  async uploadReport(
    @UploadedFile() reportFile: Express.Multer.File,
    @Param('studentId', ParseIntPipe) studentId: number,
    @Param('termId', ParseIntPipe) termId: number,
  ): Promise<{ code: number; message: string }> {
    if (reportFile.mimetype !== PDF_MIME_TYPE) {
      throw errInvalidFileType(PDF_MIME_TYPE, reportFile.mimetype);
    }

    await this.termService
      .uploadReport(termId, studentId, reportFile)
      .catch((e) => {
        throw errFailedToUploadFile(e);
      });

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async getTermLecturerListing({
    termId,
  }: GetTermLecturerListingRequest): Promise<GetTermLecturerListingResponse> {
    const lecturers = await this.termService.getTermLecturerListing(termId);

    return {
      code: status.OK,
      message: 'success',
      data: lecturers.map((l) => ({
        id: l.id!,
        fullName: l.fullName!,
      })),
    };
  }

  async exportTermStudents({
    q,
    sort,
    filter,
  }: ExportTermStudentsRequest): Promise<ExportTermStudentsResponse> {
    const allowedCols = ['id', 'fullName', 'studentIdNumber'];

    const downloadUrl = await this.termService.exportTermStudents({
      q,
      sort: parseSortQuery(sort, { allowedCols }),
      filter,
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        downloadUrl,
      },
    };
  }

  async exportTermPartners({
    filter,
    sort,
    q,
  }: ExportTermPartnersRequest): Promise<ExportTermPartnersResponse> {
    const allowedCols = ['name', 'joinedAt'];

    const downloadUrl = await this.termService.exportTermPartners({
      q,
      sort: parseSortQuery(sort, { allowedCols }),
      filter: {
        ...filter,
        status:
          filter?.status == null
            ? undefined
            : (partnerStatusToJSON(filter.status) as PartnerInternshipStatus),
      },
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        downloadUrl,
      },
    };
  }

  async exportStudentApplications({
    filter,
    sort,
    q,
  }: ExportStudentApplicationsRequest): Promise<ExportStudentApplicationsResponse> {
    const allowedCols = ['id', 'fullName', 'studentIdNumber'];

    let filterInternshipType: InternshipType | undefined;
    let filterStatus: RegistrationStatus | undefined;
    try {
      if (filter?.status != null) {
        filterStatus = RegistrationStatusMethods.fromPb(filter.status);
      }
      if (filter?.internshipType != null) {
        filterInternshipType = InternshipTypeMethods.fromPb(
          filter.internshipType,
        );
      }
    } catch (e) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_ENUM',
      });
    }
    const downloadUrl = await this.termService.exportStudentApplications({
      q,
      sort: parseSortQuery(sort, { allowedCols }),
      filter: {
        ...filter,
        internshipType: filterInternshipType,
        status: filterStatus,
      },
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        downloadUrl,
      },
    };
  }

  async unregisterPartner({
    termId,
    partnerId,
    studentId,
  }: UnregisterPartnerRequest): Promise<UnregisterPartnerResponse> {
    await this.termService.unregisterPartner(termId, partnerId, studentId);

    return {
      code: status.OK,
      message: 'success',
    };
  }
}
