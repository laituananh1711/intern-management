import { deserializeJSON } from '@app/utils';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { KafkaService, SubscribeTo } from '@uetwork/nestjs-kafka';
import { Repository } from 'typeorm';
import {
  KAFKA_LECTURERS_TOPIC,
  KAFKA_ORGS_TOPIC,
  KAFKA_PARTNERS_TOPIC,
  KAFKA_PARTNER_CONTACT_TOPIC,
  KAFKA_PARTNER_ORG_TOPIC,
  KAFKA_PROVIDER,
  KAFKA_SCHOOL_CLASSES_TOPIC,
  KAFKA_STUDENTS_TOPIC,
} from '../../constants';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Organization } from '../../database/entities/organization.entity';
import { PartnerContact } from '../../database/entities/partner-contact.entity';
import { PartnerOrganization } from '../../database/entities/partner-org.entity';
import { Partner } from '../../database/entities/partner.entity';
import { SchoolClass } from '../../database/entities/school-class.entity';
import { Student } from '../../database/entities/student.entity';
import { LecturerValue } from './messages/lecturer.message';
import { OrganizationValue } from './messages/org.message';
import {
  microUnixTimestampToDate,
  PartnerContactValue,
} from './messages/partner-contact.message';
import {
  daysSinceEpochToDate,
  PartnerOrgValue,
} from './messages/partner-org.message';
import { PartnerValue } from './messages/partner.message';
import { SchoolClassValue } from './messages/school-class.message';
import { StudentValue } from './messages/student.message';

/**
 * This service handles miscellaneous events from kafka, like replication events.
 */
@Injectable()
export class ConsumerService {
  private logger = new Logger(ConsumerService.name);

  constructor(
    @InjectRepository(Student) private studentRepo: Repository<Student>,
    @InjectRepository(Organization) private orgRepo: Repository<Organization>,
    @InjectRepository(Lecturer) private lecturerRepo: Repository<Lecturer>,
    @InjectRepository(Partner) private partnerRepo: Repository<Partner>,
    @InjectRepository(SchoolClass)
    private schoolClassRepo: Repository<SchoolClass>,
    @InjectRepository(PartnerOrganization)
    private partnerOrgRepo: Repository<PartnerOrganization>,
    @InjectRepository(PartnerContact)
    private partnerContactRepo: Repository<PartnerContact>,
    @Inject(KAFKA_PROVIDER) private client: KafkaService,
  ) {}

  onModuleInit() {
    const topics: string[] = [
      KAFKA_STUDENTS_TOPIC,
      KAFKA_LECTURERS_TOPIC,
      KAFKA_PARTNERS_TOPIC,
      KAFKA_ORGS_TOPIC,
      KAFKA_SCHOOL_CLASSES_TOPIC,
      KAFKA_PARTNER_ORG_TOPIC,
      KAFKA_PARTNER_CONTACT_TOPIC,
    ];

    for (const topic of topics) {
      this.client.subscribeToResponseOf(topic, this);
    }
  }

  @SubscribeTo(KAFKA_STUDENTS_TOPIC)
  async handleStudentChanged(payload: unknown): Promise<void> {
    if (payload === null) {
      return;
    }
    const { value, err } = deserializeJSON(StudentValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize student value:', err);
      throw err;
    }

    const studentRawEntity: Partial<Student> = {
      id: value.id,
      fullName: value.fullName,
      studentIdNumber: value.studentIdNumber,
      phoneNumber: value.phoneNumber,
      resumeUrl: value.resumeUrl,
      schoolClassId: value.schoolClassId,
      organizationId: value.organizationId,
      userId: value.userId,
      orgEmail: value.orgEmail,
      personalEmail: value.personalEmail,
    };

    if (value.__deleted !== 'true') {
      await this.studentRepo.save(new Student(studentRawEntity));
    } else {
      await this.studentRepo.delete(value.id);
    }
  }

  @SubscribeTo(KAFKA_ORGS_TOPIC)
  async handleOrgChanged(payload: unknown) {
    if (payload === null) return;

    const { value, err } = deserializeJSON(OrganizationValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize org value:', err);
      throw err;
    }

    const { id, name, __deleted } = value;
    if (__deleted !== 'true') {
      await this.orgRepo.save(new Organization({ id, name }));
    } else {
      await this.orgRepo.delete(value.id);
    }
  }

  @SubscribeTo(KAFKA_LECTURERS_TOPIC)
  async handleLecturerChange(payload: unknown) {
    if (payload == null) return;

    const { value, err } = deserializeJSON(LecturerValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize lecturer value:', err);
      throw err;
    }

    const {
      id,
      fullName,
      organizationId,
      userId,
      orgEmail,
      personalEmail,
      __deleted,
    } = value;
    if (__deleted !== 'true') {
      this.lecturerRepo.save(
        new Lecturer({
          id,
          fullName,
          organizationId,
          userId,
          orgEmail,
          personalEmail: personalEmail ?? '',
        }),
      );
    } else {
      await this.lecturerRepo.delete(id);
    }
  }

  @SubscribeTo(KAFKA_PARTNERS_TOPIC)
  async handlePartnerChange(payload: unknown) {
    if (payload == null) return;

    const { value, err } = deserializeJSON(PartnerValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize partner value:', err);
      throw err;
    }

    const { __deleted } = value;
    if (__deleted !== 'true') {
      this.partnerRepo.save(
        new Partner({
          id: value.id,
          name: value.name,
          logoUrl: value.logoUrl,
          homepageUrl: value.homepageUrl,
          description: value.description,
          userId: value.userId,
          email: value.email,
          phoneNumber: value.phoneNumber,
          address: value.address,
        }),
      );
    } else {
      await this.partnerRepo.delete(value.id);
    }
  }

  @SubscribeTo(KAFKA_SCHOOL_CLASSES_TOPIC)
  async handleSchoolClassChange(payload: unknown) {
    if (payload == null) return;

    const { value, err } = deserializeJSON(SchoolClassValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize school class value:', err);
    }

    const { __deleted, id, programName, name } = value;
    if (__deleted !== 'true') {
      this.schoolClassRepo.save(
        new SchoolClass({
          id,
          programName,
          name,
        }),
      );
    } else {
      this.schoolClassRepo.delete(id);
    }
  }

  @SubscribeTo(KAFKA_PARTNER_ORG_TOPIC)
  async handlePartnerOrgChange(payload: unknown) {
    if (payload == null) return;

    const { value, err } = deserializeJSON(PartnerOrgValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize partner org value:', err);
    }

    const { __deleted, id, organizationId, createdAt, partnerId, expiredDate } =
      value;
    if (__deleted !== 'true') {
      this.partnerOrgRepo.save({
        id,
        partnerId,
        createdAt: new Date(createdAt),
        expiredDate:
          expiredDate == null
            ? undefined
            : daysSinceEpochToDate(expiredDate).toISOString().substring(0, 10),
        organizationId,
      });
    } else {
      this.partnerOrgRepo.delete(id);
    }
  }

  @SubscribeTo(KAFKA_PARTNER_CONTACT_TOPIC)
  async handlePartnerContactChange(payload: unknown) {
    if (payload == null) return;

    const { value, err } = deserializeJSON(PartnerContactValue, payload);
    if (err != null) {
      this.logger.error('cannot deserialize partner contact value', err);
      return;
    }

    const {
      email,
      fullName,
      id,
      partnerId,
      phoneNumber,
      deletedAt,
      __deleted,
    } = value;
    if (__deleted !== 'true') {
      this.partnerContactRepo.save({
        email,
        fullName,
        id,
        partnerId,
        phoneNumber,
        deletedAt:
          deletedAt == null ? undefined : microUnixTimestampToDate(deletedAt),
      });
    } else {
      this.partnerContactRepo.delete(id);
    }
  }
}
