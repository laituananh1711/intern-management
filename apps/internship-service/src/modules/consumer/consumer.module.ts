import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Organization } from '../../database/entities/organization.entity';
import { PartnerContact } from '../../database/entities/partner-contact.entity';
import { PartnerOrganization } from '../../database/entities/partner-org.entity';
import { Partner } from '../../database/entities/partner.entity';
import { SchoolClass } from '../../database/entities/school-class.entity';
import { Student } from '../../database/entities/student.entity';
import { ConsumerService } from './consumer.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Student,
      Organization,
      SchoolClass,
      Lecturer,
      Partner,
      PartnerOrganization,
      PartnerContact,
    ]),
  ],
  providers: [ConsumerService],
  exports: [TypeOrmModule],
})
export class ConsumerModule {}
