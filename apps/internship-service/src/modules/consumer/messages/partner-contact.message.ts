import { Expose } from 'class-transformer';
import { IsInt, IsNumber, IsOptional, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class PartnerContactValue extends ChangedMessage {
  @IsInt()
  id!: number;

  @IsString()
  @Expose({ name: 'full_name' })
  fullName!: string;

  @IsString()
  @Expose({ name: 'phone_number' })
  phoneNumber!: string;

  @IsInt()
  @Expose({
    name: 'partner_id',
  })
  partnerId!: number;

  @IsString()
  email!: string;

  @IsNumber()
  @IsOptional()
  @Expose({
    name: 'deleted_at',
  })
  deletedAt?: number;
}

export function microUnixTimestampToDate(inp: number): Date {
  return new Date(inp / 1_000);
}
