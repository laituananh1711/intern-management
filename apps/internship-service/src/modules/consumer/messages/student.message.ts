import { Expose } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class StudentValue extends ChangedMessage {
  @IsNumber()
  id!: number;

  @IsNumber()
  @IsOptional()
  @Expose({ name: 'user_id' })
  userId?: number;

  @IsString()
  @Expose({ name: 'student_id_number' })
  studentIdNumber!: string;

  @IsString()
  @Expose({ name: 'full_name' })
  fullName!: string;

  @IsString()
  @IsOptional()
  @Expose({ name: 'resume_url' })
  resumeUrl?: string;

  @IsString()
  @IsOptional()
  @Expose({ name: 'phone_number' })
  phoneNumber?: string;

  @IsNumber()
  @IsOptional()
  @Expose({ name: 'school_class_id' })
  schoolClassId?: number;

  @IsString()
  @Expose({ name: 'org_email' })
  orgEmail!: string;

  @IsString()
  @IsOptional()
  @Expose({ name: 'personal_email' })
  personalEmail?: string;

  @IsNumber()
  @Expose({ name: 'organization_id' })
  organizationId!: number;
}

export class StudentKey {
  @IsNumber()
  id!: number;
}
