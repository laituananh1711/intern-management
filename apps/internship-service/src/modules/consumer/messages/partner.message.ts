import { Expose } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class PartnerKey {
  @IsNumber()
  id!: number;
}

export class PartnerValue extends ChangedMessage {
  @IsNumber()
  id!: number;

  @IsString()
  name!: string;

  @IsString()
  email!: string;

  @IsString()
  @Expose({ name: 'homepage_url' })
  homepageUrl!: string;

  @IsNumber()
  @IsOptional()
  @Expose({ name: 'user_id' })
  userId?: number;

  @IsString()
  @Expose({ name: 'logo_url' })
  logoUrl!: string;

  @IsString()
  @Expose({ name: 'phone_number' })
  @IsOptional()
  phoneNumber?: string;

  @IsString()
  @IsOptional()
  address?: string;

  @IsString()
  description!: string;
}
