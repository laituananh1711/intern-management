import { Expose } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class LecturerKey {
  @IsNumber()
  id!: number;
}

export class LecturerValue extends ChangedMessage {
  @IsNumber()
  id!: number;

  @IsNumber()
  @IsOptional()
  @Expose({ name: 'user_id' })
  userId?: number;

  @IsString()
  @Expose({ name: 'full_name' })
  fullName!: string;

  @IsString()
  @Expose({ name: 'org_email' })
  orgEmail!: string;

  @IsString()
  @IsOptional()
  @Expose({ name: 'personal_email' })
  personalEmail?: string;

  @IsNumber()
  @Expose({ name: 'organization_id' })
  organizationId!: number;
}
