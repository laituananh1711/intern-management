import { Expose } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class PartnerOrgValue extends ChangedMessage {
  @IsNumber()
  id!: number;

  @IsNumber()
  @Expose({ name: 'partner_id' })
  partnerId!: number;

  @IsNumber()
  @Expose({ name: 'organization_id' })
  organizationId!: number;

  @IsNumber()
  @IsOptional()
  @Expose({ name: 'expired_date' })
  // io.debezium.time.Date: number of days since epoch.
  expiredDate?: number;

  @Expose({ name: 'created_at' })
  @IsString()
  createdAt!: string;
}

export function daysSinceEpochToDate(inp: number): Date {
  return new Date(inp * 86_400_000);
}
