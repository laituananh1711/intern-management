import { IsOptional, IsString } from 'class-validator';

/**
 * This class contains extra fields specified in record extraction
 * connector config.
 */
export class ChangedMessage {
  @IsString()
  @IsOptional()
  __deleted?: 'true' | 'false';

  @IsString()
  @IsOptional()
  __op?: 'c' | 'r' | 'u' | 'd';
}
