import { IsNumber, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class OrganizationValue extends ChangedMessage {
  @IsNumber()
  id!: number;

  @IsString()
  name!: string;
}

export class OrganizationKey {
  @IsNumber()
  id!: number;
}
