import { IsInt, IsOptional, IsString } from 'class-validator';
import { ChangedMessage } from './common';

export class SchoolClassValue extends ChangedMessage {
  @IsInt()
  id!: number;

  @IsString()
  name!: string;

  @IsString()
  @IsOptional()
  programName?: string;
}
