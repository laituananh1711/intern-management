export const errFetchingMailingList = (e?: Error) =>
  new Error('error fetching mailing list: ' + e);
