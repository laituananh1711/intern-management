import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { OnEvent } from '@nestjs/event-emitter';
import { join } from 'path';
import { EntityManager, In } from 'typeorm';
import { events } from '../../constants';
import { InternshipTerm } from '../../database/entities/internship-term.entity';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Partner } from '../../database/entities/partner.entity';
import { Student } from '../../database/entities/student.entity';
import {
  RegistrationStatus,
  TermPartnerRegistration,
} from '../../database/entities/term-partner-registration.entity';
import { PartnerInternshipStatus } from '../../database/entities/term-partner.entity';
import { InternshipTermRepository } from '../term/term.repository';
import {
  BatchPartnerRegistrationChangedEvent,
  PartnerStatusChangedEvent,
  ScoreGivenEvent,
  SupervisorAssignedEvent,
  TermSignUpEvent,
} from '../term/types';
import { errFetchingMailingList } from './errors';

@Injectable()
export class MailService {
  private manager: EntityManager;
  private logger = new Logger(MailerService.name);
  private clientHost: string;

  constructor(
    private mailerService: MailerService,
    private termRepo: InternshipTermRepository,
    configService: ConfigService,
  ) {
    this.manager = this.termRepo.manager;
    this.clientHost = configService.get('CLIENT_HOST', 'http://localhost:3000');
  }

  @OnEvent(events.TERM_SIGN_UP)
  async handleTermSignedUp({
    termId,
    studentId,
  }: TermSignUpEvent): Promise<void> {
    this.logger.debug(
      `handling term sign up event: termId ${termId}, studentId ${studentId}`,
    );
    const termPromise = await this.termRepo.findOne({
      select: ['year', 'term'],
      where: {
        id: termId,
      },
    });

    const studentPromise = await this.manager.findOne(Student, {
      select: ['orgEmail'],
      where: {
        id: studentId,
      },
    });

    const [term, student] = await Promise.all([termPromise, studentPromise]);
    if (term == null || student == null) {
      Logger.error(
        'error sending term sign up email for term ' +
          termId +
          'to student ' +
          studentId,
      );
      return;
    }

    await this.mailerService.sendMail({
      to: student.orgEmail!,
      subject: 'Đăng ký kỳ thực tập thành công',
      template: join(__dirname, './templates/vi/term_sign_up'),
      context: {
        year: term.year!,
        term: term.term!,
        clientHost: this.clientHost,
      },
    });
  }

  @OnEvent(events.PARTNER_STATUS_CHANGED)
  async handlePartnerStatusChanged({
    termId,
    partnerId,
    newStatus,
  }: PartnerStatusChangedEvent) {
    this.logger.debug(
      `handling partner status changed event: termId ${termId}, partnerId: ${partnerId}, status: ${newStatus}`,
    );
    const registrationsPromise = this.manager
      .createQueryBuilder(TermPartnerRegistration, 'tpr')
      .leftJoinAndSelect('tpr.termStudent', 'ts')
      .leftJoinAndSelect('ts.student', 's')
      .where('tpr.termId = :termId AND tpr.partnerId = :partnerId', {
        termId,
        partnerId,
      })
      .getMany();

    const partnerPromise = this.manager.findOne(Partner, {
      select: ['name'],
      where: {
        id: partnerId,
      },
    });
    const [registrations, partner] = await Promise.all([
      registrationsPromise,
      partnerPromise,
    ]);

    if (registrations.length === 0) {
      this.logger.error('no user signed up for partner ' + partnerId);
      return;
    }
    if (partner == null) {
      this.logger.error('partner not found ' + partnerId);
      return;
    }

    const mailingList: string[] = [];
    for (const reg of registrations) {
      mailingList.push(reg.termStudent!.student!.orgEmail!);
    }

    const sendMailOptions: ISendMailOptions = {
      to: mailingList,
    };
    if (newStatus === PartnerInternshipStatus.ACCEPTED) {
      sendMailOptions.subject = 'Công ty bạn đăng ký đã được khoa chấp nhận';
      sendMailOptions.template = join(
        __dirname,
        './templates/vi/partner_status_accepted',
      );
      sendMailOptions.context = {
        partnerName: partner.name!,
        clientHost: this.clientHost,
      };
    } else if (newStatus === PartnerInternshipStatus.REJECTED) {
      sendMailOptions.subject = 'Công ty bạn đăng ký đã bị khoa từ chối';
      sendMailOptions.template = join(
        __dirname,
        './templates/vi/partner_status_rejected',
      );
      sendMailOptions.context = {
        partnerName: partner.name!,
        clientHost: this.clientHost,
      };
    } else {
      Logger.error('unknown status');
      return;
    }

    await this.mailerService.sendMail(sendMailOptions);
  }

  @OnEvent(events.SUPERVISOR_ASSIGNED)
  async handleSupervisorAssignedEvent({
    termId,
    studentIds,
    supervisorId,
  }: SupervisorAssignedEvent): Promise<void> {
    this.logger.debug(`handling supervisor assigned event`);
    const termPromise = this.manager.findOne(InternshipTerm, {
      select: ['year', 'term'],
      where: {
        id: termId,
      },
    });
    const studentsPromise = this.manager.find(Student, {
      select: ['orgEmail'],
      where: {
        id: In(studentIds),
      },
    });
    const supervisorPromise = this.manager.findOne(Lecturer, {
      select: ['orgEmail', 'fullName', 'personalEmail'],
      where: {
        id: supervisorId,
      },
    });

    try {
      const [term, students, supervisor] = await Promise.all([
        termPromise,
        studentsPromise,
        supervisorPromise,
      ]);

      if (term == null || students.length === 0 || supervisor == null) {
        this.logger.error(`can't find term/supervisor or students`);
        return;
      }

      const { fullName, orgEmail, personalEmail } = supervisor;
      const { year, term: termNum } = term;

      const mailingList = students.map((s) => s.orgEmail!);

      await this.mailerService.sendMail({
        to: mailingList,
        subject: 'Thông tin giảng viên hướng dẫn của bạn',
        template: join(__dirname, './templates/vi/supervisor_assigned'),
        context: {
          fullName,
          orgEmail,
          personalEmail,
          phoneNumber: '',
          year,
          term: termNum,
        },
      });
    } catch (e) {
      this.logger.error('error getting info for event: ' + e);
    }
  }

  @OnEvent(events.SCORE_GIVEN)
  async handleScoreGivenEvent({ termId, score, studentId }: ScoreGivenEvent) {
    this.logger.debug('handling score given event');
    const termPromise = this.manager.findOne(InternshipTerm, {
      select: ['year', 'term'],
      where: {
        id: termId,
      },
    });
    const mailingListPromise = this.getMailingList([studentId]);

    try {
      const [term, mailingList] = await Promise.all([
        termPromise,
        mailingListPromise,
      ]);

      if (term == null || mailingList.length === 0) {
        this.logger.error('error fetching term info or mailing list');
        return;
      }

      const { year, term: termNum } = term;

      await this.mailerService.sendMail({
        to: mailingList,
        subject: 'Điểm thực tập của bạn',
        template: join(__dirname, './templates/vi/score_given'),
        context: {
          year,
          term: termNum,
          score: score.toFixed(1),
        },
      });
    } catch (e) {
      this.logger.error('error sending score given email: ', e);
    }
  }

  @OnEvent(events.BATCH_PARTNER_REG_STATUS_CHANGED)
  async handleBatchPartnerRegStatusChangedEvent({
    newStatus,
    registrationIds,
  }: BatchPartnerRegistrationChangedEvent): Promise<void> {
    this.logger.debug(
      'handling partner reg status changed event',
      registrationIds,
      newStatus,
    );
    try {
      const registrationsPromise = this.manager
        .createQueryBuilder(TermPartnerRegistration, 'tpr')
        .select([
          's.orgEmail',
          'p.name',
          'tpr.id',
          'ts',
          'tp',
          't.year',
          't.term',
        ])
        .leftJoin('tpr.termStudent', 'ts')
        .leftJoin('ts.student', 's')
        .leftJoin('tpr.termPartner', 'tp')
        .leftJoin('tp.partner', 'p')
        .leftJoin('tp.term', 't') // should be ts.term, not tpr.term because of typeorm bug.
        .where('tpr.id IN (:regIds)', {
          regIds: registrationIds,
        })
        .getMany();

      const registrations = await registrationsPromise;

      if (registrations.length === 0) {
        this.logger.error('error getting info about registrations or term');
        return;
      }

      const sendMailPromises: Promise<void>[] = [];
      for (const registration of registrations) {
        const orgEmail = registration.termStudent!.student!.orgEmail!;
        const partnerName = registration.termPartner!.partner!.name!;

        const sendMailPromise = this.manager
          .transaction(async (manager) => {
            await manager.update(TermPartnerRegistration, registration.id, {
              isMailSent: true,
            });

            const term = registration.termPartner!.term!;

            const sendMailOptions: ISendMailOptions = {
              to: orgEmail,
            };
            if (newStatus === RegistrationStatus.PASSED) {
              sendMailOptions.subject =
                'Thông báo đỗ phỏng vấn: ' + partnerName;
              sendMailOptions.template = join(
                __dirname,
                './templates/vi/partner_reg_passed',
              );
              sendMailOptions.context = {
                partnerName,
                year: term.year,
                term: term.term,
                clientHost: this.clientHost,
              };
            } else if (newStatus === RegistrationStatus.FAILED) {
              sendMailOptions.subject = 'Thông báo trượt phỏng vấn';
              sendMailOptions.template = join(
                __dirname,
                './templates/vi/partner_reg_failed',
              );
              sendMailOptions.context = {
                partnerName,
                year: term.year,
                term: term.term,
                clientHost: this.clientHost,
              };
            } else {
              throw new Error('unknown registration status: ' + newStatus);
            }

            await this.mailerService.sendMail(sendMailOptions);
          })
          .catch((e) => {
            this.logger.error(
              `error sending email and updating status for registration ${registration.id}: ${e} `,
            );
          });

        sendMailPromises.push(sendMailPromise);
      }

      await Promise.allSettled(sendMailPromises);
    } catch (e) {
      this.logger.error(`error handling reg status changed event: ${e}`);
    }
  }

  private async getMailingList(studentIds: number[]): Promise<string[]> {
    const students = await this.manager
      .find(Student, {
        select: ['orgEmail'],
        where: {
          id: In(studentIds),
        },
      })
      .catch((e) => {
        throw errFetchingMailingList(e);
      });

    return students.map((s) => s.orgEmail!);
  }
}
