import { Controller, Get } from '@nestjs/common';

@Controller('health')
export class HealthController {
  @Get('live')
  checkLiveness() {
    return 'OK';
  }

  @Get('ready')
  checkReadiness() {
    return 'OK';
  }
}
