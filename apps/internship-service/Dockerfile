FROM node:14-alpine AS builder

ARG BUILD_CONTEXT

WORKDIR /app

COPY package.json yarn.lock /app/
COPY ./apps/$BUILD_CONTEXT/package.json /app/apps/$BUILD_CONTEXT/
COPY ./libs/package.json /app/libs/package.json

# TODO: use --only-dev when it is available
RUN yarn install --frozen-lockfile

COPY . .

RUN yarn run build internship-service

FROM node:14-alpine
RUN apk add dumb-init curl

ARG BUILD_CONTEXT
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /app

COPY package.json yarn.lock /app/
COPY ./apps/$BUILD_CONTEXT/package.json /app/apps/$BUILD_CONTEXT/
COPY ./libs/package.json /app/libs/package.json

RUN yarn install --prod --cache-folder /tmp/.ycache; rm -rf /tmp/.ycache;

# Copy migrations file
COPY ./apps/$BUILD_CONTEXT/src/database/migrations /app/apps/$BUILD_CONTEXT/src/database/migrations
COPY ./apps/$BUILD_CONTEXT/src/ormconfig.ts /app/apps/$BUILD_CONTEXT/src/ormconfig.ts

# Copy proto file
COPY ./proto /app/proto
COPY --from=builder /app/dist ./dist

# USER node

EXPOSE 4001
EXPOSE 4101

CMD ["dumb-init", "node", "dist/apps/internship-service/main.js"]

