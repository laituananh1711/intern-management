import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import * as helmet from 'helmet';
import { HttpExceptionFilter } from '../../../libs/utils/src/exception-filters/http-exception.filter';
import { AppModule } from './app.module';
import { AllFilter } from './common/all.filter';
import { RpcExceptionFilter } from './common/rpc-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  app.use(helmet());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  app.useGlobalFilters(
    new AllFilter(),
    new HttpExceptionFilter(),
    new RpcExceptionFilter(),
  );

  const httpPort = configService.get<number>('HTTP_PORT', 3400);

  app.enableCors();
  await app.listen(httpPort);
}
bootstrap();
