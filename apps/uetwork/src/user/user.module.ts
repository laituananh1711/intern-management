import { USER_SERVICE_PACKAGE_NAME } from '@app/pb/user_service/service.pb';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { apiKeyAuthGrpcClientInterceptorFactory } from '../common/api-key-auth.grpc-interceptor';
import { LecturerController } from './lecturer.controller';
import { PartnerController } from './partner.controller';
import { UserController } from './user.controller';
import { AuthController } from './auth.controller';
import { MeController } from './me.controller';
import { OrganizationController } from './organization.controller';
import { SchoolClassController } from './school-class.controller';
import { HttpModule } from '@nestjs/axios';
import { StudentController } from './student.controller';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: USER_SERVICE_PACKAGE_NAME,
        useFactory: (configService: ConfigService) => {
          const userServiceUrl = configService.get(
            'USER_SERVICE_GRPC_URL',
            '0.0.0.0:4100',
          );

          const userServiceSecretHeader = configService.get(
            'USER_SERVICE_SECRET_HEADER',
            'x-uetwork-api',
          );
          const userServiceSecretKey = configService.get(
            'USER_SERVICE_SECRET_KEY',
            'secret',
          );

          const apikeyAuthClientInterceptor =
            apiKeyAuthGrpcClientInterceptorFactory(
              userServiceSecretHeader,
              userServiceSecretKey,
            );

          return {
            transport: Transport.GRPC,
            options: {
              package: USER_SERVICE_PACKAGE_NAME,
              protoPath: 'proto/user_service/service.proto',
              url: userServiceUrl,
              loader: {
                // also serialize empty array
                arrays: true,
              },
              // Pass list of interceptors in `channelOptions` object.
              // https://github.com/nestjs/nest/issues/9079
              channelOptions: {
                interceptors: [apikeyAuthClientInterceptor],
              },
            } as any,
          };
        },
        inject: [ConfigService],
      },
    ]),
    HttpModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const userServiceHttpUrl = configService.get(
          'USER_SERVICE_HTTP_URL',
          'http://0.0.0.0:4000',
        );

        const userServiceSecretHeader = configService.get(
          'USER_SERVICE_SECRET_HEADER',
          'x-uetwork-api',
        );
        const userServiceSecretKey = configService.get(
          'USER_SERVICE_SECRET_KEY',
          'secret',
        );

        return {
          baseURL: userServiceHttpUrl,
          headers: {
            [userServiceSecretHeader]: userServiceSecretKey,
          },
          timeout: 15000,
        };
      },
    }),
  ],
  controllers: [
    UserController,
    PartnerController,
    LecturerController,
    AuthController,
    MeController,
    OrganizationController,
    SchoolClassController,
    StudentController,
  ],
  exports: [ClientsModule],
})
export class UserModule {}
