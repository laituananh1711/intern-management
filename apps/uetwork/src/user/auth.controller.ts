import { Body, Controller, Headers, Inject, Post, Put } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import {
  AuthServiceClient,
  USER_SERVICE_PACKAGE_NAME,
  AUTH_SERVICE_NAME,
  LoginWithEmailResponse,
  GetUserFromTokenResponse,
  GetAccessTokenResponse,
  ChangePasswordResponse,
  ResetPasswordResponse,
  UserServiceClient,
  USER_SERVICE_NAME,
  CreateStudentUserResponse,
  CompleteResetPasswordResponse,
  LoginWithGoogleResponse,
} from '@app/pb/user_service/service.pb';
import {
  ChangePasswordDto,
  CompleteResetPasswordDto,
  LoginDto,
  RegisterStudentDto,
} from './types';
import { lastValueFrom } from 'rxjs';
import { ParseStringPipe, User } from '@app/utils';
import { errUnauthorized, rethrowRpcError } from '../common/errors';

@Controller('auth')
export class AuthController {
  private authServiceClient!: AuthServiceClient;
  private userServiceClient!: UserServiceClient;

  constructor(
    @Inject(USER_SERVICE_PACKAGE_NAME) private usClient: ClientGrpc,
  ) {}

  onModuleInit() {
    this.authServiceClient =
      this.usClient.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
    this.userServiceClient =
      this.usClient.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  @Post('login')
  async loginWithEmail(@Body() dto: LoginDto): Promise<unknown> {
    const resp = this.authServiceClient.loginWithEmail(dto);

    return LoginWithEmailResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('login/google')
  async loginWithGoogle(
    @Body('idToken', ParseStringPipe) idToken: string,
  ): Promise<unknown> {
    const resp = this.authServiceClient.loginWithGoogle({ idToken });

    return LoginWithGoogleResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('info')
  async getUserFromToken(
    @Headers('authorization') bearerToken: string | undefined,
  ): Promise<unknown> {
    if (bearerToken == null || !bearerToken.startsWith('Bearer ')) {
      throw errUnauthorized();
    }
    const accessToken = bearerToken.split(' ')[1];

    const resp = this.authServiceClient.getUserFromToken({ accessToken });

    return GetUserFromTokenResponse.toJSON(
      await lastValueFrom(resp).catch((e) => {
        throw errUnauthorized(e);
      }),
    );
  }

  @Post('refresh')
  async getAccessToken(
    @Body('refreshToken', ParseStringPipe) refreshToken: string,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getAccessToken({ refreshToken });

    return GetAccessTokenResponse.toJSON(
      await lastValueFrom(resp).catch((e) => {
        throw errUnauthorized(e);
      }),
    );
  }

  @Put('password')
  async changePassword(
    @Body() dto: ChangePasswordDto,
    @User('id') userId: number | undefined,
  ): Promise<unknown> {
    if (userId == null) {
      throw errUnauthorized();
    }

    const resp = this.authServiceClient.changePassword({
      ...dto,
      userId,
    });

    return ChangePasswordResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('reset-password')
  async resetPassword(
    @Body('email', ParseStringPipe) email: string,
  ): Promise<unknown> {
    const resp = this.authServiceClient.resetPassword({ email });

    return ResetPasswordResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('reset-password/complete')
  async completeResetPassword(
    @Body() dto: CompleteResetPasswordDto,
  ): Promise<unknown> {
    const resp = this.authServiceClient.completeResetPassword(dto);

    return CompleteResetPasswordResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('register')
  async registerStudent(@Body() dto: RegisterStudentDto): Promise<unknown> {
    const [studentIdNumber] = dto.email.split('@', 2);

    const resp = this.userServiceClient.createStudentUser({
      ...dto,
      loginEmail: dto.email,
      studentIdNumber,
    });

    return CreateStudentUserResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
