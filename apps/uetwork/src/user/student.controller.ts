import { Controller, Get, Inject, Param, Query } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import {
  StudentServiceClient,
  USER_SERVICE_PACKAGE_NAME,
  STUDENT_SERVICE_NAME,
  DownloadCvResponse,
  GetStudentsResponse,
} from '../../../../libs/pb/user_service/service.pb';
import { AppRole, Roles } from '../../../../libs/utils/src';
import { rethrowRpcError } from '../common/errors';
import { StudentQuery } from './types';

@Controller('students')
export class StudentController {
  private studentServiceClient!: StudentServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.studentServiceClient =
      this.client.getService<StudentServiceClient>(STUDENT_SERVICE_NAME);
  }

  @Get()
  @Roles(AppRole.SYSTEM_ADMIN)
  async getStudents(@Query() query: StudentQuery): Promise<unknown> {
    const resp = this.studentServiceClient.getStudents({
      ...query,
      include: ['organization'],
    });

    return GetStudentsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':studentId/cv')
  @Roles(
    AppRole.LECTURER,
    AppRole.PARTNER,
    AppRole.ORG_ADMIN,
    AppRole.SYSTEM_ADMIN,
  )
  async downloadCv(@Param('studentId') studentId: number): Promise<unknown> {
    const resp = this.studentServiceClient.downloadCv({ studentId });

    return DownloadCvResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
