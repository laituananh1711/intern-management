import {
  Body,
  Controller,
  Get,
  Inject,
  OnModuleInit,
  Post,
  Query,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { OffsetPagingQuery } from '../../../../libs/paginate/src';
import {
  OrgServiceClient,
  USER_SERVICE_PACKAGE_NAME,
  ORG_SERVICE_NAME,
  GetClassesListingResponse,
  UpsertClassResponse,
  GetClassesResponse,
} from '../../../../libs/pb/user_service/service.pb';
import { AppRole, Roles } from '../../../../libs/utils/src';
import { rethrowRpcError } from '../common/errors';
import { UpsertClassDto } from './types';

@Controller('classes')
export class SchoolClassController implements OnModuleInit {
  private orgServiceClient!: OrgServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.orgServiceClient =
      this.client.getService<OrgServiceClient>(ORG_SERVICE_NAME);
  }

  @Get('listing')
  async getClassesListing(): Promise<unknown> {
    const resp = this.orgServiceClient.getClassesListing({});

    return GetClassesListingResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get()
  @Roles(AppRole.SYSTEM_ADMIN)
  async getClasses(@Query() query: OffsetPagingQuery): Promise<unknown> {
    const resp = this.orgServiceClient.getClasses(query);

    return GetClassesResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post()
  @Roles(AppRole.SYSTEM_ADMIN)
  async upsertClass(@Body() dto: UpsertClassDto): Promise<unknown> {
    const resp = this.orgServiceClient.upsertClass(dto);

    return UpsertClassResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
