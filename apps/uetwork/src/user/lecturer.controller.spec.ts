import { USER_SERVICE_PACKAGE_NAME } from '@app/pb/user_service/service.pb';
import { Test, TestingModule } from '@nestjs/testing';
import { LecturerController } from './lecturer.controller';

describe('LecturerController', () => {
  let controller: LecturerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LecturerController],
      providers: [
        {
          provide: USER_SERVICE_PACKAGE_NAME,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<LecturerController>(LecturerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
