import {
  GetPartnerContactsResponse,
  GetPartnersListingResponse,
  GetPartnersResponse,
  PartnerServiceClient,
  PARTNER_SERVICE_NAME,
  USER_SERVICE_PACKAGE_NAME,
} from '@app/pb/user_service/service.pb';
import {
  Controller,
  Get,
  Inject,
  OnModuleInit,
  Param,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { ClientGrpc, RpcException } from '@nestjs/microservices';
import { catchError, lastValueFrom } from 'rxjs';
import { AppRole, Roles } from '../../../../libs/utils/src';
import { rethrowRpcError } from '../common/errors';
import { PartnerQuery } from './types';

@Controller('partners')
export class PartnerController implements OnModuleInit {
  private partnerServiceClient!: PartnerServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.partnerServiceClient =
      this.client.getService<PartnerServiceClient>(PARTNER_SERVICE_NAME);
  }

  @Get('listing')
  async getPartnersListing(): Promise<unknown> {
    const resp = this.partnerServiceClient.getPartnersListing({});

    return GetPartnersListingResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':partnerId/contacts')
  @Roles(AppRole.SYSTEM_ADMIN, AppRole.ORG_ADMIN)
  async getPartnerContacts(
    @Param('partnerId', ParseIntPipe) partnerId: number,
  ): Promise<unknown> {
    const resp = this.partnerServiceClient.getPartnerContacts({ partnerId });

    return GetPartnerContactsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get()
  @Roles(AppRole.SYSTEM_ADMIN)
  async getPartners(
    @Query() { page, perPage, q, sort, include }: PartnerQuery,
  ): Promise<unknown> {
    const resp = this.partnerServiceClient.getPartners({
      page,
      perPage,
      q: q ?? '',
      sort: sort ?? [],
      include: include ?? [],
    });

    return GetPartnersResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
