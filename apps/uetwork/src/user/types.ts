import { OffsetPagingQuery } from '@app/paginate';
import {
  IsOptionalOrEmpty,
  TransformBoolean,
  TransformNumber,
} from '@app/utils';
import { Type } from 'class-transformer';
import {
  ArrayMaxSize,
  IsArray,
  IsBoolean,
  IsEmail,
  IsInt,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUrl,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { BATCH_IMPORT_MAX_SIZE } from '../common/constants';

export class StandardFullPagingQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';
}

export class UserFilterQuery {
  @IsString()
  @IsOptional()
  role?: string;
}

export class UserQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort?: string[];

  @IsString()
  @IsOptional()
  q?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => UserFilterQuery)
  filter?: UserFilterQuery;
}

export class PartnerQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort?: string[];

  @IsString()
  @IsOptional()
  q?: string;

  @IsString({ each: true })
  @IsOptional()
  include?: string[];
}

export class CreateUserDto {
  @IsEmail()
  orgEmail!: string;
}

export class LecturerQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  @IsOptional()
  include: string[] = [];
}

export class CreateLecturerUserDto extends CreateUserDto {
  @IsString()
  fullName!: string;

  @IsPhoneNumber('VI')
  phoneNumber!: string;

  @IsInt()
  organizationId!: number;

  @IsEmail()
  @IsOptionalOrEmpty()
  personalEmail = '';
}

export class CreatePartnerUserDto extends CreateUserDto {
  @IsString()
  name!: string;

  @IsUrl()
  homepageUrl!: string;

  @IsUrl()
  @IsOptionalOrEmpty()
  logoUrl = '';

  @IsString()
  @IsOptional()
  description?: string;
}

export class CreateStudentUserDto extends CreateUserDto {
  @IsString()
  fullName!: string;

  @IsString()
  studentIdNumber!: string;

  @IsNumber()
  organizationId!: number;
}

export class CreateOrgAdminUserDto extends CreateUserDto {
  @IsNumber()
  organizationId!: number;
}

export class AssignAccountPartnerDto extends CreateUserDto {
  @IsNumber()
  partnerId!: number;
}

export class LoginDto {
  @IsEmail()
  email!: string;
  @IsString()
  password!: string;
}

export class GetUserFromTokenDto {
  @IsString()
  accessToken!: string;
}

export class ChangePasswordDto {
  @IsString()
  currentPassword!: string;

  @IsString()
  newPassword!: string;
}

export class RegisterStudentDto {
  @IsString()
  fullName!: string;

  @IsNumber()
  organizationId!: number;

  @IsEmail()
  email!: string;
}

export class UpdateLecturerInfoDto {
  @IsString()
  @MinLength(1)
  fullName!: string;

  @IsPhoneNumber('VI')
  @IsOptionalOrEmpty()
  phoneNumber?: string;

  @IsEmail()
  @IsOptionalOrEmpty()
  personalEmail?: string;
}

export class UpdatePartnerInfoDto {
  @IsString()
  name!: string;

  @IsString()
  @IsOptionalOrEmpty()
  homepageUrl = '';

  @IsString()
  @IsOptionalOrEmpty()
  logoUrl = '';

  @IsString()
  @IsOptionalOrEmpty()
  description = '';

  @IsString()
  @IsOptionalOrEmpty()
  address = '';

  @IsString()
  @IsOptionalOrEmpty()
  phoneNumber = '';
}

export class UpdateStudentInfoDto {
  @IsString()
  @IsOptional()
  fullName?: string;

  @IsNumber()
  @IsOptional()
  schoolClassId?: number;

  @IsString()
  @IsOptional()
  personalEmail?: string;

  @IsString()
  @IsOptional()
  phoneNumber?: string;
}

export class CreateContactDto {
  @IsString()
  fullName!: string;

  @IsString()
  phoneNumber!: string;

  @IsString()
  email!: string;
}

export class UpdateContactDto extends CreateContactDto {
  @IsNumber()
  id!: number;
}

export class StudentFilterQuery {
  @IsNumber()
  @IsOptional()
  @TransformNumber
  schoolClassId?: number;

  @IsNumber()
  @IsOptional()
  @TransformNumber
  organizationId?: number;
}

export class StudentQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';

  @IsOptional()
  @ValidateNested()
  @Type(() => GetOrgStudentsFilterQuery)
  filter: StudentFilterQuery = {};
}

export class GetOrgStudentsFilterQuery {
  @IsNumber()
  @IsOptional()
  @TransformNumber
  schoolClassId?: number;
}

export class GetOrgStudentsQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';

  @IsOptional()
  @ValidateNested()
  @Type(() => GetOrgStudentsFilterQuery)
  filter: GetOrgStudentsFilterQuery = {};
}

export class GetOrgLecturersQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  @IsOptional()
  include: string[] = [];
}

export class GetOrgPartnersFilterQuery {
  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  isAssociate?: boolean;
}

export class GetOrgPartnersQuery extends StandardFullPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  include: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetOrgPartnersFilterQuery)
  filter: GetOrgPartnersFilterQuery = {};
}

export class UpsertClassDto {
  @IsString()
  name!: string;

  @IsString()
  programName = '';

  @IsInt()
  @IsOptional()
  id?: number;
}

export class GetOrgQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';
}

export class CreateOrgDtoAdmin {
  @IsEmail()
  email!: string;
}

export class CreateOrgDto {
  @IsString()
  name!: string;

  @ValidateNested()
  @Type(() => CreateOrgDtoAdmin)
  admin!: CreateOrgDtoAdmin;
}

export class CompleteResetPasswordDto {
  @IsString()
  resetToken!: string;

  @IsNumber()
  requestId!: number;

  @IsString()
  newPassword!: string;
}

export class BatchCreateStudentInfo {
  @IsString()
  fullName!: string;
  @IsString()
  orgEmail!: string;
  @IsString()
  studentIdNumber!: string;
  @IsString()
  @IsOptional()
  phoneNumber?: string;
}

export class BatchCreateStudentUsersDto {
  @IsArray()
  @ArrayMaxSize(BATCH_IMPORT_MAX_SIZE)
  @ValidateNested({ each: true })
  @Type(() => BatchCreateStudentInfo)
  students: BatchCreateStudentInfo[] = [];
}
