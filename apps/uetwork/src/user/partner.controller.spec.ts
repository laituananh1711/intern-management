import { USER_SERVICE_PACKAGE_NAME } from '@app/pb/user_service/service.pb';
import { Test, TestingModule } from '@nestjs/testing';
import { PartnerController } from './partner.controller';

describe('PartnerController', () => {
  let controller: PartnerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PartnerController],
      providers: [
        {
          provide: USER_SERVICE_PACKAGE_NAME,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<PartnerController>(PartnerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
