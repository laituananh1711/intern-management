import {
  AssignAccountPartnerResponse,
  BatchCreateStudentUsersResponse,
  CreateLecturerUserResponse,
  CreateOrgAdminUserResponse,
  CreatePartnerUserResponse,
  GetUsersResponse,
  UserServiceClient,
  USER_SERVICE_NAME,
  USER_SERVICE_PACKAGE_NAME,
} from '@app/pb/user_service/service.pb';
import { AppRole, Roles } from '@app/utils';
import {
  Body,
  Controller,
  Get,
  Inject,
  OnModuleInit,
  Post,
  Put,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ClientGrpc, RpcException } from '@nestjs/microservices';
import { catchError, lastValueFrom } from 'rxjs';
import { rethrowRpcError } from '../common/errors';
import { OrgAdmin } from '../common/org-admin.decorator';
import {
  OrgAdminInterceptor,
  ReqOrgAdmin,
} from '../common/org-admin.interceptor';
import {
  AssignAccountPartnerDto,
  BatchCreateStudentUsersDto,
  CreateLecturerUserDto,
  CreateOrgAdminUserDto,
  CreatePartnerUserDto,
  UserQuery,
} from './types';

@Controller('users')
export class UserController implements OnModuleInit {
  private userServiceClient!: UserServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.userServiceClient =
      this.client.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  /**
   * @param query
   * @returns JSON representation of GetUsersResponse.
   */
  @Get()
  @Roles(AppRole.SYSTEM_ADMIN)
  async getUsers(@Query() query: UserQuery): Promise<unknown> {
    const resp = this.userServiceClient
      .getUsers({
        page: query.page,
        perPage: query.perPage,
        q: query.q ?? '',
        filter: {
          role: query.filter?.role ?? '',
        },
        sort: query.sort ?? [],
      })
      .pipe(
        catchError((err) => {
          throw new RpcException(err);
        }),
      );

    return GetUsersResponse.toJSON(await lastValueFrom(resp));
  }

  @Post('partners')
  @Roles(AppRole.SYSTEM_ADMIN)
  async createPartnerUser(@Body() dto: CreatePartnerUserDto): Promise<unknown> {
    const resp = this.userServiceClient
      .createPartnerUser({
        name: dto.name,
        logoUrl: dto.logoUrl,
        loginEmail: dto.orgEmail,
        description: dto.description ?? '',
        homepageUrl: dto.homepageUrl,
      })
      .pipe(
        catchError((err) => {
          throw new RpcException(err);
        }),
      );

    return CreatePartnerUserResponse.toJSON(await lastValueFrom(resp));
  }

  @Put('partners')
  @Roles(AppRole.SYSTEM_ADMIN)
  async assignAccountPartner(
    @Body() dto: AssignAccountPartnerDto,
  ): Promise<unknown> {
    const resp = this.userServiceClient
      .assignAccountPartner({
        loginEmail: dto.orgEmail,
        partnerId: dto.partnerId,
      })
      .pipe(
        catchError((err) => {
          throw new RpcException(err);
        }),
      );

    return AssignAccountPartnerResponse.toJSON(await lastValueFrom(resp));
  }

  @Post('lecturers')
  @Roles(AppRole.SYSTEM_ADMIN)
  async createLecturerUser(
    @Body() dto: CreateLecturerUserDto,
  ): Promise<unknown> {
    const resp = this.userServiceClient
      .createLecturerUser({
        orgEmail: dto.orgEmail,
        fullName: dto.fullName,
        phoneNumber: dto.phoneNumber,
        personalEmail: dto.personalEmail,
        organizationId: dto.organizationId,
      })
      .pipe(
        catchError((err) => {
          throw new RpcException(err);
        }),
      );

    return CreateLecturerUserResponse.toJSON(await lastValueFrom(resp));
  }

  @Post('org-admins')
  @Roles(AppRole.SYSTEM_ADMIN)
  async createOrgAdminUser(
    @Body() dto: CreateOrgAdminUserDto,
  ): Promise<unknown> {
    const resp = this.userServiceClient
      .createOrgAdminUser({
        organizationId: dto.organizationId,
        loginEmail: dto.orgEmail,
      })
      .pipe(
        catchError((err) => {
          throw new RpcException(err);
        }),
      );

    return CreateOrgAdminUserResponse.toJSON(await lastValueFrom(resp));
  }

  @Post('students/import')
  @Roles(AppRole.ORG_ADMIN)
  @UseInterceptors(OrgAdminInterceptor)
  async batchCreateStudentUsers(
    @Body() { students }: BatchCreateStudentUsersDto,
    @OrgAdmin() { organizationId }: ReqOrgAdmin,
  ): Promise<unknown> {
    const resp = this.userServiceClient.batchCreateStudentUsers({
      students,
      organizationId,
    });

    return BatchCreateStudentUsersResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
