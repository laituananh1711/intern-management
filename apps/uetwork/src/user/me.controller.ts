import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  ParseIntPipe,
  Post,
  Put,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom, mergeMap, of } from 'rxjs';
import {
  USER_SERVICE_PACKAGE_NAME,
  LecturerServiceClient,
  LECTURER_SERVICE_NAME,
  AuthServiceClient,
  AUTH_SERVICE_NAME,
  GetLecturerDetailsResponse,
  UpdateLecturerInfoResponse,
  UpdatePartnerInfoResponse,
  PartnerServiceClient,
  PARTNER_SERVICE_NAME,
  GetPartnerDetailsResponse,
  CreatePartnerContactResponse,
  UpdatePartnerContactResponse,
  DeletePartnerContactResponse,
  GetPartnerContactsResponse,
  GetStudentDetailsResponse,
  StudentServiceClient,
  STUDENT_SERVICE_NAME,
  UpdateStudentInfoResponse,
} from '@app/pb/user_service/service.pb';
import { AppRole, Roles, User } from '@app/utils';
import { rethrowRpcError } from '../common/errors';
import {
  UpdateLecturerInfoDto,
  UpdatePartnerInfoDto,
  CreateContactDto,
  UpdateContactDto,
  UpdateStudentInfoDto,
} from './types';
import { CV_FILE_KEY, CV_MAX_SIZE_IN_BYTES } from '../common/constants';
import { FileInterceptor } from '@nestjs/platform-express';
import { HttpService } from '@nestjs/axios';
import FormData = require('form-data');

@Controller('me')
export class MeController {
  private authServiceClient!: AuthServiceClient;
  private lecturerServiceClient!: LecturerServiceClient;
  private partnerServiceClient!: PartnerServiceClient;
  private studentServiceClient!: StudentServiceClient;

  constructor(
    @Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc,
    private httpService: HttpService,
  ) {}

  onModuleInit() {
    this.authServiceClient =
      this.client.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
    this.lecturerServiceClient = this.client.getService<LecturerServiceClient>(
      LECTURER_SERVICE_NAME,
    );
    this.partnerServiceClient =
      this.client.getService<PartnerServiceClient>(PARTNER_SERVICE_NAME);
    this.studentServiceClient =
      this.client.getService<StudentServiceClient>(STUDENT_SERVICE_NAME);
  }

  @Get('lecturer/info')
  @Roles(AppRole.LECTURER)
  async getLecturerInfo(@User('id') userId: number): Promise<unknown> {
    const resp = this.authServiceClient.getLecturerDetails({ userId });

    return GetLecturerDetailsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('lecturer/info')
  @Roles(AppRole.LECTURER)
  async updateLecturerInfo(
    @Body() dto: UpdateLecturerInfoDto,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getLecturerDetails({ userId }).pipe(
      mergeMap((res) => {
        const { id } = res.data!;

        return of(id);
      }),
      mergeMap((id) => {
        return this.lecturerServiceClient.updateLecturerInfo({
          lecturerId: id,
          ...dto,
        });
      }),
    );

    return UpdateLecturerInfoResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('partner/info')
  @Roles(AppRole.PARTNER)
  async getPartnerInfo(@User('id') userId: number): Promise<unknown> {
    const resp = this.authServiceClient.getPartnerDetails({ userId });

    return GetPartnerDetailsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('partner/info')
  @Roles(AppRole.PARTNER)
  async updatePartnerInfo(
    @Body() dto: UpdatePartnerInfoDto,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getPartnerDetails({ userId }).pipe(
      mergeMap((res) => {
        const { id } = res.data!;

        return of(id);
      }),
      mergeMap((id) => {
        return this.partnerServiceClient.updatePartnerInfo({
          partnerId: id,
          ...dto,
        });
      }),
    );

    return UpdatePartnerInfoResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('partner/contacts')
  @Roles(AppRole.PARTNER)
  async createContact(
    @Body() dto: CreateContactDto,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getPartnerDetails({ userId }).pipe(
      mergeMap((res) => {
        const { id } = res.data!;

        return of(id);
      }),
      mergeMap((id) => {
        return this.partnerServiceClient.createPartnerContact({
          partnerId: id,
          ...dto,
        });
      }),
    );

    return CreatePartnerContactResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('partner/contacts')
  @Roles(AppRole.PARTNER)
  async getContacts(@User('id') userId: number): Promise<unknown> {
    const resp = this.authServiceClient.getPartnerDetails({ userId }).pipe(
      mergeMap((res) => {
        const { id } = res.data!;

        return of(id);
      }),
      mergeMap((id) => {
        return this.partnerServiceClient.getPartnerContacts({ partnerId: id });
      }),
    );

    return GetPartnerContactsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('partner/contacts')
  @Roles(AppRole.PARTNER)
  async updateContact(
    @Body() dto: UpdateContactDto,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getPartnerDetails({ userId }).pipe(
      mergeMap((res) => {
        const { id } = res.data!;

        return of(id);
      }),
      mergeMap((id) => {
        return this.partnerServiceClient.updatePartnerContact({
          filterPartnerId: id,
          ...dto,
        });
      }),
    );

    return UpdatePartnerContactResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Delete('partner/contacts')
  @Roles(AppRole.PARTNER)
  async deleteContact(
    @Body('id', ParseIntPipe) contactId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getPartnerDetails({ userId }).pipe(
      mergeMap((res) => {
        const { id } = res.data!;

        return of(id);
      }),
      mergeMap((id) => {
        return this.partnerServiceClient.deletePartnerContact({
          filterPartnerId: id,
          id: contactId,
        });
      }),
    );

    return DeletePartnerContactResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('student/info')
  @Roles(AppRole.STUDENT)
  async getStudentInfo(@User('id') userId: number): Promise<unknown> {
    const resp = this.authServiceClient.getStudentDetails({ userId });

    return GetStudentDetailsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('student/info')
  @Roles(AppRole.STUDENT)
  async updateStudentInfo(
    @Body() dto: UpdateStudentInfoDto,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.authServiceClient.getStudentDetails({ userId }).pipe(
      mergeMap((res) => {
        const studentId = res.data!.id;

        return this.studentServiceClient.updateStudentInfo({
          studentId,
          ...dto,
        });
      }),
    );

    return UpdateStudentInfoResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('student/cv')
  @Roles(AppRole.STUDENT)
  @UseInterceptors(
    FileInterceptor(CV_FILE_KEY, {
      limits: {
        fileSize: CV_MAX_SIZE_IN_BYTES,
      },
    }),
  )
  async uploadCV(
    @UploadedFile() cvFile: Express.Multer.File,
    @User('id') userId: number,
  ): Promise<{ code: number; message: string }> {
    const resp = this.authServiceClient.getStudentDetails({ userId }).pipe(
      mergeMap((res) => {
        const studentId = res.data!.id;

        const formData = new FormData();
        formData.append(CV_FILE_KEY, cvFile.buffer, cvFile.originalname);

        return this.httpService.post<{ code: number; message: string }>(
          `students/${studentId}/cv/upload`,
          formData,
          { headers: formData.getHeaders() },
        );
      }),
      mergeMap((res) => {
        return of(res.data);
      }),
    );

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }

  @Get('student/cv')
  @Roles(AppRole.STUDENT)
  async downloadCV(@User('id') userId: number): Promise<unknown> {
    const resp = this.authServiceClient.getStudentDetails({ userId }).pipe(
      mergeMap((res) => {
        const studentId = res.data!.id;

        return this.studentServiceClient.downloadCv({
          studentId,
        });
      }),
    );

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }
}
