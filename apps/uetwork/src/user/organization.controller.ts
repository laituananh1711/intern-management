import {
  CreateOrgResponse,
  GetLecturersResponse,
  GetOrgListingResponse,
  GetOrgPartnersResponse,
  GetOrgsResponse,
  GetStudentsResponse,
  LecturerServiceClient,
  LECTURER_SERVICE_NAME,
  OrgServiceClient,
  ORG_SERVICE_NAME,
  StudentServiceClient,
  STUDENT_SERVICE_NAME,
  USER_SERVICE_PACKAGE_NAME,
} from '@app/pb/user_service/service.pb';
import { AppRole, Roles } from '@app/utils';
import {
  Body,
  Controller,
  Get,
  Inject,
  OnModuleInit,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { rethrowRpcError } from '../common/errors';
import { OrgAdmin } from '../common/org-admin.decorator';
import {
  OrgAdminInterceptor,
  ReqOrgAdmin,
} from '../common/org-admin.interceptor';
import {
  CreateOrgDto,
  GetOrgLecturersQuery,
  GetOrgPartnersQuery,
  GetOrgQuery,
  GetOrgStudentsQuery,
} from './types';

@Controller('orgs')
export class OrganizationController implements OnModuleInit {
  // private userServiceClient!: UserServiceClient;
  private studentServiceClient!: StudentServiceClient;
  private lecturerServiceClient!: LecturerServiceClient;
  // private partnerServiceClient!: PartnerServiceClient;
  private orgServiceClient!: OrgServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.studentServiceClient =
      this.client.getService<StudentServiceClient>(STUDENT_SERVICE_NAME);
    this.lecturerServiceClient = this.client.getService<LecturerServiceClient>(
      LECTURER_SERVICE_NAME,
    );
    this.orgServiceClient =
      this.client.getService<OrgServiceClient>(ORG_SERVICE_NAME);
  }

  @Get()
  @Roles(AppRole.SYSTEM_ADMIN)
  async getOrgs(@Query() query: GetOrgQuery): Promise<unknown> {
    const resp = this.orgServiceClient.getOrgs(query);

    return GetOrgsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post()
  @Roles(AppRole.SYSTEM_ADMIN)
  async createOrg(@Body() { name, admin }: CreateOrgDto): Promise<unknown> {
    const resp = this.orgServiceClient.createOrg({
      name,
      admin,
    });

    return CreateOrgResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('listing')
  @Roles(AppRole.SYSTEM_ADMIN)
  async getOrgListing(): Promise<unknown> {
    const resp = this.orgServiceClient.getOrgListing({});

    return GetOrgListingResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('students')
  @Roles(AppRole.ORG_ADMIN)
  @UseInterceptors(OrgAdminInterceptor)
  async getStudents(
    @OrgAdmin() { organizationId }: ReqOrgAdmin,
    @Query() query: GetOrgStudentsQuery,
  ): Promise<unknown> {
    const resp = this.studentServiceClient.getStudents({
      ...query,
      filter: {
        ...query.filter,
        organizationId,
      },
      include: [],
    });

    return GetStudentsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('lecturers')
  @Roles(AppRole.ORG_ADMIN)
  @UseInterceptors(OrgAdminInterceptor)
  async getLecturers(
    @OrgAdmin() { organizationId }: ReqOrgAdmin,
    @Query() query: GetOrgLecturersQuery,
  ): Promise<unknown> {
    const resp = this.lecturerServiceClient.getLecturers({
      ...query,
      filter: {
        organizationId,
      },
    });

    return GetLecturersResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('partners')
  @Roles(AppRole.ORG_ADMIN)
  @UseInterceptors(OrgAdminInterceptor)
  async getPartners(
    @OrgAdmin() { organizationId }: ReqOrgAdmin,
    @Query() query: GetOrgPartnersQuery,
  ): Promise<unknown> {
    const resp = this.orgServiceClient.getOrgPartners({
      ...query,
      filter: {
        ...query.filter,
      },
      organizationId,
    });

    return GetOrgPartnersResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
