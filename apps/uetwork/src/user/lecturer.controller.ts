import {
  GetLecturersResponse,
  LecturerServiceClient,
  LECTURER_SERVICE_NAME,
  USER_SERVICE_PACKAGE_NAME,
} from '@app/pb/user_service/service.pb';
import { Controller, Get, Inject, OnModuleInit, Query } from '@nestjs/common';
import { ClientGrpc, RpcException } from '@nestjs/microservices';
import { catchError, lastValueFrom } from 'rxjs';
import { AppRole, Roles } from '../../../../libs/utils/src';
import { LecturerQuery } from './types';

@Controller('lecturers')
export class LecturerController implements OnModuleInit {
  private lecturerServiceClient!: LecturerServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.lecturerServiceClient = this.client.getService<LecturerServiceClient>(
      LECTURER_SERVICE_NAME,
    );
  }

  @Get()
  @Roles(AppRole.SYSTEM_ADMIN)
  async getLecturers(
    @Query() { include, page, perPage, q, sort }: LecturerQuery,
  ): Promise<unknown> {
    const resp = this.lecturerServiceClient
      .getLecturers({
        include,
        page,
        perPage,
        q,
        sort,
        filter: undefined,
      })
      .pipe(
        catchError((err) => {
          throw new RpcException(err);
        }),
      );

    return GetLecturersResponse.toJSON(await lastValueFrom(resp));
  }
}
