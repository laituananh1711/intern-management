import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Response } from 'express';
import { errorCodes } from './errors';
import { ErrorResponse } from './types';

@Catch()
export class AllFilter implements ExceptionFilter {
  private logger = new Logger(AllFilter.name);

  catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();

    const response = ctx.getResponse<Response>();

    this.logger.error('unknown exception catched: ', exception.message);

    const payload = new ErrorResponse(
      HttpStatus.INTERNAL_SERVER_ERROR,
      errorCodes.ERR_UNKNOWN,
      undefined,
      exception.message,
    );

    response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(payload);
  }
}
