export class ErrorResponse {
  code: number;
  message: string;
  details: string[] = [];
  error: string;

  constructor(
    code: number,
    message: string,
    error?: string,
    ...details: string[]
  ) {
    this.code = code;
    this.message = message ?? error;
    this.error = error ?? message;
    this.details = details ?? [];
  }
}
