export const CV_FILE_KEY = 'file';
export const CV_MAX_SIZE_IN_BYTES = 10 * 1024 * 1024;
export const FILE_SEPARATOR = '/';
export const REPORT_FILE_KEY = 'file';
export const REPORT_MAX_SIZE_IN_BYTES = 10 * 1024 * 1024;
export const PDF_MIME_TYPE = 'application/pdf';
export const BATCH_IMPORT_MAX_SIZE = 1000;
