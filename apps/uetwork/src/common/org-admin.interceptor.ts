import {
  CallHandler,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { catchError, lastValueFrom, mergeMap, Observable } from 'rxjs';
import {
  AuthServiceClient,
  USER_SERVICE_PACKAGE_NAME,
  AUTH_SERVICE_NAME,
  AuthUser,
} from '@app/pb/user_service/service.pb';
import { errForbidden, rethrowRpcError } from './errors';

export interface ReqOrgAdmin {
  organizationId: number;
}

@Injectable()
export class OrgAdminInterceptor implements NestInterceptor {
  private authServiceClient!: AuthServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.authServiceClient =
      this.client.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const request: any = ctx.getRequest();

    if (request.user == null) {
      throw errForbidden();
    }

    const { id } = request.user as AuthUser;

    const details$ = this.authServiceClient
      .getOrgAdminDetails({
        userId: id,
      })
      .pipe(
        mergeMap((res) => {
          request.orgAdmin = res.data;

          return next.handle();
        }),
        catchError(rethrowRpcError),
      );

    return details$;
  }
}
