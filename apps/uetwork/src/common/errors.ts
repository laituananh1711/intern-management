import { ServiceError } from '@grpc/grpc-js';
import {
  UnauthorizedException,
  HttpStatus,
  ForbiddenException,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { ErrorResponse } from './types';

export const errorCodes = {
  ERR_UNAUTHORIZED: 'ERR_UNAUTHORIZED',
  ERR_INVALID_BOOL_VALUE: 'ERR_INVALID_BOOL_VALUE',
  ERR_FORBIDDEN: 'ERR_FORBIDDEN',
  ERR_UNKNOWN: 'ERR_UNKNOWN',
};

export const errUnauthorized = (e?: Error) =>
  new UnauthorizedException(
    new ErrorResponse(
      HttpStatus.UNAUTHORIZED,
      errorCodes.ERR_UNAUTHORIZED,
      e?.message,
    ),
  );

export const errForbidden = (e?: Error) =>
  new ForbiddenException(
    new ErrorResponse(
      HttpStatus.FORBIDDEN,
      errorCodes.ERR_FORBIDDEN,
      e?.message,
    ),
  );

// why did I add this function? Grpc Client doesn't throw any catchable
// exceptions, so you have to convert it to RpcException first.
// https://github.com/nestjs/nest/issues/1953
export const rethrowRpcError = (e: ServiceError) => {
  throw new RpcException(e);
};
