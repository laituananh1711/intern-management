import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const OrgAdmin = createParamDecorator(
  (field: string, ctx: ExecutionContext): any => {
    const req = ctx.switchToHttp().getRequest();

    return field != null ? req.orgAdmin?.[field] : req.orgAdmin;
  },
);
