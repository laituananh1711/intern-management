import { InterceptingCall, Interceptor, RequesterBuilder } from '@grpc/grpc-js';
import { Logger } from '@nestjs/common';

/** Return a new gRPC client interceptor which will appends secret header and
 * secret key to all outgoing metadata.
 */
export const apiKeyAuthGrpcClientInterceptorFactory = function (
  secretHeader: string,
  secretKey: string,
): Interceptor {
  const interceptor: Interceptor = function (
    options,
    nextCall,
  ): InterceptingCall {
    const requester = new RequesterBuilder()
      .withStart(function (metadata, listener, next) {
        metadata.set(secretHeader, secretKey);

        next(metadata, listener);
      })
      .build();

    return new InterceptingCall(nextCall(options), requester);
  };

  return interceptor;
};
