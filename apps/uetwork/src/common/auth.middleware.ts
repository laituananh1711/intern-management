import {
  Inject,
  Injectable,
  Logger,
  NestMiddleware,
  OnModuleInit,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { NextFunction, Response } from 'express';
import {
  AuthServiceClient,
  AUTH_SERVICE_NAME,
  USER_SERVICE_PACKAGE_NAME,
} from '@app/pb/user_service/service.pb';
import { lastValueFrom } from 'rxjs';
import { errUnauthorized } from './errors';

/**
 * This middleware examines the 'Authorization' header
 * and set `req.user` as the decoded data in that header.
 */
@Injectable()
export class AuthMiddleware implements NestMiddleware, OnModuleInit {
  private authServiceClient!: AuthServiceClient;

  constructor(@Inject(USER_SERVICE_PACKAGE_NAME) private client: ClientGrpc) {}

  onModuleInit() {
    this.authServiceClient =
      this.client.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
  }

  async use(req: any, res: Response, next: NextFunction) {
    const authHeader: string = req.headers.authorization;

    if (authHeader == null || !authHeader.startsWith('Bearer ')) {
      Logger.debug('authorization header is missing or not in Bearer format');
      return next();
    }

    const token = authHeader.split(' ')[1];

    try {
      const { data: user } = await lastValueFrom(
        this.authServiceClient.getUserFromToken({ accessToken: token }),
      );

      Logger.debug('authenticated user: ', user);

      req.user = user;
    } catch (e: any) {
      Logger.error('error getting user from access token: ', e);
      next(errUnauthorized(e));
    } finally {
      next();
    }
  }
}
