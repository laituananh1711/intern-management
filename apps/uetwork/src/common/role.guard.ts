import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthUser } from '@app/pb/user_service/service.pb';
import { errForbidden } from './errors';

@Injectable()
export class RoleGuard implements CanActivate {
  private logger = new Logger(RoleGuard.name);

  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }

    const req = context.switchToHttp().getRequest();

    this.logger.debug(`Endpoint ${req.path} requires roles: ${roles}`);

    const user: AuthUser | undefined = req.user;

    if (user == null) {
      throw errForbidden(new Error('can not find the requesting user'));
    }

    this.logger.debug(`Requesting user: ${JSON.stringify(user)}`);

    if (!this.isValidUser(user)) {
      throw errForbidden(new Error('user does not have necessary permissions'));
    }

    return roles.includes(user.role);
  }

  private isValidUser(user: any): boolean {
    return !(user == null || user.role == null);
  }
}
