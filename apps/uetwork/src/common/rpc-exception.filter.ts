import { ServiceError } from '@grpc/grpc-js';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Response } from 'express';
import { errorCodes } from './errors';
import { ErrorResponse } from './types';
import { status } from '@grpc/grpc-js';

const grpcCodeToHttpStatusMap: Record<number, number> = {
  [status.OK]: HttpStatus.OK,
  [status.NOT_FOUND]: HttpStatus.NOT_FOUND,
  [status.UNAUTHENTICATED]: HttpStatus.UNAUTHORIZED,
  [status.INVALID_ARGUMENT]: HttpStatus.BAD_REQUEST,
  [status.INTERNAL]: HttpStatus.INTERNAL_SERVER_ERROR,
  [status.UNKNOWN]: HttpStatus.INTERNAL_SERVER_ERROR,
};

const grpcCodeToHttpStatus = (grpcCode: number): number => {
  const httpStatus = grpcCodeToHttpStatusMap[grpcCode];

  if (httpStatus != null) {
    return httpStatus;
  } else {
    return HttpStatus.INTERNAL_SERVER_ERROR;
  }
};

@Catch(RpcException)
export class RpcExceptionFilter implements ExceptionFilter {
  private logger = new Logger(RpcExceptionFilter.name);

  catch(exception: RpcException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();

    const response = ctx.getResponse<Response>();

    const error = exception.getError() as ServiceError;

    const httpStatus = grpcCodeToHttpStatus(error.code);

    const payload = new ErrorResponse(
      httpStatus,
      error.details ?? errorCodes.ERR_UNKNOWN,
    );

    this.logger.error('rpcException catched: ', { exception });

    response.status(httpStatus).json(payload);
  }
}
