import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { InternshipModule } from './internship/internship.module';
import { AuthMiddleware } from './common/auth.middleware';
import { APP_GUARD } from '@nestjs/core';
import { RoleGuard } from './common/role.guard';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    UserModule,
    ConfigModule.forRoot({
      envFilePath: 'apps/uetwork/.env',
      isGlobal: true,
    }),
    InternshipModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        'auth/login',
        'auth/info',
        'auth/refresh',
        'auth/reset-password',
        'auth/register',
      )
      .forRoutes('*');
  }
}
