import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { AppRole, Roles, User } from '@app/utils';
import { ClientGrpc } from '@nestjs/microservices';
import {
  TermServiceClient,
  INTERNSHIP_SERVICE_PACKAGE_NAME,
  TERM_SERVICE_NAME,
  GetTermPartnerListingResponse,
  GetLatestTermResponse,
  SelfRegisterPartnerResponse,
  SignUpForTermResponse,
  GetTermStudentsResponse,
  TermLittleInfo,
  GetLecturerTermListingResponse,
  GiveScoreResponse,
  GetPartnerTermListingResponse,
  GetStudentApplicationsResponse,
  BatchUpdateRegistrationStatusResponse,
  registrationStatusFromJSON,
  RegistrationStatus,
  CreatePostResponse,
  GetPostByIdResponse,
  UpdatePostResponse,
  GetTermStudentDetailsResponse,
  GetStudentTermListingResponse,
  SelectInternshipPartnerResponse,
  GetPostsResponse,
  ExportStudentApplicationsResponse,
  CancelTermSignUpResponse,
  GetStudentPostByIdResponse,
  GetStudentPostsResponse,
  ApplyToPartnerResponse,
  UnregisterPartnerResponse,
  GetTermLecturerListingResponse,
  DownloadStudentReportResponse,
} from '@app/pb/internship_service/service.pb';
import { TermAuthService } from './term-auth.service';
import { lastValueFrom, mergeMap, of } from 'rxjs';
import { errForbidden, rethrowRpcError } from '../common/errors';
import {
  AssignScoreDto,
  BatchUpdateRegistrationStatusDto,
  CreatePartnerPostDto,
  ExportPartnerStudentApplicationsQuery,
  GetMyStudentsQuery,
  GetPartnerPostsQuery,
  GetPartnerStudentApplicationsQuery,
  GetStudentsWorkingAtPartnerQuery,
  UpdatePostDto,
} from './types';
import { AuthUser } from '@app/pb/user_service/service.pb';
import { toTimestamp } from '@app/pb';
import { HttpService } from '@nestjs/axios';
import FormData = require('form-data');
import { FileInterceptor } from '@nestjs/platform-express';
import { REPORT_FILE_KEY, REPORT_MAX_SIZE_IN_BYTES } from '../common/constants';
import { OffsetPagingQuery } from '../../../../libs/paginate/src';

@Controller('terms')
export class TermController {
  private termServiceClient!: TermServiceClient;

  constructor(
    @Inject(INTERNSHIP_SERVICE_PACKAGE_NAME) private client: ClientGrpc,
    private termAuthService: TermAuthService,
    private httpService: HttpService,
  ) {}

  onModuleInit() {
    this.termServiceClient =
      this.client.getService<TermServiceClient>(TERM_SERVICE_NAME);
  }

  @Get('latest')
  @Roles(AppRole.STUDENT)
  async getLatestTerm(@User('id') userId: number): Promise<unknown> {
    const studentDetails$ = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId });

    const { data } = await lastValueFrom(studentDetails$).catch(
      rethrowRpcError,
    );
    const { organizationId, id: studentId } = data!;

    const resp = this.termServiceClient.getLatestTerm({
      organizationId,
      studentId,
    });
    return GetLatestTermResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':termId/partners/listing')
  @Roles(AppRole.STUDENT, AppRole.ORG_ADMIN, AppRole.SYSTEM_ADMIN)
  async getTermPartnerListing(
    @Param('termId', ParseIntPipe) termId: number,
  ): Promise<unknown> {
    const resp = this.termServiceClient.getTermPartnerListing({ termId });

    return GetTermPartnerListingResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':termId/lecturers/listing')
  @Roles(AppRole.ORG_ADMIN, AppRole.SYSTEM_ADMIN)
  async getTermLecturersListing(
    @Param('termId', ParseIntPipe) termId: number,
  ): Promise<unknown> {
    // TODO: limit access if needed
    const resp = this.termServiceClient.getTermLecturerListing({ termId });

    return GetTermLecturerListingResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post(':termId/partners/:partnerId/register')
  @Roles(AppRole.STUDENT)
  async selfRegisterPartner(
    @Param('partnerId', ParseIntPipe) partnerId: number,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const studentDetails$ = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId });

    const { data } = await lastValueFrom(studentDetails$).catch(
      rethrowRpcError,
    );
    const { id: studentId } = data!;

    const { data: checkStudentInTerm } = await lastValueFrom(
      this.termServiceClient.checkStudentInTerm({ termId, studentId }),
    ).catch(rethrowRpcError);

    if (!checkStudentInTerm!.isIn) {
      throw errForbidden(new Error('student not in term'));
    }

    const resp = this.termServiceClient.selfRegisterPartner({
      termId,
      partnerId,
      studentId,
    });

    return SelfRegisterPartnerResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post(':termId/register')
  @Roles(AppRole.STUDENT)
  async registerTerm(
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const studentDetails$ = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId });

    const { data } = await lastValueFrom(studentDetails$).catch(
      rethrowRpcError,
    );
    const { organizationId, id: studentId } = data!;
    const checkTermInOrg$ = this.termServiceClient.checkTermInOrg({
      termId,
      organizationId,
    });

    const { data: checkTermInOrgData } = await lastValueFrom(
      checkTermInOrg$,
    ).catch(rethrowRpcError);

    const { isIn } = checkTermInOrgData!;
    if (!isIn) {
      throw errForbidden(new Error('error student not in term'));
    }

    const resp = this.termServiceClient.signUpForTerm({ termId, studentId });

    return SignUpForTermResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Delete(':termId/register')
  @Roles(AppRole.STUDENT)
  async unregisterTerm(
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const studentDetails$ = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId });

    const { data } = await lastValueFrom(studentDetails$).catch(
      rethrowRpcError,
    );
    const { organizationId, id: studentId } = data!;
    const checkTermInOrg$ = this.termServiceClient.checkTermInOrg({
      termId,
      organizationId,
    });

    const { data: checkTermInOrgData } = await lastValueFrom(
      checkTermInOrg$,
    ).catch(rethrowRpcError);

    const { isIn } = checkTermInOrgData!;
    if (!isIn) {
      throw errForbidden(new Error("error term not in student's org"));
    }

    const resp = this.termServiceClient.cancelTermSignUp({ termId, studentId });

    return CancelTermSignUpResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('listing')
  @Roles(AppRole.STUDENT, AppRole.LECTURER, AppRole.PARTNER)
  async getTermListing(@User() authUser: AuthUser): Promise<unknown> {
    let resp: { code: number; message: string; data: TermLittleInfo[] };
    switch (authUser.role) {
      case AppRole.LECTURER:
        resp = await this.getLecturerTermListing(authUser.id);
        break;
      case AppRole.PARTNER:
        resp = await this.getPartnerTermListing(authUser.id);
        break;
      case AppRole.STUDENT:
        resp = await this.getStudentTermListing(authUser.id);
        break;
      default:
        throw new Error('unknown role received');
    }

    return resp;
  }

  @Get('my-students')
  @Roles(AppRole.LECTURER)
  async getMyStudents(
    @User('id') userId: number,
    @Query() query: GetMyStudentsQuery,
  ): Promise<unknown> {
    const lecturerDetails$ = this.termAuthService
      .getAuthClient()
      .getLecturerDetails({ userId });

    const { data } = await lastValueFrom(lecturerDetails$).catch(
      rethrowRpcError,
    );
    const { id: lecturerId } = data!;

    const resp = this.termServiceClient.getTermStudents({
      ...query,
      filter: {
        supervisorId: lecturerId,
        ...query.filter,
      },
    });

    return GetTermStudentsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post(':termId/my-students/:studentId/score')
  @Roles(AppRole.LECTURER)
  async assignScore(
    @Body() { score }: AssignScoreDto,
    @Param('termId', ParseIntPipe) termId: number,
    @Param('studentId', ParseIntPipe) studentId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getLecturerDetails({ userId })
      .pipe(
        mergeMap((res) => {
          return this.termServiceClient.giveScore({
            score,
            studentId,
            termId,
            filterLecturerId: res.data!.id,
          });
        }),
      );

    return GiveScoreResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':termId/my-students/:studentId/report')
  @Roles(AppRole.LECTURER)
  async downloadStudentReport(
    @Param('termId', ParseIntPipe) termId: number,
    @Param('studentId', ParseIntPipe) studentId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getLecturerDetails({ userId })
      .pipe(
        mergeMap((res) => {
          return this.termServiceClient.downloadStudentReport({
            studentId,
            termId,
            filterLecturerId: res.data!.id,
          });
        }),
      );

    return DownloadStudentReportResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  /** Get a list of applications to this partner.
   * An application is a registration with `WAITING` status.
   */
  @Get('applications')
  @Roles(AppRole.PARTNER)
  async getPartnerStudentApplications(
    @Query() query: GetPartnerStudentApplicationsQuery,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => {
          return of(res.data!.id);
        }),
        mergeMap((id) => {
          return this.termServiceClient.getStudentApplications({
            ...query,
            filter: {
              partnerId: id,
              status: RegistrationStatus.WAITING,
              ...query.filter,
            },
          });
        }),
      );

    return GetStudentApplicationsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('applications/export')
  @Roles(AppRole.PARTNER)
  async exportPartnerStudentApplications(
    @Query() query: ExportPartnerStudentApplicationsQuery,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => {
          return of(res.data!.id);
        }),
        mergeMap((id) => {
          return this.termServiceClient.exportStudentApplications({
            ...query,
            filter: {
              partnerId: id,
              status: RegistrationStatus.WAITING,
              ...query.filter,
            },
          });
        }),
      );

    return ExportStudentApplicationsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('applications/status')
  @Roles(AppRole.PARTNER)
  async updateApplicationStatus(
    @User('id') userId: number,
    @Body() dto: BatchUpdateRegistrationStatusDto,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => of(res.data!.id)),
        mergeMap((id) => {
          return this.termServiceClient.batchUpdateRegistrationStatus({
            ...dto,
            partnerId: id,
            status: registrationStatusFromJSON(dto.status),
          });
        }),
      );

    return BatchUpdateRegistrationStatusResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('passed-students')
  @Roles(AppRole.PARTNER)
  async getStudentsWorkingAtPartner(
    @User('id') userId: number,
    @Query() query: GetStudentsWorkingAtPartnerQuery,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => of(res.data!.id)),
        mergeMap((id) => {
          return this.termServiceClient.getTermStudents({
            ...query,
            filter: {
              ...query.filter,
              selectedPartnerId: id,
            },
          });
        }),
      );

    return GetTermStudentsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('posts')
  @Roles(AppRole.PARTNER)
  async createPost(
    @User('id') userId: number,
    @Body() dto: CreatePartnerPostDto,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => of(res.data!.id)),
        mergeMap((id) => {
          return this.termServiceClient.createPost({
            ...dto,
            partnerId: id,
            startRegAt: toTimestamp(new Date(dto.startRegAt)),
            endRegAt: toTimestamp(new Date(dto.endRegAt)),
          });
        }),
      );

    return CreatePostResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('posts')
  @Roles(AppRole.PARTNER)
  async getPosts(
    @Param('termId') termId: number,
    @User('id') userId: number,
    @Query() query: GetPartnerPostsQuery,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const partnerId = res.data!.id;

          return this.termServiceClient.getPosts({
            ...query,
            filter: {
              termId: query.filter?.termId,
              partnerId: partnerId,
            },
          });
        }),
      );

    return GetPostsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('posts/:postId')
  @Roles(AppRole.PARTNER, AppRole.STUDENT)
  async getPostById(
    @Param('postId', ParseIntPipe) postId: number,
  ): Promise<unknown> {
    const resp = this.termServiceClient.getPostById({ postId });

    return GetPostByIdResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('posts/:postId')
  @Roles(AppRole.PARTNER)
  async updatePost(
    @User('id') userId: number,
    @Param('postId', ParseIntPipe) postId: number,
    @Body() dto: UpdatePostDto,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId })
      .pipe(
        mergeMap((res) => of(res.data!.id)),
        mergeMap((id) => {
          return this.termServiceClient.updatePost({
            ...dto,
            id: postId,
            startRegAt: toTimestamp(new Date(dto.startRegAt)),
            endRegAt: toTimestamp(new Date(dto.endRegAt)),
            filterPartnerId: id,
          });
        }),
      );

    return UpdatePostResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':termId/internship-details')
  @Roles(AppRole.STUDENT)
  async getInternshipDetails(
    @User('id') userId: number,
    @Param('termId', ParseIntPipe) termId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.getTermStudentDetails({
            studentId,
            termId,
          });
        }),
      );

    return GetTermStudentDetailsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post(':termId/partners/:partnerId/select')
  @Roles(AppRole.STUDENT)
  async selectInternshipPartner(
    @User('id') userId: number,
    @Param('termId', ParseIntPipe) termId: number,
    @Param('partnerId', ParseIntPipe) partnerId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.selectInternshipPartner({
            studentId,
            partnerId,
            termId,
          });
        }),
      );

    return SelectInternshipPartnerResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post(':termId/report')
  @Roles(AppRole.STUDENT)
  @UseInterceptors(
    FileInterceptor(REPORT_FILE_KEY, {
      limits: {
        fileSize: REPORT_MAX_SIZE_IN_BYTES,
      },
    }),
  )
  async uploadReport(
    @UploadedFile() reportFile: Express.Multer.File,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<{ code: number; message: string }> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          const formData = new FormData();
          formData.append(
            REPORT_FILE_KEY,
            reportFile.buffer,
            reportFile.originalname,
          );

          return this.httpService.post<{ code: number; message: string }>(
            `terms/${termId}/students/${studentId}/report`,
            formData,
            { headers: formData.getHeaders() },
          );
        }),
        mergeMap((res) => {
          return of(res.data);
        }),
      );

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }

  @Get(':termId/report')
  @Roles(AppRole.STUDENT)
  async downloadReport(
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.downloadStudentReport({
            termId,
            studentId,
          });
        }),
      );

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }

  @Get(':termId/student-posts/:postId')
  @Roles(AppRole.STUDENT)
  async getStudentPostsById(
    @Param('termId', ParseIntPipe) termId: number,
    @Param('postId', ParseIntPipe) postId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.getStudentPostById({
            studentId,
            postId,
          });
        }),
      );

    return GetStudentPostByIdResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get(':termId/student-posts')
  @Roles(AppRole.STUDENT)
  async getStudentPosts(
    @Query() query: OffsetPagingQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.getStudentPosts({
            studentId,
            termId,
            ...query,
          });
        }),
      );

    return GetStudentPostsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post(':termId/posts/:postId/apply')
  @Roles(AppRole.STUDENT)
  async applyPost(
    @User('id') userId: number,
    @Param('termId', ParseIntPipe) termId: number,
    @Param('postId', ParseIntPipe) postId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertStudentCanAccessTerm(userId, termId);

    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.applyToPartner({
            studentId,
            postId,
            termId,
          });
        }),
      );

    return ApplyToPartnerResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Delete(':termId/partners/:partnerId/unregister')
  @Roles(AppRole.STUDENT)
  async unregisterPartner(
    @User('id') userId: number,
    @Param('termId', ParseIntPipe) termId: number,
    @Param('partnerId', ParseIntPipe) partnerId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertStudentCanAccessTerm(userId, termId);

    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.unregisterPartner({
            studentId,
            termId,
            partnerId,
          });
        }),
      );

    return UnregisterPartnerResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  private async getLecturerTermListing(
    userId: number,
  ): Promise<GetLecturerTermListingResponse> {
    const lecturerDetails$ = this.termAuthService
      .getAuthClient()
      .getLecturerDetails({ userId });

    const { data } = await lastValueFrom(lecturerDetails$).catch(
      rethrowRpcError,
    );
    const { id: lecturerId } = data!;

    const resp = this.termServiceClient.getLecturerTermListing({ lecturerId });

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }

  private async getPartnerTermListing(
    userId: number,
  ): Promise<GetPartnerTermListingResponse> {
    const partnerDetails$ = this.termAuthService
      .getAuthClient()
      .getPartnerDetails({ userId });

    const { data } = await lastValueFrom(partnerDetails$).catch(
      rethrowRpcError,
    );
    const { id: partnerId } = data!;

    const resp = this.termServiceClient.getPartnerTermListing({ partnerId });

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }

  private async getStudentTermListing(
    userId: number,
  ): Promise<GetStudentTermListingResponse> {
    const resp = this.termAuthService
      .getAuthClient()
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const studentId = res.data!.id;

          return this.termServiceClient.getStudentTermListing({ studentId });
        }),
      );

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }
}
