import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsBooleanString,
  IsDateString,
  IsEnum,
  IsIn,
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
  ValidateNested,
} from 'class-validator';
import { OffsetPagingQuery } from '@app/paginate';
import { TransformBoolean, TransformNumber } from '@app/utils';

export enum InternshipType {
  ASSOCIATE = 'ASSOCIATE',
  OTHER = 'OTHER',
}

export enum RegistrationStatus {
  WAITING = 'WAITING',
  PASSED = 'PASSED',
  FAILED = 'FAILED',
  SELECTED = 'SELECTED',
}

export class GetStudentApplicationsFilterQuery {
  @IsEnum(InternshipType)
  @IsOptional()
  internshipType?: InternshipType;

  @IsInt()
  @IsOptional()
  partnerId?: number;

  @IsBooleanString()
  @IsOptional()
  isMailSent?: string;

  @IsEnum(RegistrationStatus)
  @IsOptional()
  status?: RegistrationStatus;
}

export class GetStudentApplicationsQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetStudentApplicationsFilterQuery)
  filter?: GetStudentApplicationsFilterQuery;
}

export class GetPostsQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsArray()
  @Type(() => String)
  sort: string[] = [];
}

export class GetPartnerPostsFilterQuery {
  @IsNumber()
  @IsOptional()
  @TransformNumber
  termId?: number;
}

export class GetPartnerPostsQuery extends GetPostsQuery {
  @IsOptional()
  @ValidateNested()
  @Type(() => GetPartnerPostsFilterQuery)
  filter?: GetPartnerPostsFilterQuery;
}

export class GetTermsQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  sort: string[] = [];
}

export class GetTermStudentsFilterQuery {
  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  partnerSelected?: boolean;

  @IsNumber()
  @IsOptional()
  @TransformNumber
  supervisorId?: number;

  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  scoreGiven?: boolean;

  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  reportSubmitted?: boolean;

  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  supervisorAssigned?: boolean;
}

export class ExportTermStudentsQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetTermStudentsFilterQuery)
  filter?: GetTermStudentsFilterQuery;
}

export class GetTermStudentsQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetTermStudentsFilterQuery)
  filter?: GetTermStudentsFilterQuery;
}

export class GetTermLecturersQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];
}

export class GetTermPartnersFilterQuery {
  @IsIn(['PENDING', 'ACCEPTED', 'REJECTED'])
  status?: string;
}

export class ExportTermPartnersQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetTermPartnersFilterQuery)
  filter?: GetTermPartnersFilterQuery;
}

export class GetTermPartnersQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetTermPartnersFilterQuery)
  filter?: GetTermPartnersFilterQuery;
}

export class UpdatePostDto {
  @IsNumber()
  partnerContactId!: number;

  @IsString()
  @MaxLength(500)
  title!: string;

  @IsString()
  @MaxLength(4000)
  content!: string;

  @IsNumber()
  @Min(1)
  jobCount!: number;

  @IsDateString()
  startRegAt!: string;

  @IsDateString()
  endRegAt!: string;
}

export class CreatePartnerPostDto extends UpdatePostDto {
  @IsNumber()
  termId!: number;
}

export class CreatePostDto extends UpdatePostDto {
  @IsNumber()
  partnerId!: number;
}

export class GetMyStudentsFilterQuery {
  @IsNumber()
  @IsOptional()
  @TransformNumber
  termId?: number;

  @IsNumber()
  @IsOptional()
  @TransformNumber
  schoolClassId?: number;

  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  reportSubmitted?: boolean;

  @IsBoolean()
  @IsOptional()
  @TransformBoolean
  scoreGiven?: boolean;
}

export class GetMyStudentsQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  sort: string[] = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetMyStudentsFilterQuery)
  filter?: GetMyStudentsFilterQuery;
}

export class GetPartnerStudentApplicationsFilterQuery {
  @IsNumber()
  @IsOptional()
  @TransformNumber
  termId?: number;
}

export class ExportPartnerStudentApplicationsQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  @IsOptional()
  sort = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetPartnerStudentApplicationsFilterQuery)
  filter?: GetPartnerStudentApplicationsFilterQuery;
}

export class GetPartnerStudentApplicationsQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  @IsOptional()
  sort = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetPartnerStudentApplicationsFilterQuery)
  filter?: GetPartnerStudentApplicationsFilterQuery;
}

export class AssignScoreDto {
  @IsNumber()
  @Min(0)
  @Max(10)
  score!: number;
}

export class BatchUpdateRegistrationStatusDto {
  @IsNumber()
  @IsOptional()
  termId?: number;

  @IsNumber(undefined, { each: true })
  registrationIds!: number[];

  @IsIn(['PASSED', 'FAILED'])
  status!: string;
}

export class GetStudentsWorkingAtPartnerFilterQuery {
  @IsNumber()
  @IsOptional()
  @TransformNumber
  termId?: number;
}

export class GetStudentsWorkingAtPartnerQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  @IsOptional()
  sort = [];

  @IsOptional()
  @ValidateNested()
  @Type(() => GetStudentsWorkingAtPartnerFilterQuery)
  filter?: GetStudentsWorkingAtPartnerFilterQuery;
}

export class UpsertTermDto {
  @IsNumber()
  @IsOptional()
  termId?: number;

  @IsNumber()
  term!: number;

  @IsDateString()
  startRegAt!: string;

  @IsDateString()
  endRegAt!: string;

  @IsDateString()
  startDate!: string;

  @IsDateString()
  endDate!: string;

  @IsNumber()
  year!: number;
}

export class AssignmentDto {
  @IsNumber()
  supervisorId!: number;

  @IsNumber({}, { each: true })
  studentIds!: number[];
}

export class AssignSupervisorDto {
  @ValidateNested()
  assignments!: AssignmentDto[];
}

export class BatchAddPartnerToTermDto {
  @IsNumber({}, { each: true })
  partnerIds!: number[];
}

export class BatchChangeTermPartnerStatusDto {
  @IsIn(['ACCEPTED', 'REJECTED'])
  status!: string;

  @IsNumber({}, { each: true })
  partnerIds!: number[];
}
