import { BadRequestException, HttpStatus } from '@nestjs/common';
import { errorCodes } from '../common/errors';
import { ErrorResponse } from '../common/types';

export const errInvalidBoolValue = new BadRequestException(
  new ErrorResponse(HttpStatus.BAD_REQUEST, errorCodes.ERR_INVALID_BOOL_VALUE),
);
