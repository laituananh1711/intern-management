import { INTERNSHIP_SERVICE_PACKAGE_NAME } from '@app/pb/internship_service/service.pb';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { apiKeyAuthGrpcClientInterceptorFactory } from '../common/api-key-auth.grpc-interceptor';
import { UserModule } from '../user/user.module';
import { OrganizationController } from './organization.controller';
import { TermController } from './term.controller';
import { TermAuthService } from './term-auth.service';
import { HttpModule } from '@nestjs/axios';
import { DashboardController } from './dashboard.controller';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: INTERNSHIP_SERVICE_PACKAGE_NAME,
        useFactory: (configService: ConfigService) => {
          const internshipServiceUrl = configService.get(
            'INTERNSHIP_SERVICE_GRPC_URL',
            '0.0.0.0:4101',
          );

          const internshipServiceSecretHeader = configService.get(
            'INTERNSHIP_SERVICE_SECRET_HEADER',
            'x-uetwork-api',
          );
          const internshipServiceSecretKey = configService.get(
            'INTERNSHIP_SERVICE_SECRET_KEY',
            'secret',
          );

          const apikeyAuthClientInterceptor =
            apiKeyAuthGrpcClientInterceptorFactory(
              internshipServiceSecretHeader,
              internshipServiceSecretKey,
            );

          return {
            transport: Transport.GRPC,
            options: {
              package: INTERNSHIP_SERVICE_PACKAGE_NAME,
              protoPath: 'proto/internship_service/service.proto',
              url: internshipServiceUrl,
              loader: {
                // also serialize empty array
                arrays: true,
              },
              // Pass list of interceptors in `channelOptions` object.
              // https://github.com/nestjs/nest/issues/9079
              channelOptions: {
                interceptors: [apikeyAuthClientInterceptor],
              },
            } as any, // Workaround for invalid gRPC configuration. Will remove when there's an update to this issue. https://github.com/nestjs/nest/issues/9079
          };
        },
        inject: [ConfigService],
      },
    ]),
    UserModule,
    HttpModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const internshipServiceHttpUrl = configService.get(
          'INTERNSHIP_SERVICE_HTTP_URL',
          'http://0.0.0.0:4001',
        );

        const internshipServiceSecretHeader = configService.get(
          'INTERNSHIP_SERVICE_SECRET_HEADER',
          'x-uetwork-api',
        );
        const internshipServiceSecretKey = configService.get(
          'INTERNSHIP_SERVICE_SECRET_KEY',
          'secret',
        );

        return {
          baseURL: internshipServiceHttpUrl,
          headers: {
            [internshipServiceSecretHeader]: internshipServiceSecretKey,
          },
          timeout: 15000,
        };
      },
    }),
  ],
  controllers: [TermController, OrganizationController, DashboardController],
  providers: [TermAuthService],
})
export class InternshipModule {}
