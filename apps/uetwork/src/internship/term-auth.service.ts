import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import {
  TermServiceClient,
  INTERNSHIP_SERVICE_PACKAGE_NAME,
  TERM_SERVICE_NAME,
} from '@app/pb/internship_service/service.pb';
import {
  AuthServiceClient,
  USER_SERVICE_PACKAGE_NAME,
  AUTH_SERVICE_NAME,
} from '@app/pb/user_service/service.pb';
import { lastValueFrom, mergeMap, of } from 'rxjs';
import { errForbidden, errUnauthorized } from '../common/errors';

@Injectable()
export class TermAuthService implements OnModuleInit {
  private termServiceClient!: TermServiceClient;
  private authServiceClient!: AuthServiceClient;

  constructor(
    @Inject(INTERNSHIP_SERVICE_PACKAGE_NAME) private client: ClientGrpc,
    @Inject(USER_SERVICE_PACKAGE_NAME) private usClient: ClientGrpc,
  ) {}

  onModuleInit() {
    this.termServiceClient =
      this.client.getService<TermServiceClient>(TERM_SERVICE_NAME);
    this.authServiceClient =
      this.usClient.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
  }

  /** Check if this org admin user can access the data from this term.
   * Throw `errUnauthorized` if the user is unauthorized.
   */
  async assertOrgAdminCanAccessTerm(
    userId: number,
    termId: number,
  ): Promise<void> {
    const orgAdminCanAccessTerm$ = this.authServiceClient
      .getOrgAdminDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const organizationId = res.data!.organizationId;

          return this.termServiceClient.checkTermInOrg({
            termId,
            organizationId,
          });
        }),
        mergeMap((res) => {
          if (!res.data!.isIn) {
            throw errForbidden(new Error('admin not in org'));
          }

          return of(null);
        }),
      );

    await lastValueFrom(orgAdminCanAccessTerm$);
    return;
  }

  async assertStudentCanAccessTerm(
    userId: number,
    termId: number,
  ): Promise<void> {
    const studentCanAccessTerm$ = this.authServiceClient
      .getStudentDetails({ userId })
      .pipe(
        mergeMap((res) => {
          const { id: studentId } = res.data!;
          return this.termServiceClient.checkStudentInTerm({
            termId,
            studentId,
          });
        }),
        mergeMap((res) => {
          if (!res.data!.isIn) {
            // TODO: should I return 401 here?
            throw errForbidden(new Error('student not in term'));
          }
          return of(null);
        }),
      );
    await lastValueFrom(studentCanAccessTerm$);
  }

  async getOrgAdminOrgId(userId: number): Promise<number> {
    const res = await lastValueFrom(
      this.authServiceClient.getOrgAdminDetails({ userId }),
    );

    return res.data!.organizationId;
  }

  getAuthClient(): AuthServiceClient {
    return this.authServiceClient;
  }
}
