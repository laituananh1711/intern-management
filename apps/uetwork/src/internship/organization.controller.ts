import {
  AssignSupervisorToStudentsResponse,
  BatchAddPartnersToTermResponse,
  BatchChangeTermPartnerStatusResponse,
  CreatePostResponse,
  ExportTermPartnersResponse,
  ExportTermStudentsResponse,
  GetPostByIdResponse,
  GetPostsResponse,
  GetStudentApplicationsResponse,
  GetTermLecturersResponse,
  GetTermListingResponse,
  GetTermPartnersResponse,
  GetTermsResponse,
  GetTermStudentsResponse,
  internshipTypeFromJSON,
  INTERNSHIP_SERVICE_PACKAGE_NAME,
  partnerStatusFromJSON,
  registrationStatusFromJSON,
  TermServiceClient,
  TERM_SERVICE_NAME,
  UpdatePostResponse,
} from '@app/pb/internship_service/service.pb';
import {
  Body,
  Controller,
  Get,
  Inject,
  OnModuleInit,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom, Observable } from 'rxjs';
import { AppRole, Roles, User } from '@app/utils';
import {
  GetStudentApplicationsQuery,
  GetTermStudentsQuery,
  GetTermsQuery,
  GetTermLecturersQuery,
  GetTermPartnersQuery,
  GetPostsQuery,
  CreatePostDto,
  UpdatePostDto,
  ExportTermStudentsQuery,
  UpsertTermDto,
  ExportTermPartnersQuery,
  AssignSupervisorDto,
  BatchAddPartnerToTermDto,
  BatchChangeTermPartnerStatusDto,
} from './types';
import { TermAuthService } from './term-auth.service';
import { errInvalidBoolValue } from './errors';
import { rethrowRpcError } from '../common/errors';
import { toTimestamp, toTimestampNullable } from '../../../../libs/pb';
import {
  OrgAdminInterceptor,
  ReqOrgAdmin,
} from '../common/org-admin.interceptor';
import { OrgAdmin } from '../common/org-admin.decorator';

const trueValue = 'true';
const falseValue = 'false';

@Controller('orgs')
export class OrganizationController implements OnModuleInit {
  private termServiceClient!: TermServiceClient;

  constructor(
    @Inject(INTERNSHIP_SERVICE_PACKAGE_NAME) private client: ClientGrpc,
    private termAuthService: TermAuthService,
  ) {}

  onModuleInit() {
    this.termServiceClient =
      this.client.getService<TermServiceClient>(TERM_SERVICE_NAME);
  }

  @Get('terms')
  @Roles(AppRole.ORG_ADMIN)
  async getTerms(@Query() query: GetTermsQuery, @User('id') userId: number) {
    const organizationId = await this.termAuthService.getOrgAdminOrgId(userId);

    const resp = this.termServiceClient.getTerms({
      ...query,
      filter: {
        organizationId,
      },
    });

    return GetTermsResponse.toJSON(await lastValueFrom(resp));
  }

  @Post('terms')
  @Roles(AppRole.ORG_ADMIN)
  @UseInterceptors(OrgAdminInterceptor)
  async upsertTerm(
    @Body() dto: UpsertTermDto,
    @User('id') userId: number,
    @OrgAdmin() { organizationId }: ReqOrgAdmin,
  ): Promise<unknown> {
    let resp: Observable<{
      code: number;
      message: string;
      data?: { id: number };
    }>;
    if (dto.termId != null) {
      resp = this.termServiceClient.updateTerm({
        ...dto,
        id: dto.termId,
        startRegAt: toTimestampNullable(new Date(dto.startRegAt)),
        endRegAt: toTimestampNullable(new Date(dto.endRegAt)),
        filterOrganizationId: organizationId,
      });
    } else {
      resp = this.termServiceClient.createTerm({
        ...dto,
        startRegAt: toTimestampNullable(new Date(dto.startRegAt)),
        endRegAt: toTimestampNullable(new Date(dto.endRegAt)),
        organizationId,
      });
    }

    return await lastValueFrom(resp).catch(rethrowRpcError);
  }

  @Get('terms/:termId/students')
  @Roles(AppRole.ORG_ADMIN)
  async getTermStudents(
    @Query() query: GetTermStudentsQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.getTermStudents({
      ...query,
      filter: {
        termId,
        ...query.filter,
      },
    });
    return GetTermStudentsResponse.toJSON(await lastValueFrom(resp));
  }

  @Get('terms/:termId/students/export')
  @Roles(AppRole.ORG_ADMIN)
  async exportTermStudents(
    @Query() query: ExportTermStudentsQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.exportTermStudents({
      ...query,
      filter: {
        termId,
        ...query.filter,
      },
    });

    return ExportTermStudentsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('terms/:termId/lecturers')
  @Roles(AppRole.ORG_ADMIN)
  async getTermLecturers(
    @Query() query: GetTermLecturersQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.getTermLecturers({
      ...query,
      filter: {
        termId,
      },
    });

    return GetTermLecturersResponse.toJSON(await lastValueFrom(resp));
  }

  @Get('terms/:termId/partners/export')
  @Roles(AppRole.ORG_ADMIN)
  async exportTermPartners(
    @Query() query: ExportTermPartnersQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.exportTermPartners({
      ...query,
      filter: {
        termId,
        status:
          query.filter?.status == null
            ? undefined
            : partnerStatusFromJSON(query.filter.status),
      },
    });

    return ExportTermPartnersResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('terms/:termId/partners')
  @Roles(AppRole.ORG_ADMIN)
  async getTermPartners(
    @Query() query: GetTermPartnersQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.getTermPartners({
      ...query,
      filter: {
        termId,
        status:
          query.filter?.status == null
            ? undefined
            : partnerStatusFromJSON(query.filter.status),
      },
    });

    return GetTermPartnersResponse.toJSON(await lastValueFrom(resp));
  }

  @Get('terms/:termId/students/applications')
  @Roles(AppRole.ORG_ADMIN)
  async getStudentApplications(
    @Query() query: GetStudentApplicationsQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    let filterIsMailSent: boolean | undefined;
    if (query.filter?.isMailSent != null) {
      const isMailSent = query.filter.isMailSent;
      if (isMailSent === trueValue) {
        filterIsMailSent = true;
      } else if (isMailSent === falseValue) {
        filterIsMailSent = false;
      } else {
        throw errInvalidBoolValue;
      }
    }

    const resp = this.termServiceClient.getStudentApplications({
      ...query,
      filter: {
        status:
          query.filter?.status == null
            ? undefined
            : registrationStatusFromJSON(query.filter.status),
        partnerId: query.filter?.partnerId,
        isMailSent: filterIsMailSent,
        internshipType:
          query.filter?.internshipType == null
            ? undefined
            : internshipTypeFromJSON(query.filter.internshipType),
        termId,
      },
    });

    return GetStudentApplicationsResponse.toJSON(await lastValueFrom(resp));
  }

  @Get('terms/:termId/posts')
  @Roles(AppRole.ORG_ADMIN)
  async getPosts(
    @Query() query: GetPostsQuery,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.getPosts({
      ...query,
      filter: {
        termId,
      },
    });

    return GetPostsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('terms/:termId/posts')
  @Roles(AppRole.ORG_ADMIN)
  async createPost(
    @Body() dto: CreatePostDto,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.createPost({
      ...dto,
      termId,
      startRegAt: toTimestamp(new Date(dto.startRegAt)),
      endRegAt: toTimestamp(new Date(dto.endRegAt)),
    });

    return CreatePostResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('terms/:termId/posts/:postId')
  @Roles(AppRole.ORG_ADMIN)
  async updatePost(
    @Body() dto: UpdatePostDto,
    @Param('termId', ParseIntPipe) termId: number,
    @Param('postId', ParseIntPipe) postId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.updatePost({
      ...dto,
      filterTermId: termId,
      startRegAt: toTimestamp(new Date(dto.startRegAt)),
      endRegAt: toTimestamp(new Date(dto.endRegAt)),
      id: postId,
    });

    return UpdatePostResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('terms/:termId/posts/:postId')
  @Roles(AppRole.ORG_ADMIN)
  async getPostById(
    @Param('termId', ParseIntPipe) termId: number,
    @Param('postId', ParseIntPipe) postId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.getPostById({ postId, termId });

    return GetPostByIdResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('terms/listing')
  @Roles(AppRole.ORG_ADMIN)
  async getTermListing(@User('id') userId: number): Promise<unknown> {
    const authServiceClient = this.termAuthService.getAuthClient();

    const orgAdminDetails$ = authServiceClient.getOrgAdminDetails({ userId });

    const { data } = await lastValueFrom(orgAdminDetails$).catch(
      rethrowRpcError,
    );
    const { organizationId } = data!;

    const resp = this.termServiceClient.getTermListing({
      organizationId,
    });

    return GetTermListingResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('terms/:termId/lecturers/assign')
  @Roles(AppRole.ORG_ADMIN)
  async assignSupervisor(
    @Body() dto: AssignSupervisorDto,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    // TODO: make sure lecturer assigned belongs to this org.
    const resp = this.termServiceClient.assignSupervisorToStudents({
      ...dto,
      termId,
    });

    return AssignSupervisorToStudentsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Post('terms/:termId/partners')
  @Roles(AppRole.ORG_ADMIN)
  async batchAddPartnersToTerm(
    @Body() { partnerIds }: BatchAddPartnerToTermDto,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.batchAddPartnersToTerm({
      termId,
      partnerIds,
    });

    return BatchAddPartnersToTermResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Put('terms/:termId/partners/status')
  @Roles(AppRole.ORG_ADMIN)
  async batchChangeTermPartnerStatus(
    @Body() { status, partnerIds }: BatchChangeTermPartnerStatusDto,
    @Param('termId', ParseIntPipe) termId: number,
    @User('id') userId: number,
  ): Promise<unknown> {
    await this.termAuthService.assertOrgAdminCanAccessTerm(userId, termId);

    const resp = this.termServiceClient.batchChangeTermPartnerStatus({
      termId,
      partnerIds,
      status: partnerStatusFromJSON(status),
    });

    return BatchChangeTermPartnerStatusResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }
}
