import { Controller, Get, HttpStatus, Inject } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import {
  GetMyStudentStatsResponse,
  INTERNSHIP_SERVICE_PACKAGE_NAME,
  TermServiceClient,
  TERM_SERVICE_NAME,
} from '../../../../libs/pb/internship_service/service.pb';
import { AppRole, Roles, User } from '../../../../libs/utils/src';
import { rethrowRpcError } from '../common/errors';
import { TermAuthService } from './term-auth.service';

@Controller('dashboard')
export class DashboardController {
  private termServiceClient!: TermServiceClient;

  constructor(
    @Inject(INTERNSHIP_SERVICE_PACKAGE_NAME) private client: ClientGrpc,
    private termAuthService: TermAuthService,
  ) {}

  onModuleInit() {
    this.termServiceClient =
      this.client.getService<TermServiceClient>(TERM_SERVICE_NAME);
  }

  @Get('lecturer')
  @Roles(AppRole.LECTURER)
  async getLecturerDashboard(@User('id') userId: number) {
    const { data } = await lastValueFrom(
      this.termAuthService.getAuthClient().getLecturerDetails({ userId }),
    );

    const { id, organizationId } = data!;

    const resp = this.termServiceClient.getMyStudentStats({
      lecturerId: id,
      organizationId,
    });

    return GetMyStudentStatsResponse.toJSON(
      await lastValueFrom(resp).catch(rethrowRpcError),
    );
  }

  @Get('partner')
  async getPartnerDashboard() {
    return {
      code: HttpStatus.OK,
      message: 'success',
      data: {
        workingStudentsStat: {
          total: 20,
        },
        recentApplications: [],
      },
    };
  }

  @Get('org-admin')
  async getOrgAdminDashboard() {
    return {
      code: HttpStatus.OK,
      message: 'success',
      data: {
        termStudentsStat: {
          lastTotal: 40,
          currentTotal: 85,
        },
        termPartnersStat: {
          lastTotal: 90,
          currentTotal: 100,
        },
        termLecturersStat: {
          lastTotal: 50,
          currentTotal: 48,
        },
        termPartnersStatusStat: {
          totalPending: 12,
          totalAccepted: 20,
          totalRejected: 16,
        },
        mostPopularPartnersStat: [
          {
            partners: [
              {
                id: 0,
                name: 'Công ty A',
                studentsCount: 100,
                logoUrl: 'string',
              },
            ],
          },
        ],
      },
    };
  }

  @Get('sys-admin')
  async getSysAdminDashboard() {
    return {
      code: HttpStatus.OK,
      message: 'success',
      data: {
        orgsStat: {
          total: 2,
        },
        classesStat: {
          total: 13,
        },
        studentsStat: {
          total: 286,
        },
        partnersStat: {
          total: 403,
        },
        lecturersStat: {
          total: 81,
        },
      },
    };
  }
}
