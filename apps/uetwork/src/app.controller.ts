import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('health/live')
  checkLiveness() {
    return 'OK';
  }

  @Get('health/ready')
  checkReadiness() {
    return 'OK';
  }
}
