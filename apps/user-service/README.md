# Setup User Service

## Run Migrations

You might need to run `CREATE DATABASE user_service` before migrating.

```shell
make user-service-migrate-up
```

## Start the service

Make sure you already installed Nest CLI using `npm i -g @nestjs/cli`

```sh
nest start user-service --watch
```
