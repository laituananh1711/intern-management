import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
// NOTE: don't use `@app/utils` because it will cause migration gen cli to fail.
import {
  isZeroValue,
  normalizeEmail,
} from '../../../../../libs/utils/src/utils';
import { PartnerContact } from './partner-contact.entity';
import { PartnerOrganization } from './partner-org.entity';
import { User } from './user.entity';

@Entity('partners')
export class Partner {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Index('idx_partners_name', {
    fulltext: true,
  })
  @Column({
    type: 'varchar',
    length: '255',
    comment: 'Company name',
  })
  name?: string;

  @Column({
    type: 'varchar',
    length: '255',
    comment: 'partner email, may be different from login email',
    default: '',
  })
  email?: string;

  // NOTE: a prefix index is added in the migration files, but not
  // specified here with @Index(). See https://github.com/typeorm/typeorm/issues/749.
  @Column({
    type: 'varchar',
    length: '1000',
  })
  homepageUrl?: string;

  @Column({
    type: 'varchar',
    length: '1000',
  })
  logoUrl?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  phoneNumber?: string;

  @Column({
    type: 'varchar',
    default: '',
  })
  address?: string;

  @Column({
    type: 'varchar',
    length: '4000',
    default: '',
  })
  description?: string;

  @Column({ type: 'int', unsigned: true, nullable: true })
  userId?: number;

  @OneToOne(() => User, { cascade: ['insert'] })
  @JoinColumn()
  user?: User;

  @OneToMany(() => PartnerContact, (contact) => contact.partner)
  contacts?: PartnerContact[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @OneToMany(() => PartnerOrganization, (po) => po.partner)
  partnerOrgs?: PartnerOrganization[];

  @BeforeInsert()
  @BeforeUpdate()
  normalizeEmail() {
    if (!isZeroValue(this.email)) {
      this.email = normalizeEmail(this.email!);
    }
  }

  constructor(partner: Partial<Partner>) {
    Object.assign(this, partner);
  }
}
