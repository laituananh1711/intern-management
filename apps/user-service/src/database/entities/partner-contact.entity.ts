import { PartnerContact as PbPartnerContact } from '@app/pb/user_service/service.pb';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Partner } from './partner.entity';

@Entity('partner_contacts')
export class PartnerContact {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'varchar',
    length: '255',
  })
  fullName?: string;

  @Column({
    type: 'varchar',
    length: '20',
  })
  phoneNumber?: string;

  @Column({ type: 'varchar', length: '255', default: '' })
  email?: string;

  @Column({
    type: 'int',
    unsigned: true,
  })
  partnerId?: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @ManyToOne(() => Partner)
  partner?: Partner;

  @DeleteDateColumn()
  deletedAt?: Date;

  constructor(contact: Partial<PartnerContact>) {
    Object.assign(this, contact);
  }
}

export const PartnerContactMethods = {
  toPb(c: PartnerContact | undefined): PbPartnerContact | undefined {
    if (c == undefined) {
      return undefined;
    }

    return {
      email: c.email ?? '',
      fullName: c.fullName ?? '',
      id: c.id ?? 0,
      partnerId: c.partnerId ?? 0,
      phoneNumber: c.phoneNumber ?? '',
    };
  },
  toPbs(contacts: PartnerContact[] | undefined): PbPartnerContact[] {
    if (contacts == undefined) {
      return [];
    }

    const pbContacts: PbPartnerContact[] = [];
    for (const c of contacts) {
      pbContacts.push(this.toPb(c)!);
    }

    return pbContacts;
  },
};
