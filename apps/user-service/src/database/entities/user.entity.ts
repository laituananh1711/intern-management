import { Exclude } from 'class-transformer';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { isZeroValue, normalizeEmail } from '../../../../../libs/utils/src';
import { Role } from './role.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'varchar',
    length: '255',
  })
  avatarUrl?: string;

  @Index({
    unique: true,
  })
  @Column({
    type: 'varchar',
    length: '255',
    comment: 'University-provided email (ex. 18020222@vnu.edu.vn)',
  })
  orgEmail?: string;

  @Column({
    type: 'int',
    unsigned: true,
  })
  roleId?: number;

  @Exclude()
  @Column({
    type: 'char',
    length: '60',
    comment: "User's password hashed with bcrypt",
    select: false,
  })
  password?: string;

  @Column({
    type: 'timestamp',
    nullable: true,
  })
  lastLoginAt?: Date;

  @Column({
    type: 'bool',
    default: false,
  })
  isEmailVerified?: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @Exclude()
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @BeforeInsert()
  @BeforeUpdate()
  normalizeEmail() {
    if (!isZeroValue(this.orgEmail)) {
      this.orgEmail = normalizeEmail(this.orgEmail!);
    }
  }

  // TODO: add delete function later

  @ManyToOne(() => Role)
  role?: Role;

  constructor(user: Partial<User>) {
    Object.assign(this, user);
  }
}
