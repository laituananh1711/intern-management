import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { Organization } from './organization.entity';
import { Partner } from './partner.entity';

@Entity('partner_organizations')
@Unique(['partnerId', 'organizationId'])
export class PartnerOrganization {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'int',
    unsigned: true,
  })
  partnerId?: number;

  @Column({
    type: 'int',
    unsigned: true,
  })
  organizationId?: number;

  @Column({ type: 'date', nullable: true })
  expiredDate?: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @ManyToOne(() => Partner)
  partner?: Partner;

  @ManyToOne(() => Organization)
  organization?: Organization;
}
