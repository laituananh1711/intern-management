import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('school_classes')
export class SchoolClass {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  name?: string;

  @Column({
    type: 'varchar',
    nullable: false,
    default: '',
  })
  programName?: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;
}
