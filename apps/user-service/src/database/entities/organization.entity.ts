import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Student } from './student.entity';
import { User } from './user.entity';

@Entity('organizations')
export class Organization {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id?: number;

  @Index('idx_organizations_name', {
    fulltext: true,
  })
  @Column({
    type: 'varchar',
    length: '255',
    unique: true,
  })
  name?: string;

  @Column({ type: 'int', unsigned: true, nullable: true })
  adminId?: number;

  @OneToOne(() => User, { cascade: ['insert'] })
  @JoinColumn()
  admin?: User;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @OneToMany(() => Student, (student) => student.organization)
  students?: Student[];

  constructor(org: Partial<Organization>) {
    Object.assign(this, org);
  }
}
