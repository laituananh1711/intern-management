import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { isZeroValue, normalizeEmail } from '../../../../../libs/utils/src';
import { Organization } from './organization.entity';
import { SchoolClass } from './school-class.entity';
import { User } from './user.entity';

@Entity('students')
export class Student {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'char',
    length: '8',
    comment: 'A 8-digit id number provided by the school (i.e. 18020001)',
  })
  studentIdNumber?: string;

  @Index('idx_students_full_name', {
    fulltext: true,
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  fullName?: string;

  @Column({
    type: 'varchar',
    nullable: true,
    length: '1000',
  })
  resumeUrl?: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  phoneNumber?: string;

  @Column({
    type: 'int',
    unsigned: true,
    nullable: true,
  })
  schoolClassId?: number;

  @ManyToOne(() => SchoolClass, { onDelete: 'SET NULL' })
  schoolClass?: SchoolClass;

  @Column({
    type: 'varchar',
  })
  orgEmail?: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  personalEmail?: string;

  @Column({ type: 'int', unsigned: true, nullable: true })
  userId?: number;

  @OneToOne(() => User, { cascade: ['insert'] })
  @JoinColumn()
  user?: User;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: Date;

  @Column({ type: 'int', unsigned: true })
  organizationId?: number;

  @ManyToOne(() => Organization, (org) => org.students)
  organization?: Organization;

  @BeforeInsert()
  @BeforeUpdate()
  normalizeEmail() {
    if (!isZeroValue(this.orgEmail)) {
      this.orgEmail = normalizeEmail(this.orgEmail!);
    }
    if (!isZeroValue(this.personalEmail)) {
      this.personalEmail = normalizeEmail(this.personalEmail!);
    }
  }

  constructor(student: Partial<Student>) {
    Object.assign(this, student);
  }
}
