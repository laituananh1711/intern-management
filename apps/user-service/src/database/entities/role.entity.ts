import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity('roles')
export class Role {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Index('idx_roles_name', { unique: true })
  @Column({ type: 'varchar', length: 255 })
  name?: string;

  constructor(role: Partial<Role>) {
    Object.assign(this, role);
  }
}
