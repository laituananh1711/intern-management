import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { isZeroValue, normalizeEmail } from '../../../../../libs/utils/src';
import { Organization } from './organization.entity';
import { User } from './user.entity';

@Entity('lecturers')
export class Lecturer {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Index('idx_lecturers_full_name', {
    fulltext: true,
  })
  @Column({
    type: 'varchar',
    length: '255',
  })
  fullName?: string;

  @Column({
    type: 'varchar',
    length: '255',
  })
  orgEmail?: string;

  @Column({
    type: 'varchar',
    length: '255',
    nullable: true,
  })
  personalEmail?: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  phoneNumber?: string;

  @Column({ type: 'int', unsigned: true, nullable: true })
  userId?: number;

  @OneToOne(() => User, { cascade: ['insert'] })
  @JoinColumn()
  user?: User;

  @Column({ type: 'int', unsigned: true })
  organizationId?: number;

  @ManyToOne(() => Organization)
  organization?: Organization;

  @BeforeInsert()
  @BeforeUpdate()
  normalizeEmail() {
    if (!isZeroValue(this.orgEmail)) {
      this.orgEmail = normalizeEmail(this.orgEmail!);
    }
    if (!isZeroValue(this.personalEmail)) {
      this.personalEmail = normalizeEmail(this.personalEmail!);
    }
  }

  constructor(lecturer: Partial<Lecturer>) {
    Object.assign(this, lecturer);
  }
}
