import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('reset_password_requests')
export class ResetPasswordRequest {
  @PrimaryGeneratedColumn({
    type: 'int',
    unsigned: true,
  })
  id?: number;

  @Column({
    type: 'int',
    unsigned: true,
  })
  userId?: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  token?: string;

  @Column({ type: 'timestamp' })
  expiredAt?: Date;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt?: Date;

  @ManyToOne(() => User)
  user?: User;

  constructor(obj: Partial<ResetPasswordRequest>) {
    Object.assign(this, obj);
  }
}
