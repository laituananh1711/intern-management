import { MigrationInterface, QueryRunner } from 'typeorm';

export class ogranizationAddUniqueName1647966675106
  implements MigrationInterface
{
  name = 'ogranizationAddUniqueName1647966675106';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`organizations\` ADD UNIQUE INDEX \`IDX_9b7ca6d30b94fef571cff87688\` (\`name\`)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`organizations\` DROP INDEX \`IDX_9b7ca6d30b94fef571cff87688\``,
    );
  }
}
