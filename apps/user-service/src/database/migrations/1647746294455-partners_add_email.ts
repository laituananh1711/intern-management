import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnersAddEmail1647746294455 implements MigrationInterface {
  name = 'partnersAddEmail1647746294455';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX \`IDX_3492d90109961ff92f974cc687\` ON \`organizations\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`partners\` ADD \`email\` varchar(255) NOT NULL COMMENT 'partner email, may be different from login email' DEFAULT ''`,
    );
    await queryRunner.query(
      `ALTER TABLE \`partner_contacts\` ADD \`email\` varchar(255) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`partner_contacts\` DROP COLUMN \`email\``,
    );
    await queryRunner.query(`ALTER TABLE \`partners\` DROP COLUMN \`email\``);
    await queryRunner.query(
      `CREATE UNIQUE INDEX \`IDX_3492d90109961ff92f974cc687\` ON \`organizations\` (\`admin_id\`)`,
    );
  }
}
