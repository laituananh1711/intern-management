import { MigrationInterface, QueryRunner } from 'typeorm';

export class initTables1629461277394 implements MigrationInterface {
  name = 'initTables1629461277394';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `majors` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL COMMENT 'ex. Máy tính và Robot', PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'CREATE TABLE `roles` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, UNIQUE INDEX `idx_roles_name` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      "CREATE TABLE `users` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `user_name` varchar(255) NOT NULL, `avatar_url` varchar(255) NOT NULL, `org_email` varchar(255) NOT NULL COMMENT 'University-provided email (ex. 18020222@vnu.edu.vn)', `role_id` int UNSIGNED NOT NULL, `password` char(60) NOT NULL COMMENT 'User''s password hashed with bcrypt', `last_login_at` timestamp NULL, `is_email_verified` tinyint NOT NULL DEFAULT 0, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX `IDX_f6d1517ea6b64a530374bd6608` (`org_email`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      "CREATE TABLE `students` (`id` int UNSIGNED NOT NULL, `student_id_number` char(8) NOT NULL COMMENT 'A 8-digit id number provided by the school (i.e. 18020001)', `full_name` varchar(255) NOT NULL, `resume_url` varchar(1000) NULL, `phone_number` varchar(255) NULL, `major_id` int UNSIGNED NULL, `personal_email` varchar(255) NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `organization_id` int UNSIGNED NOT NULL, FULLTEXT INDEX `idx_students_full_name` (`full_name`), UNIQUE INDEX `REL_7d7f07271ad4ce999880713f05` (`id`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'CREATE TABLE `organizations` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `lecturers` (`id` int UNSIGNED NOT NULL, `full_name` varchar(255) NOT NULL, `personal_email` varchar(255) NULL, `phone_number` varchar(255) NOT NULL, `organization_id` int UNSIGNED NOT NULL, UNIQUE INDEX `REL_4dffa0b38d36bfd09610d64b39` (`id`), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      "CREATE TABLE `partners` (`id` int UNSIGNED NOT NULL, `name` varchar(255) NOT NULL COMMENT 'Company name', `homepage_url` varchar(1000) NOT NULL, `logo_url` varchar(1000) NOT NULL, `description` varchar(4000) NOT NULL DEFAULT '', FULLTEXT INDEX `idx_partners_name` (`name`), UNIQUE INDEX `REL_998645b20820e4ab99aeae03b4` (`id`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      'CREATE TABLE `partner_contacts` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `full_name` varchar(255) NOT NULL, `phone_number` varchar(20) NOT NULL, `partner_id` int UNSIGNED NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'CREATE TABLE `partner_organizations` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `partner_id` int UNSIGNED NOT NULL, `organization_id` int UNSIGNED NOT NULL, `expired_date` date NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB',
    );
    await queryRunner.query(
      'ALTER TABLE `users` ADD CONSTRAINT `FK_a2cecd1a3531c0b041e29ba46e1` FOREIGN KEY (`role_id`) REFERENCES `roles`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD CONSTRAINT `FK_43252183688e683a449d3a719c8` FOREIGN KEY (`major_id`) REFERENCES `majors`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD CONSTRAINT `FK_7d7f07271ad4ce999880713f05e` FOREIGN KEY (`id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD CONSTRAINT `FK_9571384818ecf499779d3a9d141` FOREIGN KEY (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD CONSTRAINT `FK_4dffa0b38d36bfd09610d64b399` FOREIGN KEY (`id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD CONSTRAINT `FK_6253138c3c52b0539463b35c1ee` FOREIGN KEY (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` ADD CONSTRAINT `FK_998645b20820e4ab99aeae03b41` FOREIGN KEY (`id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` ADD CONSTRAINT `FK_608759e68dbcd493b2e3e9af0d1` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` ADD CONSTRAINT `FK_b9d2fddf47707c420bf7d4142a7` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` ADD CONSTRAINT `FK_3286960e0b1f1c2e5a1fa9bf1ee` FOREIGN KEY (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` DROP FOREIGN KEY `FK_3286960e0b1f1c2e5a1fa9bf1ee`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` DROP FOREIGN KEY `FK_b9d2fddf47707c420bf7d4142a7`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` DROP FOREIGN KEY `FK_608759e68dbcd493b2e3e9af0d1`',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` DROP FOREIGN KEY `FK_998645b20820e4ab99aeae03b41`',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` DROP FOREIGN KEY `FK_6253138c3c52b0539463b35c1ee`',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` DROP FOREIGN KEY `FK_4dffa0b38d36bfd09610d64b399`',
    );
    await queryRunner.query(
      'ALTER TABLE `students` DROP FOREIGN KEY `FK_9571384818ecf499779d3a9d141`',
    );
    await queryRunner.query(
      'ALTER TABLE `students` DROP FOREIGN KEY `FK_7d7f07271ad4ce999880713f05e`',
    );
    await queryRunner.query(
      'ALTER TABLE `students` DROP FOREIGN KEY `FK_43252183688e683a449d3a719c8`',
    );
    await queryRunner.query(
      'ALTER TABLE `users` DROP FOREIGN KEY `FK_a2cecd1a3531c0b041e29ba46e1`',
    );
    await queryRunner.query('DROP TABLE `partner_organizations`');
    await queryRunner.query('DROP TABLE `partner_contacts`');
    await queryRunner.query(
      'DROP INDEX `REL_998645b20820e4ab99aeae03b4` ON `partners`',
    );
    await queryRunner.query('DROP INDEX `idx_partners_name` ON `partners`');
    await queryRunner.query('DROP TABLE `partners`');
    await queryRunner.query(
      'DROP INDEX `REL_4dffa0b38d36bfd09610d64b39` ON `lecturers`',
    );
    await queryRunner.query('DROP TABLE `lecturers`');
    await queryRunner.query('DROP TABLE `organizations`');
    await queryRunner.query(
      'DROP INDEX `REL_7d7f07271ad4ce999880713f05` ON `students`',
    );
    await queryRunner.query(
      'DROP INDEX `idx_students_full_name` ON `students`',
    );
    await queryRunner.query('DROP TABLE `students`');
    await queryRunner.query(
      'DROP INDEX `IDX_f6d1517ea6b64a530374bd6608` ON `users`',
    );
    await queryRunner.query('DROP TABLE `users`');
    await queryRunner.query('DROP INDEX `idx_roles_name` ON `roles`');
    await queryRunner.query('DROP TABLE `roles`');
    await queryRunner.query('DROP TABLE `majors`');
  }
}
