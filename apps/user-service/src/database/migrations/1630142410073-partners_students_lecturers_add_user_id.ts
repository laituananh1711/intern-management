import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnersStudentsLecturersAddUserId1630142410073
  implements MigrationInterface
{
  name = 'partnersStudentsLecturersAddUserId1630142410073';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `students` DROP FOREIGN KEY `FK_7d7f07271ad4ce999880713f05e`',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` DROP FOREIGN KEY `FK_4dffa0b38d36bfd09610d64b399`',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` DROP FOREIGN KEY `FK_998645b20820e4ab99aeae03b41`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_7d7f07271ad4ce999880713f05` ON `students`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_4dffa0b38d36bfd09610d64b39` ON `lecturers`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_998645b20820e4ab99aeae03b4` ON `partners`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` DROP COLUMN `email`',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD `user_id` int UNSIGNED NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD UNIQUE INDEX `IDX_fb3eff90b11bddf7285f9b4e28` (`user_id`)',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD `user_id` int UNSIGNED NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD UNIQUE INDEX `IDX_6cf38bc4109df019cbd4990e26` (`user_id`)',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` ADD `user_id` int UNSIGNED NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` ADD UNIQUE INDEX `IDX_6aee7fd33891dbfa5ccbbdfe08` (`user_id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_fb3eff90b11bddf7285f9b4e28` ON `students` (`user_id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_6cf38bc4109df019cbd4990e26` ON `lecturers` (`user_id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_6aee7fd33891dbfa5ccbbdfe08` ON `partners` (`user_id`)',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD CONSTRAINT `FK_fb3eff90b11bddf7285f9b4e281` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD CONSTRAINT `FK_6cf38bc4109df019cbd4990e269` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` ADD CONSTRAINT `FK_6aee7fd33891dbfa5ccbbdfe084` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `partners` DROP FOREIGN KEY `FK_6aee7fd33891dbfa5ccbbdfe084`',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` DROP FOREIGN KEY `FK_6cf38bc4109df019cbd4990e269`',
    );
    await queryRunner.query(
      'ALTER TABLE `students` DROP FOREIGN KEY `FK_fb3eff90b11bddf7285f9b4e281`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_6aee7fd33891dbfa5ccbbdfe08` ON `partners`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_6cf38bc4109df019cbd4990e26` ON `lecturers`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_fb3eff90b11bddf7285f9b4e28` ON `students`',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` DROP INDEX `IDX_6aee7fd33891dbfa5ccbbdfe08`',
    );
    await queryRunner.query('ALTER TABLE `partners` DROP COLUMN `user_id`');
    await queryRunner.query(
      'ALTER TABLE `lecturers` DROP INDEX `IDX_6cf38bc4109df019cbd4990e26`',
    );
    await queryRunner.query('ALTER TABLE `lecturers` DROP COLUMN `user_id`');
    await queryRunner.query(
      'ALTER TABLE `students` DROP INDEX `IDX_fb3eff90b11bddf7285f9b4e28`',
    );
    await queryRunner.query('ALTER TABLE `students` DROP COLUMN `user_id`');
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` ADD `email` varchar(255) NOT NULL',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_998645b20820e4ab99aeae03b4` ON `partners` (`id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_4dffa0b38d36bfd09610d64b39` ON `lecturers` (`id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_7d7f07271ad4ce999880713f05` ON `students` (`id`)',
    );
    await queryRunner.query(
      'ALTER TABLE `partners` ADD CONSTRAINT `FK_998645b20820e4ab99aeae03b41` FOREIGN KEY (`id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD CONSTRAINT `FK_4dffa0b38d36bfd09610d64b399` FOREIGN KEY (`id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `students` ADD CONSTRAINT `FK_7d7f07271ad4ce999880713f05e` FOREIGN KEY (`id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }
}
