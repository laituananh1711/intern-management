import { MigrationInterface, QueryRunner } from 'typeorm';

export class organizationAddAdminId1630159102492 implements MigrationInterface {
  name = 'organizationAddAdminId1630159102492';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `organizations` ADD `admin_id` int UNSIGNED NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `organizations` ADD UNIQUE INDEX `IDX_3492d90109961ff92f974cc687` (`admin_id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `REL_3492d90109961ff92f974cc687` ON `organizations` (`admin_id`)',
    );
    await queryRunner.query(
      'ALTER TABLE `organizations` ADD CONSTRAINT `FK_3492d90109961ff92f974cc687b` FOREIGN KEY (`admin_id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `organizations` DROP FOREIGN KEY `FK_3492d90109961ff92f974cc687b`',
    );
    await queryRunner.query(
      'DROP INDEX `REL_3492d90109961ff92f974cc687` ON `organizations`',
    );
    await queryRunner.query(
      'ALTER TABLE `organizations` DROP INDEX `IDX_3492d90109961ff92f974cc687`',
    );
    await queryRunner.query(
      'ALTER TABLE `organizations` DROP COLUMN `admin_id`',
    );
  }
}
