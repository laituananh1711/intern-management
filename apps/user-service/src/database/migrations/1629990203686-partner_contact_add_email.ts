import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerContactAddEmail1629990203686 implements MigrationInterface {
  name = 'partnerContactAddEmail1629990203686';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` ADD `email` varchar(255) NOT NULL',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` DROP COLUMN `email`',
    );
  }
}
