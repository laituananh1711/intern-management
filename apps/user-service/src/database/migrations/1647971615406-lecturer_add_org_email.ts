import { MigrationInterface, QueryRunner } from 'typeorm';

export class lecturerAddOrgEmail1647971615406 implements MigrationInterface {
  name = 'lecturerAddOrgEmail1647971615406';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` ADD \`org_email\` varchar(255) NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`lecturers\` DROP COLUMN \`org_email\``,
    );
  }
}
