import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerStudentLecturerIdAutoIncrement1630143730615
  implements MigrationInterface
{
  name = 'partnerStudentLecturerIdAutoIncrement1630143730615';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'DROP INDEX `IDX_fb3eff90b11bddf7285f9b4e28` ON `students`',
    );
    await queryRunner.query(
      'DROP INDEX `IDX_6cf38bc4109df019cbd4990e26` ON `lecturers`',
    );
    await queryRunner.query(
      'DROP INDEX `IDX_6aee7fd33891dbfa5ccbbdfe08` ON `partners`',
    );
    await queryRunner.query('ALTER TABLE `students` DROP PRIMARY KEY');
    await queryRunner.query('ALTER TABLE `students` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `students` ADD `id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
    );
    await queryRunner.query('ALTER TABLE `lecturers` DROP PRIMARY KEY');
    await queryRunner.query('ALTER TABLE `lecturers` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD `id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` DROP FOREIGN KEY `FK_608759e68dbcd493b2e3e9af0d1`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` DROP FOREIGN KEY `FK_b9d2fddf47707c420bf7d4142a7`',
    );
    await queryRunner.query('ALTER TABLE `partners` DROP PRIMARY KEY');
    await queryRunner.query('ALTER TABLE `partners` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `partners` ADD `id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` ADD CONSTRAINT `FK_608759e68dbcd493b2e3e9af0d1` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` ADD CONSTRAINT `FK_b9d2fddf47707c420bf7d4142a7` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` DROP FOREIGN KEY `FK_b9d2fddf47707c420bf7d4142a7`',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` DROP FOREIGN KEY `FK_608759e68dbcd493b2e3e9af0d1`',
    );
    await queryRunner.query('ALTER TABLE `partners` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `partners` ADD `id` int UNSIGNED NOT NULL',
    );
    await queryRunner.query('ALTER TABLE `partners` ADD PRIMARY KEY (`id`)');
    await queryRunner.query(
      'ALTER TABLE `partner_organizations` ADD CONSTRAINT `FK_b9d2fddf47707c420bf7d4142a7` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query(
      'ALTER TABLE `partner_contacts` ADD CONSTRAINT `FK_608759e68dbcd493b2e3e9af0d1` FOREIGN KEY (`partner_id`) REFERENCES `partners`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    );
    await queryRunner.query('ALTER TABLE `lecturers` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `lecturers` ADD `id` int UNSIGNED NOT NULL',
    );
    await queryRunner.query('ALTER TABLE `lecturers` ADD PRIMARY KEY (`id`)');
    await queryRunner.query('ALTER TABLE `students` DROP COLUMN `id`');
    await queryRunner.query(
      'ALTER TABLE `students` ADD `id` int UNSIGNED NOT NULL',
    );
    await queryRunner.query('ALTER TABLE `students` ADD PRIMARY KEY (`id`)');
    await queryRunner.query(
      'CREATE UNIQUE INDEX `IDX_6aee7fd33891dbfa5ccbbdfe08` ON `partners` (`user_id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `IDX_6cf38bc4109df019cbd4990e26` ON `lecturers` (`user_id`)',
    );
    await queryRunner.query(
      'CREATE UNIQUE INDEX `IDX_fb3eff90b11bddf7285f9b4e28` ON `students` (`user_id`)',
    );
  }
}
