import { MigrationInterface, QueryRunner } from 'typeorm';

export class schoolClassAddProgramName1650354263354
  implements MigrationInterface
{
  name = 'schoolClassAddProgramName1650354263354';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD \`program_name\` varchar(255) NOT NULL DEFAULT ''`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP COLUMN \`program_name\``,
    );
  }
}
