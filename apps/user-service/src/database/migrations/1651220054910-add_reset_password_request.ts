import { MigrationInterface, QueryRunner } from 'typeorm';

export class addResetPasswordRequest1651220054910
  implements MigrationInterface
{
  name = 'addResetPasswordRequest1651220054910';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP FOREIGN KEY \`FK_4c4c8394e6fbbd92ab0b436a862\``,
    );
    await queryRunner.query(
      `CREATE TABLE \`reset_password_requests\` (\`id\` int UNSIGNED NOT NULL AUTO_INCREMENT, \`token\` varchar(255) NOT NULL, \`expired_at\` timestamp NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`user_id\` int UNSIGNED NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP COLUMN \`major_id\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`reset_password_requests\` ADD CONSTRAINT \`FK_ee8e27ae141766ec71b0088fdc3\` FOREIGN KEY (\`user_id\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query('DROP TABLE `majors`');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE IF NOT EXISTS `majors` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL COMMENT 'ex. Máy tính và Robot', PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
    await queryRunner.query(
      `ALTER TABLE \`reset_password_requests\` DROP FOREIGN KEY \`FK_ee8e27ae141766ec71b0088fdc3\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD \`major_id\` int UNSIGNED NOT NULL`,
    );
    await queryRunner.query(`DROP TABLE \`reset_password_requests\``);
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD CONSTRAINT \`FK_4c4c8394e6fbbd92ab0b436a862\` FOREIGN KEY (\`major_id\`) REFERENCES \`majors\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
