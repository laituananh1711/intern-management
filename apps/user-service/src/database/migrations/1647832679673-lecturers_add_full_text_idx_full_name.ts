import { MigrationInterface, QueryRunner } from 'typeorm';

export class lecturersAddFullTextIdxFullName1647832679673
  implements MigrationInterface
{
  name = 'lecturersAddFullTextIdxFullName1647832679673';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE FULLTEXT INDEX \`idx_lecturers_full_name\` ON \`lecturers\` (\`full_name\`)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX \`idx_lecturers_full_name\` ON \`lecturers\``,
    );
  }
}
