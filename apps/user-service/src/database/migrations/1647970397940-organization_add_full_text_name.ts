import { MigrationInterface, QueryRunner } from 'typeorm';

export class organizationAddFullTextName1647970397940
  implements MigrationInterface
{
  name = 'organizationAddFullTextName1647970397940';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE FULLTEXT INDEX \`idx_organizations_name\` ON \`organizations\` (\`name\`)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX \`idx_organizations_name\` ON \`organizations\``,
    );
  }
}
