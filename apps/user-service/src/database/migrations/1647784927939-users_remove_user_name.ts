import { MigrationInterface, QueryRunner } from 'typeorm';

export class usersRemoveUserName1647784927939 implements MigrationInterface {
  name = 'usersRemoveUserName1647784927939';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`users\` DROP COLUMN \`user_name\``);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`users\` ADD \`user_name\` varchar(255) NOT NULL`,
    );
  }
}
