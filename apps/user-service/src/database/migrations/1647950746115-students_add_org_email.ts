import { MigrationInterface, QueryRunner } from 'typeorm';

export class studentsAddOrgEmail1647950746115 implements MigrationInterface {
  name = 'studentsAddOrgEmail1647950746115';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP FOREIGN KEY \`FK_41278fa5aff0e9706aa39c282be\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD \`org_email\` varchar(255) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD UNIQUE INDEX \`IDX_bdd22c83d22219556d673e8299\` (\`name\`)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD CONSTRAINT \`FK_41278fa5aff0e9706aa39c282be\` FOREIGN KEY (\`school_class_id\`) REFERENCES \`school_classes\`(\`id\`) ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP FOREIGN KEY \`FK_41278fa5aff0e9706aa39c282be\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP INDEX \`IDX_bdd22c83d22219556d673e8299\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP COLUMN \`org_email\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD CONSTRAINT \`FK_41278fa5aff0e9706aa39c282be\` FOREIGN KEY (\`school_class_id\`) REFERENCES \`school_classes\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
