import { MigrationInterface, QueryRunner } from 'typeorm';

export class addSchoolClass1647884872845 implements MigrationInterface {
  name = 'addSchoolClass1647884872845';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP FOREIGN KEY \`FK_43252183688e683a449d3a719c8\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` CHANGE \`major_id\` \`school_class_id\` int UNSIGNED NULL`,
    );
    await queryRunner.query(
      `CREATE TABLE \`school_classes\` (\`id\` int UNSIGNED NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`major_id\` int UNSIGNED NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` ADD CONSTRAINT \`FK_4c4c8394e6fbbd92ab0b436a862\` FOREIGN KEY (\`major_id\`) REFERENCES \`majors\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD CONSTRAINT \`FK_41278fa5aff0e9706aa39c282be\` FOREIGN KEY (\`school_class_id\`) REFERENCES \`school_classes\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`students\` DROP FOREIGN KEY \`FK_41278fa5aff0e9706aa39c282be\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`school_classes\` DROP FOREIGN KEY \`FK_4c4c8394e6fbbd92ab0b436a862\``,
    );
    await queryRunner.query(`DROP TABLE \`school_classes\``);
    await queryRunner.query(
      `ALTER TABLE \`students\` CHANGE \`school_class_id\` \`major_id\` int UNSIGNED NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`students\` ADD CONSTRAINT \`FK_43252183688e683a449d3a719c8\` FOREIGN KEY (\`major_id\`) REFERENCES \`majors\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
