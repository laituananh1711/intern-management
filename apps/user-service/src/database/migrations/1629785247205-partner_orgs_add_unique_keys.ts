import { MigrationInterface, QueryRunner } from 'typeorm';

export class partnerOrgsAddUniqueKeys1629785247205
  implements MigrationInterface
{
  name = 'partnerOrgsAddUniqueKeys1629785247205';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'CREATE UNIQUE INDEX `IDX_6861dd09ea23534ac32d86237b` ON `partner_organizations` (`partner_id`, `organization_id`)',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'DROP INDEX `IDX_6861dd09ea23534ac32d86237b` ON `partner_organizations`',
    );
  }
}
