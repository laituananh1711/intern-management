/*
  This file is used to initialize DB connection in NestJS app
  and as a config file for TypeORM CLI.

  To run migrations, see Makefile.
*/

import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { config } from 'dotenv';
import { join } from 'path';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

config({
  path: 'apps/user-service/.env',
});

const typeOrmOptions: TypeOrmModuleOptions = {
  type: 'mysql',
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT ? parseInt(process.env.DB_PORT) : 3307,
  username: process.env.DB_USER || 'root',
  password: process.env.DB_PASSWORD || 'root',
  database: process.env.DB_NAME || 'user_service',
  namingStrategy: new SnakeNamingStrategy(), // transform TypeScript attribute to snake_case when querying SQL DB
  autoLoadEntities: true,
  logging: ['query', 'error'],
  entities: [join(__dirname, 'database/entities/**/*.ts')],
  migrations: [join(__dirname, 'database/migrations/**/*.ts')],
  subscribers: [join(__dirname, 'database/subscribers/**/*.ts')],
  cli: {
    entitiesDir: 'apps/user-service/src/database/entities',
    migrationsDir: 'apps/user-service/src/database/migrations',
    subscribersDir: 'apps/user-service/src/database/subscribers',
  },
};
export default typeOrmOptions;
