export const BCRYPT_SALT = 10;

// TODO: standardize this?
export enum AppRole {
  STUDENT = 'student',
  LECTURER = 'lecturer',
  PARTNER = 'partner',
  SYSTEM_ADMIN = 'system_admin',
  ORG_ADMIN = 'org_admin',
}

export enum AppRoleId {
  STUDENT = 1,
  LECTURER = 2,
  PARTNER = 3,
  SYSTEM_ADMIN = 4,
  ORG_ADMIN = 5,
}

export const KAFKA_PROVIDER = 'KAFKA';

export const VNU_EMAIL_DOMAIN = 'vnu.edu.vn';

export const events = {
  USER_CREATED: 'user.created',
  RESET_PASSWORD: 'user.password.reset',
  PASSWORD_CHANGED: 'user.password.changed',
  RESET_PASSWORD_COMPLETE: 'user.password.reset_complete',
};

export const PASSWORD_MIN_LENGTH = 8;

export const CV_FILE_KEY = 'file';
export const CV_MAX_SIZE_IN_BYTES = 10 * 1024 * 1024;

export const FILE_SEPARATOR = '/';
export const CV_FOLDER_NAME = 'cv';

export const PDF_MIME_TYPE = 'application/pdf';

export const CV_DOWNLOAD_URL_EXPIRY_SECONDS = 15 * 60; // 15 minutes
