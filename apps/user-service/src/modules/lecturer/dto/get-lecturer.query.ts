import { OffsetPagingQuery } from '@app/paginate';
import { Type } from 'class-transformer';
import {
  IsIn,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class LecturerFilterQuery {
  @IsInt()
  @IsOptional()
  organizationId?: number;
}

const allowedInclude = ['organization'];

export class LecturerQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => LecturerFilterQuery)
  filter?: LecturerFilterQuery;

  @IsIn(allowedInclude, { each: true })
  @IsOptional()
  include: string[] = [];
}
