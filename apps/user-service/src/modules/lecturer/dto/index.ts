import {
  IsEmail,
  IsInt,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export * from './get-lecturer.query';

export class UpdateLecturerInfoDto {
  @IsInt()
  lecturerId!: number;

  @IsString()
  @IsOptional()
  fullName?: string;

  @IsEmail()
  @IsOptional()
  personalEmail?: string;

  @IsPhoneNumber('VI')
  @IsOptional()
  phoneNumber?: string;
}
