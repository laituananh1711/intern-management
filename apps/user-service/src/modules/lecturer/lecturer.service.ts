import { paginate, PaginatedResults } from '@app/paginate';
import { isZeroValue } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, OrderByCondition, Repository } from 'typeorm';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { UpdateLecturerInfoDto } from './dto';
import { FindLecturerOptions } from './types';

@Injectable()
export class LecturerService {
  constructor(
    @InjectRepository(Lecturer) private lecturerRepo: Repository<Lecturer>,
  ) {}

  getLecturers({
    page,
    perPage,
    q,
    sort,
    filter,
    include,
  }: FindLecturerOptions): Promise<PaginatedResults<Lecturer>> {
    let queryBuilder = this.lecturerRepo
      .createQueryBuilder('l')
      .leftJoinAndSelect('l.user', 'user');

    if (filter != null) {
      if (!isZeroValue(filter.organizationId)) {
        queryBuilder = queryBuilder.where('l.organizationId = :orgId', {
          orgId: filter.organizationId,
        });
      }
    }

    if (!isZeroValue(q)) {
      queryBuilder = queryBuilder.andWhere(
        new Brackets((qb) =>
          qb
            .where(`MATCH (l.fullName) AGAINST (:q in natural language mode)`, {
              q,
            })
            .orWhere(`user.orgEmail like CONCAT('%', :orgEmail, '%')`, {
              orgEmail: q,
            })
            .orWhere(`l.personalEmail like CONCAT('%', :personalEmail, '%')`, {
              personalEmail: q,
            }),
        ),
      );
    }

    if (include != null) {
      if (include.organization)
        queryBuilder = queryBuilder.leftJoinAndSelect('l.organization', 'org');
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};

      if (sort.id != null) orderBy['l.id'] = sort.id;
      if (sort.name != null) orderBy['l.fullName'] = sort.name;

      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return paginate(queryBuilder, {
      page,
      perPage,
    });
  }

  getLecturerInfo(lecturerId: number): Promise<Lecturer> {
    return this.lecturerRepo.findOneOrFail(lecturerId);
  }

  async updateLecturerInfo({
    lecturerId,
    ...dto
  }: UpdateLecturerInfoDto): Promise<void> {
    await this.lecturerRepo.update(lecturerId, dto);
  }
}
