import { defaultPage, defaultPerPage, parseSortQuery } from '@app/paginate';
import {
  GetLecturerInfoRequest,
  GetLecturerInfoResponse,
  GetLecturersRequest,
  GetLecturersResponse,
  LecturerServiceController,
  LecturerServiceControllerMethods,
  UpdateLecturerInfoRequest,
  UpdateLecturerInfoResponse,
} from '@app/pb/user_service/service.pb';
import { deserializeAsync } from '@app/utils';
import { status } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { from, Observable } from 'rxjs';
import { UpdateLecturerInfoDto } from './dto';
import { LecturerQuery } from './dto/get-lecturer.query';
import { LecturerService } from './lecturer.service';

@Controller('lecturers')
@LecturerServiceControllerMethods()
export class LecturerController implements LecturerServiceController {
  constructor(private readonly lecturerService: LecturerService) {}
  getLecturerInfo(
    req: GetLecturerInfoRequest,
  ): Observable<GetLecturerInfoResponse> {
    const promise = async (): Promise<GetLecturerInfoResponse> => {
      const lecturer = await this.lecturerService
        .getLecturerInfo(req.lecturerId)
        .catch((e) => {
          throw new RpcException({
            code: status.NOT_FOUND,
            message: 'error no lecturer found: ' + e.message,
          });
        });

      return {
        code: status.OK,
        message: 'success',
        data: {
          fullName: lecturer.fullName!,
          id: lecturer.id!,
          orgEmail: lecturer.orgEmail!,
          personalEmail: lecturer.personalEmail ?? '',
          phoneNumber: lecturer.phoneNumber ?? '',
          userId: lecturer.userId!,
        },
      };
    };

    return from(promise());
  }

  updateLecturerInfo(
    req: UpdateLecturerInfoRequest,
  ): Observable<UpdateLecturerInfoResponse> {
    const promise = async (): Promise<UpdateLecturerInfoResponse> => {
      const dtoRaw: UpdateLecturerInfoDto = req;

      const { value: dto, err } = await deserializeAsync(
        UpdateLecturerInfoDto,
        dtoRaw,
      );
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: err.message,
        });
      }

      await this.lecturerService.updateLecturerInfo(dto);

      return {
        code: status.OK,
        message: 'success',
      };
    };

    return from(promise());
  }

  getLecturers({
    page,
    perPage,
    sort,
    filter,
    q,
    include,
  }: GetLecturersRequest): Observable<GetLecturersResponse> {
    const promise = async (): Promise<GetLecturersResponse> => {
      const dtoRaw: LecturerQuery = {
        page: defaultPage(page),
        perPage: defaultPerPage(perPage),
        sort,
        q,
        filter,
        include,
      };

      const { value: dto, err } = await deserializeAsync(LecturerQuery, dtoRaw);

      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: err.message,
        });
      }

      return this.getLecturersImpl(dto);
    };

    return from(promise());
  }

  private async getLecturersImpl({
    page,
    perPage,
    q,
    sort,
    filter,
    include,
  }: LecturerQuery): Promise<GetLecturersResponse> {
    const allowedCols = ['id', 'name'];

    const { items, meta } = await this.lecturerService.getLecturers({
      perPage,
      page,
      q: q,
      filter: {
        organizationId: filter?.organizationId,
      },
      sort: parseSortQuery(sort, { allowedCols }),
      include: {
        organization: include.indexOf('organization') !== -1,
      },
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        lecturers: items.map((l) => ({
          id: l.id ?? 0,
          userId: l.userId ?? 0,
          fullName: l.fullName ?? '',
          orgEmail: l.user?.orgEmail ?? '',
          phoneNumber: l.phoneNumber ?? '',
          personalEmail: l.personalEmail ?? '',
          organizationId: l.organizationId ?? 0,
          organization:
            l.organization == null
              ? undefined
              : {
                  id: l.organizationId!,
                  name: l.organization.name!,
                },
        })),
      },
    };
  }
}
