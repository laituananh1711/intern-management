import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { UserModule } from '../user/user.module';
import { LecturerController } from './lecturer.controller';
import { LecturerService } from './lecturer.service';

@Module({
  imports: [TypeOrmModule.forFeature([Lecturer]), UserModule],
  controllers: [LecturerController],
  providers: [LecturerService],
  exports: [TypeOrmModule],
})
export class LecturerModule {}
