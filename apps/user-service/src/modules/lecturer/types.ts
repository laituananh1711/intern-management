export type FindLecturerOptions = {
  perPage: number;
  page: number;
  filter?: LecturerFilterOptions;
  q?: string;
  sort?: LecturerSortOptions;
  include?: LecturerIncludeOptions;
};

export type LecturerFilterOptions = {
  organizationId?: number;
};

export type LecturerSortOptions = {
  name?: 'ASC' | 'DESC';
  id?: 'ASC' | 'DESC';
};

export type LecturerIncludeOptions = {
  organization: boolean;
};
