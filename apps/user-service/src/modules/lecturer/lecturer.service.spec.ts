import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { LecturerService } from './lecturer.service';

describe('LecturerService', () => {
  let service: LecturerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LecturerService,
        {
          provide: getRepositoryToken(Lecturer),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<LecturerService>(LecturerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
