import { ErrorResponse } from '@app/utils';
import { status } from '@grpc/grpc-js';
import {
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';

export const errMaxFileSizeExceeded = new BadRequestException(
  new ErrorResponse(status.INVALID_ARGUMENT, 'ERR_MAX_FILE_SIZE_EXCEEDED'),
);

export const errInvalidFileType = (
  requiredMimeType: string,
  actualMimeType: string,
) =>
  new BadRequestException(
    new ErrorResponse(
      status.INVALID_ARGUMENT,
      'ERR_INVALID_FILE_TYPE',
      undefined,
      `error invalid type received: ${actualMimeType}, need ${requiredMimeType}`,
    ),
  );

export const errFileNotFound = () =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_FILE_NOT_FOUND',
  });

export const errFailedToUploadFile = (e?: Error) =>
  new InternalServerErrorException(
    new ErrorResponse(
      status.INTERNAL,
      'ERR_FAILED_TO_UPLOAD_FILE',
      undefined,
      e?.message ?? '',
    ),
  );
