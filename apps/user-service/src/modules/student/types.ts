export type FindStudentSortOptions = {
  id?: 'ASC' | 'DESC';
  fullName?: 'ASC' | 'DESC';
};

export type FindStudentsFilterOptions = {
  schoolClassId?: number;
  organizationId?: number;
};

export type FindStudentsIncludeOptions = {
  organization?: boolean;
};

export type FindStudentsOptions = {
  page: number;
  perPage: number;
  q: string;
  sort: FindStudentSortOptions;
  filter?: FindStudentsFilterOptions;
  include?: FindStudentsIncludeOptions;
};
