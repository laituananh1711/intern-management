import { paginate, PaginatedResults } from '@app/paginate';
import { isZeroValue } from '@app/utils';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from 'minio';
import { MINIO_CONNECTION } from 'nestjs-minio';
import {
  Brackets,
  EntityNotFoundError,
  OrderByCondition,
  Repository,
} from 'typeorm';
import {
  CV_DOWNLOAD_URL_EXPIRY_SECONDS,
  CV_FOLDER_NAME,
  FILE_SEPARATOR,
} from '../../constants';
import { Student } from '../../database/entities/student.entity';
import { UpdateStudentInfoDto } from './dto';
import { errFileNotFound } from './errors';
import { FindStudentsOptions } from './types';

@Injectable()
export class StudentService {
  private userBucketName: string;

  constructor(
    @InjectRepository(Student) private studentRepo: Repository<Student>,
    @Inject(MINIO_CONNECTION) private readonly minioClient: Client,
    configService: ConfigService,
  ) {
    this.userBucketName = configService.get('USER_BUCKET_NAME', 'user-service');
  }

  findAll({
    page,
    perPage,
    sort,
    filter,
    q,
    include,
  }: FindStudentsOptions): Promise<PaginatedResults<Student>> {
    let queryBuilder = this.studentRepo
      .createQueryBuilder('s')
      .select([
        's.id',
        's.fullName',
        's.studentIdNumber',
        's.personalEmail',
        's.phoneNumber',
        's.resumeUrl',
        's.orgEmail',
        's.createdAt',
        's.updatedAt',
        's.organizationId',
        's.userId',
      ])
      .leftJoinAndSelect('s.schoolClass', 'cls');

    if (!isZeroValue(q)) {
      queryBuilder = queryBuilder.where(
        new Brackets((qb) =>
          qb
            .orWhere(
              'MATCH(s.fullName) AGAINST (:q in natural language mode)',
              { q },
            )
            .orWhere(`s.orgEmail like CONCAT('%', :email, '%')`, {
              email: q,
            }),
        ),
      );
    }

    if (include != null) {
      if (include.organization) {
        queryBuilder = queryBuilder.leftJoinAndSelect('s.organization', 'o');
      }
    }

    if (filter != null) {
      if (!isZeroValue(filter.organizationId)) {
        queryBuilder = queryBuilder.andWhere('s.organizationId = :orgId', {
          orgId: filter.organizationId,
        });
      }

      if (!isZeroValue(filter.schoolClassId)) {
        queryBuilder = queryBuilder.andWhere('s.schoolClassId = :classId', {
          classId: filter.schoolClassId,
        });
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.id != null) orderBy['s.id'] = sort.id;
      if (sort.fullName != null) orderBy['s.fullName'] = sort.fullName;

      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return paginate(queryBuilder, { page, perPage });
  }

  getStudentInfo(studentId: number): Promise<Student> {
    return this.studentRepo.findOneOrFail(studentId);
  }

  async updateStudentInfo(
    studentId: number,
    dto: UpdateStudentInfoDto,
  ): Promise<void> {
    await this.studentRepo.update(studentId, dto);
  }

  async uploadCV(
    studentId: number,
    cvFile: Express.Multer.File,
  ): Promise<void> {
    const objectName = getCvFileObjectName(studentId, cvFile.originalname);
    await this.studentRepo.manager.transaction(async (manager) => {
      await this.minioClient.putObject(
        this.userBucketName,
        objectName,
        cvFile.buffer,
      );

      await manager.update(Student, studentId, {
        resumeUrl: objectName,
      });
    });
  }

  async downloadCV(studentId: number): Promise<string> {
    const { resumeUrl } = await this.studentRepo
      .findOneOrFail({
        select: ['id', 'resumeUrl'], //wtf moment: we have to select a field in order to filter by it?
        where: {
          id: studentId,
        },
      })
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errFileNotFound();
        }
        throw e;
      });

    if (resumeUrl == null) {
      throw errFileNotFound();
    }

    return await this.minioClient.presignedGetObject(
      this.userBucketName,
      resumeUrl,
      CV_DOWNLOAD_URL_EXPIRY_SECONDS,
    );
  }
}

const getCvFileObjectName = (studentId: number, fileName: string): string => {
  return [CV_FOLDER_NAME, studentId.toString(), fileName].join(FILE_SEPARATOR);
};
