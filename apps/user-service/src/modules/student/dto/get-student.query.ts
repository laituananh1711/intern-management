import { OffsetPagingQuery } from '@app/paginate';
import { Type } from 'class-transformer';
import {
  IsIn,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

const allowInclude = ['organization'];

export class StudentFilterQuery {
  @IsInt()
  @IsOptional()
  organizationId?: number;

  @IsInt()
  @IsOptional()
  schoolClassId?: number;
}

export class StudentQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptional()
  q = '';

  @IsOptional()
  @ValidateNested()
  @Type(() => StudentFilterQuery)
  filter?: StudentFilterQuery;

  @IsIn(allowInclude, { each: true })
  @IsOptional()
  include: string[] = [];
}
