import {
  IsEmail,
  IsInt,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export * from './get-student.query';

export class UpdateStudentInfoDto {
  @IsString()
  @IsOptional()
  fullName?: string;

  @IsInt()
  @IsOptional()
  schoolClassId?: number;

  @IsEmail()
  @IsOptional()
  personalEmail?: string;

  @IsPhoneNumber('VI')
  @IsOptional()
  phoneNumber?: string;
}
