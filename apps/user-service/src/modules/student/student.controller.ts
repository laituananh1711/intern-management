import {
  DEFAULT_PAGE_NUM,
  DEFAULT_PER_PAGE,
  parseSortQuery,
} from '@app/paginate';
import { toTimestampNullable } from '@app/pb';
import {
  DownloadCvRequest,
  DownloadCvResponse,
  GetStudentInfoRequest,
  GetStudentInfoResponse,
  GetStudentsRequest,
  GetStudentsResponse,
  StudentServiceController,
  StudentServiceControllerMethods,
  UpdateStudentInfoRequest,
  UpdateStudentInfoResponse,
} from '@app/pb/user_service/service.pb';
import { deserializeAsync, HttpResponse } from '@app/utils';
import { status } from '@grpc/grpc-js';
import {
  Controller,
  Get,
  Inject,
  Logger,
  Param,
  ParseIntPipe,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RpcException } from '@nestjs/microservices';
import { FileInterceptor } from '@nestjs/platform-express';
import { Client } from 'minio';
import { MINIO_CONNECTION } from 'nestjs-minio';
import { from, Observable } from 'rxjs';
import {
  CV_FILE_KEY,
  CV_MAX_SIZE_IN_BYTES,
  PDF_MIME_TYPE,
} from '../../constants';
import { UpdateStudentInfoDto } from './dto';
import { StudentQuery } from './dto/get-student.query';
import { errFailedToUploadFile, errInvalidFileType } from './errors';
import { StudentService } from './student.service';

@Controller('students')
@StudentServiceControllerMethods()
export class StudentController implements StudentServiceController {
  private userBucketName: string;

  constructor(
    private readonly studentService: StudentService,
    private readonly configService: ConfigService,
    @Inject(MINIO_CONNECTION) private readonly minioClient: Client,
  ) {
    this.userBucketName = configService.get('USER_BUCKET_NAME', 'user-service');
  }

  getStudents({
    filter,
    page,
    perPage,
    q,
    sort,
    include,
  }: GetStudentsRequest): Observable<GetStudentsResponse> {
    const promise = async (): Promise<GetStudentsResponse> => {
      const dtoRaw: StudentQuery = {
        filter,
        page: page <= 0 ? DEFAULT_PAGE_NUM : page,
        perPage: perPage <= 0 ? DEFAULT_PER_PAGE : perPage,
        q,
        sort,
        include,
      };

      const { value: dto, err } = await deserializeAsync(StudentQuery, dtoRaw);
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: err.message,
        });
      }

      return this.getStudentsImpl(dto);
    };

    return from(promise());
  }

  async getStudentsImpl({
    sort,
    include,
    ...query
  }: StudentQuery): Promise<GetStudentsResponse> {
    const allowedCols = ['id', 'fullName'];

    const { items, meta } = await this.studentService
      .findAll({
        ...query,
        sort: parseSortQuery(sort, { allowedCols }),
        include: {
          organization: include.indexOf('organization') !== -1,
        },
      })
      .catch((e) => {
        throw new RpcException({
          code: status.UNKNOWN,
          message: 'error getting students: ' + e.message,
        });
      });

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        students: items.map((s) => ({
          createdAt: toTimestampNullable(s.createdAt),
          fullName: s.fullName ?? '',
          id: s.id ?? 0,
          orgEmail: s.orgEmail ?? '',
          organizationId: s.organizationId ?? 0,
          personalEmail: s.personalEmail ?? '',
          phoneNumber: s.phoneNumber ?? '',
          resumeUrl: s.resumeUrl ?? '',
          schoolClass:
            s.schoolClass == null
              ? undefined
              : {
                  id: s.schoolClass.id ?? 0,
                  name: s.schoolClass.name ?? '',
                },
          studentIdNumber: s.studentIdNumber ?? '',
          updatedAt: toTimestampNullable(s.updatedAt),
          userId: s.userId ?? 0,
          organization:
            s.organization == null
              ? undefined
              : {
                  id: s.organization.id!,
                  name: s.organization.name!,
                },
        })),
      },
    };
  }

  getStudentInfo(
    request: GetStudentInfoRequest,
  ): Observable<GetStudentInfoResponse> {
    const promise = async (): Promise<GetStudentInfoResponse> => {
      const student = await this.studentService
        .getStudentInfo(request.studentId)
        .catch((err) => {
          Logger.error('error while getting student info:', err);

          throw new RpcException({
            code: status.NOT_FOUND,
            message: 'cannot find student',
          });
        });

      return {
        code: status.OK,
        data: {
          fullName: student.fullName ?? '',
          id: student.id ?? 0,
          orgEmail: student.orgEmail ?? '',
          personalEmail: student.personalEmail ?? '',
          phoneNumber: student.phoneNumber ?? '',
          schoolClassId: student.schoolClassId ?? 0,
          userId: student.userId ?? 0,
          studentIdNumber: student.studentIdNumber ?? '',
        },
        message: 'success',
      };
    };

    return from(promise());
  }

  updateStudentInfo({
    studentId,
    ...req
  }: UpdateStudentInfoRequest): Observable<UpdateStudentInfoResponse> {
    const promise = async (): Promise<UpdateStudentInfoResponse> => {
      const dtoRaw: UpdateStudentInfoDto = req;

      const { value: dto, err } = await deserializeAsync(
        UpdateStudentInfoDto,
        dtoRaw,
      );
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: err.message,
        });
      }

      await this.studentService.updateStudentInfo(studentId, dto);
      return {
        code: status.OK,
        message: 'success',
      };
    };

    return from(promise());
  }

  @Post(':studentId/cv/upload')
  @UseInterceptors(
    FileInterceptor(CV_FILE_KEY, {
      limits: {
        fileSize: CV_MAX_SIZE_IN_BYTES,
      },
    }),
  )
  async uploadCV(
    @UploadedFile() cvFile: Express.Multer.File,
    @Param('studentId', ParseIntPipe) studentId: number,
  ): Promise<{ code: number; message: string }> {
    if (cvFile.mimetype !== PDF_MIME_TYPE) {
      throw errInvalidFileType(PDF_MIME_TYPE, cvFile.mimetype);
    }

    await this.studentService.uploadCV(studentId, cvFile).catch((e) => {
      throw errFailedToUploadFile(e);
    });

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async downloadCv({
    studentId,
  }: DownloadCvRequest): Promise<DownloadCvResponse> {
    const presignedUrl = await this.studentService.downloadCV(studentId);

    return {
      code: status.OK,
      message: 'success',
      data: {
        downloadUrl: presignedUrl,
      },
    };
  }
}
