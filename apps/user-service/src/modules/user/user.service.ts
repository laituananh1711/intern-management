import { PaginatedResults } from '@app/paginate';
import { deserializeAsync, generatePassword, getGravatarURL } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { hashSync } from 'bcrypt';
import { In, Repository } from 'typeorm';
import { AppRoleId, BCRYPT_SALT, events } from '../../constants';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Partner } from '../../database/entities/partner.entity';
import { Role } from '../../database/entities/role.entity';
import { Student } from '../../database/entities/student.entity';
import { User } from '../../database/entities/user.entity';
import {
  AssignAccountPartnerDto,
  CreateLecturerUserDto,
  CreateOrgAdminUserDto,
  CreatePartnerUserDto,
  CreateStudentUserDto,
} from './dto/create-user.dto';
import {
  FindUserOptions,
  ImportStudentRecord,
  UserCreatedEvent,
} from './types';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(
    private userRepository: UserRepository,
    @InjectRepository(Role) private roleRepo: Repository<Role>,
    private eventEmitter: EventEmitter2,
  ) {}

  findAll(pagingOptions: FindUserOptions): Promise<PaginatedResults<User>> {
    return this.userRepository.getAllUsers(pagingOptions);
  }

  /**
   * Create a user with role LECTURER.
   * @param dto CreateLecturerUserDto
   * @returns new lecturer
   */
  async createLecturerUser(dto: CreateLecturerUserDto): Promise<Lecturer> {
    const generatedPassword = generatePassword();

    return await this.userRepository
      .createLecturerUser(
        new User({
          password: hashSync(generatedPassword, BCRYPT_SALT),
          roleId: AppRoleId.LECTURER,
          orgEmail: dto.orgEmail,
          avatarUrl: getGravatarURL(dto.orgEmail),
          isEmailVerified: true,
        }),
        new Lecturer({
          fullName: dto.fullName,
          phoneNumber: dto.phoneNumber,
          organizationId: dto.organizationId,
          personalEmail: dto.personalEmail,
          orgEmail: dto.orgEmail,
        }),
      )
      .then((res) => {
        this.eventEmitter.emit(
          events.USER_CREATED,
          new UserCreatedEvent(dto.orgEmail, dto.fullName, generatedPassword),
        );
        return res;
      });
  }

  /**
   * Create a new user with role PARTNER.
   * @param dto
   */
  async createPartnerUser(dto: CreatePartnerUserDto): Promise<Partner> {
    const generatedPassword = generatePassword();
    return await this.userRepository
      .createPartnerUser(
        new User({
          password: hashSync(generatedPassword, BCRYPT_SALT),
          roleId: AppRoleId.PARTNER,
          orgEmail: dto.orgEmail,
          avatarUrl:
            dto.logoUrl === '' ? getGravatarURL(dto.orgEmail) : dto.logoUrl,
          isEmailVerified: true, // user created using admin API will have verified email by default
        }),
        new Partner({
          description: dto.description,
          homepageUrl: dto.homepageUrl,
          logoUrl: dto.logoUrl,
          name: dto.name,
          email: dto.orgEmail,
        }),
      )
      .then((res) => {
        this.eventEmitter.emit(
          events.USER_CREATED,
          new UserCreatedEvent(dto.orgEmail, dto.name, generatedPassword),
        );
        return res;
      });
  }

  async createStudentUser(dto: CreateStudentUserDto): Promise<Student> {
    const generatedPassword = generatePassword();
    return await this.userRepository
      .createStudentUser(
        new User({
          password: hashSync(generatedPassword, BCRYPT_SALT),
          roleId: AppRoleId.STUDENT,
          orgEmail: dto.orgEmail,
          avatarUrl: getGravatarURL(dto.orgEmail),
          isEmailVerified: true,
        }),
        new Student({
          fullName: dto.fullName,
          studentIdNumber: dto.studentIdNumber,
          schoolClassId: dto.schoolClassId,
          organizationId: dto.organizationId,
          orgEmail: dto.orgEmail,
        }),
      )
      .then((res) => {
        this.eventEmitter.emit(
          events.USER_CREATED,
          new UserCreatedEvent(dto.orgEmail, dto.fullName, generatedPassword),
        );
        return res;
      });
  }

  async createOrgAdminUser(dto: CreateOrgAdminUserDto): Promise<User> {
    return await this.userRepository.createOrgAdminUser(
      new User({
        password: hashSync(generatePassword(), BCRYPT_SALT),
        roleId: AppRoleId.ORG_ADMIN,
        orgEmail: dto.orgEmail,
        avatarUrl: getGravatarURL(dto.orgEmail),
        isEmailVerified: true,
      }),
      dto.organizationId,
    );
  }

  async assignAccountPartner(dto: AssignAccountPartnerDto): Promise<User> {
    return await this.userRepository.manager.transaction(async (manager) => {
      const user = await manager.save<User>(
        new User({
          password: hashSync(generatePassword(), BCRYPT_SALT),
          roleId: AppRoleId.PARTNER,
          orgEmail: dto.orgEmail,
          avatarUrl: getGravatarURL(dto.orgEmail),
          isEmailVerified: true,
        }),
      );

      await manager.update(Partner, dto.partnerId, {
        userId: user.id,
      });

      return user;
    });
  }

  /**
   * Create student users in batch. Return a list of invalid user.
   */
  async batchCreateStudentUsers(
    records: ImportStudentRecord[],
    organizationId: number,
  ): Promise<{ index: number; message: string }[]> {
    // validate records
    const errors = [];
    const validatedRecords: Student[] = [];
    for (let i = 0; i < records.length; i++) {
      const record = records[i];
      const { value: validatedRecord, err } = await deserializeAsync(
        ImportStudentRecord,
        record,
      );
      if (err != null) {
        errors.push({ index: i, message: err.message });
        continue;
      }
      const { fullName, orgEmail, studentIdNumber, phoneNumber } =
        validatedRecord;
      validatedRecords.push(
        new Student({
          fullName: fullName,
          user: new User({
            orgEmail,
            roleId: AppRoleId.STUDENT,
            avatarUrl: getGravatarURL(orgEmail),
            password: generatePassword(), // TODO: leave it as null?
          }),
          phoneNumber,
          studentIdNumber,
          orgEmail,
          organizationId,
        }),
      );
    }

    if (errors.length > 0) return errors;

    await this.userRepository.manager.transaction(async (manager) => {
      // remove entities with email already existed.
      // this is a workaround since we couldn't INSERT ON DUPLICATE KEY
      // with TypeORM `.save()` function.
      //
      // this is not a complete workaround because we have to modify the code for each additional
      // unique keys.
      const userEmailsPromise = manager
        .find(User, {
          select: ['orgEmail'],
          where: {
            orgEmail: In(validatedRecords.map((r) => r.orgEmail)),
          },
        })
        .then((res) => res.map((r) => r.orgEmail));

      const studentEmailsPromise = manager
        .find(Student, {
          select: ['orgEmail'],
          where: {
            orgEmail: In(validatedRecords.map((r) => r.orgEmail)),
          },
        })
        .then((res) => res.map((r) => r.orgEmail));

      const [userEmails, studentEmails] = await Promise.all([
        userEmailsPromise,
        studentEmailsPromise,
      ]);
      const existingEmailSet = new Set([...userEmails, ...studentEmails]);
      const filteredRecords = validatedRecords.filter(
        (v) => !existingEmailSet.has(v.orgEmail),
      );

      return manager.save(filteredRecords);
    });

    return [];
  }

  getAllRoles(): Promise<Role[]> {
    return this.roleRepo.find();
  }
}
