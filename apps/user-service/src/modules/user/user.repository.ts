import { paginate, PaginatedResults } from '@app/paginate';
import { EntityRepository, OrderByCondition, Repository } from 'typeorm';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Organization } from '../../database/entities/organization.entity';
import { Partner } from '../../database/entities/partner.entity';
import { Student } from '../../database/entities/student.entity';
import { User } from '../../database/entities/user.entity';
import { FindUserOptions } from './types';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  getAllUsers({
    page,
    perPage,
    filter,
    q,
    sort,
  }: FindUserOptions): Promise<PaginatedResults<User>> {
    let queryBuilder = this.createQueryBuilder('u')
      .select([
        'u.id',
        'u.orgEmail',
        'u.lastLoginAt',
        'u.createdAt',
        'u.avatarUrl',
        'u.isEmailVerified',
        'u.roleId',
      ])
      .innerJoinAndSelect('u.role', 'roles');

    if (filter != null) {
      if (filter['role'] != null && filter['role'] !== '') {
        queryBuilder = queryBuilder.andWhere('roles.name = :roleName', {
          roleName: filter['role'],
        });
      }
    }

    if (q != null && q !== '') {
      queryBuilder = queryBuilder.andWhere('u.orgEmail like :orgEmail', {
        orgEmail: `%${q}%`,
      });
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};
      if (sort.id != null) orderBy['u.id'] = sort.id;
      if (sort.lastLoginAt != null) orderBy['u.lastLoginAt'] = sort.lastLoginAt;
      if (sort.orgEmail != null) orderBy['u.orgEmail'] = sort.orgEmail;

      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return paginate(queryBuilder, {
      page,
      perPage,
    });
  }

  /**
   * Create a new user and student in a transaction
   * @param user new user to be created
   * @param student new student to be created
   * @returns newly-created user
   */
  registerStudentUser(user: User, student: Student): Promise<User> {
    return this.createUserAndStudent(user, student).then(() => {
      delete user.password;
      return user;
    });
  }

  createStudentUser(user: User, student: Student): Promise<Student> {
    return this.createUserAndStudent(user, student).then(() => {
      return student;
    });
  }

  private createUserAndStudent(user: User, student: Student) {
    return this.manager.transaction(async (manager) => {
      user = await manager.save<User>(user);

      student.userId = user.id;
      await manager.save<Student>(student);
    });
  }

  createLecturerUser(user: User, lecturer: Lecturer): Promise<Lecturer> {
    return this.manager
      .transaction(async (manager) => {
        user = await manager.save<User>(user);

        lecturer.userId = user.id;
        await manager.save<Lecturer>(lecturer);
      })
      .then(() => lecturer);
  }

  createPartnerUser(user: User, partner: Partner): Promise<Partner> {
    return this.manager
      .transaction(async (manager) => {
        user = await manager.save<User>(user);

        partner.userId = user.id;
        await manager.save<Partner>(partner);
      })
      .then(() => partner);
  }

  createOrgAdminUser(user: User, organizationId: number): Promise<User> {
    return this.manager.transaction(async (manager) => {
      user = await manager.save<User>(user);

      await manager.update(Organization, organizationId, {
        adminId: user.id,
      });

      return user;
    });
  }

  async updateLastLoginAt(userId: number): Promise<void> {
    this.update(userId, {
      lastLoginAt: new Date(),
    });
  }
}
