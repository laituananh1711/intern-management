import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '../../database/entities/role.entity';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';

/**
 * This module is used by admin to create and remove user.
 * This is not meant to be used to login/register user. That
 * logic is handled in the Auth Module.
 */
@Module({
  imports: [TypeOrmModule.forFeature([UserRepository, Role])],
  controllers: [UserController],
  providers: [UserService],
  exports: [TypeOrmModule, UserService],
})
export class UserModule {}
