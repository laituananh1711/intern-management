import { EventEmitter2 } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AppRole, AppRoleId } from '../../constants';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Role } from '../../database/entities/role.entity';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';

const mockUserRepo = {
  save: jest.fn(),
  find: jest.fn(),
  findAndCount: jest.fn(),
  createLecturerUser: jest.fn(),
};

const mockRoleRepo = {
  findOneOrFail: jest.fn(),
  find: jest.fn(),
};

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(UserRepository),
          useValue: mockUserRepo,
        },
        {
          provide: getRepositoryToken(Role),
          useValue: mockRoleRepo,
        },
        {
          provide: EventEmitter2,
          useValue: {
            emit: (event: any, content: any) => {},
          },
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    // NOTE: cannot test because `paginate()` use runtime checks.
  });

  describe('createLecturerUser', () => {
    mockRoleRepo.findOneOrFail.mockResolvedValue(
      new Role({
        id: AppRoleId.LECTURER,
        name: AppRole.LECTURER,
      }),
    );

    it('should create new lecturer user', async () => {
      // NOTE: this function doesn't have any transform logic, so this's redundant.
      const expectedLecturer = new Lecturer({
        id: 1,
        fullName: 'Test',
      });
      mockUserRepo.createLecturerUser.mockResolvedValue(expectedLecturer);

      const result = await service.createLecturerUser({
        fullName: 'Test',
        orgEmail: 'test@email.com',
        phoneNumber: '099',
        organizationId: 1,
        personalEmail: '',
      });

      expect(result).toEqual(expectedLecturer);
    });
  });
});
