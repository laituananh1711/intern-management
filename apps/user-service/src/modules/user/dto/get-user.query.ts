import { OffsetPagingQuery } from '@app/paginate';
import { Type } from 'class-transformer';
import { IsOptional, IsString, ValidateNested } from 'class-validator';

export class UserFilterQuery {
  @IsString()
  @IsOptional()
  role?: string;
}

export class UserQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort?: string[];

  @IsString()
  @IsOptional()
  q?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => UserFilterQuery)
  filter?: UserFilterQuery;
}
