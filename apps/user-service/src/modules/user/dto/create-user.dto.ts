import { IsOptionalOrEmpty } from '@app/utils';
import {
  IsEmail,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUrl,
} from 'class-validator';

/**
 * DTO for creating user. Password should be sent to their email.
 * TODO: implement mail service.
 */
export class CreateUserDto {
  @IsEmail()
  orgEmail!: string;

  constructor(obj: Partial<CreateUserDto>) {
    Object.assign(this, obj);
  }
}

export class CreateLecturerUserDto extends CreateUserDto {
  @IsString()
  fullName!: string;

  @IsPhoneNumber('VI')
  phoneNumber!: string;

  @IsNumber()
  organizationId!: number;

  @IsEmail()
  @IsOptionalOrEmpty()
  personalEmail = '';
}

export class CreatePartnerUserDto extends CreateUserDto {
  @IsString()
  name!: string;

  @IsUrl()
  homepageUrl!: string;

  @IsUrl()
  @IsOptionalOrEmpty()
  logoUrl = '';

  @IsString()
  @IsOptional()
  description?: string;

  // login email is also org email in users table.
  @IsEmail()
  loginEmail!: string;
}

export class CreateStudentUserDto extends CreateUserDto {
  @IsString()
  fullName!: string;

  @IsString()
  studentIdNumber!: string;

  @IsNumber()
  @IsOptional()
  schoolClassId?: number;

  @IsNumber()
  organizationId!: number;
}

export class CreateOrgAdminUserDto extends CreateUserDto {
  @IsNumber()
  organizationId!: number;
}

export class AssignAccountPartnerDto extends CreateUserDto {
  @IsNumber()
  partnerId!: number;
}
