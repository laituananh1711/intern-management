import {
  DEFAULT_PAGE_NUM,
  DEFAULT_PER_PAGE,
  parseSortQuery,
} from '@app/paginate';
import { toTimestamp } from '@app/pb';
import {
  AssignAccountPartnerRequest,
  AssignAccountPartnerResponse,
  BatchCreateStudentUsersRequest,
  BatchCreateStudentUsersResponse,
  CreateLecturerUserRequest,
  CreateLecturerUserResponse,
  CreateOrgAdminUserRequest,
  CreateOrgAdminUserResponse,
  CreatePartnerUserRequest,
  CreatePartnerUserResponse,
  CreateStudentUserRequest,
  CreateStudentUserResponse,
  GetUsersRequest,
  GetUsersResponse,
  UserServiceController,
  UserServiceControllerMethods,
} from '@app/pb/user_service/service.pb';
import { deserializeAsync } from '@app/utils';
import { Metadata, status } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { from, Observable } from 'rxjs';
import { QueryFailedError } from 'typeorm';
import { VNU_EMAIL_DOMAIN } from '../../constants';
import {
  AssignAccountPartnerDto,
  CreateLecturerUserDto,
  CreateOrgAdminUserDto,
  CreatePartnerUserDto,
  CreateStudentUserDto,
} from './dto/create-user.dto';
import { UserQuery } from './dto/get-user.query';
import { MysqlDuplicateEntryCode } from './types';
import { UserService } from './user.service';

@Controller('users')
@UserServiceControllerMethods()
export class UserController implements UserServiceController {
  constructor(private readonly userService: UserService) {}

  createLecturerUser(
    request: CreateLecturerUserRequest,
  ): Observable<CreateLecturerUserResponse> {
    const promise = async (): Promise<CreateLecturerUserResponse> => {
      const createUserDtoRaw: CreateLecturerUserDto = {
        fullName: request.fullName,
        orgEmail: request.orgEmail,
        organizationId: request.organizationId,
        personalEmail: request.personalEmail,
        phoneNumber: request.phoneNumber,
      };

      const { value: dto, err } = await deserializeAsync(
        CreateLecturerUserDto,
        createUserDtoRaw,
      );
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: err.message,
        });
      }

      // TODO: judge the effectiveness of this approach
      const lecturer = await this.userService
        .createLecturerUser(dto)
        .catch((err) => {
          throw new RpcException({
            code: status.INVALID_ARGUMENT,
            message: 'Error while creating lecturer user',
          });
        });

      return {
        code: status.OK,
        message: 'success',
        data: {
          lecturerId: lecturer.id!,
          userId: lecturer.userId!,
        },
      };
    };

    return from(promise());
  }

  createPartnerUser(
    req: CreatePartnerUserRequest,
  ): Observable<CreatePartnerUserResponse> {
    const promise = async (): Promise<CreatePartnerUserResponse> => {
      const createUserDtoRaw: CreatePartnerUserDto = {
        homepageUrl: req.homepageUrl,
        loginEmail: req.loginEmail,
        logoUrl: req.logoUrl,
        name: req.name,
        orgEmail: req.loginEmail,
        description: req.description,
      };

      const { value: dto, err } = await deserializeAsync(
        CreatePartnerUserDto,
        createUserDtoRaw,
      );

      if (err != null) {
        throw new RpcException(err);
      }

      const partner = await this.userService
        .createPartnerUser(dto)
        .catch((err: QueryFailedError) => {
          if (err.driverError.code === MysqlDuplicateEntryCode) {
            throw new RpcException({
              code: status.ALREADY_EXISTS,
              message: 'error creating new partner: email already existed',
            });
          }
          throw new RpcException(err);
        });

      return {
        code: status.OK,
        message: 'success',
        data: {
          partnerId: partner.id!,
          userId: partner.userId!,
        },
      };
    };

    return from(promise());
  }

  createStudentUser(
    req: CreateStudentUserRequest,
  ): Observable<CreateStudentUserResponse> {
    const promise = async (): Promise<CreateStudentUserResponse> => {
      const dtoRaw: CreateStudentUserDto = {
        fullName: req.fullName,
        orgEmail: req.loginEmail,
        organizationId: req.organizationId,
        schoolClassId: req.schoolClassId,
        studentIdNumber: req.studentIdNumber,
      };

      const { value: dto, err } = await deserializeAsync(
        CreateStudentUserDto,
        dtoRaw,
      );

      if (err != null) {
        throw new RpcException(err);
      }

      if (!isOrgEmailValid(dto.orgEmail)) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'email must be of vnu.edu.vn domain',
        });
      }

      const student = await this.userService
        .createStudentUser(dto)
        .catch((err: QueryFailedError) => {
          if (err.driverError.code === MysqlDuplicateEntryCode) {
            throw new RpcException({
              code: status.ALREADY_EXISTS,
              message: 'error creating new student: email already existed',
            });
          }
          throw new RpcException({
            code: status.UNKNOWN,
            message: err.message,
          });
        });

      return {
        code: status.OK,
        message: 'success',
        data: {
          studentId: student.id!,
          userId: student.userId!,
        },
      };
    };

    return from(promise());
  }

  createOrgAdminUser(
    req: CreateOrgAdminUserRequest,
  ): Observable<CreateOrgAdminUserResponse> {
    const promise = async (): Promise<CreateOrgAdminUserResponse> => {
      const dtoRaw: CreateOrgAdminUserDto = {
        organizationId: req.organizationId,
        orgEmail: req.loginEmail,
      };

      const { value: dto, err } = await deserializeAsync(
        CreateOrgAdminUserDto,
        dtoRaw,
      );

      if (err != null) {
        throw new RpcException(err);
      }

      const user = await this.userService.createOrgAdminUser(dto);

      return {
        code: status.OK,
        message: 'success',
        data: {
          userId: user.id!,
        },
      };
    };

    return from(promise());
  }

  assignAccountPartner(
    req: AssignAccountPartnerRequest,
  ): Observable<AssignAccountPartnerResponse> {
    const promise = async (): Promise<AssignAccountPartnerResponse> => {
      const dtoRaw: AssignAccountPartnerDto = {
        orgEmail: req.loginEmail,
        partnerId: req.partnerId,
      };

      const { value: dto, err } = await deserializeAsync(
        AssignAccountPartnerDto,
        dtoRaw,
      );

      if (err != null) {
        throw new RpcException(err);
      }

      const user = await this.userService.assignAccountPartner(dto);

      return {
        code: status.OK,
        message: 'success',
        data: {
          userId: user.id!,
        },
      };
    };

    return from(promise());
  }

  getUsers(request: GetUsersRequest): Observable<GetUsersResponse> {
    const promise = async () => {
      const userQueryRaw: UserQuery = {
        page: request.page === 0 ? DEFAULT_PAGE_NUM : request.page,
        perPage: request.perPage === 0 ? DEFAULT_PER_PAGE : request.perPage,
        filter: request.filter,
        q: request.q,
        sort: request.sort,
      };

      const { value: userQuery, err } = await deserializeAsync(
        UserQuery,
        userQueryRaw,
      );
      if (err != null) {
        throw new RpcException(err);
      }

      return this.getUsersImpl(userQuery);
    };

    // workaround: https://www.notion.so/laituananh/Getting-back-on-track-37700b37661c4e5e916f1b8f5cf253ef#9d1aabd4c13a4370be72141b28cad99d
    return from(promise());
  }

  async getUsersImpl({
    page,
    perPage,
    q,
    sort,
    filter,
  }: UserQuery): Promise<GetUsersResponse> {
    const allowedSortCols = ['id', 'lastLoginAt', 'orgEmail'];

    const { items, meta } = await this.userService.findAll({
      page,
      perPage,
      sort: parseSortQuery(sort ?? [], { allowedCols: allowedSortCols }),
      q,
      filter: {
        role: filter?.role,
      },
    });

    return {
      code: status.OK,
      data: {
        users: items.map((u) => ({
          avatarUrl: u.avatarUrl ?? '',
          id: u.id ?? 0,
          isEmailVerified: u.isEmailVerified ?? false,
          roleId: u.roleId ?? 0,
          role: u.role?.name ?? '',
          orgEmail: u.orgEmail ?? '',
          lastLoginAt:
            u.lastLoginAt != null ? toTimestamp(u.lastLoginAt) : undefined,
          createdAt: u.createdAt != null ? toTimestamp(u.createdAt) : undefined,
          updatedAt: u.updatedAt != null ? toTimestamp(u.updatedAt) : undefined,
        })),
        meta,
      },
      message: 'success',
    };
  }

  /**
   * Batch create student users. It first validates each record and insert invalid records to `failedRecords` array. After that, it tries to insert valid records to db.
   */
  async batchCreateStudentUsers({
    students,
    organizationId,
  }: BatchCreateStudentUsersRequest): Promise<BatchCreateStudentUsersResponse> {
    const failedRecords = await this.userService.batchCreateStudentUsers(
      students,
      organizationId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        failedRecords,
      },
    };
  }
}

function isOrgEmailValid(email: string): boolean {
  const emailParts = email.split('@');

  if (emailParts.length !== 2) {
    return false;
  }

  const domain = emailParts[1];

  if (domain !== VNU_EMAIL_DOMAIN) {
    return false;
  }

  return true;
}
