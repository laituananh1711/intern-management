import {
  IsEmail,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Length,
  MinLength,
} from 'class-validator';

export type FindUserOptions = {
  perPage: number;
  page: number;
  filter?: UserFilterOptions;
  q?: string;
  sort?: UserSortOptions;
};

export type UserFilterOptions = {
  role?: string;
  organizationId?: number;
};

export type UserSortOptions = {
  id?: 'ASC' | 'DESC';
  lastLoginAt?: 'ASC' | 'DESC';
  orgEmail?: 'ASC' | 'DESC';
};

export const MysqlDuplicateEntryCode = 'ER_DUP_ENTRY';

// TODO: for now, send password to user email.
export class UserCreatedEvent {
  email: string;
  fullName: string;
  password: string;
  constructor(email: string, fullName: string, password: string) {
    this.email = email;
    this.fullName = fullName;
    this.password = password;
  }
}

export class ImportStudentRecord {
  @IsEmail()
  orgEmail!: string;

  @IsString()
  @MinLength(1)
  fullName!: string;

  @IsString()
  @Length(8)
  studentIdNumber!: string;

  @IsPhoneNumber('VI')
  @IsOptional()
  phoneNumber?: string;
}
