import {
  defaultPage,
  defaultPerPage,
  DEFAULT_PAGE_NUM,
  DEFAULT_PER_PAGE,
  parseSortQuery,
} from '@app/paginate';
import { toTimestamp, toTimestampNullable } from '@app/pb';
import {
  AssignAssociateRequest,
  AssignAssociateResponse,
  CreateOrgRequest,
  CreateOrgResponse,
  GetClassesListingRequest,
  GetClassesListingResponse,
  GetClassesRequest,
  GetClassesResponse,
  GetOrgListingRequest,
  GetOrgListingResponse,
  GetOrgPartnersListingRequest,
  GetOrgPartnersListingResponse,
  GetOrgPartnersRequest,
  GetOrgPartnersResponse,
  GetOrgsRequest,
  GetOrgsResponse,
  OrgServiceController,
  OrgServiceControllerMethods,
  UpsertClassRequest,
  UpsertClassResponse,
} from '@app/pb/user_service/service.pb';
import { Metadata, status } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { from, Observable } from 'rxjs';
import { deserializeAsync } from '../../../../../libs/utils/src';
import { CreateOrgDto } from './dto/create-org.dto';
import { GetOrgPartnerQuery } from './dto/get-org-partner.query';
import {
  AssignAssociateDto,
  ClassQuery,
  OrgQuery,
  UpsertClassDto,
} from './dto/get.query';
import { OrganizationService } from './organization.service';
import { SchoolService } from './school.service';

@Controller('orgs')
@OrgServiceControllerMethods()
export class OrganizationController implements OrgServiceController {
  constructor(
    private readonly orgService: OrganizationService,
    private readonly schoolService: SchoolService,
  ) {}

  createOrg(request: CreateOrgRequest): Observable<CreateOrgResponse> {
    const promise = async (): Promise<CreateOrgResponse> => {
      const dtoRaw: CreateOrgDto = {
        name: request.name,
        orgAdmin:
          request.admin == null
            ? undefined
            : {
                orgEmail: request.admin.email,
              },
      };

      const { value: dto, err } = await deserializeAsync(CreateOrgDto, dtoRaw);
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'error validating create org dto: ' + err.message,
        });
      }

      const org = await this.orgService.create(dto);

      return {
        code: status.OK,
        message: 'success',
        data: {
          organizationId: org.id!,
          adminId: org.adminId,
        },
      };
    };

    return from(promise());
  }

  getOrgs({
    page,
    perPage,
    q,
    sort,
  }: GetOrgsRequest): Observable<GetOrgsResponse> {
    const promise = async (): Promise<GetOrgsResponse> => {
      const dtoRaw: OrgQuery = {
        page: defaultPage(page),
        perPage: defaultPerPage(perPage),
        q,
        sort,
      };

      const { value: dto, err } = await deserializeAsync(OrgQuery, dtoRaw);
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'error validating org query: ' + err.message,
        });
      }

      const allowedSortCols = ['id', 'name', 'createdAt'];

      const { items, meta } = await this.orgService.getOrgs({
        ...dto,
        sort: parseSortQuery(sort, { allowedCols: allowedSortCols }),
      });

      return {
        code: status.OK,
        message: 'success',
        data: {
          meta,
          orgs: items.map((o) => ({
            id: o.id!,
            name: o.name!,
            createdAt: toTimestampNullable(o.createdAt),
            admin:
              o.admin == null
                ? undefined
                : {
                    email: o.admin.orgEmail!,
                    id: o.admin.id!,
                  },
          })),
        },
      };
    };

    return from(promise());
  }

  getClasses(request: GetClassesRequest): Observable<GetClassesResponse> {
    const promise = async (): Promise<GetClassesResponse> => {
      const { value: dto, err } = await deserializeAsync(ClassQuery, {
        page: defaultPage(request.page),
        perPage: defaultPerPage(request.perPage),
      });
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'error validating class query: ' + err.message,
        });
      }

      const { items, meta } = await this.schoolService.getClasses(dto);

      return {
        code: status.OK,
        data: {
          meta,
          classes: items.map((c) => ({
            createdAt: toTimestampNullable(c.createdAt),
            id: c.id!,
            name: c.name!,
            programName: c.programName!,
          })),
        },
        message: 'success',
      };
    };

    return from(promise());
  }

  upsertClass(request: UpsertClassRequest): Observable<UpsertClassResponse> {
    const promise = async (): Promise<UpsertClassResponse> => {
      const dtoRaw: UpsertClassDto = request;

      const schoolClass = await this.schoolService
        .upsertClass(dtoRaw)
        .catch((e) => {
          throw new RpcException({
            code: status.UNKNOWN,
            message: 'error cannot upsert class: ' + e.message,
          });
        });

      return {
        code: status.OK,
        data: {
          id: schoolClass.id!,
        },
        message: 'success',
      };
    };

    return from(promise());
  }

  assignAssociate(
    req: AssignAssociateRequest,
  ): Observable<AssignAssociateResponse> {
    const promise = async (): Promise<AssignAssociateResponse> => {
      const dtoRaw: AssignAssociateDto = {
        ...req,
        expirationDate: req.statusExpirationDate,
      };
      const { value: dto, err } = await deserializeAsync(
        AssignAssociateDto,
        dtoRaw,
      );
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: err.message,
        });
      }

      await this.orgService.assignAssociate(dto).catch((e) => {
        throw new RpcException({
          code: status.NOT_FOUND,
          message: 'organization or partner does not exist: ' + e.message,
        });
      });

      return {
        code: status.OK,
        message: 'success',
      };
    };

    return from(promise());
  }

  getOrgPartners({
    filter,
    include,
    organizationId,
    page,
    perPage,
    q,
    sort,
  }: GetOrgPartnersRequest): Observable<GetOrgPartnersResponse> {
    const promise = async (): Promise<GetOrgPartnersResponse> => {
      return this.getOrgPartnersImpl(organizationId, {
        include,
        filter,
        page: page <= 0 ? DEFAULT_PAGE_NUM : page,
        perPage: perPage <= 0 ? DEFAULT_PER_PAGE : perPage,
        q,
        sort,
      });
    };

    return from(promise());
  }

  async getOrgPartnersImpl(
    orgId: number,
    { page, perPage, q, sort, filter, include }: GetOrgPartnerQuery,
  ): Promise<GetOrgPartnersResponse> {
    const allowedCols = ['name', 'id'];
    const { items, meta } = await this.orgService.getOrgPartners(orgId, {
      page,
      perPage,
      q,
      sort: parseSortQuery(sort, { allowedCols }),
      filter: {
        isAssociate: filter?.isAssociate,
      },
      include: {
        contacts: include.indexOf('contacts') !== -1,
      },
    });

    function checkIsAssociate(expiredDateStr?: string): boolean {
      if (expiredDateStr == null) return true;

      const expiredDate = new Date(expiredDateStr);
      const today = new Date();

      expiredDate.setHours(0, 0, 0, 0);
      today.setHours(0, 0, 0, 0);

      return expiredDate.getTime() > today.getTime();
    }

    return {
      code: status.OK,
      message: 'success',
      data: {
        meta,
        partners: items.map((item) => ({
          contacts:
            item.contacts?.map((c) => ({
              fullName: c.fullName ?? '',
              id: c.id ?? 0,
              phoneNumber: c.phoneNumber ?? '',
              partnerId: c.partnerId ?? 0,
              email: c.email ?? '',
            })) ?? [],
          description: item.description ?? '',
          email: item.email ?? '',
          homepageUrl: item.homepageUrl ?? '',
          id: item.id ?? 0,
          logoUrl: item.logoUrl ?? '',
          name: item.name ?? '',
          userId: item.userId ?? 0,
          createdAt:
            item.createdAt != null ? toTimestamp(item.createdAt) : undefined,
          associationStatusExpiredDate:
            item.partnerOrgs?.[0]?.expiredDate ?? '',
          isAssociate:
            item.partnerOrgs?.[0] == null
              ? false
              : checkIsAssociate(item.partnerOrgs?.[0]?.expiredDate),
          phoneNumber: item.phoneNumber ?? '',
          address: item.address ?? '',
        })),
      },
    };
  }

  getOrgPartnersListing(
    request: GetOrgPartnersListingRequest,
  ): Observable<GetOrgPartnersListingResponse> {
    return from(
      this.orgService
        .getOrgPartnersListing(request.organizationId)
        .then((res) => ({
          code: status.OK,
          message: 'success',
          data: res,
        })),
    );
  }

  async getClassesListing(
    request: GetClassesListingRequest,
  ): Promise<GetClassesListingResponse> {
    const classes = await this.schoolService.getClassesListing();

    return {
      code: status.OK,
      message: 'success',
      data: classes.map((c) => ({
        id: c.id!,
        name: c.name!,
      })),
    };
  }

  async getOrgListing(
    request: GetOrgListingRequest,
  ): Promise<GetOrgListingResponse> {
    const orgs = await this.orgService.getOrgListing();

    return {
      code: status.OK,
      message: 'success',
      data: orgs.map((c) => ({
        id: c.id!,
        name: c.name!,
      })),
    };
  }
}
