import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PartnerOrganization } from '../../database/entities/partner-org.entity';
import { ResetPasswordRequest } from '../../database/entities/reset-password-request.entity';
import { SchoolClass } from '../../database/entities/school-class.entity';
import { PartnerModule } from '../partner/partner.module';
import { OrganizationController } from './organization.controller';
import { OrgRepo } from './organization.repository';
import { OrganizationService } from './organization.service';
import { SchoolService } from './school.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OrgRepo,
      SchoolClass,
      PartnerOrganization,
      ResetPasswordRequest,
    ]),
    PartnerModule,
  ],
  controllers: [OrganizationController],
  providers: [OrganizationService, SchoolService],
  exports: [TypeOrmModule],
})
export class OrganizationModule {}
