export interface OrgPartnerSuggestion {
  id: number;
  name: string;
  isAssociate: boolean;
}

export type FindOrgPartnerSortOptions = {
  id?: 'ASC' | 'DESC';
  name?: 'ASC' | 'DESC';
};

export type FindOrgPartnerOptions = {
  page: number;
  perPage: number;
  sort: FindOrgPartnerSortOptions;
  filter?: OrgPartnerFilterOptions;
  include?: OrgPartnerIncludeOptions;
  q?: string;
};

export type OrgPartnerIncludeOptions = {
  contacts?: boolean;
};

export type OrgPartnerFilterOptions = {
  isAssociate?: boolean;
};

export type FindClassOptions = {
  page: number;
  perPage: number;
  q?: string;
};

export type FindOrgOptions = {
  page: number;
  perPage: number;
  sort: FindOrgSortOptions;
  q: string;
};

export type FindOrgSortOptions = {
  id?: 'ASC' | 'DESC';
  name?: 'ASC' | 'DESC';
  createdAt?: 'ASC' | 'DESC';
};
