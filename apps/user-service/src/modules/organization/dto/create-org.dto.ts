import { Type } from 'class-transformer';
import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { CreateUserDto } from '../../user/dto/create-user.dto';

export class CreateOrgDto {
  @IsString()
  name!: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => CreateUserDto)
  orgAdmin?: CreateUserDto;

  constructor(obj: Partial<CreateOrgDto>) {
    Object.assign(this, obj);
  }
}
