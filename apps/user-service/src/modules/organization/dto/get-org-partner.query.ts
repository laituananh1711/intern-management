import { OffsetPagingQuery } from '@app/paginate';
import { IsOptionalOrEmpty } from '@app/utils';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsIn,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

const allowInclude = ['contacts'];

export class GetOrgPartnerFilterQuery {
  @IsBoolean()
  @IsOptional()
  isAssociate?: boolean;
}

export class GetOrgPartnerQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];

  @IsString()
  @IsOptionalOrEmpty()
  q = '';

  @IsOptional()
  @ValidateNested()
  @Type(() => GetOrgPartnerFilterQuery)
  filter?: GetOrgPartnerFilterQuery;

  @IsIn(allowInclude, { each: true })
  @IsOptional()
  include: string[] = [];
}
