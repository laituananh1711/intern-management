import { OffsetPagingQuery } from '@app/paginate';
import { IsOptionalOrEmpty } from '@app/utils';
import { Type } from 'class-transformer';
import {
  IsDateString,
  IsEmail,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class CreateOrgDtoAdmin {
  @IsEmail()
  email!: string;
}

export class CreateOrgDto {
  @IsString()
  name!: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => CreateOrgDtoAdmin)
  admin?: CreateOrgDtoAdmin;
}

export class UpsertClassDto {
  @IsString()
  name!: string;

  @IsString()
  programName = '';

  @IsInt()
  @IsOptional()
  id?: number;
}

export class OrgQuery extends OffsetPagingQuery {
  @IsString()
  @IsOptional()
  q = '';

  @IsString({ each: true })
  @IsOptional()
  sort: string[] = [];
}

export class ClassQuery extends OffsetPagingQuery {}

export class AssignAssociateDto {
  @IsInt()
  organizationId!: number;

  @IsInt()
  partnerId!: number;

  @IsDateString()
  @IsOptionalOrEmpty()
  expirationDate?: string;
}
