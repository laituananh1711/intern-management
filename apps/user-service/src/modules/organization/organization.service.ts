import { paginate, PaginatedResults } from '@app/paginate';
import { generatePassword, getGravatarURL, isZeroValue } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { hashSync } from 'bcrypt';
import { Brackets, OrderByCondition, Repository } from 'typeorm';
import { AppRoleId, BCRYPT_SALT, events } from '../../constants';
import { Organization } from '../../database/entities/organization.entity';
import { PartnerOrganization } from '../../database/entities/partner-org.entity';
import { Partner } from '../../database/entities/partner.entity';
import { User } from '../../database/entities/user.entity';
import { UserCreatedEvent } from '../user/types';
import { CreateOrgDto } from './dto/create-org.dto';
import { AssignAssociateDto } from './dto/get.query';
import { OrgRepo } from './organization.repository';
import {
  FindOrgOptions,
  FindOrgPartnerOptions,
  OrgPartnerSuggestion,
} from './types';

@Injectable()
export class OrganizationService {
  constructor(
    private orgRepo: OrgRepo,
    @InjectRepository(Partner)
    private partnerRepo: Repository<Partner>,
    @InjectRepository(PartnerOrganization)
    private partnerOrgRepo: Repository<PartnerOrganization>,
    private eventEmitter: EventEmitter2,
  ) {}

  create(dto: CreateOrgDto): Promise<Organization> {
    if (dto.orgAdmin == null) {
      return this.orgRepo.save<Organization>({
        name: dto.name,
      });
    }

    const generatedPassword = generatePassword();

    return this.orgRepo
      .createOrg(
        new Organization({
          name: dto.name,
        }),
        new User({
          ...dto.orgAdmin,
          password: hashSync(generatedPassword, BCRYPT_SALT),
          roleId: AppRoleId.ORG_ADMIN,
          avatarUrl: getGravatarURL(dto.orgAdmin.orgEmail),
          isEmailVerified: true,
        }),
      )
      .then((res) => {
        this.eventEmitter.emit(
          events.USER_CREATED,
          new UserCreatedEvent(
            dto.orgAdmin!.orgEmail,
            'admin',
            generatedPassword,
          ),
        );
        return res;
      });
  }

  async getOrgs({
    page,
    perPage,
    q,
    sort,
  }: FindOrgOptions): Promise<PaginatedResults<Organization>> {
    let queryBuilder = this.orgRepo
      .createQueryBuilder('o')
      .leftJoinAndSelect('o.admin', 'a');

    if (!isZeroValue(q)) {
      queryBuilder = queryBuilder.where(
        `MATCH(o.name) AGAINST (:q in natural language mode)`,
        {
          q,
        },
      );
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};

      if (sort.id != null) orderBy['o.id'] = sort.id;
      if (sort.name != null) orderBy['o.name'] = sort.name;
      if (sort.createdAt != null) orderBy['o.createdAt'] = sort.createdAt;

      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return paginate(queryBuilder, { page, perPage });
  }

  getOrgListing(): Promise<Organization[]> {
    return this.orgRepo.find({
      select: ['id', 'name'],
      order: {
        name: 'ASC',
      },
    });
  }

  async getOrgPartners(
    orgId: number,
    { page, perPage, sort, filter, include, q }: FindOrgPartnerOptions,
  ): Promise<PaginatedResults<Partner>> {
    let queryBuilder = this.partnerRepo
      .createQueryBuilder('p')
      .leftJoinAndSelect('p.partnerOrgs', 'po', 'po.organizationId = :orgId', {
        orgId,
      });

    if (q != null && q != '') {
      queryBuilder = queryBuilder.andWhere(
        new Brackets((qb) =>
          qb
            .where(`MATCH(p.name) AGAINST (:q in natural language mode)`, {
              q: q,
            })
            .orWhere(`p.email like CONCAT('%', :email, '%')`, {
              email: q,
            }),
        ),
      );
    }

    if (filter != null) {
      if (filter.isAssociate != null) {
        if (filter.isAssociate) {
          queryBuilder = queryBuilder.andWhere(
            'po.partnerId IS NOT NULL AND (po.expiredDate IS NULL OR po.expiredDate > :today)',
            {
              today: new Date(),
            },
          );
        } else {
          queryBuilder = queryBuilder.andWhere(
            'po.partnerId IS NULL OR po.expiredDate <= :today',
            {
              today: new Date(),
            },
          );
        }
      }
    }

    if (include != null) {
      if (include.contacts != null && include.contacts) {
        queryBuilder = queryBuilder.leftJoinAndSelect('p.contacts', 'contacts');
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};

      if (sort.id != null) orderBy['p.id'] = sort.id;
      if (sort.name != null) orderBy['p.name'] = sort.name;

      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return paginate(queryBuilder, { perPage, page });
  }

  async getOrgPartnersListing(orgId: number): Promise<OrgPartnerSuggestion[]> {
    const orgPartnerSuggestionsRaw: {
      id: number;
      name: string;
      isAssociate: string;
    }[] = await this.partnerRepo.manager.query(
      'select p.id, p.name, if(po.id IS NOT NULL AND (po.expired_date is NULL OR po.expired_date > ?), 1, 0) as isAssociate from partners p ' +
        'left join partner_organizations po on p.id = po.partner_id and po.organization_id = ?',
      [new Date(), orgId],
    );

    return orgPartnerSuggestionsRaw.map((v) => ({
      id: v.id,
      name: v.name,
      isAssociate: v.isAssociate === '1' ? true : false,
    }));
  }

  async assignAssociate({
    organizationId,
    partnerId,
    expirationDate,
  }: AssignAssociateDto): Promise<void> {
    if (isZeroValue(expirationDate)) {
      expirationDate = undefined;
    }

    await this.partnerOrgRepo.query(
      'INSERT INTO partner_organizations (organization_id, partner_id, expired_date) ' +
        'VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE expired_date=VALUES(expired_date)',
      [organizationId, partnerId, expirationDate],
    );
  }
}
