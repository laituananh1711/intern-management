import { EntityRepository, Repository } from 'typeorm';
import { Organization } from '../../database/entities/organization.entity';
import { User } from '../../database/entities/user.entity';

@EntityRepository(Organization)
export class OrgRepo extends Repository<Organization> {
  createOrg(newOrg: Organization, newOrgAdmin: User): Promise<Organization> {
    return this.manager
      .transaction(async (manager) => {
        newOrgAdmin = await manager.save<User>(newOrgAdmin);

        delete newOrgAdmin.password;

        newOrg.adminId = newOrgAdmin.id;

        newOrg = await manager.save<Organization>(newOrg);
      })
      .then(() => {
        newOrg.admin = newOrgAdmin;
        return newOrg;
      });
  }
}
