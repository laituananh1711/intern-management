import { EventEmitter2 } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { when } from 'jest-when';
import { getGravatarURL } from '../../../../../libs/utils/src';
import { AppRoleId } from '../../constants';
import { Organization } from '../../database/entities/organization.entity';
import { PartnerOrganization } from '../../database/entities/partner-org.entity';
import { Partner } from '../../database/entities/partner.entity';
import { User } from '../../database/entities/user.entity';
import { OrgRepo } from './organization.repository';
import { OrganizationService } from './organization.service';

describe('OrganizationService', () => {
  let service: OrganizationService;
  const mockOrgRepo = {
    createOrg: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrganizationService,
        {
          provide: OrgRepo,
          useValue: mockOrgRepo,
        },
        {
          provide: getRepositoryToken(PartnerOrganization),
          useValue: {},
        },
        {
          provide: getRepositoryToken(Partner),
          useValue: {},
        },
        {
          provide: EventEmitter2,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<OrganizationService>(OrganizationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    const expectedResult = new Organization({
      id: 1,
      name: 'test',
      adminId: 2,
      admin: new User({
        id: 2,
        avatarUrl:
          'https://www.gravatar.com/avatar/93942e96f5acd83e2e047ad8fe03114d?d=retro&f=y',
        roleId: AppRoleId.ORG_ADMIN,
        isEmailVerified: true,
        orgEmail: 'test@email.com',
        password: 'pass',
      }),
    });

    when(mockOrgRepo.createOrg)
      .calledWith(
        new Organization({
          name: 'test',
        }),
        new User({
          orgEmail: 'test@email.com',
          password: 'pass',
          roleId: AppRoleId.ORG_ADMIN,
          avatarUrl: getGravatarURL('test@email.com'),
          isEmailVerified: true,
        }),
      )
      .mockResolvedValueOnce(expectedResult);
  });
});
