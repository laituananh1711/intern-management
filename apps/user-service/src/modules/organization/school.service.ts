import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { paginate, PaginatedResults } from '../../../../../libs/paginate/src';
import { isZeroValue } from '../../../../../libs/utils/src';
import { SchoolClass } from '../../database/entities/school-class.entity';
import { UpsertClassDto } from './dto/get.query';
import { FindClassOptions } from './types';

@Injectable()
export class SchoolService {
  constructor(
    @InjectRepository(SchoolClass) private classRepo: Repository<SchoolClass>,
  ) {}

  upsertClass(dto: UpsertClassDto): Promise<SchoolClass> {
    return this.classRepo.save<SchoolClass>(dto);
  }

  getClasses({
    page,
    perPage,
    q,
  }: FindClassOptions): Promise<PaginatedResults<SchoolClass>> {
    let queryBuilder = this.classRepo.createQueryBuilder('c');

    if (!isZeroValue(q)) {
      queryBuilder = queryBuilder.where(`c.name like CONCAT('%', :q, '%')`, {
        q,
      });
    }

    return paginate(queryBuilder, { page, perPage });
  }

  getClassesListing(): Promise<SchoolClass[]> {
    return this.classRepo.find({
      select: ['id', 'name'],
      order: {
        name: 'ASC',
      },
    });
  }
}
