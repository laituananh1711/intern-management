import { Controller } from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  AuthServiceController,
  AuthServiceControllerMethods,
  ChangePasswordRequest,
  ChangePasswordResponse,
  CompleteResetPasswordRequest,
  CompleteResetPasswordResponse,
  GetAccessTokenRequest,
  GetAccessTokenResponse,
  GetLecturerDetailsRequest,
  GetLecturerDetailsResponse,
  GetOrgAdminDetailsRequest,
  GetOrgAdminDetailsResponse,
  GetPartnerDetailsRequest,
  GetPartnerDetailsResponse,
  GetStudentDetailsRequest,
  GetStudentDetailsResponse,
  GetUserFromTokenRequest,
  GetUserFromTokenResponse,
  LoginWithEmailRequest,
  LoginWithEmailResponse,
  LoginWithGoogleRequest,
  LoginWithGoogleResponse,
  ResetPasswordRequest,
  ResetPasswordResponse,
} from '@app/pb/user_service/service.pb';
import { Metadata, status } from '@grpc/grpc-js';
import { toTimestampNullable } from '@app/pb';
import {
  ChangePasswordDto,
  CompleteResetPasswordDto,
  ResetPasswordDto,
} from './dto';
import { errInvalidArg } from '../../../../internship-service/src/modules/term/errors';
import { deserializeAsync } from '../../../../../libs/utils/src';
import { Observable } from 'rxjs';

@Controller('auth')
@AuthServiceControllerMethods()
export class AuthController implements AuthServiceController {
  constructor(private authService: AuthService) {}

  async getAccessToken({
    refreshToken,
  }: GetAccessTokenRequest): Promise<GetAccessTokenResponse> {
    const accessToken = this.authService.getAccessToken(refreshToken);

    return {
      code: status.OK,
      message: 'success',
      data: {
        accessToken,
      },
    };
  }

  async loginWithEmail({
    email,
    password,
  }: LoginWithEmailRequest): Promise<LoginWithEmailResponse> {
    const { user, accessToken, refreshToken } =
      await this.authService.loginWithEmail({ email, password });

    return {
      code: status.OK,
      message: 'success',
      data: {
        refreshToken,
        accessToken,
        user: {
          id: user.id!,
          role: user.role!.name!,
          roleId: user.role!.id!,
          orgEmail: user.orgEmail!,
          avatarUrl: user.avatarUrl!,
          isEmailVerified: user.isEmailVerified!,
        },
      },
    };
  }

  async loginWithGoogle({
    idToken,
  }: LoginWithGoogleRequest): Promise<LoginWithGoogleResponse> {
    const { user, accessToken, refreshToken } =
      await this.authService.loginWithGoogle(idToken);

    return {
      code: status.OK,
      message: 'success',
      data: {
        refreshToken,
        accessToken,
        user: {
          id: user.id!,
          role: user.role!.name!,
          roleId: user.role!.id!,
          orgEmail: user.orgEmail!,
          avatarUrl: user.avatarUrl!,
          isEmailVerified: user.isEmailVerified!,
        },
      },
    };
  }

  async getUserFromToken({
    accessToken,
  }: GetUserFromTokenRequest): Promise<GetUserFromTokenResponse> {
    const user = await this.authService.getUserFromToken(accessToken);

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: user.id!,
        role: user.role!.name!,
        roleId: user.roleId!,
        orgEmail: user.orgEmail!,
        avatarUrl: user.avatarUrl!,
        isEmailVerified: user.isEmailVerified!,
      },
    };
  }

  async getStudentDetails({
    userId,
  }: GetStudentDetailsRequest): Promise<GetStudentDetailsResponse> {
    const student = await this.authService.getStudentDetails(userId);

    return {
      code: status.OK,
      data: {
        fullName: student.fullName ?? '',
        id: student.id ?? 0,
        orgEmail: student.orgEmail ?? '',
        personalEmail: student.personalEmail ?? '',
        phoneNumber: student.phoneNumber ?? '',
        userId: student.userId ?? 0,
        studentIdNumber: student.studentIdNumber ?? '',
        schoolClass:
          student.schoolClass == null
            ? undefined
            : {
                id: student.schoolClass.id!,
                name: student.schoolClass.name!,
              },
        organizationId: student.organizationId ?? 0,
        resumeUrl: student.resumeUrl ?? '',
        createdAt: toTimestampNullable(student.createdAt),
        updatedAt: toTimestampNullable(student.updatedAt),
        organization: undefined,
      },
      message: 'success',
    };
  }

  async getLecturerDetails({
    userId,
  }: GetLecturerDetailsRequest): Promise<GetLecturerDetailsResponse> {
    const lecturer = await this.authService.getLecturerDetails(userId);

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: lecturer.id!,
        userId: lecturer.userId!,
        organizationId: lecturer.organizationId!,
        phoneNumber: lecturer.phoneNumber ?? '',
        personalEmail: lecturer.personalEmail ?? '',
        orgEmail: lecturer.orgEmail!,
        fullName: lecturer.fullName!,
        organization: undefined,
      },
    };
  }

  async getPartnerDetails({
    userId,
  }: GetPartnerDetailsRequest): Promise<GetPartnerDetailsResponse> {
    const partner = await this.authService.getPartnerDetails(userId);

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: partner.id!,
        userId: partner.userId!,
        name: partner.name!,
        email: partner.email ?? '',
        logoUrl: partner.logoUrl ?? '',
        contacts:
          partner.contacts?.map((c) => ({
            id: c.id!,
            email: c.email!,
            fullName: c.fullName!,
            phoneNumber: c.phoneNumber!,
            partnerId: c.partnerId!,
          })) ?? [],
        description: partner.description ?? '',
        homepageUrl: partner.homepageUrl ?? '',
        phoneNumber: partner.phoneNumber ?? '',
        address: partner.address ?? '',
        createdAt: toTimestampNullable(partner.createdAt),
      },
    };
  }

  async getOrgAdminDetails({
    userId,
  }: GetOrgAdminDetailsRequest): Promise<GetOrgAdminDetailsResponse> {
    const { organizationId } = await this.authService.getOrgAdminDetails(
      userId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        organizationId,
      },
    };
  }

  async changePassword(
    request: ChangePasswordRequest,
  ): Promise<ChangePasswordResponse> {
    const { value: dto, err } = await deserializeAsync(
      ChangePasswordDto,
      request,
    );
    if (err != null) {
      throw errInvalidArg(err);
    }

    await this.authService.changePassword(dto);

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async resetPassword(
    request: ResetPasswordRequest,
  ): Promise<ResetPasswordResponse> {
    const { value: dto, err } = await deserializeAsync(
      ResetPasswordDto,
      request,
    );
    if (err != null) {
      throw errInvalidArg(err);
    }

    await this.authService.resetPassword(dto);

    return {
      code: status.OK,
      message: 'success',
    };
  }

  async completeResetPassword(
    request: CompleteResetPasswordRequest,
  ): Promise<CompleteResetPasswordResponse> {
    const { value: dto, err } = await deserializeAsync(
      CompleteResetPasswordDto,
      request,
    );
    if (err != null) {
      throw errInvalidArg(err);
    }
    await this.authService.completeResetPassword(dto);

    return {
      code: status.OK,
      message: 'success',
    };
  }
}
