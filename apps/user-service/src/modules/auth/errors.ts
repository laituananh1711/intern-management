import { status } from '@grpc/grpc-js';
import { RpcException } from '@nestjs/microservices';

export const errInvalidRefreshToken = (e?: Error) =>
  new RpcException({
    code: status.UNAUTHENTICATED,
    message: 'ERR_INVALID_REFRESH_TOKEN',
    error: e,
  });

export const errWrongCredentials = (e?: Error) =>
  new RpcException({
    code: status.UNAUTHENTICATED,
    message: 'ERR_WRONG_CREDENTIALS',
    error: e,
  });

export const errDetailsNotFound = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_DETAILS_NOT_FOUND',
    error: e,
  });

export const errInvalidAccessToken = (e?: Error) =>
  new RpcException({
    code: status.UNAUTHENTICATED,
    message: 'ERR_INVALID_ACCESS_TOKEN',
    error: e,
  });

export const errUserNotFound = (e?: Error) =>
  new RpcException({
    code: status.NOT_FOUND,
    message: 'ERR_USER_NOT_FOUND',
    error: e,
  });

export const errInvalidResetToken = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_INVALID_RESET_TOKEN',
    error: e,
  });

export const errResetTokenExpired = (e?: Error) =>
  new RpcException({
    code: status.INVALID_ARGUMENT,
    message: 'ERR_RESET_TOKEN_EXPIRED',
    error: e,
  });
