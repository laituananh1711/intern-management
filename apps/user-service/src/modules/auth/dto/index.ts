import { IsEmail, IsNumber, IsString, MinLength } from 'class-validator';
import { PASSWORD_MIN_LENGTH } from '../../../constants';

export class ChangePasswordDto {
  @IsNumber()
  userId!: number;

  @IsString()
  currentPassword!: string;

  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  newPassword!: string;
}

export class ResetPasswordDto {
  @IsEmail()
  email!: string;
}

export class CompleteResetPasswordDto {
  @IsString()
  resetToken!: string;

  @IsNumber()
  requestId!: number;

  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  newPassword!: string;
}
