import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { compareSync, hashSync } from 'bcrypt';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Partner } from '../../database/entities/partner.entity';
import { Lecturer } from '../../database/entities/lecturer.entity';
import { Student } from '../../database/entities/student.entity';
import { AppRole, BCRYPT_SALT, events } from '../../constants';
import { User } from '../../database/entities/user.entity';
import { UserRepository } from '../user/user.repository';
import { LoginRequestDto } from './dto/login.dto';
import {
  errDetailsNotFound,
  errInvalidAccessToken,
  errInvalidRefreshToken,
  errInvalidResetToken,
  errResetTokenExpired,
  errUserNotFound,
  errWrongCredentials,
} from './errors';
import {
  AccessTokenPayload,
  PasswordChangedEvent,
  RefreshTokenPayload,
  ResetPasswordEvent,
} from './types';
import { OrgRepo } from '../organization/organization.repository';
import {
  ChangePasswordDto,
  CompleteResetPasswordDto,
  ResetPasswordDto,
} from './dto';
import { normalizeEmail } from '@app/utils';
import { EventEmitter2 } from '@nestjs/event-emitter';
import * as crypto from 'crypto';
import { ResetPasswordRequest } from '../../database/entities/reset-password-request.entity';
import { OAuth2Client } from 'google-auth-library';

@Injectable()
export class AuthService {
  accessTokenSecret: string;
  refreshTokenSecret: string;
  accessTokenExp: string;
  refreshTokenExp: string;
  googleClientId: string;

  constructor(
    private userRepo: UserRepository,
    private configService: ConfigService,
    private jwtService: JwtService,
    @InjectRepository(Student) private studentRepo: Repository<Student>,
    @InjectRepository(Lecturer) private lecturerRepo: Repository<Lecturer>,
    @InjectRepository(Partner) private partnerRepo: Repository<Partner>,
    private orgRepo: OrgRepo,
    private eventEmitter: EventEmitter2,
  ) {
    this.accessTokenSecret = this.configService.get(
      'ACCESS_TOKEN_SECRET',
      'atsecret',
    );
    this.refreshTokenSecret = this.configService.get(
      'REFRESH_TOKEN_SECRET',
      'rtsecret',
    );
    this.accessTokenExp = this.configService.get('ACCESS_TOKEN_EXP', '15m');
    this.refreshTokenExp = this.configService.get('REFRESH_TOKEN_EXP', '7d');
    this.googleClientId = this.configService.get('GOOGLE_CLIENT_ID', '');
  }

  async loginWithEmail(
    dto: LoginRequestDto,
  ): Promise<{ accessToken: string; refreshToken: string; user: User }> {
    const user = await this.userRepo
      .findOneOrFail({
        select: ['id', 'avatarUrl', 'orgEmail', 'isEmailVerified', 'password'],
        relations: ['role'],
        where: {
          orgEmail: dto.email,
        },
      })
      .catch((e) => {
        throw errWrongCredentials(e);
      });

    if (user.password == null || !compareSync(dto.password, user.password)) {
      throw errWrongCredentials(
        new Error('error password is null or not matched'),
      );
    }

    const accessToken = this.createAccessToken({
      uid: user.id!.toString(),
      claims: {
        role: user.role!.name! as AppRole,
      },
    });

    const refreshToken = this.createRefreshToken({
      uid: user.id!.toString(),
      claims: {
        role: user.role!.name! as AppRole,
      },
    });

    return {
      user,
      accessToken,
      refreshToken,
    };
  }

  async loginWithGoogle(
    idToken: string,
  ): Promise<{ accessToken: string; refreshToken: string; user: User }> {
    const client = new OAuth2Client(this.googleClientId);

    const ticket = await client.verifyIdToken({
      idToken,
      audience: this.googleClientId,
    });

    const payload = ticket.getPayload();

    const email = payload?.email;

    if (email == null) {
      throw errUserNotFound();
    }

    const user = await this.userRepo
      .findOneOrFail({
        select: ['id', 'avatarUrl', 'orgEmail', 'isEmailVerified', 'password'],
        relations: ['role'],
        where: {
          orgEmail: email,
        },
      })
      .catch((e) => {
        throw errUserNotFound(e);
      });

    return {
      user,
      ...this.createTokenPair(user.id!, user.role!.name as AppRole),
    };
  }

  private createTokenPair(
    userId: number,
    userRole: AppRole,
  ): { accessToken: string; refreshToken: string } {
    const accessToken = this.createAccessToken({
      uid: userId.toString(),
      claims: {
        role: userRole,
      },
    });

    const refreshToken = this.createRefreshToken({
      uid: userId.toString(),
      claims: {
        role: userRole,
      },
    });

    return { accessToken, refreshToken };
  }

  getAccessToken(refreshToken: string): string {
    try {
      const payload: RefreshTokenPayload = this.jwtService.verify(
        refreshToken,
        {
          secret: this.refreshTokenSecret,
        },
      );

      return this.createAccessToken({
        uid: payload.uid,
        claims: payload.claims,
      });
    } catch (e) {
      throw errInvalidRefreshToken(e as Error);
    }
  }

  async getUserFromToken(accessToken: string): Promise<User> {
    const { uid } = await this.verifyAccessToken(accessToken).catch((e) => {
      throw errInvalidAccessToken(e);
    });

    return this.userRepo
      .findOneOrFail({
        relations: ['role'],
        where: {
          id: parseInt(uid),
        },
      })
      .catch((e) => {
        throw errDetailsNotFound(e);
      });
  }

  async changePassword({
    userId,
    currentPassword,
    newPassword,
  }: ChangePasswordDto): Promise<void> {
    const user = await this.userRepo
      .findOneOrFail(userId, {
        select: ['orgEmail', 'password'],
      })
      .catch((e) => {
        throw errWrongCredentials(e);
      });

    if (user.password == null || !compareSync(currentPassword, user.password)) {
      throw errWrongCredentials(
        new Error('error password is null or not matched'),
      );
    }

    await this.userRepo.update(userId, {
      password: this.hashPassword(newPassword),
    });

    this.eventEmitter.emit(
      events.PASSWORD_CHANGED,
      new PasswordChangedEvent(userId, user.orgEmail!),
    );
  }

  async resetPassword({ email }: ResetPasswordDto): Promise<void> {
    const user = await this.userRepo.findOne({
      select: ['orgEmail', 'id'],
      where: {
        orgEmail: normalizeEmail(email),
      },
    });
    if (user == null) {
      throw errUserNotFound();
    }

    const [token, hashedToken] = this.generateResetToken();
    const manager = this.userRepo.manager;

    const { id } = await manager.save<ResetPasswordRequest>(
      new ResetPasswordRequest({
        userId: user.id,
        token: hashedToken,
        expiredAt: new Date(new Date().getTime() + 1 * 60 * 60 * 1000), // TODO: use const
      }),
    );

    this.eventEmitter.emit(
      events.RESET_PASSWORD,
      new ResetPasswordEvent(id!, user.orgEmail!, token),
    );
  }

  async completeResetPassword({
    requestId,
    resetToken: token,
    newPassword,
  }: CompleteResetPasswordDto): Promise<void> {
    const manager = this.userRepo.manager;

    const request = await manager
      .findOneOrFail(ResetPasswordRequest, {
        id: requestId,
      })
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errUserNotFound(e);
        }
        throw e;
      });

    if (!this.compareResetToken(token, request.token!)) {
      throw errInvalidResetToken();
    }

    if (request.expiredAt!.getTime() < new Date().getTime()) {
      throw errResetTokenExpired();
    }

    await this.userRepo.manager.transaction(async (manager) => {
      await manager.update(User, request.userId!, {
        password: this.hashPassword(newPassword),
      });

      // delete reset token after reset
      await manager.delete(ResetPasswordRequest, {
        id: request.id!,
      });
    });

    const { orgEmail: userEmail } = await this.userRepo
      .findOneOrFail({
        select: ['id', 'orgEmail'],
        where: {
          id: request.userId!,
        },
      })
      .catch((e) => {
        if (e instanceof EntityNotFoundError) {
          throw errUserNotFound(e);
        }
        throw e;
      });

    this.eventEmitter.emit(
      events.PASSWORD_CHANGED,
      new PasswordChangedEvent(request.userId!, userEmail!),
    );
  }

  getStudentDetails(userId: number): Promise<Student> {
    return this.studentRepo
      .findOneOrFail({
        relations: ['schoolClass'],
        where: {
          userId,
        },
      })
      .catch((e) => {
        throw errDetailsNotFound(e);
      });
  }

  getLecturerDetails(userId: number): Promise<Lecturer> {
    return this.lecturerRepo
      .findOneOrFail({
        where: {
          userId,
        },
      })
      .catch((e) => {
        throw errDetailsNotFound(e);
      });
  }

  getPartnerDetails(userId: number): Promise<Partner> {
    return this.partnerRepo
      .findOneOrFail({
        relations: ['contacts'],
        where: { userId },
      })
      .catch((e) => {
        throw errDetailsNotFound(e);
      });
  }

  getOrgAdminDetails(userId: number): Promise<{ organizationId: number }> {
    return this.orgRepo
      .findOneOrFail({
        select: ['id'],
        where: {
          adminId: userId,
        },
      })
      .then((res) => {
        return {
          organizationId: res.id!,
        };
      })
      .catch((e) => {
        throw errDetailsNotFound(e);
      });
  }

  private createAccessToken(payload: AccessTokenPayload): string {
    return this.jwtService.sign(payload, {
      secret: this.accessTokenSecret,
      expiresIn: this.accessTokenExp,
    });
  }

  private createRefreshToken(payload: RefreshTokenPayload): string {
    // TODO: implement blacklist/whitelist + logout if we have the time.
    return this.jwtService.sign(payload, {
      secret: this.refreshTokenSecret,
      expiresIn: this.refreshTokenExp,
    });
  }

  private verifyAccessToken(token: string): Promise<AccessTokenPayload> {
    return this.jwtService.verifyAsync(token, {
      secret: this.accessTokenSecret,
    });
  }

  /**
   * Generate a reset token. Return the reset token + its hashed version.
   */
  private generateResetToken(): [string, string] {
    const resetToken = crypto.randomBytes(32).toString('hex');
    const hashedResetToken = crypto
      .createHash('sha256')
      .update(resetToken)
      .digest('base64');

    return [resetToken, hashedResetToken];
  }

  private compareResetToken(token: string, hashedToken: string): boolean {
    return (
      crypto.createHash('sha256').update(token).digest('base64') === hashedToken
    );
  }

  private hashPassword(password: string): string {
    return hashSync(password, BCRYPT_SALT);
  }
}
