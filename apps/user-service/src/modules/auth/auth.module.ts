import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ResetPasswordRequest } from '../../database/entities/reset-password-request.entity';
import { LecturerModule } from '../lecturer/lecturer.module';
import { OrganizationModule } from '../organization/organization.module';
import { PartnerModule } from '../partner/partner.module';
import { StudentModule } from '../student/student.module';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  imports: [
    UserModule,
    StudentModule,
    LecturerModule,
    PartnerModule,
    OrganizationModule,
    JwtModule.register({}),
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
