import { AppRole } from '../../constants';

export class AccessTokenPayload {
  uid: string;
  claims: {
    role: AppRole;
    [key: string]: string;
  };

  constructor(uid: string, role: AppRole, claims?: Record<string, string>) {
    this.uid = uid;
    this.claims = {
      role,
      ...claims,
    };
  }
}

export class RefreshTokenPayload extends AccessTokenPayload {}

export class PasswordChangedEvent {
  userId: number;
  email: string;

  constructor(userId: number, email: string) {
    this.userId = userId;
    this.email = email;
  }
}

/**
 * Reset password event. Instead of changing user's password and
 * send them a new one, user will receive a password reset link.
 *
 */
export class ResetPasswordEvent {
  requestId: number;
  email: string;
  resetToken: string;

  constructor(requestId: number, email: string, resetToken: string) {
    this.requestId = requestId;
    this.email = email;
    this.resetToken = resetToken;
  }
}
