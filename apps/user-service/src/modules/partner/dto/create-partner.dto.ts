import {
  IsEmail,
  IsInt,
  IsOptional,
  IsString,
  IsUrl,
  MinLength,
} from 'class-validator';
import { IsOptionalOrEmpty } from '../../../../../../libs/utils/src';

export class UpsertPartnerDto {
  @IsString()
  @MinLength(1)
  name!: string;

  @IsUrl()
  homepageUrl!: string;

  @IsUrl()
  @IsOptionalOrEmpty()
  logoUrl!: string;

  @IsString()
  description!: string;

  @IsInt()
  @IsOptional()
  partnerId?: number;

  @IsEmail()
  @IsOptionalOrEmpty()
  email = '';
}
