import {
  IsEmail,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export class CreatePartnerContactDto {
  @IsString()
  fullName!: string;

  @IsPhoneNumber('VI')
  phoneNumber!: string;

  @IsEmail()
  email!: string;
}

export class UpdatePartnerContactDto extends CreatePartnerContactDto {
  @IsNumber()
  id!: number;

  @IsNumber()
  @IsOptional()
  filterPartnerId?: number;
}
