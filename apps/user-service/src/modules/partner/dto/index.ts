import { IsOptional, IsPhoneNumber, IsString, IsUrl } from 'class-validator';
import { IsOptionalOrEmpty } from '@app/utils';

export * from './create-partner-contact.dto';
export * from './create-partner.dto';
export * from './get-partner.query';

export class UpdatePartnerInfoDto {
  @IsString()
  @IsOptional()
  name?: string;

  @IsUrl()
  @IsOptionalOrEmpty()
  homepageUrl?: string;

  @IsUrl()
  @IsOptionalOrEmpty()
  logoUrl?: string;

  @IsString()
  @IsOptional()
  description?: string;

  @IsString()
  @IsOptionalOrEmpty()
  address?: string;

  @IsPhoneNumber('VI')
  @IsOptionalOrEmpty()
  phoneNumber?: string;
}
