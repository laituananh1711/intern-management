import { IsIn, IsOptional, IsString } from 'class-validator';
import { OffsetPagingQuery } from '../../../../../../libs/paginate/src';

const allowInclude = ['contacts'];

export class PartnerQuery extends OffsetPagingQuery {
  @IsString({ each: true })
  @IsOptional()
  sort?: string[];

  @IsIn(allowInclude, { each: true })
  @IsOptional()
  include?: string[];

  @IsString()
  @IsOptional()
  q?: string;
}
