export type FindPartnerOptions = {
  perPage: number;
  page: number;
  q?: string;
  sort?: PartnerSortOptions;
  include?: PartnerIncludeOptions;
};

export type PartnerSortOptions = {
  id?: 'ASC' | 'DESC';
  name?: 'ASC' | 'DESC';
};

export type PartnerIncludeOptions = {
  contacts?: boolean;
};

export interface PartnerSuggestion {
  id: number;
  name: string;
}
