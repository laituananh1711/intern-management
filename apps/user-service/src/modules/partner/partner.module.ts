import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PartnerContact } from '../../database/entities/partner-contact.entity';
import { PartnerOrganization } from '../../database/entities/partner-org.entity';
import { Partner } from '../../database/entities/partner.entity';
import { UserModule } from '../user/user.module';
import { PartnerController } from './partner.controller';
import { PartnerService } from './partner.service';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([Partner, PartnerContact, PartnerOrganization]),
  ],
  controllers: [PartnerController],
  providers: [PartnerService],
  exports: [TypeOrmModule],
})
export class PartnerModule {}
