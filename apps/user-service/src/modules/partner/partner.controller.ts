import { parseSortQuery } from '@app/paginate';
import { toTimestamp } from '@app/pb';
import {
  CreatePartnerContactRequest,
  CreatePartnerContactResponse,
  DeletePartnerContactRequest,
  DeletePartnerContactResponse,
  GetPartnerContactsRequest,
  GetPartnerContactsResponse,
  GetPartnerInfoRequest,
  GetPartnerInfoResponse,
  GetPartnersListingResponse,
  GetPartnersRequest,
  GetPartnersResponse,
  PartnerServiceController,
  PartnerServiceControllerMethods,
  UpdatePartnerContactRequest,
  UpdatePartnerContactResponse,
  UpdatePartnerInfoRequest,
  UpdatePartnerInfoResponse,
  UpsertPartnerRequest,
  UpsertPartnerResponse,
} from '@app/pb/user_service/service.pb';
import { deserializeAsync } from '@app/utils';
import { Metadata, status } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { from, Observable } from 'rxjs';
import { PartnerContactMethods } from '../../database/entities/partner-contact.entity';
import { UpdatePartnerInfoDto } from './dto';
import {
  CreatePartnerContactDto,
  UpdatePartnerContactDto,
} from './dto/create-partner-contact.dto';
import { UpsertPartnerDto } from './dto/create-partner.dto';
import { PartnerQuery } from './dto/get-partner.query';
import { PartnerService } from './partner.service';

@Controller('partners')
@PartnerServiceControllerMethods()
export class PartnerController implements PartnerServiceController {
  constructor(private readonly partnerService: PartnerService) {}

  getPartnerInfo(
    request: GetPartnerInfoRequest,
  ): Observable<GetPartnerInfoResponse> {
    const promise = async (): Promise<GetPartnerInfoResponse> => {
      const partner = await this.partnerService.getPartnerInfo(
        request.partnerId,
      );

      return {
        code: status.OK,
        message: 'success',
        data: {
          contacts: PartnerContactMethods.toPbs(partner.contacts),
          description: partner.description ?? '',
          homepageUrl: partner.homepageUrl ?? '',
          id: partner.id!,
          logoUrl: partner.logoUrl ?? '',
          name: partner.name!,
          userId: partner.userId!,
        },
      };
    };

    return from(promise());
  }

  updatePartnerInfo({
    partnerId,
    ...req
  }: UpdatePartnerInfoRequest): Observable<UpdatePartnerInfoResponse> {
    const promise = async (): Promise<UpdatePartnerInfoResponse> => {
      const dtoRaw: UpdatePartnerInfoDto = req;

      const { value: dto, err } = await deserializeAsync(
        UpdatePartnerInfoDto,
        dtoRaw,
      );
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'error validating request: ' + err.message,
        });
      }

      await this.partnerService.updatePartnerInfo(partnerId, dto);

      return {
        code: status.OK,
        message: 'success',
      };
    };

    return from(promise());
  }

  upsertPartner(req: UpsertPartnerRequest): Observable<UpsertPartnerResponse> {
    const promise = async (): Promise<UpsertPartnerResponse> => {
      const dtoRaw: UpsertPartnerDto = {
        name: req.name,
        description: req.description,
        homepageUrl: req.homepageUrl,
        logoUrl: req.logoUrl,
        partnerId: req.partnerId === 0 ? undefined : req.partnerId,
        email: req.email,
      };

      const { value: dto, err } = await deserializeAsync(
        UpsertPartnerDto,
        dtoRaw,
      );
      if (err != null) {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: 'error validating upsert partner dto: ' + err.message,
        });
      }

      const partner = await this.partnerService.upsertPartner(dto);

      return {
        code: status.OK,
        message: 'success',
        data: {
          partnerId: partner.id!,
        },
      };
    };

    return from(promise());
  }

  getPartnersListing(): Observable<GetPartnersListingResponse> {
    const promise = async (): Promise<GetPartnersListingResponse> => {
      const partnerSuggestions = await this.partnerService.getPartnersListing();

      return {
        code: status.OK,
        message: 'success',
        data: partnerSuggestions,
      };
    };

    return from(promise());
  }

  getPartners(request: GetPartnersRequest): Observable<GetPartnersResponse> {
    const promise = async () => {
      const partnerQueryRaw: PartnerQuery = {
        page: request.page,
        perPage: request.perPage,
        include: request.include,
        q: request.q,
        sort: request.sort,
      };

      const { value: partnerQuery, err } = await deserializeAsync(
        PartnerQuery,
        partnerQueryRaw,
      );

      if (err != null) {
        throw new RpcException(err);
      }

      return this.getPartnersImpl(partnerQuery);
    };

    return from(promise());
  }

  async getPartnersImpl({
    page,
    perPage,
    include,
    q,
    sort,
  }: PartnerQuery): Promise<GetPartnersResponse> {
    const allowedCols = ['id', 'name'];

    const { items, meta } = await this.partnerService.getPartners({
      page,
      perPage,
      sort: parseSortQuery(sort ?? [], { allowedCols }),
      q,
      include:
        include != null
          ? {
              contacts: include.indexOf('contacts') !== -1,
            }
          : {},
    });

    return {
      code: status.OK,
      message: 'success',
      data: {
        partners: items.map((item) => ({
          contacts:
            item.contacts?.map((c) => ({
              fullName: c.fullName ?? '',
              id: c.id ?? 0,
              phoneNumber: c.phoneNumber ?? '',
              partnerId: c.partnerId ?? 0,
              email: c.email ?? '',
            })) ?? [],
          description: item.description ?? '',
          email: item.email ?? '',
          homepageUrl: item.homepageUrl ?? '',
          id: item.id ?? 0,
          logoUrl: item.logoUrl ?? '',
          name: item.name ?? '',
          userId: item.userId ?? 0,
          phoneNumber: item.phoneNumber ?? '',
          address: item.address ?? '',
          createdAt:
            item.createdAt != null ? toTimestamp(item.createdAt) : undefined,
        })),
        meta,
      },
    };
  }

  async getPartnerContacts({
    partnerId,
  }: GetPartnerContactsRequest): Promise<GetPartnerContactsResponse> {
    const contacts = await this.partnerService.getPartnerContacts(partnerId);

    return {
      code: status.OK,
      message: 'success',
      data: contacts.map((c) => ({
        id: c.id!,
        partnerId: c.partnerId!,
        phoneNumber: c.phoneNumber!,
        email: c.email!,
        fullName: c.fullName!,
      })),
    };
  }

  async createPartnerContact({
    partnerId,
    ...rest
  }: CreatePartnerContactRequest): Promise<CreatePartnerContactResponse> {
    // why does this function return Promise instead of Observable like the other function?
    // Because I figured that returning Promise also work later than expected.

    const dtoRaw: CreatePartnerContactDto = rest;
    const { value: dto, err } = await deserializeAsync(
      CreatePartnerContactDto,
      dtoRaw,
    );

    if (err != null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_ARG',
        error: err,
      });
    }

    const { id } = await this.partnerService.addPartnerContact(partnerId, dto);

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: id!,
      },
    };
  }

  async updatePartnerContact(
    request: UpdatePartnerContactRequest,
  ): Promise<UpdatePartnerContactResponse> {
    const dtoRaw: UpdatePartnerContactDto = request;
    const { value: dto, err } = await deserializeAsync(
      UpdatePartnerContactDto,
      dtoRaw,
    );

    if (err != null) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'ERR_INVALID_ARG',
        error: err,
      });
    }

    const id = await this.partnerService.updatePartnerContact(dto);

    return {
      code: status.OK,
      message: 'success',
      data: {
        id,
      },
    };
  }

  async deletePartnerContact(
    request: DeletePartnerContactRequest,
  ): Promise<DeletePartnerContactResponse> {
    const id = await this.partnerService.softDeleteContact(
      request.id,
      request.filterPartnerId,
    );

    return {
      code: status.OK,
      message: 'success',
      data: {
        id: id!,
      },
    };
  }
}
