import { paginate, PaginatedResults } from '@app/paginate';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, FindCondition, OrderByCondition, Repository } from 'typeorm';
import { cleanse, isZeroValue } from '../../../../../libs/utils/src';
import { PartnerContact } from '../../database/entities/partner-contact.entity';
import { Partner } from '../../database/entities/partner.entity';
import { UpdatePartnerInfoDto } from './dto';
import {
  CreatePartnerContactDto,
  UpdatePartnerContactDto,
} from './dto/create-partner-contact.dto';
import { UpsertPartnerDto } from './dto/create-partner.dto';
import { FindPartnerOptions, PartnerSuggestion } from './types';

@Injectable()
export class PartnerService {
  constructor(
    @InjectRepository(PartnerContact)
    private contactRepo: Repository<PartnerContact>,
    @InjectRepository(Partner)
    private partnerRepo: Repository<Partner>,
  ) {}

  /**
   * createPartner creates a non-user partner.
   * @param dto
   * @returns
   */
  upsertPartner({ partnerId, ...dto }: UpsertPartnerDto): Promise<Partner> {
    const partner = new Partner(dto);

    if (!isZeroValue(partnerId)) {
      partner.id = partnerId;
    }

    return this.partnerRepo.save(partner);
  }

  getPartners({
    page,
    perPage,
    include,
    q,
    sort,
  }: FindPartnerOptions): Promise<PaginatedResults<Partner>> {
    let queryBuilder = this.partnerRepo.createQueryBuilder('p');

    if (q != null && q != '') {
      queryBuilder = queryBuilder.andWhere(
        new Brackets((qb) =>
          qb
            .where(`MATCH(p.name) AGAINST (:q in natural language mode)`, {
              q: q,
            })
            .orWhere(`p.email like CONCAT('%', :email, '%')`, {
              email: q,
            }),
        ),
      );
    }

    if (include != null) {
      if (include.contacts != null && include.contacts) {
        queryBuilder = queryBuilder.leftJoinAndSelect('p.contacts', 'contacts');
      }
    }

    if (sort != null) {
      const orderBy: OrderByCondition = {};

      if (sort.id != null) orderBy['p.id'] = sort.id;
      if (sort.name != null) orderBy['p.name'] = sort.name;

      queryBuilder = queryBuilder.orderBy(orderBy);
    }

    return paginate(queryBuilder, { page, perPage });
  }

  /**
   * get all partners with id and name. Suitable for
   * autocompletion.
   * @returns
   */
  getPartnersListing(): Promise<PartnerSuggestion[]> {
    return this.partnerRepo
      .find({
        select: ['id', 'name'],
      })
      .then((res) =>
        res.map((p) => ({
          id: p.id!,
          name: p.name!,
        })),
      );
  }

  addPartnerContact(
    partnerId: number,
    dto: CreatePartnerContactDto,
  ): Promise<PartnerContact> {
    return this.contactRepo.save(
      new PartnerContact({
        ...dto,
        partnerId,
      }),
    );
  }

  updatePartnerContact({
    filterPartnerId,
    ...dto
  }: UpdatePartnerContactDto): Promise<number> {
    const whereClause: FindCondition<PartnerContact> = { id: dto.id };
    if (filterPartnerId != null) {
      whereClause.partnerId = filterPartnerId;
    }

    return this.contactRepo.update(whereClause, dto).then(() => dto.id);
  }

  getPartnerContacts(partnerId: number): Promise<PartnerContact[]> {
    return this.contactRepo.find({
      where: {
        partnerId,
      },
    });
  }

  softDeleteContact(contactId: number, partnerId?: number): Promise<number> {
    const whereClause: FindCondition<PartnerContact> = { id: contactId };
    if (partnerId != null) {
      whereClause.partnerId = partnerId;
    }

    return this.contactRepo.softDelete(whereClause).then(() => contactId);
  }

  async updatePartnerInfo(
    partnerId: number,
    dto: UpdatePartnerInfoDto,
  ): Promise<void> {
    await this.partnerRepo.update(partnerId, cleanse(dto));
    return;
  }

  getPartnerInfo(partnerId: number): Promise<Partner> {
    return this.partnerRepo.findOneOrFail(partnerId, {
      relations: ['contacts'],
    });
  }
}
