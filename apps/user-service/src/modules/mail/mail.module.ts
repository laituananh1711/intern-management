import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { readFileSync } from 'fs';
import { join } from 'path';
import { MailService } from './mail.service';
import Handlebars from 'handlebars';

@Module({
  imports: [
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const layoutHbs = readFileSync(join(__dirname, 'templates/layout.hbs'));
        Handlebars.registerPartial('layout', layoutHbs.toString('utf8'));

        const authUser = configService.get('MAIL_USER', '');
        const authPass = configService.get('MAIL_PASS', '');
        const mailHost = configService.get('MAIL_HOST', '');
        const mailPort = configService.get<number>('MAIL_PORT', 25);
        return {
          transport: {
            host: mailHost,
            port: mailPort,
            auth: {
              user: authUser,
              pass: authPass,
            },
          },
          defaults: {
            from: '"No Reply" <noreply@uetwork.vnu.edu.vn>',
          },
          template: {
            dir: join(__dirname, 'templates'),
            adapter: new HandlebarsAdapter(),
            options: {
              strict: true,
            },
          },
        };
      },
    }),
  ],
  providers: [MailService],
})
export class MailModule {}
