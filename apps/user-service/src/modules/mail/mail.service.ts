import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { OnEvent } from '@nestjs/event-emitter';
import { join } from 'path';
import { events } from '../../constants';
import { PasswordChangedEvent, ResetPasswordEvent } from '../auth/types';
import { UserCreatedEvent } from '../user/types';

@Injectable()
export class MailService {
  private clientHost: string;
  private logger = new Logger(MailService.name);

  constructor(
    private mailerService: MailerService,
    configService: ConfigService,
  ) {
    this.clientHost = configService.get('CLIENT_HOST', 'http://localhost:3000');
  }

  @OnEvent(events.USER_CREATED)
  handleUserCreatedEvent({
    email,
    fullName,
    password,
  }: UserCreatedEvent): void {
    // TODO: send different languages based on user preferences.
    this.mailerService
      .sendMail({
        to: email,
        subject: 'Chào mừng bạn đến với UETWork!',
        // NOTE: you must use this path format since node/nestjs is stupid.
        // after typescript code are transpiled to js, the path structure changes substantially.
        template: join(__dirname, './templates/vi/welcome_old'),
        context: {
          fullName,
          password,
        },
      })
      .then((r) => this.logger.log(r))
      .catch((e) => this.logger.error(e));
  }

  @OnEvent(events.RESET_PASSWORD)
  handleResetPasswordEvent({
    email,
    requestId,
    resetToken,
  }: ResetPasswordEvent): void {
    this.mailerService
      .sendMail({
        to: email,
        subject: 'Hướng dẫn đặt lại mật khẩu',
        template: join(__dirname, './templates/vi/reset_password'),
        context: {
          resetToken,
          requestId,
          clientHost: this.clientHost,
        },
      })
      .then((r) => Logger.log(r))
      .catch((e) => Logger.error(e));
  }

  @OnEvent(events.PASSWORD_CHANGED)
  handlePasswordChangedEvent({ email }: PasswordChangedEvent): void {
    this.mailerService
      .sendMail({
        to: email,
        subject: 'Mật khẩu được đổi thành công',
        template: join(__dirname, './templates/vi/password_changed'),
      })
      .then((r) => Logger.log(r))
      .catch((e) => Logger.error(e));
  }
}
