import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HealthController } from './health.controller';
import { AuthModule } from './modules/auth/auth.module';
import { LecturerModule } from './modules/lecturer/lecturer.module';
import { MailModule } from './modules/mail/mail.module';
import { OrganizationModule } from './modules/organization/organization.module';
import { PartnerModule } from './modules/partner/partner.module';
import { StudentModule } from './modules/student/student.module';
import { UserModule } from './modules/user/user.module';
import typeOrmOptions from './ormconfig';
import { NestMinioModule, NestMinioOptions } from 'nestjs-minio';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: 'apps/user-service/.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(typeOrmOptions),
    UserModule,
    LecturerModule,
    PartnerModule,
    StudentModule,
    OrganizationModule,
    EventEmitterModule.forRoot(),
    MailModule,
    NestMinioModule.registerAsync({
      inject: [ConfigService],
      imports: [ConfigModule],
      useFactory: (configService: ConfigService): NestMinioOptions => {
        const portStr = configService.get('MINIO_PORT', '9000');
        const port = parseInt(portStr);
        const useSSL = configService.get('MINIO_SECURE', 'false');

        return {
          endPoint: configService.get('MINIO_ENDPOINT', 'localhost'),
          accessKey: configService.get('MINIO_ACCESS_KEY', 'root'),
          secretKey: configService.get('MINIO_SECRET_KEY', 'password'),
          port,
          useSSL: useSSL === 'true' || useSSL === true,
        };
      },
    }),
    AuthModule,
  ],
  providers: [],
  controllers: [HealthController],
})
export class UserServiceModule {}
