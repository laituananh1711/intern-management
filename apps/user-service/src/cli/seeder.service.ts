import { InjectRepository } from '@nestjs/typeorm';
import { Command, Console } from 'nestjs-console';
import { EntityManager, Repository } from 'typeorm';
import { AppRole, getGravatarURL } from '@app/utils';
import { Role } from '../database/entities/role.entity';
import { OrgRepo } from '../modules/organization/organization.repository';
import { Organization } from '../database/entities/organization.entity';
import { faker } from '@faker-js/faker';
import { User } from '../database/entities/user.entity';
import { SchoolClass } from '../database/entities/school-class.entity';
import { Student } from '../database/entities/student.entity';
import { Lecturer } from '../database/entities/lecturer.entity';
import { Partner } from '../database/entities/partner.entity';
import { PartnerContact } from '../database/entities/partner-contact.entity';

@Console({
  command: 'seed',
})
export class SeederService {
  private manager: EntityManager;

  constructor(
    @InjectRepository(Role) private roleRepo: Repository<Role>,
    private orgRepo: OrgRepo,
    @InjectRepository(Student) private studentRepo: Repository<Student>,
    @InjectRepository(SchoolClass)
    private schoolClassRepo: Repository<SchoolClass>,
    @InjectRepository(Lecturer) private lecturerRepo: Repository<Lecturer>,
  ) {
    this.manager = this.roleRepo.manager;
  }

  @Command({
    command: 'all',
  })
  async seedAll() {
    await this.seedRoles();
    await this.seedOrganizations();
    await this.seedSchoolClasses();
    await Promise.all([
      this.seedStudents(),
      this.seedLecturers(),
      this.seedPartners(),
    ]);
  }

  @Command({
    command: 'role',
  })
  async seedRoles() {
    const roles: Role[] = [
      {
        id: 1,
        name: AppRole.STUDENT,
      },
      {
        id: 2,
        name: AppRole.LECTURER,
      },
      {
        id: 3,
        name: AppRole.PARTNER,
      },
      {
        id: 4,
        name: AppRole.SYSTEM_ADMIN,
      },
      {
        id: 5,
        name: AppRole.ORG_ADMIN,
      },
    ];
    await this.roleRepo.save(roles);
  }

  @Command({
    command: 'user',
  })
  async seedUser() {
    const password =
      '$2a$10$03ulcGM6kp4nsB9X4VQjR.NVCMFAgOxT7oegDamxtWMO1rKQSRnwa'; //password

    const orgEmail = 'admin@gmail.com';
    await this.manager.insert(
      User,
      new User({
        orgEmail,
        password,
        roleId: AppRoleId.SYSTEM_ADMIN,
        isEmailVerified: true,
        avatarUrl: getGravatarURL(orgEmail),
      }),
    );
  }

  @Command({
    command: 'org',
    options: [
      {
        flags: '-n, --number <number of item>',
        required: false,
      },
    ],
  })
  async seedOrganizations(options?: SeedOptions) {
    const seedAmount = options?.number ?? defaultSeedAmount.org;
    const orgs = this.generateOrgs(seedAmount);

    await this.orgRepo.save(orgs);
  }

  @Command({
    command: 'class',
  })
  async seedSchoolClasses() {
    const classes = this.generateSchoolClasses();

    await this.schoolClassRepo.insert(classes);
  }

  @Command({
    command: 'student',
    options: [
      {
        flags: '-n, --number <number of item>',
        required: false,
      },
    ],
  })
  async seedStudents(options?: SeedOptions) {
    const seedAmount = options?.number ?? defaultSeedAmount.student;
    const orgs = await this.orgRepo.find({
      select: ['id'],
    });

    const orgIds = orgs.map((o) => o.id!);
    const students = this.generateStudents(seedAmount, orgIds);

    await this.studentRepo.save(students);
  }

  @Command({
    command: 'lecturer',
    options: [
      {
        flags: '-n, --number <number of item>',
        required: false,
      },
    ],
  })
  async seedLecturers(options?: SeedOptions) {
    const seedAmount = options?.number ?? defaultSeedAmount.lecturer;
    const orgs = await this.orgRepo.find({
      select: ['id'],
    });

    const orgIds = orgs.map((o) => o.id!);
    const lecturers = this.generateLecturers(seedAmount, orgIds);

    await this.lecturerRepo.save(lecturers);
  }

  @Command({
    command: 'partner',
    options: [
      {
        flags: '-n, --number <number of item>',
        required: false,
      },
    ],
  })
  async seedPartners(options?: SeedOptions) {
    const seedAmount = options?.number ?? defaultSeedAmount.partner;
    const partners = this.generatePartners(seedAmount);

    await this.manager.save(Partner, partners);

    const partnerContacts = [];
    let seedContactsAmount = 0;
    for (const partner of partners) {
      seedContactsAmount = faker.datatype.number({ min: 0, max: 3 });
      partnerContacts.push(
        ...this.generateContacts(seedContactsAmount, partner.id!),
      );
    }

    await this.manager.insert(PartnerContact, partnerContacts);
  }

  private generateLecturers(count: number, orgIds: number[]): Lecturer[] {
    const items: Lecturer[] = [];
    for (let i = 0; i < count; i++) {
      const user = this.generateUser(AppRoleId.LECTURER);
      const item: Lecturer = new Lecturer({
        user,
        fullName: faker.name.findName(),
        orgEmail: user.orgEmail!,
        phoneNumber: faker.phone.phoneNumber('+84#########'),
        organizationId: faker.random.arrayElement(orgIds),
        personalEmail: faker.internet.email(),
      });

      items.push(item);
    }

    return items;
  }

  private generateOrgs(count: number): Organization[] {
    const items: Organization[] = [];
    for (let i = 0; i < count; i++) {
      const item: Organization = {
        name: faker.name.jobTitle(),
        admin: this.generateUser(AppRoleId.ORG_ADMIN),
      };

      items.push(item);
    }

    return items;
  }

  private generateSchoolClasses(): SchoolClass[] {
    const names = ['QH-2018-CQ-J', 'QH-2019-CQ-CLC', 'QH-2020-CQ-C'];

    const classes: SchoolClass[] = [];

    for (const name of names) {
      const schoolClass: SchoolClass = {
        name,
        programName: faker.company.companyName(),
      };

      classes.push(schoolClass);
    }

    return classes;
  }

  private generateStudents(count: number, orgIds: number[]): Student[] {
    const items: Student[] = [];
    for (let i = 0; i < count; i++) {
      const user = this.generateUser(AppRoleId.STUDENT);
      const item: Student = new Student({
        user,
        fullName: faker.name.findName(),
        orgEmail: user.orgEmail!,
        resumeUrl: faker.internet.url(),
        phoneNumber: this.generateVietnamesePhoneNumber(),
        organizationId: faker.random.arrayElement(orgIds),
        studentIdNumber: this.generateStudentIDNumber(),
        personalEmail: faker.internet.email(),
      });
      items.push(item);
    }

    return items;
  }

  private generatePartners(count: number): Partner[] {
    const items: Partner[] = [];
    for (let i = 0; i < count; i++) {
      let user: User | undefined = undefined;
      user = this.generateUser(AppRoleId.PARTNER, false);

      const item: Partner = new Partner({
        name: faker.company.companyName(),
        user,
        email: user?.orgEmail ?? faker.internet.email(),
        logoUrl: faker.image.avatar(),
        description: faker.company.bs(),
        homepageUrl: faker.internet.url(),
        phoneNumber: this.generateVietnamesePhoneNumber(),
        address: faker.address.streetAddress(true),
      });

      items.push(item);
    }

    return items;
  }

  private generateContacts(count: number, partnerId: number): PartnerContact[] {
    const items: PartnerContact[] = [];
    for (let i = 0; i < count; i++) {
      const item: PartnerContact = new PartnerContact({
        partnerId,
        email: faker.internet.email(),
        fullName: faker.name.findName(),
        phoneNumber: this.generateVietnamesePhoneNumber(),
      });

      items.push(item);
    }

    return items;
  }

  private generateUser(roleId: AppRoleId, isVnu = true): User {
    const password =
      '$2a$10$03ulcGM6kp4nsB9X4VQjR.NVCMFAgOxT7oegDamxtWMO1rKQSRnwa'; //password

    let orgEmail = this.generateVnuEmail();
    if (!isVnu) {
      orgEmail = faker.internet.email();
    }

    return new User({
      roleId,
      orgEmail,
      password,
      avatarUrl: getGravatarURL(orgEmail),
      isEmailVerified: true,
    });
  }

  private generateVnuEmail(): string {
    return faker.internet.email(undefined, undefined, 'vnu.edu.vn');
  }

  private generateStudentIDNumber(): string {
    return faker.datatype.number({ min: 1600000, max: 2100000 }).toString();
  }

  private generateVietnamesePhoneNumber(): string {
    return faker.phone.phoneNumber('+849########');
  }
}

export enum AppRoleId {
  STUDENT = 1,
  LECTURER = 2,
  PARTNER = 3,
  SYSTEM_ADMIN = 4,
  ORG_ADMIN = 5,
}

interface SeedOptions {
  number?: number;
}

const defaultSeedAmount = {
  org: 3,
  student: 50,
  lecturer: 20,
  partner: 50,
};
