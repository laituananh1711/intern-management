import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConsoleModule } from 'nestjs-console';
import { Partner } from '../database/entities/partner.entity';
import { Lecturer } from '../database/entities/lecturer.entity';
import { Role } from '../database/entities/role.entity';
import { SchoolClass } from '../database/entities/school-class.entity';
import { Student } from '../database/entities/student.entity';
import { User } from '../database/entities/user.entity';
import { OrgRepo } from '../modules/organization/organization.repository';
import typeOrmOptions from '../ormconfig';
import { SeederService } from './seeder.service';

@Module({
  imports: [
    ConsoleModule,
    TypeOrmModule.forRoot({
      ...typeOrmOptions,
      logging: ['error'],
    }),
    TypeOrmModule.forFeature([
      Student,
      OrgRepo,
      SchoolClass,
      User,
      Role,
      Lecturer,
      Partner,
    ]),
  ],
  providers: [SeederService],
})
export class CliModule {}
