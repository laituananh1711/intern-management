## Type of change

Please delete options that are not relevant.

- [ ] Bug fix
- [ ] New feature
- [ ] Improvement

# Description

Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.

## What?

Explain the changes you’ve made. Reference an open issue if possible.

## Why?

The “why” tells us what business or engineering goal this change achieves

## How?

Draw attention to the significant design decisions.

## Screenshots (optional)

Screenshots are especially helpful for UI-related changes. Let provide it if you can.

## Anything Else?

You may want to delve into possible architecture changes or technical debt here. Call out challenges, optimizations, etc.

## API Docs

# Checklist

- [ ] My code follows the style guidelines of this project
- [ ] I have performed a self-review of my own code
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] I have made corresponding changes to the documentation

# How Has This Been Tested?

Please describe the tests that you ran to verify your changes. Provide instructions, so we can reproduce. Please also list any relevant details for your test configuration

- [ ] Test A
- [ ] Test B

### Please do not approve this request if the author did not fill in the Merge request description and checklist
