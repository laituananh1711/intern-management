# students

## Description

<details>
<summary><strong>Table Definition</strong></summary>

```sql
CREATE TABLE `students` (
  `student_id_number` char(8) NOT NULL COMMENT 'A 8-digit id number provided by the school (i.e. 18020001)',
  `full_name` varchar(255) NOT NULL,
  `resume_url` varchar(1000) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `organization_id` int unsigned NOT NULL,
  `school_class_id` int unsigned DEFAULT NULL,
  `org_email` varchar(255) NOT NULL,
  `personal_email` varchar(255) DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `idx_students_full_name` (`full_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

</details>

## Columns

| Name | Type | Default | Nullable | Extra Definition | Children | Parents | Comment |
| ---- | ---- | ------- | -------- | ---------------- | -------- | ------- | ------- |
| student_id_number | char(8) |  | false |  |  |  | A 8-digit id number provided by the school (i.e. 18020001) |
| full_name | varchar(255) |  | false |  |  |  |  |
| resume_url | varchar(1000) |  | true |  |  |  |  |
| phone_number | varchar(255) |  | true |  |  |  |  |
| organization_id | int unsigned |  | false |  |  |  |  |
| school_class_id | int unsigned |  | true |  |  |  |  |
| org_email | varchar(255) |  | false |  |  |  |  |
| personal_email | varchar(255) |  | true |  |  |  |  |
| created_at | timestamp(6) | CURRENT_TIMESTAMP(6) | false | DEFAULT_GENERATED |  |  |  |
| updated_at | timestamp(6) | CURRENT_TIMESTAMP(6) | false | DEFAULT_GENERATED on update CURRENT_TIMESTAMP(6) |  |  |  |
| id | int unsigned |  | false | auto_increment | [term_students](term_students.md) |  |  |
| user_id | int unsigned |  | true |  |  |  |  |

## Constraints

| Name | Type | Definition |
| ---- | ---- | ---------- |
| PRIMARY | PRIMARY KEY | PRIMARY KEY (id) |

## Indexes

| Name | Definition |
| ---- | ---------- |
| idx_students_full_name | KEY idx_students_full_name (full_name) USING FULLTEXT |
| PRIMARY | PRIMARY KEY (id) USING BTREE |

## Relations

![er](students.svg)

---

> Generated by [tbls](https://github.com/k1LoW/tbls)
